<?php

namespace Libs\Adapters;

class Sorter
{
    private $sorts = [];

    /**
     * Sorter constructor.
     *
     * @param array $sorts
     */
    public function __construct(array $sorts)
    {
        $this->sorts = $sorts;
    }

    /**
     * Get Sorts.
     *
     * @return array
     */
    public function getSorts()
    {
        return $this->sorts;
    }
}
