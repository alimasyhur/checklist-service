<?php

namespace Libs\Presenters;

use App\Http\Requests\SearchRequest;
use CorePackage\Infrastructures\LengthAwareOffsetPaginator as Paginator;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\JsonApiSerializer;
use League\Fractal\Serializer\SerializerAbstract;

class JsonApiPresenter implements JsonApiPresenterInterface
{
    protected $fractal;

    protected $statusCode = 200;

    protected $response;

    protected $resourceKey = null;

    protected $transformer = null;

    protected $meta = [];

    protected $mSparseFieldSets = [];

    protected $baseUrl = null;

    public function __construct(Manager $fractal)
    {
        $this->fractal = $fractal;
        $this->baseUrl = app('url')->to('/');
    }

    /**
     * Render.
     */
    public function render()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * {@inheritdoc}
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setTransformer(TransformerAbstract $transformer)
    {
        $this->transformer = $transformer;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize(SerializerAbstract $serializer, $resourceKey = null)
    {
        $this->fractal->setSerializer($serializer);
        $this->resourceKey = $resourceKey;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function parseIncludes($includes = '')
    {
        if (is_null($includes)) {
            $includes = '';
        }
        $this->fractal->parseIncludes($includes);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function parseSparseFieldsets($name, array $fieldsets = [])
    {
        if (null == $this->transformer) {
            throw new PresenterException('No transformer is set. Set it using setTransfomer().');
        }

        $this->transformer->parseSparseFieldsets($name, $fieldsets);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setMultipleModelSparseFieldsets(array $modelFieldsets = [])
    {
        $this->mSparseFieldSets = $modelFieldsets;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function renderArray(array $array, array $headers = [])
    {
        $headers['Content-Type'] = 'application/vnd.api+json';
        //  TODO: For debug purpose remove this after app get stable version
        if (env('APP_DEBUG', false) && app('request')->input('query_debug', 0)) {
            $queryLog                     = \DB::getQueryLog();
            $array['meta']['query_count'] = count($queryLog);
            $array['meta']['queries']     = $queryLog;
        }

        return response()->json($array, $this->statusCode, $headers);
    }

    /**
     * {@inheritdoc}
     */
    public function setMeta(array $meta)
    {
        // We should make setMeta worked on renderItem and renderCollection.
        // Currently, this only work on renderItem.
        // There is also `meta()`function on vendor/league/fractal/src/Serializer/JsonApiSerializer.php
        // Maybe we could utilize that.
        $this->meta += $meta;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function renderItem($item)
    {
        $result = $this->serializeItem($item);

        return $this->renderArray($result);
    }

    /**
     * Serialize item.
     */
    public function serializeItem($item)
    {
        if (null == $this->transformer) {
            throw new PresenterException('No transformer is set. Set it using setTransfomer().');
        }

        if (is_null($this->resourceKey)) {
            $resource = new Item($item, $this->transformer);
        } else {
            $resource = new Item($item, $this->transformer, $this->resourceKey);
        }

        $rootScope = $this->fractal->createData($resource);

        return $this->appendMeta($rootScope->toArray());
    }

    /**
     * {@inheritdoc}
     */
    public function renderCollection($collection)
    {
        if (null == $this->transformer) {
            throw new PresenterException('No transformer is set. Set it using setTransfomer().');
        }

        if (is_null($this->resourceKey)) {
            $resource = new Collection($collection, $this->transformer);
        } else {
            $resource = new Collection($collection, $this->transformer, $this->resourceKey);
        }

        $rootScope = $this->fractal->createData($resource);
        $data      = ['meta' => [
            'count' => $collection->count(),
            'total' => $collection->count(), ],
        ];

        $data = array_merge($data, $rootScope->toArray());

        if ($collection instanceof Paginator) {
            if ($this->baseUrl && $this->resourceKey) {
                $collection = $collection->setBaseUrl($this->getBaseUrl() . '/' . $this->resourceKey);
            }

            $firstPage = $collection->url(0);
            $lastPage  = $collection->url($collection->lastPage());
            $nextPage  = $collection->nextPageUrl();
            $prevPage  = $collection->previousPageUrl();

            /*
             * Hack to allow custom contacts routing on contacts microservice.
             */
            if (! empty($overrides = json_decode(env('PRESENTER_LINKS_OVERRIDE', ''), true))) {
                foreach ($overrides as $from => $to) {
                    $firstPage = str_replace($from, $to, $firstPage);
                    $lastPage  = str_replace($from, $to, $lastPage);
                    $nextPage  = str_replace($from, $to, $nextPage) ?: null;
                    $prevPage  = str_replace($from, $to, $prevPage) ?: null;
                }
            }

            $data['links'] = [
                'first' => (! is_null($firstPage)) ? urldecode($firstPage) : null,
                'last'  => (! is_null($lastPage)) ? urldecode($lastPage) : null,
                'next'  => (! is_null($nextPage)) ? urldecode($nextPage) : null,
                'prev'  => (! is_null($prevPage)) ? urldecode($prevPage) : null,
            ];

            $data['meta']['total'] = $collection->total();
        }

        $data['meta'] = array_merge($data['meta'], $this->meta);

        return $this->renderArray($data);
    }

    /**
     * {@inheritdoc}
     */
    public function renderMultipleCollections($collection)
    {
        if (null == $this->transformer) {
            throw new PresenterException('No transformer is set. Set it using setTransfomer().');
        }

        $multipleSparseFields = $this->mSparseFieldSets;
        $resources            = [];
        foreach ($collection as $key => $items) {
            $transformerNamespace = $this->getTransformerByKey($key);

            if (! class_exists($transformerNamespace)) {
                continue;
            }

            $key              = snake_case(ucfirst($key));
            $transformBuilder = self::setTransformer(new $transformerNamespace());

            $fields[$key] = $multipleSparseFields[$key];
            $transform    = $transformBuilder->parseSparseFieldsets($key, $fields)
                                        ->serialize(new JsonApiSerializer(url('api/v1')), $key)
                                        ->renderCollection($items);

            $resources[$key] = $transform->getData(true);
        }

        $resources = collect($resources);
        $data      = ['meta' => [
            'count' => $resources->count(),
            'total' => $resources->count(), ],
        ];

        $data = array_merge($data, ['result' => $resources->toArray()]);

        return $this->renderArray($data);
    }

    /**
     * Build Transformer class by tablename.
     *
     * @param  $tableName: string of tablename
     *
     * @return string
     */
    public function getTransformerByKey($tableName)
    {
        $locationsList  = config('es-search.model_folder_locations');
        $folderLocation = isset($locationsList[$tableName]) ? $locationsList[$tableName] : $tableName;

        $location = studly_case(str_singular($folderLocation));

        $className = $this->changesEloquentName($tableName);

        return sprintf("\App\Presenters\%s\%sTransformer", $location, $className);
    }

    /**
     * {@inheritdoc}
     */
    public function setBaseUrl($url)
    {
        $this->baseUrl = $url;

        return $this;
    }

    public function setCustomBaseUrl($data, bool $isChecklist = false)
    {
        $hasOrgId = false;
        if ($data instanceof SearchRequest) {
            $hasOrgId = array_has($data->getFilters(), 'org_id.is');
        } else {
            $hasOrgId = $data->org_id !== null;
        }

        $url = $this->baseUrl;
        $url = str_replace("/checklist", "", $url);
        if ($hasOrgId) {
            $url = sprintf("%s/mc-gateway", $url);
        }

        $url = ($isChecklist) ? $url : sprintf("%s/checklists", $url);

        $this->baseUrl = $url;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * Append meta key to given array.
     *
     * @param array $result
     *
     * @return array
     */
    protected function appendMeta(array $result)
    {
        if (count($this->meta)) {
            $result = array_merge($result, ['meta' => $this->meta]);
        }

        return $result;
    }

    /**
     * change eloquent name if it is different from table name.
     *
     * @param Sting $tableName
     *
     * @return string of converted eloquent name
     */
    protected function changesEloquentName($tableName)
    {
        $className = studly_case(str_singular($tableName));
        if (in_array($tableName, ['custom_fields_values', 'customFieldsValues'])) {
            $className = studly_case(str_singular('custom_field_values'));
        } elseif (in_array($tableName, ['action_logs', 'actionLogs'])) {
            $className = studly_case(str_singular('logs'));
        }

        return $className;
    }
}
