<?php

namespace Libs\Presenters;

use League\Fractal\Serializer\SerializerAbstract;

interface JsonApiPresenterInterface
{
    public function render();


    /**
     * Set API base url.
     *
     * @param $data
     * @param bool $isChecklist
     */
    public function setCustomBaseUrl($data, bool $isChecklist = false);
    
    /**
     * Set transformer.
     *
     * @param $transformer TransfomerInterface
     *
     * @return self
     */
    public function setTransformer(TransformerAbstract $transformer);

    /**
     * Getter for statusCode.
     *
     * @return int
     */
    public function getStatusCode();

    /**
     * Setter for statusCode.
     *
     * @param int $statusCode Value to set
     *
     * @return self
     */
    public function setStatusCode($statusCode);

    /**
     * Serialize result.
     *
     * @param $serializer SerializerAbstract
     * @param $resourceKey string
     *
     * @return self
     */
    public function serialize(SerializerAbstract $serializer, $resourceKey = null);

    /**
     * Parse Include String.
     *
     * @param array|string $includes Array or csv string of resources to include
     *
     * @return $this
     */
    public function parseIncludes($includes = '');

    /**
     * Parse Sparse Fieldsets.
     *
     * @param array|string $includes Array or csv string of resources to include
     * @throw PresenterException
     *
     * @return $this
     */
    public function parseSparseFieldsets($name, array $fieldsets = []);

    /**
     * Set meta for response.
     *
     * @param array $meta Array to include as meta
     *
     * @return $this
     */
    public function setMeta(array $meta);

    /**
     * Give response with fractal item.
     *
     * @param $item mixed
     * @throw PresenterException
     *
     * @return JsonResponse
     */
    public function renderItem($item);

    /**
     * Give response with fractal collection.
     *
     * @param $collection mixed
     * @throw PresenterException
     *
     * @return JsonResponse
     */
    public function renderCollection($collection);

    /**
     * get multiple tables data and itrate on respective transformers to format data.
     *
     * @param $collection mixed
     * @throw PresenterException
     *
     * @return JsonResponse
     */
    public function renderMultipleCollections($collection);

    /**
     * Use array as JSON Response.
     *
     * @param $array array
     * @param @headers array
     *
     * @return JsonResponse
     */
    public function renderArray(array $array, array $headers = []);

    /**
     * Set API base url.
     *
     * @param string $url
     */
    public function setBaseUrl($url);

    /**
     * Get API base url.
     *
     * @return string
     */
    public function getBaseUrl();
}
