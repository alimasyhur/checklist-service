<?php

namespace Libs\Presenters;

use Illuminate\Http\Request;

trait ApiResponseTrait
{
    /**
     * Respond.
     *
     * @param array  $data
     * @param string $response_message
     * @param int    $status_code
     *
     * @return JsonResponse
     */
    public function respond($data = [], $response_message = 'Success', $status_code = 200)
    {
        // if this is and internal request, we only return the data
        // if ($this->request->input('no-json'))
        //  return $data;

        $message = [
            'status'  => true,
            'message' => $response_message,
            'data'    => $data,
        ];

        // Log::info('respond');

        return response()->json($message, $status_code);
    }

    /**
     * Respond with errors.
     *
     * @param string $errors
     * @param int    $error_code
     * @param int    $status_code
     *
     * @return JsonResponse
     */
    public function respondWithErrors($errors = 'Fail', $error_code = 10, $status_code = 400)
    {
        if (is_string($errors)) {
            $errors = [$errors];
        }

        $message = [
            'status'     => false,
            'error_code' => $error_code,
            'errors'     => $errors,
        ];

        return response()->json($message, $status_code);
    }

    /**
     * Respond with validation errors.
     *
     * @param $errors
     * @param int $status_code
     *
     * @return JsonResponse
     */
    public function respondWithValidationErrors($errors, $status_code = 400)
    {
        $message = [
            'status'            => false,
            'message'           => 'Please double check your form',
            'validation_errors' => $errors,
        ];

        return response()->json($message, $status_code);
    }

    /**
     * Respond the resource was created.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondCreate($message = 'Resource created')
    {
        return $this->respond($message, 201);
    }

    /**
     * @param $error_code
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondUnauthorized($error_code, $message = 'You are not authorized for this')
    {
        return $this->respondWithErrors($message, $error_code, 403);
    }

    /**
     * @param $error_code
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondUnauthenticated($error_code, $message = 'You are not authenticated for this')
    {
        return $this->respondWithErrors($message, $error_code, 401);
    }

    /**
     * Respond Internal Error.
     *
     * @param $error_code
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondInternalError($error_code, $message = 'Internal error')
    {
        return $this->respondWithErrors($message, $error_code, 500);
    }

    /**
     * Respond Ok.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondOk($message = 'Done')
    {
        return $this->respond([], $message, 200);
    }

    public function isRestoreDelete(Request $request)
    {
        return (bool) $request->header('X-API-RESTORE') && 'PATCH' == $request->method();
    }
}
