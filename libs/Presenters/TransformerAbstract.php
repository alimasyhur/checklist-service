<?php

namespace Libs\Presenters;

use Auth;
use League\Fractal\Resource\ResourceInterface;
use League\Fractal\Scope;
use League\Fractal\TransformerAbstract as FractalTransfomerAbstract;

abstract class TransformerAbstract extends FractalTransfomerAbstract
{
    protected $user;
    protected $user_timezone;

    private $availableSparseFieldsets = [];
    private $sparseFieldsets          = [];
    private $parentScopeResource;

    public function __construct()
    {
        // $this->setUser(Auth::user());
    }

    /**
     * Parse Sparse Fieldsets.
     *
     * @param $includeName
     * @param array $fields
     * @param null  $previous
     */
    public function parseSparseFieldsets($includeName, array $fields = [], $previous = null)
    {
        $this->availableSparseFieldsets = $fields;

        if (! is_null($previous)) {
            $this->availableSparseFieldsets = $previous;
        }
        if (array_key_exists($includeName, $this->availableSparseFieldsets)) {
            $this->sparseFieldsets = $this->availableSparseFieldsets[$includeName];
        }
    }

    /**
     * Call Include Method.
     *
     * @param Scope  $scope
     * @param string $includeName
     * @param mixed  $data
     *
     * @throws \Exception
     *
     * @return ResourceInterface
     */
    protected function callIncludeMethod(Scope $scope, $includeName, $data)
    {
        $resource = parent::callIncludeMethod($scope, $includeName, $data);

        if ($resource && array_key_exists($includeName, $this->availableSparseFieldsets)) {
            $resource->getTransformer()->parseSparseFieldsets(
                $includeName,
                $this->availableSparseFieldsets[$includeName],
                $this->availableSparseFieldsets
            );
        }

        return $resource;
    }

    /**
     * Apply Sparse Fieldsets.
     *
     * @param array $data
     *
     * @return array
     */
    protected function applySparseFieldsets(array $data)
    {
        $data = transform_date($data);
        if (count($this->sparseFieldsets) > 0) {
            $appliedData = [
                'id' => $data['id'],
            ];
            foreach ($this->sparseFieldsets as $key) {
                if (isset($data[$key])) {
                    $appliedData[$key] = $data[$key];
                }
            }

            return $appliedData;
        }

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    protected function item($data, $transformer, $resourceKey = null)
    {
        //Check if data is not null
        if ($data) {
            return parent::item($data, $transformer, $resourceKey);
        }
    }

    protected function getParentScopeResource()
    {
        return $this->parentScopeResource;
    }

    protected function setParentScopeResource($resource)
    {
        $this->parentScopeResource = $resource;

        return $this;
    }

    /**
     * Get current user timezone.
     */
    protected function getUserTimezone()
    {
        if (! $this->user_timezone) {
            $this->user_timezone = $this->getUser()->getTimezone();
        }

        return $this->user_timezone ?: 'America/New_York';
    }

    protected function getUser()
    {
        return $this->user;
    }

    protected function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}
