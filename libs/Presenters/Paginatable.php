<?php

namespace Libs\Presenters;

use CorePackage\Infrastructures\LengthAwareOffsetPaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

trait Paginatable
{
    /**
     * Retrieve all data of repository, paginated.
     *
     * @param int   $limit
     * @param int   $offset
     * @param array $columns
     * @param array $queryParameter Request query paramters
     *
     * @return LengthAwareOffsetPaginator
     */
    public function scopeBrowse($query, $limit = 10, $offset = 0, $columns = ['*'], array $queryParameter = [])
    {
        $baseQuery = $query->toBase();
        // Only get total count from unique distinct value
        $total = $baseQuery->getCountForPagination();

        if (! $total || (int) $limit <= 0 || $offset < 0 || $offset >= $total) {
            return new Collection();
        }

        $results = $query->limit($limit)->offset($offset)->get($columns);

        $results = new LengthAwareOffsetPaginator($results, $total, $limit, $offset, [
            'path'           => Paginator::resolveCurrentPath(),
            'queryParameter' => $queryParameter,
        ]);

        return $results;
    }
}
