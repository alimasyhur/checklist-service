<?php

namespace Libs\Presenters;

use League\Fractal\Serializer\JsonApiSerializer as FractalJsonApiSerializer;

class JsonApiSerializer extends FractalJsonApiSerializer
{
    /**
     * Hack to allow custom contacts routing on contacts microservice.
     */
    public function item($resourceKey, array $data)
    {
        $resource = parent::item($resourceKey, $data);
        // $self = $resource['data']['links']['self'];

        if (! empty($overrides = json_decode(env('PRESENTER_LINKS_OVERRIDE', ''), true))) {
            foreach ($overrides as $from => $to) {
                $resource['data']['links']['self'] = str_replace($from, $to, $self);
            }
        }

        return $resource;
    }

    /**
     * This is temporary solution until following PR to fractal approved.
     * https://github.com/thephpleague/fractal/pull/350.
     */
    protected function parseRelationships($includedData)
    {
        $relationships = parent::parseRelationships($includedData);

        foreach ($includedData as $inclusion) {
            foreach (array_keys($inclusion) as $includeKey) {
                if (isset($includedData[0][$includeKey]['meta'])) {
                    $relationships[$includeKey][0]['meta'] = $includedData[0][$includeKey]['meta'];
                }
            }
        }

        return $relationships;
    }
}
