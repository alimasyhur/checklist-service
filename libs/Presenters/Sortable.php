<?php

namespace Libs\Presenters;

trait Sortable
{
    protected $allowSort = [
        'time',
        'rank',
    ];

    /**
     * Retrieve all data of repository, sorted.
     *
     * @param array $sorter
     *
     * @return
     */
    public function scopeGeosorter($query, array $sorter = [])
    {
        foreach ($sorter as $key => $value) {
            if (in_array($key, $this->allowSort)) {
                $query->orderBy($key, $value);
            }
        }

        return $query;
    }
}
