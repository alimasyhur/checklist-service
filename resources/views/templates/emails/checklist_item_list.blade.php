@foreach($items as $key => $item)
<table width="100%" bgcolor="#fafafa" align="center" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td class="pattern" width="100%" align="center">
				<table cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td>
								<table width="550" cellpadding="0" cellspacing="0" style="{{ $key > 0 ? 'padding-top: 35px;' : ''}}  bgcolor="#FFFFFF">
									<tbody>
										<tr>
											<td width="550" align="center" bgcolor="#F0F8F9">
												<table cellpadding="0" cellspacing="0">
													<tbody>
														<tr>
															<td>
																<table cellpadding="0" cellspacing="0">
																	<tbody>
																	<tr>
																	    <td width="500" align="center" class="description" style="padding:30px 50px;">
																	        <ul>
																				@if(filled($item['opportunityName']))<li><p style="text-align:left;font-weight:300;font-size:16px;line-height:33px;color:#1A9AA7;list-style:disc inside; mso-special-format:bullet;">{{$item['opportunityName']}}</p></li>@endif
																	            <li><p style="text-align:left;font-weight:300;font-size:16px;line-height:33px;color:#1A9AA7;list-style:disc inside; mso-special-format:bullet;">{{$item['taskName']}}</p></li>
																				@if(filled($item['contactName']))<li><p style="text-align:left;font-weight:300;font-size:16px;line-height:33px;color:#1A9AA7;list-style:disc inside; mso-special-format:bullet;">{{$item['contactName']}}</p></li>@endif
                                                                                <li><p style="text-align:left;font-weight:300;font-size:16px;line-height:33px;color:#1A9AA7;list-style:disc inside; mso-special-format:bullet;">{{$item['dueDate']}}</p></li>
																	        </ul>
																	    </td>
																	</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
@endforeach