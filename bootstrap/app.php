<?php

require_once __DIR__ . '/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__ . '/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__ . '/../')
);

$app->withFacades();

$app->withEloquent();

/**
 * -Load Config from config dir
 */
$configPath = $app->basePath('config');

$glob = glob($configPath . '/*.php');

if ($glob !== false) {
    // To get the appropriate files, we'll simply glob the directory and filter
    // out any "files" that are not truly files so we do not end up with any
    // directories in our list, but only true files within the directory.
    $configPathLength = strlen($configPath);
    array_walk($glob, function ($file) use ($app, $configPathLength) {
        if (filetype($file) == 'file') {
            $configName = substr(substr($file, 1, -(5 - strlen($file))), $configPathLength);
            $app->configure($configName);
        }
    });
}
/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(
    Libs\Presenters\JsonApiPresenterInterface::class,
    Libs\Presenters\JsonApiPresenter::class
);

// Register provider that listed in config/app.php
$providers = $app->make('config')->get('app.providers');
if (! empty($providers)) {
    array_walk($providers, function ($provider) use ($app) {
        $app->register($provider);
    });
}

$app->middleware(CorePackage\Middleware\StatsdMiddleware::class);


app('translator')->setLocale('en');

$app->afterResolving(Illuminate\Http\Request::class, function ($request) {
    if (env('APP_DEBUG', false) && $request->input('query_debug', 0)) {
        DB::enableQueryLog();
    }
});

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

// $app->middleware([
//    App\Http\Middleware\ExampleMiddleware::class
// ]);

$app->routeMiddleware([
    'auth' => App\Http\Middleware\Authenticate::class,
    'team-gateway' => App\Http\Middleware\TeamGateway::class,
    'opportunity-team-gateway' => App\Http\Middleware\OpportunityTeamGateway::class,
    'mc-gateway' => App\Http\Middleware\MarketCenterGateway::class,
    'sanitize-user-id-filter' => App\Http\Middleware\SanitizeUserIdFilter::class
]);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/
boot($app);
$app->router->group([
    'namespace'  => 'App\Http\Controllers',
    'prefix'     => 'api/v1',
    'middleware' => ['auth'],
], function ($router) {
    require __DIR__ . '/../routes/web.php';
});

return $app;
