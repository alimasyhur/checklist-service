<?php

use App\Services\Roles\RoleService;

return [
    'providers' => [
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        App\Providers\KubeHealthcheckServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Laravel\Horizon\HorizonServiceProvider::class,
    ],

    'env' => env('APP_ENV', 'dev'),
    'kwc_url' => env('KWC_URL', 'https://dev-kong.command-api.kw.com'),
    'fake_org_ids' => [
        5663 => 21,
        5664 => 6127,
    ],

    'roles_api' => [
        'url' => env('PEOPLE_ENDPOINT', 'https://roles.kwiq.kw.com/api/v1.1/person'),
        'key' => env('PEOPLE_API_KEY'),
    ],

    'destructive_action_role' => [
        RoleService::ROLE_TEAM_LEADER,
        RoleService::ROLE_TL_ASSISTANT,
        RoleService::ROLE_GENERAL_MANAGER,
        RoleService::ROLE_PRINCIPAL,
        RoleService::ROLE_MCA,
        RoleService::ROLE_ASSISTANT_MCA,
    ],
];
