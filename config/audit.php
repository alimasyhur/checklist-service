<?php

return [
    'type' => 'TASKS',
    'sub_type' => [
        'item' => [
            'add' => 'Add Checklist Item',
            'edit' => 'Edit Checklist Item',
            'delete' => 'Delete Checklist Item',
            'complete' => 'Marked as Completed',
            'incomplete' => 'Marked as Incomplete',
        ],
    ],
];
