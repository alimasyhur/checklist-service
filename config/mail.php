<?php

return [
    'public_key' => env('MJ_APIKEY_PUBLIC'),
    'private_key' => env('MJ_APIKEY_PRIVATE'),
    'sender' => env('MJ_EMAIL_SENDER'),
    'name' => env('MJ_EMAIL_NAME'),
    'grouped_due_item_template_id' => env('MJ_GROUPED_DUE_ITEM_TEMPLATE_ID', 776105),
    'checklist_item_notification_template_id' => env('MJ_CHECKLIST_ITEM_NOTIFICATION_TEMPLATE_ID', 777548)
];
