<?php

namespace App\Services\Notifications;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LastDueReminderEloquent extends Model
{
    use SoftDeletes;

    protected $dates = ['due'];

    protected $table = 'last_due_reminders';

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kwuid', 'due'
    ];
}
