<?php

namespace App\Services\Notifications\Repositories;

use App\Services\Notifications\LastDueReminderEloquent;

class LastDueReminderRepository
{
    public function __construct()
    {
    }

    public function isReminderExists($kwuid, $due)
    {
        $count = LastDueReminderEloquent::where('kwuid', $kwuid)
            ->where('due', $due)
            ->count();

        return $count > 0;
    }

    public function create($data)
    {
        return LastDueReminderEloquent::create($data);
    }
}
