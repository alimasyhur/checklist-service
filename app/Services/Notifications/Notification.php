<?php

namespace App\Services\Notifications;

use App\Services\Consumers\ConsumersService;
use Illuminate\Support\Facades\Cache;
use App\Jobs\SendNotificationJob;

class Notification
{
    private $token;
    private $user;
    private $title;
    private $message;
    private $origin;
    private $domain;
    private $objectID;
    private $type;
    private $logs = [];
    private $id;
    private $targets           = [];
    private $from              = [];
    private $emailNotification = [];
    private $queueTags         = [];

    public function requestToken($userId, $expiredAt)
    {
        $token = Cache::remember(
            'due-checklist-notification-token-user-' . $userId,
            5,
            function () use ($userId, $expiredAt) {
                $consumersService = new ConsumersService();
                $result = $consumersService->createConsumerToken($userId, $expiredAt);

                return $result['jwt'];
            }
        );

        return $token;
    }

    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getKey()
    {
        return $this->id;
    }

    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setOrigin($origin)
    {
        $this->origin = $origin;

        return $this;
    }

    public function getOrigin()
    {
        return $this->origin;
    }

    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setObjectID($objectID)
    {
        $this->objectID = $objectID;

        return $this;
    }

    public function getObjectID()
    {
        return $this->objectID;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setTargets(array $targets = [])
    {
        $this->targets = $targets;

        return $this;
    }

    public function getTargets()
    {
        return $this->targets;
    }

    public function setEmailNotification(array $emailNotification = [])
    {
        $this->emailNotification = $emailNotification;

        return $this;
    }

    public function getEmailNotification()
    {
        return $this->emailNotification;
    }

    public function setFrom($id, $type = 'user')
    {
        $this->from = ['id' => $id, 'type' => $type];

        return $this;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function setQueueTags($tags)
    {
        $this->queueTags = $tags;

        return $this;
    }

    public function getQueueTags()
    {
        return $this->queueTags;
    }

    public function send()
    {
        $from    = $this->getFrom();
        $targets = $this->getTargets();

        if (empty($from['id'])) {
            return;
        }

        foreach ($targets as $target) {
            if (empty($target['id'])) {
                return;
            }
        }

        dispatch(new SendNotificationJob($this));
    }

    public function sendSync()
    {
        return (new SendNotificationJob($this))->handle();
    }

    public function toParams()
    {
        $data = [
            'data' => [
                'attributes' => [
                    'title'     => $this->getTitle(),
                    'message'   => $this->getMessage(),
                    'origin'    => $this->getOrigin(),
                    'domain'    => $this->getDomain(),
                    'object_id' => $this->getObjectID(),
                    'type'      => $this->getType(),
                    'targets'   => $this->getTargets(),
                    'from'      => $this->getFrom(),
                ],
            ],
        ];

        return $data;
    }
}
