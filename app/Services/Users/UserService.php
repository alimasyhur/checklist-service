<?php

namespace App\Services\Users;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;

class UserService
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getProfile($authToken)
    {
        $baseUri = env('KWC_URL', 'https://dev-kong.command-api.kw.com');
        $endpoint = "$baseUri/users/me";

        try {
            $request = $this->client->request('GET', $endpoint, [
                'headers' => [
                    'Authorization' => "Bearer $authToken",
                ]
            ]);

            $response = $request->getBody()->getContents();
            $response = json_decode($response, true);

            return $response;
        } catch (RequestException $e) {
            Log::info($e);
            return null;
        }
    }
}
