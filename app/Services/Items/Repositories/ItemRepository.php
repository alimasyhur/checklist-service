<?php

namespace App\Services\Items\Repositories;

use App\Jobs\Audit\CompleteItemJob;
use App\Jobs\Audit\CreateItemJob;
use App\Jobs\Audit\DeleteItemJob;
use App\Jobs\Audit\IncompleteItemJob;
use App\Jobs\Audit\UpdateItemJob;
use App\Services\Checklists\ChecklistEloquent;
use App\Services\Checklists\Repositories\ChecklistRepository;
use App\Services\Items\ItemEloquent;
use App\Services\History\Repositories\HistoryRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Contracts\Cache\Repository as Cache;
use CorePackage\Infrastructures\Adapters\SearchAdapter;
use Illuminate\Support\Facades\DB;
use App\Services\Summaries\Repositories\SummaryRepository;

class ItemRepository
{
    private const DEAL_OBJECT = 'deals';

    public function __construct(
        Cache $cache,
        Carbon $carbon,
        ChecklistRepository $checklistRepo,
        HistoryRepository $historyRepo,
        SummaryRepository $summaryRepo
    ) {
        $this->cache = $cache;
        $this->carbon = $carbon;
        $this->checklistRepo = $checklistRepo;
        $this->historyRepo = $historyRepo;
        $this->summaryRepo = $summaryRepo;
    }

    /**
     * browse data.
     *
     * @return LengthAwareOffsetPaginator
     */
    public function browse(SearchAdapter $searchAdapter)
    {
        $query = new ItemEloquent();
        if ($searchAdapter) {
            $criteria = new ItemSearchCriteria();
            $query    = $criteria->apply($query, $searchAdapter);
            $pager    = $searchAdapter->getPager();
            if (null != $pager) {
                return $query->browse(
                    $pager->getLimit(),
                    $pager->getOffset(),
                    ['*'],
                    $pager->getQueries()
                );
            }
        }

        return $query->browse();
    }

    /**
     * browse data.
     *
     * @return Model
     */
    public function find($id)
    {
        return ItemEloquent::findOrFail($id);
    }

    public function findByIds($ids)
    {
        return ItemEloquent::whereIn('id', $ids)->get();
    }

    /**
     * create data.
     *
     * @return bool
     */
    public function create(ItemEloquent $item)
    {
        $data = $this->setupPayload($item);
        $item = ItemEloquent::create($data);

        if ($item->checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $user = app(Auth::class)->user();
            dispatch(new CreateItemJob($user, $item));
        }

        $this->invalidateChecklistItemCountCache($item->getAttribute('checklist_id'));

        return $item;
    }

    public function setupPayload(ItemEloquent $item)
    {
        return [
            'description'  => $item->getAttribute('description'),
            'meta'         => $item->getAttribute('meta'),
            'is_completed' => $item->getAttribute('is_completed'),
            'completed_at' => $item->getAttribute('completed_at'),
            'completed_by' => $item->getAttribute('completed_by'),
            'due'          => $item->getAttribute('due'),
            'urgency'      => $item->getAttribute('urgency'),
            'updated_by'   => $item->getAttribute('updated_by'),
            'user_id'      => $item->getAttribute('user_id'),
            'assignee_id'  => $item->getAttribute('assignee_id'),
            'checklist_id' => $item->getAttribute('checklist_id'),
            'task_id'      => $item->getAttribute('task_id'),
            'notify_client' => $item->getAttribute('notify_client'),
            'client_update_sent_at' => $item->getAttribute('client_update_sent_at'),
            'org_id' => $item->getAttribute('org_id'),
        ];
    }

    /**
     * update data.
     *
     * @return Model
     */
    public function update(ItemEloquent $item)
    {
        $old = $item->getOriginal();

        $data = $this->setupPayload($item);
        $item->fill($data);
        $item->save();

        if ($item->checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $user = app(Auth::class)->user();
            dispatch(new UpdateItemJob($user, $item, $old));
        }

        return $item->fresh();
    }

    public function index(ItemEloquent $item)
    {
        return $item->all();
    }

    /**
     * delete data.
     *
     * @return int
     */
    public function delete(ItemEloquent $item)
    {
        $result = $item->delete();

        if ($item->checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $user = app(Auth::class)->user();
            dispatch(new DeleteItemJob($user, $item, Carbon::now()));
        }

        $this->invalidateChecklistItemCountCache($item->getAttribute('checklist_id'));

        return $result;
    }

    
    public function complete(array $ids, $userIds, $orgId = null)
    {
        $incompleteItems = ItemEloquent::whereIn('id', $ids)
            ->whereNull('completed_at')
            ->where('is_completed', 0);

        if ($orgId) {
            $incompleteItems->where('org_id', $orgId);
        } else {
            $incompleteItems->where(function ($query) use ($userIds) {
                $query->whereIn('user_id', $userIds)
                    ->orWhereIn('assignee_id', $userIds);
            });
        }

        $user = app(Auth::class)->user();
        $userId = $user->getKey();

        $this->historyRepo->completeItems($user->getKey(), ...$incompleteItems->get());

        $toCompleteItems = $this->getItemsCompleteness($incompleteItems->get());

        $incompleteItems->update([
            'is_completed' => 1,
            'completed_at' => $this->carbon::now(),
            'completed_by' => $userId,
        ]);

        if (!empty($toCompleteItems)) {
            foreach ($this->findByIds($toCompleteItems) as $item) {
                if ($item->checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
                    dispatch(new CompleteItemJob($user, $item));
                }
            }
        }

        $completedItems = ItemEloquent::whereIn('id', $ids);

        if ($orgId) {
            $completedItems->where('org_id', $orgId);
        } else {
            $completedItems->where(function ($query) use ($userIds) {
                $query->whereIn('user_id', $userIds)
                    ->orWhereIn('assignee_id', $userIds);
            });
        }

        $completedItems = $completedItems->get();

        $checklistIds = $completedItems->map(function ($item) {
            return $item->getAttribute('checklist_id');
        })->unique();

        foreach ($checklistIds as $checklistId) {
            $checklist               = ChecklistEloquent::find($checklistId);
            $checklistItemsNotCompleted = $checklist->items->where('is_completed', 0);
            if (0 == $checklistItemsNotCompleted->count()) {
                $checklist->is_completed = 1;
                $checklist->completed_at = $this->carbon::now();
                $checklist->save();
            }

            $this->invalidateChecklistItemCountCache($checklistId);
        }

        $response = $completedItems->map(function ($item) {
            return [
                'item_id'      => $item->getKey(),
                'checklist_id' => $item->getAttribute('checklist_id'),
                'is_completed' => $item->getAttribute('is_completed'),
                'completed_by' => $item->getAttribute('completed_by'),
                'completed_at' => $item->getAttribute('completed_at')
                    ? $item->getAttribute('completed_at')->toIso8601String()
                    : null,
            ];
        });

        if ($response->count() > 0) {
            return $this->mapItemCompletenessResponse($response, $ids);
        }

        return $this->mapNoAccessResponse($ids);
    }

    public function incomplete(array $ids, $userIds, $orgId = null)
    {
        $completeItems = ItemEloquent::whereIn('id', $ids)
            ->whereNotNull('completed_at')
            ->where('is_completed', 1);

        if ($orgId) {
            $completeItems->where('org_id', $orgId);
        } else {
            $completeItems->where(function ($query) use ($userIds) {
                $query->whereIn('user_id', $userIds)
                    ->orWhereIn('assignee_id', $userIds);
            });
        }

        $checklistIds = $completeItems->get()->map(function ($item) {
            return $item->getAttribute('checklist_id');
        })->unique();

        foreach ($checklistIds as $checklistId) {
            $checklist               = ChecklistEloquent::find($checklistId);
            $checklist->is_completed = 0;
            $checklist->completed_at = null;
            $checklist->save();

            $this->invalidateChecklistItemCountCache($checklistId);
        }

        $user = app(Auth::class)->user();

        $this->historyRepo->incompleteItems($user->getKey(), ...$completeItems->get());

        $toIncompleteItems = $this->getItemsCompleteness($completeItems->get());

        $completeItems->update([
            'is_completed' => 0,
            'completed_at' => null,
            'completed_by' => null
        ]);

        if (!empty($toIncompleteItems)) {
            foreach ($this->findByIds($toIncompleteItems) as $item) {
                if ($item->checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
                    dispatch(new IncompleteItemJob($user, $item));
                }
            }
        }

        $incompleteItems = ItemEloquent::whereIn('id', $ids);

        if ($orgId) {
            $incompleteItems->where('org_id', $orgId);
        } else {
            $incompleteItems->where(function ($query) use ($userIds) {
                $query->whereIn('user_id', $userIds)
                    ->orWhereIn('assignee_id', $userIds);
            });
        }

        $response = $incompleteItems->get()->map(function ($item) {
            return [
                'item_id'      => $item->getKey(),
                'checklist_id' => $item->getAttribute('checklist_id'),
                'is_completed' => (bool) $item->getAttribute('is_completed'),
                'completed_by' => $item->getAttribute('completed_by'),
                'completed_at' => $item->getAttribute('completed_at')
                    ? $item->getAttribute('completed_at')->toIso8601String()
                    : null,
            ];
        });

        if ($response->count() > 0) {
            return $this->mapItemCompletenessResponse($response, $ids);
        }

        return $this->mapNoAccessResponse($ids);
    }

    private function getItemsCompleteness($itemCompleteness)
    {
        $toItemCompleteness = [];
        foreach ($itemCompleteness as $item) {
            if ($item->assignee_id) {
                $this->summaryRepo->invalidateSummaryByAssigneeId($item->assignee_id);
            }

            array_push($toItemCompleteness, $item->id);
        }

        return $toItemCompleteness;
    }

    private function mapItemCompletenessResponse($response, $ids)
    {
        $noAccess = collect($ids)->filter(function ($id) use ($response) {
            return !$response->contains(function ($element) use ($id) {
                return $element['item_id'] == $id;
            });
        })->map(function ($id) {
            return [
                'item_id' => $id,
                'result'  => 'no-access',
            ];
        });

        return $response->merge($noAccess);
    }

    private function mapNoAccessResponse($ids)
    {
        return collect($ids)->map(function ($id) {
            return [
                'item_id' => $id,
                'result'  => 'no-access',
            ];
        });
    }

    public function getOpportunityTaskDueReminder($assigneeId, $due)
    {
        $startDate = $due->copy()->setTime(0, 0, 0)->format('Y-m-d H:i:s');
        $endDate = $due->copy()->setTime(23, 59, 59)->format('Y-m-d H:i:s');

        return DB::table('items')
            ->select('items.*', 'checklists.object_domain', 'checklists.object_id')
            ->leftJoin('checklists', 'items.checklist_id', '=', 'checklists.id')
            ->where('checklists.object_domain', 'deals')
            ->whereBetween('items.due', [$startDate, $endDate])
            ->where('items.assignee_id', $assigneeId)
            ->where('items.is_completed', false)
            ->get();
    }

    public function summary($kwuid, $date, $constraint = [], $orgId = null)
    {
        $tz = array_get($constraint, 'tz', 'UTC');
        $objectDomain = array_get($constraint, 'object_domain');
        $date->setTimezone($tz);
        $date->setTime(0, 0, 0);

        $encoded = json_encode([$kwuid, $date->toIso8601String(), $objectDomain]);
        $tags = ["summary", "item-summary-assignee-id-$kwuid"];
        if ($orgId) {
            $encoded = json_encode([$kwuid, $date->toIso8601String(), $objectDomain, $orgId]);
            $tags = ["summary", "item-summary-org-id-$orgId"];
        }

        return $this->cache->tags($tags)->remember(
            "summary-$encoded",
            60 * 24,
            function () use ($kwuid, $date, $objectDomain, $orgId) {
                $summaryFilters = [
                    'today', 'past_due', 'week.current', 'week.past', 'month.current', 'month.past', 'total'
                ];

                $summary = [];
                foreach ($summaryFilters as $filter) {
                    $allUppercaseFilter = $this->getStudlyCase($filter);
                    $query = $this->{'summary' . $allUppercaseFilter}($kwuid, $date, $orgId);

                    if (filled($objectDomain)) {
                        $query->leftJoin('checklists', 'items.checklist_id', '=', 'checklists.id')
                              ->where('checklists.object_domain', $objectDomain);
                    }

                    $count = $query->count();
                    array_set($summary, $filter, $count);
                }

                return $summary;
            }
        );
    }

    private function summaryToday($kwuid, $date, $orgId = null)
    {
        $query = new ItemEloquent();
        $startDue = $date->copy()->startOfDay()->toIso8601ZuluString();
        $endDue = $date->copy()->endOfDay()->toIso8601ZuluString();

        $query = $query->whereBetween('items.due', [$startDue, $endDue])
            ->where('items.is_completed', 0);
        if ($orgId) {
            return $query->where('items.org_id', $orgId);
        }
        return $query->where('items.assignee_id', $kwuid);
    }

    private function summaryPastDue($kwuid, $date, $orgId = null)
    {
        $query = new ItemEloquent();
        $due = Carbon::createFromDate($date->year, $date->month, $date->day)->toIso8601ZuluString();

        $query = $query->where('items.due', '<=', $due)
            ->where('items.is_completed', 0);
        if ($orgId) {
            return $query->where('items.org_id', $orgId);
        }
        return $query->where('items.assignee_id', $kwuid);
    }

    private function summaryWeekCurrent($kwuid, $date, $orgId = null)
    {
        $query = new ItemEloquent();
        $startDue = $date->copy()->startOfWeek()->subDays(1)->toIso8601ZuluString();
        $endDue = $date->copy()->endOfWeek()->subDays(1)->toIso8601ZuluString();

        $query = $query->whereBetween('items.due', [$startDue, $endDue])
            ->where('items.is_completed', 0);
        if ($orgId) {
            return $query->where('items.org_id', $orgId);
        }
        return $query->where('items.assignee_id', $kwuid);
    }

    private function summaryWeekPast($kwuid, $date, $orgId = null)
    {
        $query = new ItemEloquent();
        $pastWeek = $date->copy()->subWeeks(1);
        $startDue = $pastWeek->copy()->startOfWeek()->subDays(1)->toIso8601ZuluString();
        $endDue = $pastWeek->copy()->endOfWeek()->subDays(1)->toIso8601ZuluString();

        $query = $query->whereBetween('items.due', [$startDue, $endDue])
            ->where('items.is_completed', 0);
        if ($orgId) {
            return $query->where('items.org_id', $orgId);
        }
        return $query->where('items.assignee_id', $kwuid);
    }

    private function summaryMonthCurrent($kwuid, $date, $orgId = null)
    {
        $query = new ItemEloquent();
        $startDue = $date->copy()->firstOfMonth()->toIso8601ZuluString();
        $endDue = $date->copy()->endOfMonth()->toIso8601ZuluString();

        $query = $query->whereBetween('items.due', [$startDue, $endDue])
            ->where('items.is_completed', 0);
        if ($orgId) {
            return $query->where('items.org_id', $orgId);
        }
        return $query->where('items.assignee_id', $kwuid);
    }

    private function summaryMonthPast($kwuid, $date, $orgId = null)
    {
        $query = new ItemEloquent();
        $pastMonth = $date->copy()->firstOfMonth()->subMonths(1);
        $startDue = $pastMonth->copy()->firstOfMonth()->toIso8601ZuluString();
        $endDue = $pastMonth->copy()->endOfMonth()->toIso8601ZuluString();

        $query = $query->whereBetween('items.due', [$startDue, $endDue])
            ->where('items.is_completed', 0);
        if ($orgId) {
            return $query->where('items.org_id', $orgId);
        }
        return $query->where('items.assignee_id', $kwuid);
    }

    private function summaryTotal($kwuid, $date, $orgId = null)
    {
        $query = new ItemEloquent();
        $startDue = $date->copy()->firstOfMonth()->toIso8601ZuluString();
        $endDue = $date->copy()->endOfMonth()->toIso8601ZuluString();

        $query = $query->whereNotNull('items.due')
            ->where('items.is_completed', 0);
        if ($orgId) {
            return $query->where('items.org_id', $orgId);
        }
        return $query->where('items.assignee_id', $kwuid);
    }

    public function checklistItemCount($user, $checklistIds, $orgId = null)
    {
        if (empty($checklistIds)) {
            return [];
        }

        $checklistIds = explode(',', $checklistIds);
        $summary = [];
        foreach ($checklistIds as $checklistId) {
            $checklist = $this->checklistRepo->findById($checklistId);
            if (!$checklist) {
                continue;
            }

            if ($user->cant('show', $checklist)) {
                continue;
            }

            $countChecklistItem = $this->getChecklistItemCount($checklistId, $orgId);
            array_push($summary, $countChecklistItem);
        }

        return count($summary) > 0 ? ['counts' => $summary] : [];
    }

    private function getChecklistItemCount($checklistId, $orgId)
    {
        return $this->cache->tags(['summary', "checklist-id-$checklistId"])
            ->remember(
                "checklist-item-count-id-$checklistId",
                24 * 60,
                function () use ($checklistId, $orgId) {
                    $countFilters = ['completed', 'incomplete', 'total'];
                    $result = ['checklist_id' => (int) $checklistId];
                    foreach ($countFilters as $filter) {
                        $allUppercaseFilter = $this->getStudlyCase($filter);
                        $query = $this->{'count' . $allUppercaseFilter}($checklistId, $orgId);
                        $itemsKey = sprintf('%s_items', $filter);
                        $itemsCount = [$itemsKey => $query->count()];
                        $result = array_merge($result, $itemsCount);
                    }

                    return $result;
                }
            );
    }

    private function countCompleted($checklistId, $orgId = null)
    {
        $query = new ItemEloquent();

        if ($orgId) {
            $query = $query->where('org_id', $orgId);
        }

        return $query->where('is_completed', 1)
                    ->where('checklist_id', $checklistId);
    }

    private function countIncomplete($checklistId, $orgId = null)
    {
        $query = new ItemEloquent();

        if ($orgId) {
            $query = $query->where('org_id', $orgId);
        }

        return $query->where('is_completed', 0)
                    ->where('checklist_id', $checklistId);
    }

    private function countTotal($checklistId, $orgId = null)
    {
        $query = new ItemEloquent();

        if ($orgId) {
            $query = $query->where('org_id', $orgId);
        }

        return $query->where('checklist_id', $checklistId);
    }

    private function getStudlyCase($filter)
    {
        $underscoreFilter = str_replace('.', '_', $filter);
        
        return studly_case($underscoreFilter);
    }

    public function invalidateChecklistItemCountCache($checklistId)
    {
        $this->cache->tags(['summary', "checklist-id-$checklistId"])->flush();
    }
}
