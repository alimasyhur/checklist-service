<?php

namespace App\Services\Items\Repositories;

use App\Services\AbstractSearchCriteria;
use Carbon\Carbon;
use CorePackage\Infrastructures\Adapters\Filter;

class ItemSearchCriteria extends AbstractSearchCriteria
{
    protected function parseFilterValues($filter)
    {
        if ($filter->getColumnName() == 'due' && $filter->getOperator() == 'between') {
            $value = $filter->getValue();
            $values = explode(',', $value);
            $newUTCDates = [];
            foreach ($values as $val) {
                $d = new Carbon($val);
                $newUTCDates[] = $d->setTimezone('UTC')->toDateTimeString();
            }

            return new Filter($filter->getColumnName(), implode(',', $newUTCDates), $filter->getOperator());
        }

        return $filter;
    }

    protected function applyFilters($query, $filters)
    {
        $filters->each(function ($filter) use (&$query) {
            $filter = $this->parseFilterValues($filter);
            $query = $this->applyCondition($query, $filter);
        });

        return $query;
    }
}
