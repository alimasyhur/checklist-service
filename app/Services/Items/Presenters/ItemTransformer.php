<?php

namespace App\Services\Items\Presenters;

use App\Services\Checklists\Presenters\ChecklistTransformer;
use App\Services\Items\ItemEloquent;
use Libs\Presenters\TransformerAbstract;

class ItemTransformer extends TransformerAbstract
{
    public $availableIncludes = [
        'checklist'
    ];

    /**
     * Turn this item object into a generic array.
     *
     * @return array
     */
    public function transform(ItemEloquent $model)
    {
        return $this->applySparseFieldsets([
                'id'           => $model->getAttribute('id'),
                'description'  => $model->getAttribute('description'),
                'meta'         => $model->getAttribute('meta'),
                'is_completed' => (bool) $model->getAttribute('is_completed'),
                'completed_at' => $model->getAttribute('completed_at'),
                'completed_by' => $model->getAttribute('completed_by'),
                'due'          => $model->getAttribute('due'),
                'urgency'      => $model->getAttribute('urgency'),
                'updated_by'   => $model->getAttribute('updated_by'),
                'created_by'   => $model->getAttribute('user_id'),
                'checklist_id' => (int) $model->getAttribute('checklist_id'),
                'assignee_id'  => $model->getAttribute('assignee_id'),
                'task_id'      => $model->getAttribute('task_id'),
                'notify_client' => $model->getAttribute('notify_client'),
                'client_update_sent_at' => $model->getAttribute('client_update_sent_at'),
                'deleted_at'   => $model->getAttribute('deleted_at'),
                'created_at'   => $model->getAttribute('created_at'),
                'updated_at'   => $model->getAttribute('updated_at'),
        ]);
    }

    public function includeChecklist(ItemEloquent $model)
    {
        return $this->item($model->getChecklist(), new ChecklistTransformer(), 'checklists');
    }
}
