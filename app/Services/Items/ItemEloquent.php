<?php

namespace App\Services\Items;

use App\Services\Checklists\ChecklistEloquent;
use App\Services\History\HistoryEloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Libs\Presenters\Paginatable;

class ItemEloquent extends Model
{
    use Paginatable, SoftDeletes;

    const MORPH_NAME = 'items';

    protected $dates = ['client_update_sent_at', 'deleted_at', 'due', 'completed_at'];

    protected $table = 'items';

    protected $primaryKey = 'id';

    protected $casts = [
        'is_completed' => 'boolean',
        'notify_client' => 'boolean',
        'meta' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'meta',
        'is_completed',
        'due',
        'urgency',
        'checklist_id',
        'completed_at',
        'updated_by',
        'user_id',
        'assignee_id',
        'task_id',
        'notify_client',
        'client_update_sent_at',
        'org_id',
    ];

    /**
     * get key value.
     *
     * @var string
     */
    public function getKey()
    {
        return $this->id;
    }

    public function checklist()
    {
        return $this->belongsTo(ChecklistEloquent::class, 'checklist_id');
    }

    /**
     * get primary key name.
     *
     * @var string
     */
    public function getKeyName()
    {
        return $this->primaryKey;
    }

    public function histories()
    {
        return $this->morphMany(HistoryEloquent::class, 'loggable');
    }

    public function getChecklist()
    {
        return $this->checklist;
    }
}
