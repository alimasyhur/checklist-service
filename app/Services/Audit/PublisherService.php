<?php

namespace App\Services\Audit;

use App\Exceptions\AuditPublisherServiceException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class PublisherService
{
    private $client;
    private $token;
    private $system = 'COMMAND';
    private $subSystem = 'OPPORTUNITIES';
    private $version = 1;
    private $user;
    private $org;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function setToken(?string $token): self
    {
        if (is_null($token)) {
            abort(401, 'Token is not provided.');
        }

        $this->token = $token;

        return $this;
    }

    public function setUser(array $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function setOrg(?array $org): self
    {
        $this->org = $org;

        return $this;
    }

    public function publishMessage(array $activities): ?array
    {
        if (empty($this->org)) {
            Log::info('Org data is empty.');

            return null;
        }

        $endpoint = rtrim(config('app.kwc_url'), '/');
        $endpoint .= '/audit-publisher/api/v1/message';

        $link = Arr::get($activities, 'link');
        unset($activities['link']);

        $body = [
            'system' => $this->system,
            'sub_system' => $this->subSystem,
            'version' => $this->version,
            'who' => $this->user,
            'org' => $this->org,
            'activity' => $activities,
            'self' => [
                'link' => $link,
            ],
        ];

        try {
            $response = $this->client->post($endpoint, [
                'headers' => [
                    'Authorization' => sprintf('Bearer %s', $this->token),
                    'x-userinfo' => json_encode([
                        'kwuid' => (string) data_get($this->user, 'id'),
                        'name' => 'checklist-app',
                    ]),
                ],
                'json' => $body,
            ]);

            return json_decode((string) $response->getBody(), true);
        } catch (RequestException $exception) {
            throw new AuditPublisherServiceException($exception->getMessage());
        }
    }
}
