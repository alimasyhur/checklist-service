<?php

namespace App\Services\PNO;

use GuzzleHttp\Client;
use App\Exceptions\PNOServiceException;
use Illuminate\Contracts\Cache\Repository as Cache;
use GuzzleHttp\Exception\RequestException;

class PNOService
{
    private $client;
    private $cache;
    private $cacheTtl = 60 * 24;

    public function __construct(Client $client, Cache $cache)
    {
        $this->client = $client;
        $this->cache = $cache;
    }
    
    public function getInactiveUsersByOrgId($authToken, $orgId)
    {
        $cacheKey = "pno-service-inactive-users-org-$orgId";
        $minutes = 60 * 24;
        $tags = ["org-$orgId", 'pno-service'];

        $inactiveUsers = $this->cache->tags($tags)->remember(
            $cacheKey,
            $minutes,
            function () use ($authToken, $orgId) {
                try {
                    $baseUri = config('app.kwc_url');
                    $query = "include_inactive=true&include_active=false&is_org_legacy_team=true&limit=100";
                    $endpoint = "$baseUri/pno/api/v2/orgs/$orgId/people?$query";
        
                    $request = $this->client->request('GET', $endpoint, [
                        'headers' => [
                            'Authorization' => "Bearer $authToken"
                        ]
                    ]);
        
                    $response = $request->getBody()->getContents();
                    $response = json_decode($response, true);
        
                    return $response;
                } catch (RequestException $e) {
                    \Log::info(new PNOServiceException(
                        $e->getMessage()
                    ));

                    return [];
                }
            }
        );

        if (empty($inactiveUsers)) {
            $this->cache->tags($tags)->put($cacheKey, $inactiveUsers, 5);
        }

        return $inactiveUsers;
    }

    public function getUserByKwuid($authToken, $kwuid)
    {
        $cacheKey = "pno-service-user-kwuid-$kwuid";
        $minutes = 60 * 24;
        $tags = ["user-$kwuid", 'pno-service'];

        if ($this->cache->tags($tags)->has($cacheKey)) {
            return $this->cache->tags($tags)->get($cacheKey);
        }

        $user = [];
        $is404 = false;

        try {
            $baseUri = config('app.kwc_url');
            $endpoint = "$baseUri/pno/api/v2/people/$kwuid";

            $request = $this->client->request('GET', $endpoint, [
                'headers' => [
                    'Authorization' => "Bearer $authToken"
                ]
            ]);

            $response = $request->getBody()->getContents();
            $user = json_decode($response, true);
        } catch (RequestException $e) {
            // No need to log on 404 because inactive user would give 404
            $is404 = $e->getResponse()->getStatusCode() == 404;
            if (!$is404) {
                \Log::info(new PNOServiceException(
                    $e->getMessage()
                ));
            }

            $user = [];
        }

        $this->cache->tags($tags)->set($cacheKey, $user, $minutes);

        // Save the cache for only 5 minutes when the exception is not from 404
        // Otherwise server errors would mess up the authorization
        if (empty($user) && !$is404) {
            $this->cache->tags($tags)->set($cacheKey, $user, 5);
        }

        return $user;
    }

    public function isActiveUser($authToken, $kwuid)
    {
        $user = $this->getUserByKwuid($authToken, $kwuid);

        $isActive = !empty($user) && !array_key_exists('error', $user);

        return $isActive;
    }

    public function appendInactiveMembersToTeam($authToken, $orgId, $activeMemberIds)
    {
        $orgId = strval($orgId)[0] !== 't' ? $orgId : substr($orgId, 1);

        $inactiveUsers = $this->getInactiveUsersByOrgId($authToken, $orgId);
        $teamMemberIds = collect(array_get($inactiveUsers, 'data', []))
            ->pluck('kw_uid')
            ->concat($activeMemberIds)
            ->toArray();

        return $teamMemberIds;
    }

    public function getUserOrgs($token, $kwuid): ?array
    {
        $endpoint = rtrim(config('app.kwc_url'), '/');
        $endpoint .= sprintf('/pno/api/v2/people/%s/orgs', $kwuid);

        try {
            $response = $this->client->get($endpoint, [
                'headers' => [
                    'Authorization' => sprintf('Bearer %s', $token)
                ]
            ]);

            return json_decode((string) $response->getBody(), true);
        } catch (RequestException $exception) {
            throw new PNOServiceException($exception->getMessage());
        }
    }
}
