<?php

namespace App\Services\Template\Presenters;

use App\Services\Items\Presenters\ItemTransformer;
use App\Services\Template\TemplateEloquent;
use Libs\Presenters\TransformerAbstract;

class TemplateTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @return array
     */
    public function transform(TemplateEloquent $model)
    {
        return $this->applySparseFieldsets([
            'id'   => $model->getAttribute('id'),
            'name' => $model->getAttribute('name'),
        ] + $model->getAttribute('data'));
    }

    public function includeItems(TemplateEloquent $model)
    {
        return $this->collection($model->items, new ItemTransformer(), 'items');
    }
}
