<?php

namespace App\Services\Template\Repositories;

use App\Services\Checklists\ChecklistEloquent;
use App\Services\Checklists\Repositories\ChecklistRepository;
use App\Services\Items\ItemEloquent;
use App\Services\Items\Repositories\ItemRepository;
use App\Services\Roles\RoleService;
use App\Services\Template\TemplateEloquent;
use Carbon\Carbon;
use CorePackage\Infrastructures\Adapters\SearchAdapter;

class TemplateRepository
{
    public function __construct(
        ChecklistRepository $checklistRepo,
        ItemRepository $itemRepo,
        RoleService $roleService
    ) {
        $this->checklistRepo = $checklistRepo;
        $this->itemRepo      = $itemRepo;
        $this->roleService   = $roleService;
    }

    /**
     * browse data.
     *
     * @return LengthAwareOffsetPaginator
     */
    public function browse(SearchAdapter $searchAdapter)
    {
        $query = new TemplateEloquent();
        if ($searchAdapter) {
            $criteria = new TemplateSearchCriteria();
            $query    = $criteria->apply($query, $searchAdapter);
            $pager    = $searchAdapter->getPager();
            if (null != $pager) {
                return $query->browse(
                    $pager->getLimit(),
                    $pager->getOffset(),
                    ['*'],
                    $pager->getQueries()
                );
            }
        }

        return $query->browse();
    }

    public function getRoles($kwuid)
    {
        $data = $this->roleService->getPersonRoles($kwuid);

        return $data;
    }

    /**
     * browse data.
     *
     * @return Model
     */
    public function find($id)
    {
        return TemplateEloquent::findOrFail($id);
    }

    /**
     * create data.
     *
     * @return bool
     */
    public function create(TemplateEloquent $template)
    {
        $data = $this->setupPayload($template);

        return TemplateEloquent::create($data);
    }

    public function setupPayload(TemplateEloquent $template)
    {
        return [
            'user_id' => $template->getAttribute('user_id'),
            'name'    => $template->getAttribute('name'),
            'data'    => $template->getAttribute('data'),
            'org_id'  => $template->getAttribute('org_id'),
        ];
    }

    /**
     * update data.
     *
     * @return Model
     */
    public function update(TemplateEloquent $template)
    {
        $data = $this->setupPayload($template);
        $template->fill($data);
        $template->save();

        return $template->fresh();
    }

    public function index(TemplateEloquent $template)
    {
        return $template->all();
    }

    /**
     * delete data.
     *
     * @return int
     */
    public function delete(TemplateEloquent $template)
    {
        return $template->delete();
    }

    public function assign(TemplateEloquent $template, $objectDomain, $objectId)
    {
        $data        = $template->getAttribute('data');
        $userId      = $template->getAttribute('user_id');
        $orgId       = $template->getAttribute('org_id');
        $description = array_get($data, 'checklist.description');
        $urgency     = array_get($data, 'checklist.urgency', 0);
        $dueInterval = array_get($data, 'checklist.due_interval', null);
        $dueUnit     = array_get($data, 'checklist.due_unit', null);
        $due         = $this->buildDueDate($dueInterval, $dueUnit);
        $checklist   = new ChecklistEloquent();
        $checklist
            ->setAttribute('object_domain', $objectDomain)
            ->setAttribute('object_id', $objectId)
            ->setAttribute('description', $description)
            ->setAttribute('due', $due)
            ->setAttribute('urgency', $urgency)
            ->setAttribute('is_completed', false)
            ->setAttribute('completed_at', null)
            ->setAttribute('user_id', $userId)
            ->setAttribute('org_id', $orgId);

        $checklist = $this->checklistRepo->create($checklist);

        foreach ($data['items'] ?? [] as $attributes) {
            $item        = new ItemEloquent();
            $description = array_get($attributes, 'description');
            $meta        = array_get($attributes, 'meta');
            $urgency     = array_get($attributes, 'urgency', 0);
            $dueInterval = array_get($attributes, 'due_interval', null);
            $dueUnit     = array_get($attributes, 'due_unit', null);
            $assignee_id = array_get($attributes, 'assignee_id', null);
            $notifyClient = array_get($attributes, 'notify_client', false);
            $clientUpdateSentAt = array_get($attributes, 'client_update_sent_at', null);
            $taskId      = $template->getAttribute('task_id');
            $due         = $this->buildDueDate($dueInterval, $dueUnit);
            $item
                ->setAttribute('description', $description)
                ->setAttribute('meta', $meta)
                ->setAttribute('due', $due)
                ->setAttribute('urgency', $urgency)
                ->setAttribute('is_completed', false)
                ->setAttribute('completed_at', null)
                ->setAttribute('checklist_id', $checklist->getKey())
                ->setAttribute('user_id', $userId)
                ->setAttribute('org_id', $orgId)
                ->setAttribute('assignee_id', $assignee_id)
                ->setAttribute('task_id', $taskId)
                ->setAttribute('notify_client', $notifyClient)
                ->setAttribute('client_update_sent_at', $clientUpdateSentAt);
            $this->itemRepo->create($item);
        }

        return $checklist;
    }

    protected function buildDueDate($dueInterval, $dueUnit)
    {
        if (empty($dueInterval) || empty($dueUnit)) {
            return null;
        }

        $dueMethod = 'add' . title_case($dueUnit) . 's';

        return Carbon::now()->$dueMethod($dueInterval);
    }
}
