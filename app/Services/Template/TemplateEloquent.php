<?php

namespace App\Services\Template;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Libs\Presenters\Paginatable;

class TemplateEloquent extends Model
{
    use Paginatable, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'templates';

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'data', 'org_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'json',
    ];

    /**
     * get key value.
     *
     * @var string
     */
    public function getKey()
    {
        return $this->id;
    }
}
