<?php

namespace App\Services\Emails;

interface MailServiceContract
{
    /**
     * @param $to
     * @param $subject
     * @param null $templateId
     * @param array $variables
     * @return mixed
     */
    public function send($to, $subject, $templateId = null, $variables = []);
}
