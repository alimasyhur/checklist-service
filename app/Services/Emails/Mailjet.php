<?php

namespace App\Services\Emails;

use Mailjet\Client;
use Mailjet\Resources;

class Mailjet implements MailServiceContract
{
    public function send($to, $subject, $templateId = null, $variables = [])
    {
        $mj = new Client(
            config('mail.public_key'),
            config('mail.private_key'),
            true,
            ['version' => 'v3.1']
        );

        // Workaround for issue curl timeout even in normal circumtances.
        // We don't guarantee that if we increase the timeout, it won't be timeout again.
        // We don't have a way to do retry after timeout too.
        // References:
        // - https://github.com/mailjet/mailjet-apiv3-php/issues/157
        // - https://github.com/mailjet/mailjet-apiv3-php/issues/91
        $mj->setTimeout(10);
        $mj->setConnectionTimeout(10);

        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => config('mail.sender'),
                        'Name' => config('mail.name'),
                    ],
                    'Subject' => $subject,
                ]
            ]
        ];

        $body['Messages'][0]['To'] = [];
        foreach ($to as $email => $name) {
            array_push($body['Messages'][0]['To'], [
                'Email' => $email,
                'Name' => $name,
            ]);
        }

        if ($templateId) {
            $body['Messages'][0]['TemplateID'] = (int) $templateId;
            $body['Messages'][0]['TemplateLanguage'] = true;
        }

        if ($variables) {
            $body['Messages'][0]['Variables'] = $variables;
        }

        return $mj->post(Resources::$Email, ['body' => $body]);
    }
}
