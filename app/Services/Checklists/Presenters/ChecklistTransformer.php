<?php

namespace App\Services\Checklists\Presenters;

use App\Services\Checklists\ChecklistEloquent;
use App\Services\Items\Presenters\ItemTransformer;
use Libs\Presenters\TransformerAbstract;

class ChecklistTransformer extends TransformerAbstract
{
    /**
     * Resources that can be included if requested.
     *
     * @var array
     */
    public $availableIncludes = [
        'items',
    ];

    /**
     * Turn this item object into a generic array.
     *
     * @return array
     */
    public function transform(ChecklistEloquent $model)
    {
        return $this->applySparseFieldsets([
                'id'            => $model->getAttribute('id'),
                'object_domain' => $model->getAttribute('object_domain'),
                'object_id'     => (string) $model->getAttribute('object_id'),
                'description'   => $model->getAttribute('description'),
                'is_completed'  => (bool) $model->getAttribute('is_completed'),
                'due'           => $model->getAttribute('due'),
                'urgency'       => $model->getAttribute('urgency'),
                'completed_at'  => $model->getAttribute('completed_at'),
                'updated_by'    => $model->getAttribute('updated_by'),
                'created_by'    => $model->getAttribute('user_id'),
                'created_at'    => $model->getAttribute('created_at'),
                'updated_at'    => $model->getAttribute('updated_at'),
        ]);
    }

    public function includeItems(ChecklistEloquent $model)
    {
        return $this->collection($model->items, new ItemTransformer(), 'items');
    }
}
