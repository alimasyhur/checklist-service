<?php

namespace App\Services\Checklists\Repositories;

use App\Services\Checklists\ChecklistEloquent;
use CorePackage\Infrastructures\Adapters\SearchAdapter;
use Carbon\Carbon;

class ChecklistRepository
{
    const TIME_STR_LENGTH = 19;

    public function __construct(Carbon $carbon)
    {
        $this->carbon = $carbon;
    }

    /**
     * browse data.
     *
     * @return LengthAwareOffsetPaginator
     */
    public function browse(SearchAdapter $searchAdapter)
    {
        $query = new ChecklistEloquent();
        if ($searchAdapter) {
            $criteria = new ChecklistSearchCriteria();
            $query    = $criteria->apply($query, $searchAdapter);
            $pager    = $searchAdapter->getPager();
            if (null != $pager) {
                return $query->browse(
                    $pager->getLimit(),
                    $pager->getOffset(),
                    ['*'],
                    $pager->getQueries()
                );
            }
        }

        return $query->browse();
    }

    /**
     * browse data.
     *
     * @return Model
     */
    public function find($id)
    {
        return ChecklistEloquent::findOrFail($id);
    }

    /**
     * browse data.
     *
     * @return Model
     */
    public function findById($id)
    {
        return ChecklistEloquent::find($id);
    }

    /**
     * create data.
     *
     * @return bool
     */
    public function create(ChecklistEloquent $checklist)
    {
        $data = $this->setupPayload($checklist);

        return ChecklistEloquent::create($data);
    }

    public function setupPayload(ChecklistEloquent $checklist)
    {
        return [
            'object_domain' => $checklist->getAttribute('object_domain'),
            'object_id'     => $checklist->getAttribute('object_id'),
            'description'   => $checklist->getAttribute('description'),
            'due'           => $checklist->getAttribute('due'),
            'urgency'       => $checklist->getAttribute('urgency'),
            'is_completed'  => $checklist->getAttribute('is_completed'),
            'completed_at'  => $checklist->getAttribute('completed_at'),
            'updated_by'    => $checklist->getAttribute('updated_by'),
            'user_id'       => $checklist->getAttribute('user_id'),
            'org_id'       => $checklist->getAttribute('org_id'),
        ];
    }

    /**
     * update data.
     *
     * @return Model
     */
    public function update(ChecklistEloquent $checklist)
    {
        $data = $this->setupPayload($checklist);
        $checklist->fill($data);
        $checklist->save();

        return $checklist->fresh();
    }

    public function index(ChecklistEloquent $checklist)
    {
        return $checklist->all();
    }

    /**
     * delete data.
     *
     * @return int
     */
    public function delete(ChecklistEloquent $checklist)
    {
        return $checklist->delete();
    }

    public function getIncompleteItemsCount(ChecklistEloquent $checklist)
    {
        return $checklist->items->where('is_completed', 0)->count();
    }

    public function completeChecklist(ChecklistEloquent $checklist)
    {
        $checklist->is_completed = 1;
        $checklist->completed_at = $this->carbon->now();
        $checklist->save();

        return $checklist;
    }

    public function getDueDateFormat($newDueDate = null)
    {
        if ($this->isIso8601Format($newDueDate)) {
            $newDueDate = $this->parseIso8601($newDueDate);
        }

        return $newDueDate;
    }

    private function isIso8601Format($date)
    {
        return strlen($date) > ChecklistRepository::TIME_STR_LENGTH;
    }

    private function parseIso8601($date)
    {
        $date = substr($date, 0, ChecklistRepository::TIME_STR_LENGTH);

        return str_replace('T', ' ', $date);
    }
}
