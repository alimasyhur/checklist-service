<?php

namespace App\Services\Checklists;

use App\Services\History\HistoryEloquent;
use App\Services\Items\ItemEloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Libs\Presenters\Paginatable;

class ChecklistEloquent extends Model
{
    use Paginatable, SoftDeletes;

    const MORPH_NAME = 'checklists';

    protected $dates = ['deleted_at', 'due'];

    protected $table = 'checklists';

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'object_domain', 'object_id', 'description', 'is_completed',
        'due', 'urgency', 'completed_at', 'updated_by', 'user_id',
        'org_id',
    ];

    /**
     * get key value.
     *
     * @var string
     */
    public function getKey()
    {
        return $this->id;
    }

    public function items()
    {
        return $this->hasMany(ItemEloquent::class, 'checklist_id');
    }

    /**
     * get primary key name.
     *
     * @var string
     */
    public function getKeyName()
    {
        return $this->primaryKey;
    }

    public function histories()
    {
        return $this->morphMany(HistoryEloquent::class, 'loggable');
    }
}
