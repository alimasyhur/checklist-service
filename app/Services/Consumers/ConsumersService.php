<?php

namespace App\Services\Consumers;

use Carbon\Carbon;
use GuzzleHttp\Client;
use KWRI\KongHandler\Exceptions\ConsumerNotFoundException;
use Illuminate\Support\Facades\Log;

class ConsumersService
{
    private $client;

    public function __construct()
    {
        $this->client     = new Client([]);
    }

    public function createConsumerToken($user_id, Carbon $expired_at)
    {
        $headers = [
            'apikey'       => getenv('CONSUMERS_API_KEY'),
            'Content-Type' => 'application/json;charset=utf-8',
        ];

        $now = new Carbon();
        $expiredAt = $expired_at->lte($now) ? $now : $expired_at->copy();
        $expiredAt->addDay();

        $url  = getenv('CONSUMERS_ENDPOINT') . '/checklist/service-token';
        $data = [
            'expired_at' => $expiredAt->toIso8601String(),
            'audience'   => getenv('CONSUMERS_AUDIENCE'),
            'user_id'    => (string) $user_id,
        ];

        $response = $this->client->request('POST', $url, [
            'headers' => $headers,
            'json'    => $data,
        ]);

        $body = $response->getBody();
        $decodedBody = json_decode($body, true);

        if (!isset($decodedBody['data'])) {
            Log::info($decodedBody);
            throw new ConsumerNotFoundException("Consumer not found with ID {$user_id}");
        }

        return $decodedBody['data'];
    }
}
