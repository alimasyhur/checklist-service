<?php

namespace App\Services\Consumers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConsumersToken extends Model
{
    use SoftDeletes;

    protected $table = 'consumers_token';

    protected $fillable = [
        'user_id', 'token', 'expired_at',
    ];

    protected $visibel = [
        'id', 'user_id', 'token', 'expired_at',
        'created_at', 'updated_at', 'deleted_at',
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'expired_at'];

    public function getKey()
    {
        return $this->id;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setExpiredAt($expired_at)
    {
        $this->expired_at = $expired_at;

        return $this;
    }

    public function getExpiredAt()
    {
        return $this->expired_at;
    }

    public function getDataByUserId($user_id)
    {
        return $this->where('user_id', $user_id)->first();
    }
}
