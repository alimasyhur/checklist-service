<?php

namespace App\Services\Roles;

use GuzzleHttp\Client;
use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Http\Request;

class RoleService
{
    public const ORG_TYPE_MARKET_CENTER = 3;

    public const ROLE_MCA = 3;
    public const ROLE_ROP = 8;
    public const ROLE_REG_DIRECTOR = 9;
    public const ROLE_REG_MCA = 10;
    public const ROLE_PRINCIPAL = 11;
    public const ROLE_TEAM_LEADER = 12;
    public const ROLE_ASSISTANT_MCA = 27;
    public const ROLE_REGIONAL_ASSISTANT = 28;
    public const ROLE_TL_ASSISTANT = 33;
    public const ROLE_REG_OPERATIONS_MGR = 35;
    public const ROLE_REG_OPERATIONS_MGR_ASSISTANT = 43;
    public const ROLE_REG_DIRECTOR_ASSISTANT = 44;
    public const ROLE_REG_MCA_ASSISTANT = 45;
    public const ROLE_GENERAL_MANAGER = 46;
    public const ROLE_AREA_DIRECTOR = 48;
    public const ROLE_PRODUCTIVITY_COACH = 50;
    public const ROLE_MC_ANGEL = 67;
    public const ROLE_COMPLIANCE_COORDINATOR = 184;

    protected $client;
    protected $cache;
    protected $request;

    public function __construct(Client $client, Cache $cache, Request $request)
    {
        $this->client = $client;
        $this->cache = $cache;
        $this->request = $request;
    }

    public function getPersonRoles($kwuid)
    {
        return $this->cache->tags(['team', "kwuid-roles-$kwuid"])
            ->remember("team-kwuid-roles-{$kwuid}", 24 * 60, function () use ($kwuid) {
                $request = $this->client->request('GET', "person/{$kwuid}?include=orgs");
                $response = $request->getBody()->getContents();
                $response = json_decode($response, true);

                return $response;
            });
    }

    public function hasDestructiveAction($user)
    {
        $kwuid = $user->getKey();
        $kwoid = $this->getRealOrgId($user->getKwoid());
        $response = $this->getPersonRoles($kwuid);
        $roles = array_get($response, 'data.orgs');

        $hasAccess = config('app.destructive_action_role');
        $role = collect($roles)
                    ->where('org_id', $kwoid)
                    ->where('org_type_id', self::ORG_TYPE_MARKET_CENTER)
                    ->whereIn('role_id', $hasAccess)
                    ->pluck('role_id')
                    ->first();

        return in_array($role, $hasAccess);
    }

    private function getRealOrgId($orgId)
    {
        if (empty($this->request->user()->isFaked)) {
            return $orgId;
        }

        $realOrgId = array_search($orgId, config('app.fake_org_ids'));

        if ($realOrgId) {
            return $realOrgId;
        }

        return $orgId;
    }
}
