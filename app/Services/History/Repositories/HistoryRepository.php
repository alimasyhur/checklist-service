<?php

namespace App\Services\History\Repositories;

use App\Services\Checklists\ChecklistEloquent;
use App\Services\History\HistoryEloquent;
use App\Services\Items\ItemEloquent;
use Carbon\Carbon;
use CorePackage\Infrastructures\Adapters\SearchAdapter;

class HistoryRepository
{
    public function __construct(Carbon $carbon)
    {
        $this->carbon = $carbon;
    }

    /**
     * browse data.
     *
     * @return LengthAwareOffsetPaginator
     */
    public function browse(SearchAdapter $searchAdapter)
    {
        $query = new HistoryEloquent();
        if ($searchAdapter) {
            $criteria = new HistorySearchCriteria();
            $query = $criteria->apply($query, $searchAdapter);
            $pager = $searchAdapter->getPager();
            if (null != $pager) {
                return $query->browse(
                    $pager->getLimit(),
                    $pager->getOffset(),
                    ['*'],
                    $pager->getQueries()
                );
            }
        }

        return $query->browse();
    }

    /**
     * browse data.
     *
     * @return Model
     */
    public function find($id)
    {
        return HistoryEloquent::findOrFail($id);
    }

    /**
     * create data.
     *
     * @return bool
     */
    public function create(HistoryEloquent $history)
    {
        $data = $this->setupPayload($history);

        return HistoryEloquent::create($data);
    }

    public function setupPayload(HistoryEloquent $history)
    {
        return [
            'loggable_type' => $history->getAttribute('loggable_type'),
            'loggable_id' => $history->getAttribute('loggable_id'),
            'action' => $history->getAttribute('action'),
            'kwuid' => $history->getAttribute('kwuid'),
            'value' => $history->getAttribute('value'),
        ];
    }

    public function snoozeItem($kwuid, ItemEloquent $item)
    {
        $due = $item->getAttribute('due');

        if (null != $due) {
            $due = $due->setTimezone('UTC')->toIso8601String();
        }

        $history = new HistoryEloquent([
            'loggable_type' => ItemEloquent::MORPH_NAME,
            'loggable_id' => $item->getKey(),
            'action' => HistoryEloquent::ACTION_SNOOZE,
            'kwuid' => $kwuid,
            'value' => $due,
        ]);

        return $this->create($history);
    }

    public function assignItem($kwuid, ItemEloquent $item)
    {
        $history = new HistoryEloquent([
            'loggable_type' => ItemEloquent::MORPH_NAME,
            'loggable_id' => $item->getKey(),
            'loggable_id' => $item->getKey(),
            'action' => HistoryEloquent::ACTION_ASSIGN,
            'kwuid' => $kwuid,
            'value' => $item->getAttribute('assignee_id'),
        ]);

        return $this->create($history);
    }

    public function archiveItem($kwuid, ItemEloquent $item)
    {
        $history = new HistoryEloquent([
            'loggable_type' => ItemEloquent::MORPH_NAME,
            'loggable_id' => $item->getKey(),
            'action' => HistoryEloquent::ACTION_ARCHIVE,
            'kwuid' => $kwuid,
            'value' => 1,
        ]);

        return $this->create($history);
    }

    public function completeItems($kwuid, ItemEloquent ...$items)
    {
        $items = collect($items);
        $now = Carbon::now()->setTimezone('UTC');
        $histories = $items->map(function ($item) use ($now, $kwuid) {
            return [
                'loggable_type' => ItemEloquent::MORPH_NAME,
                'loggable_id' => $item->getKey(),
                'loggable_id' => $item->getKey(),
                'action' => HistoryEloquent::ACTION_COMPLETE,
                'kwuid' => $kwuid,
                'value' => 1,
                'created_at' => $now->toIso8601String(),
                'updated_at' => $now->toIso8601String(),
            ];
        })->toArray();

        HistoryEloquent::insert($histories);
    }

    public function incompleteItems($kwuid, ItemEloquent ...$items)
    {
        $items = collect($items);
        $now = Carbon::now()->setTimezone('UTC');
        $histories = $items->map(function ($item) use ($now, $kwuid) {
            return [
                'loggable_type' => ItemEloquent::MORPH_NAME,
                'loggable_id' => $item->getKey(),
                'action' => HistoryEloquent::ACTION_COMPLETE,
                'kwuid' => $kwuid,
                'value' => 0,
                'created_at' => $now->toIso8601String(),
                'updated_at' => $now->toIso8601String(),
            ];
        })->toArray();

        HistoryEloquent::insert($histories);
    }

    public function completeChecklist($kwuid, ChecklistEloquent $checklist)
    {
        $history = new HistoryEloquent([
            'loggable_type' => ChecklistEloquent::MORPH_NAME,
            'loggable_id' => $checklist->getKey(),
            'action' => HistoryEloquent::ACTION_COMPLETE,
            'kwuid' => $kwuid,
            'value' => $checklist->getAttribute('is_completed'),
        ]);

        return $this->create($history);
    }

    public function snoozeChecklist($kwuid, ChecklistEloquent $checklist)
    {
        $history = new HistoryEloquent([
            'loggable_type' => ChecklistEloquent::MORPH_NAME,
            'loggable_id' => $checklist->getKey(),
            'action' => HistoryEloquent::ACTION_SNOOZE,
            'kwuid' => $kwuid,
            'value' => $checklist->getAttribute('due')->setTimezone('UTC')->toIso8601String(),
        ]);

        return $this->create($history);
    }

    public function archiveChecklist($kwuid, ChecklistEloquent $checklist)
    {
        $history = new HistoryEloquent([
            'loggable_type' => ChecklistEloquent::MORPH_NAME,
            'loggable_id' => $checklist->getKey(),
            'action' => HistoryEloquent::ACTION_ARCHIVE,
            'kwuid' => $kwuid,
            'value' => 1
        ]);

        return $this->create($history);
    }
}
