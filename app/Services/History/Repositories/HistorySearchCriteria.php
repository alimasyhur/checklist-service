<?php

namespace App\Services\History\Repositories;

use CorePackage\Infrastructures\Adapters\SearchAdapter;

use App\Services\AbstractSearchCriteria;

class HistorySearchCriteria extends AbstractSearchCriteria
{
    public function apply($query, SearchAdapter $searchAdapter)
    {
        foreach ($searchAdapter->getFilters() as $filter) {
            $query = $this->applyCondition($query, $filter);
        }

        $sorts = $searchAdapter->getSorter() ? $searchAdapter->getSorter()->getSorts() : [];
        foreach ($sorts as $attributeName => $direction) {
            $query = $query->orderBy($attributeName, $direction);
        }

        return $query;
    }
}
