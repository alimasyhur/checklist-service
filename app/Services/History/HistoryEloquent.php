<?php

namespace App\Services\History;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Libs\Presenters\Paginatable;

class HistoryEloquent extends Model
{
    use Paginatable, SoftDeletes;

    const ACTION_ASSIGN = 'assign';
    const ACTION_SNOOZE = 'snooze';
    const ACTION_COMPLETE = 'complete';
    const ACTION_ARCHIVE = 'archive';

    protected $dates = ['updated_at', 'completed_at'];

    protected $table = 'histories';

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['loggable_type', 'loggable_id', 'action', 'kwuid', 'value'];

    public function loggable()
    {
        return $this->morphTo();
    }
}
