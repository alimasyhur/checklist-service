<?php

namespace App\Services\History\Presenters;

use App\Services\History\HistoryEloquent;
use Libs\Presenters\TransformerAbstract;

class HistoryTransformer extends TransformerAbstract
{
    /**
     * Resources that can be included if requested.
     *
     * @var array
     */
    public $availableIncludes = [
        'histories',
    ];

    /**
     * Turn this item object into a generic array.
     *
     * @return array
     */
    public function transform(HistoryEloquent $model)
    {
        return $this->applySparseFieldsets([
            'id' => $model->getAttribute('id'),
            'loggable_type' => $model->getAttribute('loggable_type'),
            'loggable_id' => $model->getAttribute('loggable_id'),
            'action' => $model->getAttribute('action'),
            'kwuid' => $model->getAttribute('kwuid'),
            'value' => $model->getAttribute('value'),
            'created_at' => $model->getAttribute('created_at'),
            'updated_at' => $model->getAttribute('updated_at'),
        ]);
    }
}
