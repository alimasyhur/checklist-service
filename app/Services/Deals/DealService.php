<?php

namespace App\Services\Deals;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;

class DealService
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getDeal($authToken, $dealId)
    {
        $baseUri = env('KWC_URL', 'https://dev-kong.command-api.kw.com');
        $endpoint = "$baseUri/deals-manager/api/v1/deals/nokwuid/$dealId";

        try {
            $request = $this->client->request('GET', $endpoint, [
                'headers' => [
                    'Authorization' => "Bearer $authToken",
                ]
            ]);

            $response = $request->getBody()->getContents();
            $response = json_decode($response, true);

            return $response;
        } catch (RequestException $e) {
            Log::info($e);

            return null;
        }
    }
}
