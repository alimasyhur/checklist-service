<?php

namespace App\Services;

use CorePackage\Infrastructures\Adapters\Filter;
use CorePackage\Infrastructures\Adapters\SearchAdapter;
use Illuminate\Database\Eloquent\Model;

abstract class AbstractSearchCriteria
{
    public function apply($query, SearchAdapter $searchAdapter)
    {
        $filters = $this->prepareFilters($query, $searchAdapter);
        $query = $this->applyFilters($query, $filters);

        $joins = $this->prepareJoins($query, $searchAdapter);
        $query = $this->applyJoins($query, $joins);

        $sorts = $this->prepareSorts($query, $searchAdapter);
        $query = $this->applySorts($query, $sorts);

        $query = $this->applySelect($query, $searchAdapter);
        $query = $this->applyGroupBy($query, $searchAdapter);

        $resources = $this->prepareEagerLoadResources($query, $searchAdapter);
        $query = $this->applyEagerLoad($query, $resources);

        return $query;
    }

    protected function prepareFilters($query, $searchAdapter)
    {
        $relationshipFilters = $searchAdapter->getRelationshipFilters();
        $filters = $searchAdapter->getNormalFilters();

        if ($relationshipFilters->isEmpty()) {
            return $filters;
        }

        // Sometimes resource name !== table name.
        // We parse the correct table name through relationship definition
        // instead guessing it.
        $relationshipFilters = $this->fixRelationshipFiltersTableName($relationshipFilters, $query);
        // On filtering with relationship filters, we will apply a join.
        // There'll be an error like this if field with same name found on multiple joined tables.
        // > SQLSTATE[23000]: Integrity constraint violation: 1052 Column 'xxx' in where clause is ambiguous
        // Appending table name to the field will fix that.
        $filters = $this->appendTableNameToNormalFilters($filters, $query);
        $filters = $filters->merge($relationshipFilters);

        return $filters;
    }

    protected function applyCondition($query, $filter)
    {
        $columnName = $filter->getColumnName();
        $operator = $filter->getOperator();
        $value    = $filter->getValue();

        if ('like' == $operator or 'not like' == $operator) {
            return $query->where($columnName, $operator, '%' . $value . '%');
        }

        if ('null' == $value && 'is' == $operator) {
            return $query->whereNull($columnName);
        }

        if ('null' == $value && 'is not' == $operator) {
            return $query->whereNotNull($columnName);
        }

        if ('in' == $operator) {
            return $query->whereIn($columnName, explode(',', $value));
        }

        if ('not in' == $operator) {
            return $query->whereNotIn($columnName, explode(',', $value));
        }

        if ('between' == $operator) {
            return $query->whereBetween($columnName, explode(',', $value));
        }

        if ('not between' == $operator) {
            return $query->whereNotBetween($columnName, explode(',', $value));
        }

        return $query->where($columnName, $operator, $value);
    }

    /**
     * Get table name.
     *
     * @return string
     */
    protected function getTableName($query)
    {
        return $this->getModelInstance($query)->getTable();
    }

    /**
     * Get model instance.
     *
     * @return Model
     */
    protected function getModelInstance($model)
    {
        return ($model instanceof Model)
            ? $model
            : $model->getModel();
    }

    protected function fixRelationshipFiltersTableName($relationshipFilters, $query)
    {
        $model = $this->getModelInstance($query);

        return $relationshipFilters
            ->filter(function ($filter) use ($query) {
                return $this->isResourceValid($query, $filter->getResource());
            })->map(function ($filter) use ($model) {
                $relationMethod = $filter->getResource();
                $correctTableName = $model->$relationMethod()->getRelated()->getTable();

                // The underlying $filter still reference to same object.
                // We need to clone first to prevent side effect of changing
                // the filters on $searchAdapter object.
                $filter = clone $filter;

                return $filter->setResource($correctTableName);
            });
    }

    protected function appendTableNameToNormalFilters($filters, $query)
    {
        $model = $this->getModelInstance($query);

        return $filters->map(function ($filter) use ($model) {
            $tableName = $model->getTable();

            // The underlying $filter still reference to same object.
            // We need to clone first to prevent side effect of changing
            // the filters on $searchAdapter object.
            $filter = clone $filter;

            return $filter->setResource($tableName);
        });
    }

    protected function prepareEagerLoadResources($query, $searchAdapter)
    {
        $model = $this->getModelInstance($query);
        // guess what to eager load from ?include
        return $searchAdapter
            ->getResources()
            ->filter(function ($resource) use ($query) {
                return $this->isResourceValid($query, $resource->getName());
            })
            ->map(function ($resource) {
                return $resource->getName();
            })
            ->values()
            ->toArray();
    }

    protected function applyEagerLoad($query, $resources)
    {
        if (count($resources)) {
            return $query->with($resources);
        }

        return $query;
    }

    protected function applyFilters($query, $filters)
    {
        $filters->each(function ($filter) use (&$query) {
            $query = $this->applyCondition($query, $filter);
        });

        return $query;
    }

    protected function prepareSorts($query, $searchAdapter)
    {
        $model = $this->getModelInstance($query);
        $sorts = $searchAdapter->getSorter() ? $searchAdapter->getSorter()->getSorts() : [];

        return collect($sorts)
            ->mapWithKeys(function ($direction, $attributeName) use ($searchAdapter, $model) {
                if ($searchAdapter->getRelationshipFilters()->isEmpty()) {
                    return [$attributeName => $direction];
                }

                $attributeName = "{$model->getTable()}.$attributeName";
                return [$attributeName => $direction];
            });
    }

    protected function applySorts($query, $sorts)
    {
        $sorts->each(function ($direction, $attributeName) use ($query) {
            $query->orderBy($attributeName, $direction);
        });

        return $query;
    }

    protected function prepareJoins($query, $searchAdapter)
    {
        if ($searchAdapter->getRelationshipFilters()->isEmpty()) {
            return collect([]);
        }

        $model = $this->getModelInstance($query);
        return $searchAdapter
            ->getRelationshipFilters()
            ->filter(function ($filter) use ($query) {
                return $this->isResourceValid($query, $filter->getResource());
            })->map(function ($filter) {
                return $filter->getResource();
            })
            ->unique()
            ->map(function ($relationMethod) use ($query) {
                $model = $this->getModelInstance($query);
                $relationshipType = (new \ReflectionClass($model->$relationMethod()))->getShortName();
                $method = "prepare{$relationshipType}Join";

                if (method_exists($this, $method)) {
                    return $this->$method($query, $relationMethod);
                }

                $className = get_class($model);
                $message = "Filtering to '$relationshipType' relationship is not"
                    . " implemented yet. Method called '$className::{$relationMethod}()'.";
                throw new \RuntimeException($message);
            });
    }

    protected function prepareBelongsToJoin($query, $relationMethod)
    {
        $model = $this->getModelInstance($query);
        $related = $model->$relationMethod()->getRelated()->getTable();
        $ownerKey = $model->$relationMethod()->getOwnerKey();
        $parent = $model->getTable();
        $foreignKey = $model->$relationMethod()->getForeignKey();

        return [
            'related' => $related,
            'related_key' => "$related.$ownerKey",
            'parent_key' => "$parent.$foreignKey",
            'type' => 'left',
        ];
    }

    protected function prepareHasManyJoin($query, $relationMethod)
    {
        $model = $this->getModelInstance($query);
        $related = $model->$relationMethod()->getRelated()->getTable();
        $relatedOwnerKey = $model->$relationMethod()->getQualifiedForeignKeyName();
        $parentKey = $model->$relationMethod()->getQualifiedParentKeyName();

        return [
            'related' => $related,
            'related_key' => $relatedOwnerKey,
            'parent_key' => $parentKey,
            'type' => 'right',
        ];
    }

    protected function applyJoins($query, $joins)
    {
        $joins->each(function ($join) use ($query) {
            // See Illuminate\Database\Query\Builder::join()
            $query->join(
                $join['related'],
                $join['related_key'],
                "=",
                $join['parent_key'],
                $join['type']
            );
        });

        return $query;
    }

    protected function applySelect($query, $searchAdapter)
    {
        if ($searchAdapter->getRelationshipFilters()->isEmpty()) {
            return $query;
        }

        // Explicitly select "table.id" needed since there'll be multiple
        // `id` column selected after join on relationship filters.
        // The `id` will be used by transformer to set `id` attribute.
        $model = $this->getModelInstance($query);
        $tableName = $model->getTable();
        return $query->addSelect("$tableName.*");
    }

    protected function applyGroupBy($query, $searchAdapter)
    {
        if ($searchAdapter->getRelationshipFilters()->isEmpty()) {
            return $query;
        }

        // group by needed to show correct meta.count on transformer
        // when applying "right" join when using relationship filters.
        $model = $this->getModelInstance($query);
        $tableName = $model->getTable();

        return $query->groupBy("$tableName.id");
    }

    protected function isResourceValid($query, $resourceName)
    {
        $model = $this->getModelInstance($query);
        // For now, valid resource only when
        // resource name == relationship method name on model.
        return method_exists($model, $resourceName);
    }
}
