<?php

namespace App\Services\Summaries\Repositories;

use App\Services\Items\ItemEloquent;
use CorePackage\Infrastructures\Adapters\SearchAdapter;
use App\Services\Items\Repositories\ItemSearchCriteria;
use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Support\Facades\Auth;

class SummaryRepository
{
    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    public function count(SearchAdapter $searchAdapter)
    {
        // Currently, we only summaries by assignee ID from the current authenticated user.
        // So we can safely assumed to use Auth facade here for the current assignee ID.
        $assigneeId = Auth::user()->getKey();

        $filters = [];
        foreach ($searchAdapter->getFilters() as $filter) {
            $filters[] = [$filter->getColumnName(), $filter->getValue()];
        }
        $encoded = json_encode($filters);

        return $this->cache->tags(["summary", "item-summary-assignee-id-$assigneeId"])->remember(
            "summary-$encoded",
            60 * 24,
            function () use ($searchAdapter) {
                $query = new ItemEloquent();
                $criteria = new ItemSearchCriteria();
                $query = $criteria->apply($query, $searchAdapter);

                return $query->count();
            }
        );
    }

    public function invalidateSummaryByAssigneeId($assigneeId)
    {
        $this->cache->tags("item-summary-assignee-id-$assigneeId")->flush();
    }
}
