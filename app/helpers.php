<?php

use Illuminate\Support\Facades\App;

if (!function_exists('getFakeOrgId')) {
    function getFakeOrgId($orgId)
    {
        if (!App::environment(['local', 'testing', 'dev', 'qa'])) {
            return $orgId;
        }
        
        $fakeOrgIds = config('app.fake_org_ids');

        return array_get($fakeOrgIds, $orgId, $orgId);
    }
}
