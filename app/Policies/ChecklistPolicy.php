<?php

namespace App\Policies;

use App\Services\Checklists\ChecklistEloquent;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Services\PNO\PNOService;
use App\Services\Roles\RoleService;

class ChecklistPolicy
{
    use HandlesAuthorization;

    /**
     * @var PNOService
     */
    private $pno;

    /**
     * @var RoleService
     */
    private $roleService;

    /**
     * Create a new policy instance.
     */
    public function __construct(PNOService $pno, RoleService $roleService)
    {
        $this->pno = $pno;
        $this->roleService = $roleService;
    }

    /**
     * Determine if the given Checklist can be accessed by the user.
     *
     * @param User              $user
     * @param ChecklistEloquent $checklist
     *
     * @return bool
     */
    public function access($user, ChecklistEloquent $checklist)
    {
        $owner = $checklist->getAttribute('user_id');

        if ($user->isService() && $user->getConsumer() == 'teamGateway') {
            $activeMemberIds = $user->getKwGroupMembers();
            $teamMemberIds = $this->pno->appendInactiveMembersToTeam(
                $user->getToken(),
                $user->getKwoid(),
                $activeMemberIds
            );

            return in_array($owner, $teamMemberIds);
        }

        if ($user->isService() && $user->getConsumer() == 'mcGateway') {
            return (int) $user->getKwoid() === $checklist->getAttribute('org_id');
        }

        $ownedByOtherActiveUser = $user->getKey() !== $owner
            && $this->pno->isActiveUser($user->getToken(), $owner);

        if (!$owner || $ownedByOtherActiveUser) {
            return false;
        }
        
        return true;
    }

    public function destructiveAction($user, ChecklistEloquent $checklist)
    {
        $owner = $checklist->getAttribute('user_id');

        if ($user->isService() && $user->getConsumer() == 'teamGateway') {
            $activeMemberIds = $user->getKwGroupMembers();
            $teamMemberIds = $this->pno->appendInactiveMembersToTeam(
                $user->getToken(),
                $user->getKwoid(),
                $activeMemberIds
            );

            return in_array($owner, $teamMemberIds);
        }

        if ($user->isService()
            && $user->getConsumer() == 'mcGateway'
            && !is_null($checklist->getAttribute('org_id'))
        ) {
            if ($this->roleService->hasDestructiveAction($user)) {
                return (int) $user->getKwoid() === $checklist->getAttribute('org_id');
            }
            return false;
        }

        $ownedByOtherActiveUser = $user->getKey() !== $owner
            && $this->pno->isActiveUser($user->getToken(), $owner);

        if (!$owner || $ownedByOtherActiveUser) {
            return false;
        }

        return true;
    }

    /**
     * Determine if the given Checklist can be updated by the user.
     *
     * @param User              $user
     * @param ChecklistEloquent $checklist
     *
     * @return bool
     */
    public function store($user, ChecklistEloquent $checklist)
    {
        return $this->access($user, $checklist);
    }

    /**
     * Determine if the given Checklist can be deleted by the user.
     *
     * @param User              $user
     * @param ChecklistEloquent $checklist
     *
     * @return bool
     */
    public function destroy($user, ChecklistEloquent $checklist)
    {
        return $this->destructiveAction($user, $checklist);
    }

    /**
     * Determine if the given Checklist can be viewed by the user.
     *
     * @param User              $user
     * @param ChecklistEloquent $checklist
     *
     * @return bool
     */
    public function show($user, ChecklistEloquent $checklist)
    {
        return $this->access($user, $checklist);
    }

    /**
     * Determine if the given Checklist can be updated by the user.
     *
     * @param User              $user
     * @param ChecklistEloquent $checklist
     *
     * @return bool
     */
    public function update($user, ChecklistEloquent $checklist)
    {
        return $this->destructiveAction($user, $checklist);
    }
}
