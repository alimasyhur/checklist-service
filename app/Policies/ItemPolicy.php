<?php

namespace App\Policies;

use App\Services\Items\ItemEloquent;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Services\PNO\PNOService;
use App\Services\Roles\RoleService;

class ItemPolicy
{
    use HandlesAuthorization;

    /**
     * @var PNOService
     */
    private $pno;

    /**
     * @var RoleService
     */
    private $roleService;

    /**
     * Create a new policy instance.
     */
    public function __construct(PNOService $pno, RoleService $roleService)
    {
        $this->pno = $pno;
        $this->roleService = $roleService;
    }

    /**
     * Determine if the given Item can be deleted by the user.
     *
     * @param User          $user
     * @param ItemEloquent $item
     *
     * @return bool
     */
    public function destroy($user, ItemEloquent $item)
    {
        return $this->destructiveAction($user, $item);
    }

    /**
     * Determine if the given Item can be viewed by the user.
     *
     * @param User              $user
     * @param ItemEloquent      $item
     *
     * @return bool
     */
    public function show($user, ItemEloquent $item)
    {
        return $this->access($user, $item);
    }

    /**
     * Determine if the given Item can be updated by the user.
     *
     * @param User              $user
     * @param ItemEloquent      $item
     *
     * @return bool
     */
    public function update($user, ItemEloquent $item)
    {
        return $this->destructiveAction($user, $item);
    }

    /**
     * Determine if the given Item can be accessed by the user.
     *
     * @param User              $user
     * @param ItemEloquent      $item
     *
     * @return bool
     */
    public function access($user, ItemEloquent $item)
    {
        $owner = $item->getAttribute('user_id');
        $assignee = $item->getAttribute('assignee_id');

        if ($user->isService() && $user->getConsumer() == 'teamGateway') {
            $activeMemberIds = $user->getKwGroupMembers();
            $teamMemberIds = $this->pno->appendInactiveMembersToTeam(
                $user->getToken(),
                $user->getKwoid(),
                $activeMemberIds
            );

            return in_array($owner, $teamMemberIds);
        }

        if ($user->isService() && $user->getConsumer() == 'mcGateway') {
            return (int) $user->getKwoid() === $item->getAttribute('org_id');
        }

        $owned = $user->getKey() == $owner;
        $assigned = $user->getKey() == $assignee;
        $ownedByOtherActiveUser = $user->getKey() !== $owner
            && $this->pno->isActiveUser($user->getToken(), $owner);

        if (!$owned && !$assigned && $ownedByOtherActiveUser) {
            return false;
        }

        return true;
    }

    public function destructiveAction($user, ItemEloquent $item)
    {
        $owner = $item->getAttribute('user_id');
        $assignee = $item->getAttribute('assignee_id');

        if ($user->isService() && $user->getConsumer() == 'teamGateway') {
            $activeMemberIds = $user->getKwGroupMembers();
            $teamMemberIds = $this->pno->appendInactiveMembersToTeam(
                $user->getToken(),
                $user->getKwoid(),
                $activeMemberIds
            );

            return in_array($owner, $teamMemberIds);
        }

        if ($user->isService() && $user->getConsumer() == 'mcGateway') {
            if ($item->checklist->object_domain == "mc_recruits"
                && array_get($item->getAttribute('meta'), 'template_item_id') != null
            ) {
                return (int) $user->getKwoid() === $item->getAttribute('org_id') &&
                    $this->roleService->hasDestructiveAction($user);
            }

            return (int) $user->getKwoid() === $item->getAttribute('org_id');
        }

        $owned = $user->getKey() == $owner;
        $assigned = $user->getKey() == $assignee;
        $ownedByOtherActiveUser = $user->getKey() !== $owner
            && $this->pno->isActiveUser($user->getToken(), $owner);

        if (!$owned && !$assigned && $ownedByOtherActiveUser) {
            return false;
        }

        return true;
    }
}
