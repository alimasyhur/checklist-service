<?php

namespace App\Policies;

use App\Services\Template\TemplateEloquent;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Services\PNO\PNOService;
use App\Services\Roles\RoleService;

class TemplatePolicy
{
    use HandlesAuthorization;

    /**
     * @var PNOService
     */
    private $pno;

    /**
     * @var RoleService
     */
    private $roleService;

    /**
     * Create a new policy instance.
     */
    public function __construct(PNOService $pno, RoleService $roleService)
    {
        $this->pno = $pno;
        $this->roleService = $roleService;
    }

    /**
     * Determine if the given Template can be updated by the user.
     *
     * @param User             $user
     * @param TemplateEloquent $template
     *
     * @return bool
     */
    public function store($user, TemplateEloquent $template)
    {
        $owner = $template->getAttribute('user_id');
        if (!$owner || $user->getKey() !== $owner) {
            return false;
        }

        return true;
    }

    /**
     * Determine if the given Template can be deleted by the user.
     *
     * @param User             $user
     * @param TemplateEloquent $template
     *
     * @return bool
     */
    public function destroy($user, TemplateEloquent $template)
    {
        return $this->destructiveAction($user, $template);
    }

    /**
     * Determine if the given Template can be assigned by the user.
     *
     * @param User             $user
     * @param TemplateEloquent $template
     *
     * @return bool
     */
    public function assign($user, TemplateEloquent $template)
    {
        return $this->destructiveAction($user, $template);
    }

    /**
     * Determine if the given Template can be assigned by the user.
     *
     * @param User             $user
     * @param TemplateEloquent $template
     *
     * @return bool
     */
    public function assigns($user, TemplateEloquent $template)
    {
        return $this->destructiveAction($user, $template);
    }

    /**
     * Determine if the given Template can be updated by the user.
     *
     * @param User             $user
     * @param TemplateEloquent $template
     *
     * @return bool
     */
    public function update($user, TemplateEloquent $template)
    {
        return $this->destructiveAction($user, $template);
    }

    /**
     * Determine if the given Template can be viewed by the user.
     *
     * @param User             $user
     * @param TemplateEloquent $template
     *
     * @return bool
     */
    public function show($user, TemplateEloquent $template)
    {
        return $this->access($user, $template);
    }

    /**
     * Determine if the given Template can be accessed by the user.
     *
     * @param User             $user
     * @param TemplateEloquent $template
     *
     * @return bool
     */
    public function access($user, TemplateEloquent $template)
    {
        $owner = $template->getAttribute('user_id');

        if ($user->isService() && $user->getConsumer() == 'teamGateway') {
            $activeMemberIds = $user->getKwGroupMembers();
            $teamMemberIds = $this->pno->appendInactiveMembersToTeam(
                $user->getToken(),
                $user->getKwoid(),
                $activeMemberIds
            );

            return in_array($owner, $teamMemberIds);
        }

        if ($user->isService() && $user->getConsumer() == 'mcGateway') {
            return (int) $user->getKwoid() === $template->getAttribute('org_id');
        }

        $ownedByOtherActiveUser = $user->getKey() !== $owner
            && $this->pno->isActiveUser($user->getToken(), $owner);

        if (!$owner || $ownedByOtherActiveUser) {
            return false;
        }
        
        return true;
    }

    public function destructiveAction($user, TemplateEloquent $template)
    {
        $owner = $template->getAttribute('user_id');

        if ($user->isService() && $user->getConsumer() == 'teamGateway') {
            $activeMemberIds = $user->getKwGroupMembers();
            $teamMemberIds = $this->pno->appendInactiveMembersToTeam(
                $user->getToken(),
                $user->getKwoid(),
                $activeMemberIds
            );

            return in_array($owner, $teamMemberIds);
        }

        if ($user->isService()
            && $user->getConsumer() == 'mcGateway'
            && !is_null($template->getAttribute('org_id'))
        ) {
            if ($this->roleService->hasDestructiveAction($user)) {
                return (int) $user->getKwoid() === $template->getAttribute('org_id');
            }
            return false;
        }

        $ownedByOtherActiveUser = $user->getKey() !== $owner
            && $this->pno->isActiveUser($user->getToken(), $owner);

        if (!$owner || $ownedByOtherActiveUser) {
            return false;
        }

        return true;
    }
}
