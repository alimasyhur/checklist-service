<?php

namespace App\Observers;

use App\Services\Items\ItemEloquent;
use Illuminate\Contracts\Auth\Factory as Auth;
use App\Services\Checklists\Repositories\ChecklistRepository;
use App\Jobs\GroupedItemsNotificationJob;
use App\Services\Notifications\Repositories\LastDueReminderRepository;
use App\Jobs\SendItemAssignedEmailJob;
use App\Services\History\Repositories\HistoryRepository;
use App\Services\Summaries\Repositories\SummaryRepository;

class ItemsObserver
{
    public function __construct(
        ChecklistRepository $checklistRepo,
        LastDueReminderRepository $lastDueReminderRepo,
        HistoryRepository $historyRepo,
        SummaryRepository $summaryRepo
    ) {
        $this->checklistRepo = $checklistRepo;
        $this->lastDueReminderRepo = $lastDueReminderRepo;
        $this->historyRepo = $historyRepo;
        $this->summaryRepo = $summaryRepo;
    }

    /**
     * Handle to the Items "created" event.
     *
     * @param \App\Services\Items\ItemEloquent $item
     */
    public function created(ItemEloquent $item)
    {
        $user = app(Auth::class)->user();

        if ($item->due && $item->assignee_id) {
            $dueDate = $item->due->copy()->setTime(0, 0, 0);

            if (!$this->lastDueReminderRepo->isReminderExists($item->assignee_id, $dueDate)) {
                dispatch((new GroupedItemsNotificationJob($item->assignee_id, $dueDate))->delay($dueDate));

                $this->lastDueReminderRepo->create([
                    'kwuid' => $item->assignee_id,
                    'due' => $dueDate
                ]);
            }

            dispatch(new SendItemAssignedEmailJob($item, $user));
        }

        if ($item->getAttribute('assignee_id') !== null) {
            $this->historyRepo->assignItem($user->getKey(), $item);
        }

        $this->invalidateSummaryCache($item);
    }

    /**
     * Handle the Items "updated" event.
     *
     * @param \App\Items $item
     */
    public function updated(ItemEloquent $item)
    {
        $user = app(Auth::class)->user();

        if (!$item->is_completed &&
            $item->assignee_id && $item->due &&
            ($item->isDirty('assignee_id') || $item->isDirty('due'))) {
            $dueDate = $item->due->copy()->setTime(0, 0, 0);

            if (!$this->lastDueReminderRepo->isReminderExists($item->assignee_id, $dueDate)) {
                dispatch((new GroupedItemsNotificationJob($item->assignee_id, $dueDate))->delay($dueDate));

                $this->lastDueReminderRepo->create([
                        'kwuid' => $item->assignee_id,
                        'due' => $dueDate
                    ]);
            }

            dispatch(new SendItemAssignedEmailJob($item, $user));
        }

        if ($item->isDirty('assignee_id')) {
            $this->historyRepo->assignItem($user->getKey(), $item);
        }

        if ($item->isDirty('due')) {
            $this->historyRepo->snoozeItem($user->getKey(), $item);
        }

        $this->invalidateSummaryCache($item);
    }

    public function deleted(ItemEloquent $item)
    {
        $checklist = $this->checklistRepo->find($item->getAttribute('checklist_id'));
        $user = app(Auth::class)->user();

        $this->historyRepo->archiveItem($user->getKey(), $item);

        if ($this->checklistRepo->getIncompleteItemsCount($checklist) === 0) {
            $this->checklistRepo->completeChecklist($checklist);
        }

        $this->summaryRepo->invalidateSummaryByAssigneeId($item);
    }

    private function invalidateSummaryCache(ItemEloquent $item)
    {
        if ($item->isDirty('assignee_id') || $item->isDirty('due')) {
            if ($item->getOriginal() && $item->getOriginal()['assignee_id']) {
                $this->summaryRepo->invalidateSummaryByAssigneeId($item->getOriginal()['assignee_id']);
            }

            if ($item->assignee_id) {
                $this->summaryRepo->invalidateSummaryByAssigneeId($item->assignee_id);
            }
        }
    }
}
