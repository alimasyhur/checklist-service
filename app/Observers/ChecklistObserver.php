<?php

namespace App\Observers;

use App\Jobs\DueChecklistsNotificationJob;
use App\Services\Checklists\ChecklistEloquent;
use App\Services\History\Repositories\HistoryRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Factory as Auth;

class ChecklistObserver
{
    public function __construct(HistoryRepository $historyRepo)
    {
        $this->historyRepo = $historyRepo;
    }

    /**
     * Handle to the Checklist "created" event.
     *
     * @param \App\Checklist $checklist
     */
    public function created(ChecklistEloquent $checklist)
    {
        if ($checklist->due && $checklist->due->gt(Carbon::now())) {
            $user = app(Auth::class)->user();
            dispatch((new DueChecklistsNotificationJob($checklist, $user))->delay($checklist->due));
        }
    }

    /**
     * Handle the Checklist "updated" event.
     *
     * @param \App\Checklist $checklist
     */
    public function updated(ChecklistEloquent $checklist)
    {
        $user = app(Auth::class)->user();

        if ($checklist->isDirty('is_completed')) {
            $this->historyRepo->completeChecklist($user->getKey(), $checklist);
        }

        if ($checklist->isDirty('due')) {
            $this->historyRepo->snoozeChecklist($user->getKey(), $checklist);
        }

        if ((true != $checklist->is_completed)
            && ($checklist->due != $checklist->getOriginal()['due'])
            && ($checklist->due->gt(Carbon::now()))) {
            dispatch((new DueChecklistsNotificationJob($checklist, $user))->delay($checklist->due));
        }
    }

    public function deleted(ChecklistEloquent $checklist)
    {
        $user = app(Auth::class)->user();
        $this->historyRepo->archiveChecklist($user->getKey(), $checklist);
    }
}
