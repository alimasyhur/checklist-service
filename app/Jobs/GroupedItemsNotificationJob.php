<?php

namespace App\Jobs;

use App\Services\Emails\MailServiceContract;
use Carbon\Carbon;
use App\Services\Consumers\ConsumersService;
use App\Services\Users\UserService;
use App\Services\Items\Repositories\ItemRepository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use App\Services\Deals\DealService;
use KWRI\SDK\Client;
use KWRI\SDK\Traits\KwcClientTrait;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GroupedItemsNotificationJob extends Job
{
    use KwcClientTrait;

    public $assigneeId;
    public $due;

    public function __construct($assigneeId, $due)
    {
        $this->assigneeId = $assigneeId;
        $this->due = $due;
    }

    public function handle(
        MailServiceContract $mail,
        ItemRepository $itemsRepository,
        DealService $dealService,
        UserService $userService
    ) {
        $authToken = $this->getAssigneeToken();
        $timezone = $this->getTimezone();
        $items = $itemsRepository->getOpportunityTaskDueReminder($this->assigneeId, $this->due);

        $grouped = $items->groupBy('checklist_id');

        $data = [];
        foreach ($grouped as $items) {
            $dealId = $items->first()->object_id;
            $deal = $dealService->getDeal($authToken, $dealId);
            if (!$deal) {
                throw new NotFoundHttpException("No deal found with id $dealId");
            }

            $contactName = array_get($deal, 'data.0.attributes.contact_name', '-');
            $dealName = array_get($deal, 'data.0.attributes.deal_name', '-');

            foreach ($items as $item) {
                $dueDate = Carbon::createFromFormat('Y-m-d H:i:s', $item->due)
                    ->setTimezone($timezone)->format('l, m/d Y \a\t H:i');

                $data[] = [
                    'opportunityName' => isset($dealName) ? $dealName : null,
                    'contactName' => isset($contactName) ? 'Contact name: ' . $contactName : null,
                    'taskName' => $item->description,
                    'dueDate' => "Due on $dueDate ($timezone)"
                ];
            }
        }

        $profile = $userService->getProfile($authToken);
        $to = array_get($profile, 'data.attributes.email');
        $firstName = array_get($profile, 'data.attributes.first_name');
        $lastName = array_get($profile, 'data.attributes.last_name');

        $subject = 'Opportunity task due soon reminder';
        $title = "$firstName: You have 24 hours to complete these tasks.";
        $subtitle = 'Just a friendly reminder that the following tasks are due in the next 24 hours. '
            . 'The details of each task are listed below. '
            . 'Be sure to check each task as complete as soon as you finish it.';
        $detailHeader = 'Assigned Tasks:';
        $itemList = View::make('templates.emails.checklist_item_list', ['items' => $data])->render();

        $mail->send(
            [$to => $firstName . ' ' . $lastName],
            $subject,
            config('mail.checklist_item_notification_template_id'),
            [
                'title' => $title,
                'subtitle' => $subtitle,
                'detail_header' => $detailHeader,
                'task' => $itemList
            ]
        );
    }

    private function getAssigneeToken()
    {
        $consumer = app(ConsumersService::class);
        $token = $consumer->createConsumerToken($this->assigneeId, Carbon::now('UTC')->addHour());

        return $token['jwt'];
    }

    private function getTimezone()
    {
        $timezone = 'CST';
        $authToken = $this->getAssigneeToken();
        $kwc = App::make(Client::class, [$authToken]);
        $response = $kwc->preferences()->all();
        $preferences = $response->toArray();

        if (isset($preferences['users']['timezone'])) {
            $timezone = $preferences['users']['timezone'];
        } elseif (isset($preferences['timezone'])) {
            $timezone = $preferences['timezone'];
        } elseif (isset($preferences['user']['attributes']['timezone'])) {
            $timezone = $preferences['user']['attributes']['timezone'];
        }

        if (!in_array($timezone, timezone_identifiers_list())) {
            // Fallback to CST because no timezone found
            $timezone = 'CST';
        }

        return $timezone;
    }
}
