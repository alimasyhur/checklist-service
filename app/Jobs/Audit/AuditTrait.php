<?php

namespace App\Jobs\Audit;

use App\Exceptions\PNOServiceException;
use App\Services\Consumers\ConsumersService;
use App\Services\Deals\DealService;
use App\Services\PNO\PNOService;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

trait AuditTrait
{
    public function getUser(): array
    {
        return [
            'id' => (int) $this->user->getKey(),
            'name' => $this->user->getInfo()->getName(),
        ];
    }

    public function getOrg(): ?array
    {
        return Cache::remember('pno-service-kwuid-' . $this->userId, 60, function () {
            try {
                $PNO = app(PNOService::class);
                $orgs = Arr::get($PNO->getUserOrgs($this->getConsumerToken(), $this->user->getKey()), 'data');

                return [
                    'id' => (int) Arr::first($orgs)['id'] ?? null,
                    'type' => 'Team',
                ];
            } catch (PNOServiceException $exception) {
                Log::info($exception);

                return null;
            }
        });
    }

    public function getConsumerToken(): ?string
    {
        return Cache::remember('consumer-token-' . $this->userId, 30, function () {
            $consumer = app(ConsumersService::class);
            $token = $consumer->createConsumerToken($this->userId, Carbon::now()->addHour());

            return Arr::get($token, 'jwt');
        });
    }

    public function getDeal($dealId): ?array
    {
        $dealService = app(DealService::class);
        $deals = $dealService->getDeal($this->getConsumerToken(), $dealId);

        return Arr::first(Arr::get($deals, 'data'));
    }

    public function getMoreInfo(?array $deal): string
    {
        $info = [];

        array_push($info, 'deal_id:' . Arr::get($deal, 'id'));

        $keys = [
            'mc_id',
            'mc_name',
            'opportunity_id',
            'deal_name' => 'opportunity_name',
            'type' => 'opportunity_type'
        ];
        foreach ($keys as $key => $alias) {
            $index = is_numeric($key) ? $alias : $key;
            $attribute = Arr::get($deal, 'attributes.' . $index);
            if (!empty($attribute)) {
                array_push($info, sprintf('%s:%s', $alias, $attribute));
            }
        }

        return implode(';', $info);
    }

    public function logEmptyDeal($item): void
    {
        Log::info('Deal not found.', [
            'domain' => $item->checklist->getAttribute('object_domain'),
            'id' => $item->checklist->getAttribute('object_id'),
        ]);
    }
}
