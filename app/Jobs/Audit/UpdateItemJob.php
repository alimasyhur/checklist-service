<?php

namespace App\Jobs\Audit;

use App\Exceptions\AuditPublisherServiceException;
use App\Jobs\Job;
use App\Services\Audit\PublisherService;
use App\Services\Consumers\ConsumersService;
use App\Services\Items\ItemEloquent;
use App\Services\Users\UserService;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class UpdateItemJob extends Job
{
    use AuditTrait;

    private $item;
    private $user;
    private $userId;
    private $old;
    private $messages = [
        'description' => 'Checklist Item "{{ITEM}}" updated for {{DEAL}} from "{{OLD}}" to "{{NEW}}" by {{USER}}.',
        'due' => 'Checklist Item "{{ITEM}}" due changed for {{DEAL}} from "{{OLD}}" to "{{NEW}}" by {{USER}}',
        'due_only' => 'Checklist Item "{{ITEM}}" due changed for {{DEAL}} to "{{NEW}}" by {{USER}}',
        'assignee' => 'Checklist Item "{{ITEM}}" assigned for {{DEAL}} from "{{OLD}}" to "{{NEW}}" by {{USER}}',
    ];

    public function __construct($user, ItemEloquent $item, array $old)
    {
        $this->item = $item;
        $this->user = $user;
        $this->userId = $this->user->getKey();
        $this->old = $old;
    }

    private function getUserProfile(int $userId): ?array
    {
        $consumer = app(ConsumersService::class);
        $token = $consumer->createConsumerToken($userId, Carbon::now()->addHour());

        $user = app(UserService::class);
        return $user = $user->getProfile(Arr::get($token, 'jwt'));
    }

    public function handle(PublisherService $audit): void
    {
        $deal = $this->getDeal($this->item->checklist->getAttribute('object_id'));
        if (empty($deal)) {
            $this->logEmptyDeal($this->item);

            return;
        }

        $placeholders = [
            '{{USER}}' => $this->user->getInfo()->getName(),
            '{{ITEM}}' => $this->item->description,
            '{{DEAL}}' => Arr::get($deal, 'attributes.opportunity_id', Arr::get($deal, 'id')),
            '{{OLD}}' => Arr::get($this->old, 'description'),
            '{{NEW}}' => $this->item->getAttribute('description'),
        ];
        $message = $this->messages['description'];

        $oldDue = data_get($this->old, 'due');
        if (empty($oldDue) && !empty($this->item->due)) {
            $message = $this->messages['due_only'];
            $placeholders['{{NEW}}'] = $this->item->due;
        } elseif ((!empty($this->item->due) || empty($oldDue)) && $this->item->due->ne($oldDue)) {
            $message = $this->messages['due'];
            $placeholders['{{OLD}}'] = $oldDue;
            $placeholders['{{NEW}}'] = $this->item->due;
        } elseif ($this->item->assignee_id != Arr::get($this->old, 'assignee_id')) {
            $message = $this->messages['assignee'];

            $newAssignee = $this->getUserProfile($this->item->assignee_id);
            $oldAssigne = $this->getUserProfile(Arr::get($this->old, 'assignee_id'));

            $placeholders['{{NEW}}'] = vsprintf('%s %s', [
                Arr::get($newAssignee, 'data.attributes.first_name'),
                Arr::get($newAssignee, 'data.attributes.last_name'),
            ]);
            $placeholders['{{OLD}}'] = vsprintf('%s %s', [
                Arr::get($oldAssigne, 'data.attributes.first_name'),
                Arr::get($oldAssigne, 'data.attributes.last_name'),
            ]);
        }

        try {
            $audit->setToken($this->getConsumerToken())
                ->setUser($this->getUser())
                ->setOrg($this->getOrg())
                ->publishMessage([
                    'type' => config('audit.type'),
                    'sub_type' => config('audit.sub_type.item.edit'),
                    'timestamp' => $this->item->getAttribute('updated_at')->toIso8601ZuluString(),
                    'message' => str_replace(array_keys($placeholders), array_values($placeholders), $message),
                    'more_info' => $this->getMoreInfo($deal),
                    'link' => Arr::get($deal, 'links.Self'),
                ]);
        } catch (AuditPublisherServiceException $exception) {
            Log::info($exception);
        }
    }
}
