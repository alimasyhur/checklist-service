<?php

namespace App\Jobs\Audit;

use App\Exceptions\AuditPublisherServiceException;
use App\Services\Audit\PublisherService;
use App\Services\Items\ItemEloquent;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class IncompleteItemJob
{
    use AuditTrait;

    private $item;
    private $user;
    private $userId;

    public function __construct($user, ItemEloquent $item)
    {
        $this->item = $item;
        $this->user = $user;
        $this->userId = $this->user->getKey();
    }

    public function handle(PublisherService $audit): void
    {
        $deal = $this->getDeal($this->item->checklist->getAttribute('object_id'));
        if (empty($deal)) {
            $this->logEmptyDeal($this->item);

            return;
        }

        try {
            $audit->setToken($this->getConsumerToken())
                ->setUser($this->getUser())
                ->setOrg($this->getOrg())
                ->publishMessage([
                    'type' => config('audit.type'),
                    'sub_type' => config('audit.sub_type.item.incomplete'),
                    'timestamp' => $this->item->getAttribute('updated_at')->toIso8601ZuluString(),
                    'message' => vsprintf('Checklist Item "%s" marked as incomplete for Opportunity %s by %s.', [
                        $this->item->getAttribute('description'),
                        Arr::get($deal, 'attributes.opportunity_id', Arr::get($deal, 'id')),
                        $this->user->getInfo()->getName(),
                    ]),
                    'more_info' => $this->getMoreInfo($deal),
                    'link' => Arr::get($deal, 'links.Self'),
                ]);
        } catch (AuditPublisherServiceException $exception) {
            Log::info($exception);
        }
    }
}
