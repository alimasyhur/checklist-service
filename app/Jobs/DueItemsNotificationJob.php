<?php

namespace App\Jobs;

use App\Services\Checklists\Repositories\ChecklistRepository;
use App\Services\Items\Repositories\ItemRepository;
use App\Services\Notifications\Notification;

class DueItemsNotificationJob extends Job
{
    public $oldItems;

    public $user;

    /**
     * Create a new job instance.
     */
    public function __construct($oldItems, $user)
    {
        $this->oldItems = $oldItems;
        $this->user     = $user;
    }

    /**
     * Execute the job.
     */
    public function handle(ItemRepository $itemsRepo, ChecklistRepository $checklistRepo)
    {
        $currentItems = $itemsRepo->find($this->oldItems->id);
        $checklist    = $checklistRepo->find($currentItems->checklist_id);

        if ((false == $currentItems->is_completed) && $currentItems->due->eq($this->oldItems->due)) {
            $notificationJob = new Notification();
            $message = $currentItems->description . ' due date is ' . $currentItems->due->toDateTimeString();
            $tags = [
                'notif',
                'item' . $this->oldItems->id,
                'checklist' . $currentItems->checklist_id,
                'user' . $this->user->getKey()
            ];
            $notificationJob->setTitle('You have due checklist item.')
                            ->setMessage($message)
                            ->setOrigin('web')
                            ->setDomain($checklist->object_domain)
                            ->setObjectID($checklist->object_id)
                            ->setType('reminder')
                            ->setTargets([
                                ['type' => 'user', 'id' => $this->user->getKey()],
                            ])
                            ->setFrom($this->user->getKey(), 'system')
                            ->setQueueTags($tags)
                            ->send();
        }
    }

    public function tags()
    {
        return ['recheck', 'item' . $this->oldItems->id, 'user' . $this->user->getKey()];
    }
}
