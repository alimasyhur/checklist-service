<?php

namespace App\Jobs;

use App\Services\Checklists\Repositories\ChecklistRepository;
use App\Services\Notifications\Notification;

class DueChecklistsNotificationJob extends Job
{
    protected $oldChecklist;

    protected $user;

    /**
     * Create a new job instance.
     */
    public function __construct($oldChecklist, $user)
    {
        $this->user         = $user;
        $this->oldChecklist = $oldChecklist;
    }

    /**
     * Execute the job.
     */
    public function handle(ChecklistRepository $checklistRepo)
    {
        $currentChecklist = $checklistRepo->find($this->oldChecklist->id);
        if ((false == $currentChecklist->is_completed) && $currentChecklist->due->eq($this->oldChecklist->due)) {
            $notificationJob = new Notification();
            $message = $currentChecklist->description . ' due date is ' . $currentChecklist->due->toDateTimeString();
            $tags = [
                'notif',
                'checklist' . $currentChecklist->id,
                'user' . $this->user->getKey()
            ];

            $notificationJob->setTitle('You have due checklist.')
                            ->setMessage($message)
                            ->setOrigin('web')
                            ->setDomain($currentChecklist->object_domain)
                            ->setObjectID($currentChecklist->object_id)
                            ->setType('reminder')
                            ->setTargets([
                                ['type' => 'user', 'id' => $this->user->getKey()],
                            ])
                            ->setFrom($this->user->getKey(), 'system')
                            ->setQueueTags($tags)
                            ->send();
        }
    }

    public function tags()
    {
        return ['recheck', 'checklist' . $this->oldChecklist->id, 'user' . $this->user->getKey()];
    }
}
