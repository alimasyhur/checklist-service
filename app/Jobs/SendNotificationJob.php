<?php

namespace App\Jobs;

use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use App\Services\Notifications\Notification;

class SendNotificationJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private $notification;

    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    public function handle()
    {
        $from    = $this->notification->getFrom();
        $targets = $this->notification->getTargets();

        if (empty($from['id'])) {
            return;
        }

        foreach ($targets as $target) {
            if (empty($target['id'])) {
                return;
            }
        }

        $this->sendNotification();
    }

    public function sendNotification()
    {
        $expiredAt = Carbon::now('UTC')->addDay();
        $from      = $this->notification->getFrom();

        $token = $this->notification->requestToken($from['id'], $expiredAt);

        // Client should be here or will thrown serializable error
        $client = app(Client::class);
        $headers = [
            'Content-Type'  => 'application/json;charset=utf-8',
            'Authorization' => "Bearer {$token}",
        ];
        
        $url    = getenv('NOTIFICATION_ENDPOINT');
        $params = $this->notification->toParams();

        try {
            $response = $client->request('POST', $url, [
                'headers' => $headers,
                'json'    => $params,
                ]);

            return $response;
        } catch (Exception $e) {
            // Resend Notification
            if ('401' == $e->getCode()) {
                Cache::forget('due-checklist-notification-token-user-' . $from['id']);
                $this->sendNotification();
            }

            return $e->getMessage();
        }
    }

    public function tags()
    {
        return $this->notification->getQueueTags();
    }
}
