<?php

namespace App\Jobs;

use App\Services\Emails\MailServiceContract;
use App\Services\Users\UserService;
use App\Services\Consumers\ConsumersService;
use Carbon\Carbon;
use KWRI\SDK\Traits\KwcClientTrait;
use Illuminate\Support\Facades\View;
use App\Services\Deals\DealService;
use Illuminate\Support\Facades\App;
use KWRI\SDK\Client;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SendItemAssignedEmailJob extends Job
{
    use KwcClientTrait;

    public $item;
    public $user;

    /**
     * Create a new job instance.
     */
    public function __construct($item, $user)
    {
        $this->item = $item;
        $this->user = $user;
    }

    /**
     * Execute the job.
     */
    public function handle(MailServiceContract $mail, DealService $dealService)
    {
        $authToken = $this->getAssigneeToken();

        $profile = $this->getUserAssigneeProfile();
        $to = array_get($profile, 'data.attributes.email');
        $firstName = array_get($profile, 'data.attributes.first_name');
        $lastName = array_get($profile, 'data.attributes.last_name');
        $toFullName = $firstName . ' ' . $lastName;

        $timezone = $this->getTimezone();
        $dueDate = $this->item->due->setTimezone($timezone)->format('l, m/d Y \a\t H:i');

        $checklist = $this->item->checklist;

        if ($checklist->object_domain == 'deals') {
            $dealId = $checklist->object_id;
            $deal = $dealService->getDeal($authToken, $dealId);
            if (!$deal) {
                throw new NotFoundHttpException("No deal found with id $dealId");
            }

            $contactName = array_get($deal, 'data.0.attributes.contact_name', '-');
            $dealName = array_get($deal, 'data.0.attributes.deal_name', '-');
        }

        $data = [[
            'opportunityName' => isset($dealName) ? $dealName : null,
            'contactName' => isset($contactName) ? 'Contact name: ' . $contactName : null,
            'taskName' => $this->item->description,
            'dueDate' => "Due on $dueDate ($timezone)"
        ]];

        $title = "$firstName: You've been assigned to a new task.";
        $subtitle = 'Please review and complete the task by logging into Command. Keep up the great work!';
        $detailHeader = 'Task detail:';
        $message = View::make('templates.emails.checklist_item_list', ['items' => $data])->render();

        $mail->send(
            [$to => $toFullName],
            "New task assigned",
            config('mail.checklist_item_notification_template_id'),
            [
                'title' => $title,
                'subtitle' => $subtitle,
                'detail_header' => $detailHeader,
                'task' => $message
            ]
        );
    }

    private function getAssigneeToken()
    {
        $consumer = app(ConsumersService::class);
        $token = $consumer->createConsumerToken($this->item->assignee_id, Carbon::now('UTC')->addHour());

        return $token['jwt'];
    }

    private function getUserAssigneeProfile()
    {
        $userService = app(UserService::class);
        $authToken = $this->getAssigneeToken();

        return $userService->getProfile($authToken);
    }

    private function getTimezone()
    {
        $timezone = 'CST';
        $authToken = $this->getAssigneeToken();

        $kwc = App::make(Client::class, [$authToken]);
        $response = $kwc->preferences()->all();
        $preferences = $response->toArray();

        if (isset($preferences['users']['timezone'])) {
            $timezone = $preferences['users']['timezone'];
        } elseif (isset($preferences['timezone'])) {
            $timezone = $preferences['timezone'];
        } elseif (isset($preferences['user']['attributes']['timezone'])) {
            $timezone = $preferences['user']['attributes']['timezone'];
        }

        if (!in_array($timezone, timezone_identifiers_list())) {
            // Fallback to CST because no timezone found
            $timezone = 'CST';
        }

        return $timezone;
    }
}
