<?php

namespace App\Exceptions;

use CorePackage\Exceptions\Handler as BaseHandler;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use KWRI\KongHandler\Exceptions\MethodNotFoundException;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends BaseHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        MethodNotFoundException::class,
    ];

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception               $e
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ModelNotFoundException) {
            return $this->respondWithErrors(
                'Could not find the resource with the submitted id',
                $e->getCode(),
                404,
                env('APP_DEBUG') ? FlattenException::create($e)->getTrace() : null
            );
        }

        return $this->JsonApiResponseException($request, $e);
    }
}
