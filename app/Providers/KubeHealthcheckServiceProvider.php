<?php
namespace App\Providers;

use Illuminate\Support\Facades\Redis;
use Illuminate\Support\ServiceProvider;
use Rap2hpoutre\LaravelLogViewer\LaravelLogViewer;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;

class KubeHealthcheckServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     * @return void
     */
    public function register()
    {
        $this->app->register(\Rap2hpoutre\LaravelLogViewer\LaravelLogViewerServiceProvider::class);
    }


    public function boot()
    {
        // readiness check
        $this->app->router->get('health', function () {
            $statuses = [];
            $status_code = 200;

            // We got here, so its running :)
            $statuses['app'] = [
                'success' => true,
                'message' => 'Application is running',
            ];

            // Check the db
            try {
                app('db')->getPdo();
                $statuses['database'] = [
                    'success' => true,
                    'message' => 'Database is connected',
                ];
            } catch (Exception $e) {
                $statuses['database'] = [
                    'success' => false,
                    'message' => $e->getMessage(),
                ];
            }
            if (!$statuses['database']['success']) {
                $status_code = 500;
            }

            // Check redis connection status
            try {
                Redis::connection('checklist')->ping();
                $statuses['redis'] = [
                     'success' => true,
                     'message' => 'Redis is connected',
                ];
            } catch (Exception $e) {
                $statuses['redis'] = [
                     'success' => false,
                     'message' => $e->getMessage(),
                ];
            }
            // if (!$statuses['redis']['success']) { $status_code = 500; }

            // Check the exception
            $exceptionState = false;
            $exceptionMessage = 'Exception rates is unknown';
            $exceptionCount = null;
            $exceptionDetails = url('/logs');
            try {
                $logs = LaravelLogViewer::all();
                if ($logs) {
                    $allowedMax = 5; // @TODO : should be configurable onward
                    $exceptionState = true;
                    $exceptionMessage = 'Exception rates is exceeds the maximum allowed count';

                    $todayExceptions = array_filter($logs, function ($log) {
                        return isset($log['date'])
                            && isset($log['text'])
                            && Carbon::parse($log['date'])->isToday()
                            && $log['level'] == 'error';
                    });

                    $exceptionDetails = [];
                    foreach ($todayExceptions as $todayException) {
                        $k = $todayException['text'];
                        if (isset($exceptionDetails[$k])) {
                            $exceptionDetails[$k]++;
                        } else {
                            $exceptionDetails[$k] = 1;
                        }
                    }

                    $exceptionCount = count($todayExceptions);

                    if ($exceptionCount < $allowedMax) {
                        $exceptionState = true;
                        $exceptionMessage = 'Exception rates is fine';
                    }
                } else {
                    // No logs here, so we'll assume its fine
                    $exceptionState = true;
                    $exceptionMessage = 'Exception rates is fine';
                    $exceptionCount = 0;
                }
            } catch (Exception $e) {
                Log::info($e);
            }
            $statuses['exception_rates'] = [
                'success' => $exceptionState,
                'message' => $exceptionMessage,
                'failed_requests_count' => $exceptionCount,
                'details' => $exceptionDetails,
            ];

            $statuses['app']['status_code'] = $status_code;
            return response()->json($statuses, $status_code);
        });

        // liveness check
        $this->app->router->get('ping', function () {
            return response('PONG', 200);
        });
    }


    public function provides()
    {
        return ['health_check'];
    }
}
