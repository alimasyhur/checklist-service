<?php

namespace App\Providers;

use App\Observers\ChecklistObserver;
use App\Observers\ItemsObserver;
use App\Services\Checklists\ChecklistEloquent;
use App\Services\Emails\Mailjet;
use App\Services\Emails\MailServiceContract;
use App\Services\Items\ItemEloquent;
use App\Services\Roles\RoleService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use KWRI\SDK\Client;
use KWRI\SDK\Models\OAuthCredential;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Contracts\Cache\Repository as CacheRepository;
use Illuminate\Support\Arr;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        ChecklistEloquent::observe(ChecklistObserver::class);
        ItemEloquent::observe(ItemsObserver::class);

        Relation::morphMap([
            ItemEloquent::MORPH_NAME => ItemEloquent::class,
            ChecklistEloquent::MORPH_NAME => ChecklistEloquent::class,
        ]);
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        $this->app->when(RoleService::class)
            ->needs(HttpClient::class)
            ->give(function () {
                return new HttpClient([
                    'base_uri' => config('app.roles_api.url'),
                    'headers' => [
                        'x-authorization' => config('app.roles_api.key')
                    ]]);
            });

        $this->app->bind(MailServiceContract::class, function () {
            return new Mailjet();
        });

        $this->app->bind(Client::class, function ($app, $params) {
            $token = Arr::get($params, 0);
            $headers = [];

            if (env('APP_ENV') == 'local') {
                $headers['x-userinfo'] = '{"given_name":"kelle1","updated_at":"2018-01-23T07:19:36.950Z",'
                    . '"family_name":"kelle1","preferred_username":"kelle1","custom_fields":{"KW_UID":"556396"},'
                    . '"email":"kelle1@kw.com","name":"kelle1 kelle1","sub":"37087183"}';

                $headers['x-consumer-username'] = 'local';
            }

            $credential = new OAuthCredential($token, $headers);
            $httpClient = new HttpClient(['base_uri' => env('KWC_URL')]);

            return new Client($credential, $httpClient, app(CacheRepository::class));
        });
    }
}
