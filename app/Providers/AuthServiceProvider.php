<?php

namespace App\Providers;

use App\Policies\ChecklistPolicy;
use App\Policies\ItemPolicy;
use App\Policies\TemplatePolicy;
use App\Services\Checklists\ChecklistEloquent;
use App\Services\Items\ItemEloquent;
use App\Services\Template\TemplateEloquent;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register()
    {
    }

    /**
     * Boot the authentication services for the application.
     */
    public function boot()
    {
        Gate::policy(ChecklistEloquent::class, ChecklistPolicy::class);
        Gate::policy(ItemEloquent::class, ItemPolicy::class);
        Gate::policy(TemplateEloquent::class, TemplatePolicy::class);

        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->input('api_token')) {
                return User::where('api_token', $request->input('api_token'))->first();
            }
        });
    }
}
