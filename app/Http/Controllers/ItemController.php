<?php

namespace App\Http\Controllers;

use App\Http\Requests\BulkItemRequest;
use App\Http\Requests\CompleteItemsRequest;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\StoreItemsRequest;
use App\Http\Requests\UpdateItemsRequest;
use App\Services\Checklists\Repositories\ChecklistRepository;
use App\Services\Items\ItemEloquent;
use App\Services\Items\Presenters\ItemTransformer;
use App\Services\Items\Repositories\ItemRepository;
use App\Services\PNO\PNOService;
use CorePackage\Infrastructures\Adapters\SearchAdapterBuilder;
use CorePackage\Requests\JsonApiRequest;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Libs\Presenters\JsonApiPresenterInterface;
use Libs\Presenters\JsonApiSerializer;
use Carbon\Carbon;
use App\Services\Roles\RoleService;

class ItemController extends Controller
{
    protected $presenter;

    protected $itemRepo;

    protected $checklistRepo;

    protected $pno;

    protected $roleService;

    public function __construct(
        JsonApiPresenterInterface $presenter,
        ItemRepository $itemRepo,
        ChecklistRepository $checklistRepo,
        PNOService $pno,
        RoleService $roleService
    ) {
        parent::__construct($presenter);
        $this->itemRepo = $itemRepo;
        $this->checklistRepo = $checklistRepo;
        $this->presenter = $presenter;
        $this->pno = $pno;
        $this->roleService = $roleService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SearchRequest $request, $checklistId)
    {
        $checklist = $this->checklistRepo->find($checklistId);
        $this->authorize('show', $checklist);

        $filters = $request->getFilters();
        array_forget($filters, 'checklist_id');
        array_set($filters, 'checklist_id.is', $checklistId);

        $adapterBuilder = new SearchAdapterBuilder();
        $adapter = $adapterBuilder
            ->setPager($request->getLimit(), $request->getOffset(), $request->all())
            ->setSorter($request->getSorts())
            ->addFields($request->getFields())
            ->addResources($request->getIncludes())
            ->addFilters($filters)
            ->build();

        $checklistItems = $this->itemRepo->browse($adapter);

        return $this->presenter->setTransformer(new ItemTransformer())
            ->setCustomBaseUrl($request)
            ->parseIncludes($request->getIncludes())
            ->parseSparseFieldsets('items', $request->getFields())
            ->serialize(new JsonApiSerializer($this->presenter->getBaseUrl()), 'items')
            ->renderCollection($checklistItems);
    }

    public function indexWithoutChecklist(SearchRequest $request)
    {
        $adapterBuilder = new SearchAdapterBuilder();

        $adapter = $adapterBuilder
            ->setPager($request->getLimit(), $request->getOffset(), $request->all())
            ->setSorter($request->getSorts())
            ->addFields($request->getFields())
            ->addResources($request->getIncludes())
            ->addFilters($request->getFilters())
            ->build();

        $items = $this->itemRepo->browse($adapter);

        return $this->presenter->setTransformer(new ItemTransformer())
            ->setCustomBaseUrl($request)
            ->parseIncludes($request->getIncludes())
            ->parseSparseFieldsets('items', $request->getFields())
            ->serialize(new JsonApiSerializer($this->presenter->getBaseUrl()), 'items')
            ->renderCollection($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Auth $auth, StoreItemsRequest $request, $checklistId, ItemEloquent $item)
    {
        $checklist = $this->checklistRepo->find($checklistId);

        $this->authorize($checklist);

        $user = $auth->user();
        $userId = $user->getKey();
        if (in_array(config('app.env'), ['local', 'dev', 'qa'])) {
            $userId = $request->getAttribute('user_id') ?: $userId;
        }

        $item
            ->setAttribute('description', $request->getAttribute('description'))
            ->setAttribute('meta', $request->getAttribute('meta'))
            ->setAttribute('urgency', $request->getAttribute('urgency', 0))
            ->setAttribute('is_completed', false)
            ->setAttribute('completed_at', null)
            ->setAttribute('completed_by', null)
            ->setAttribute('checklist_id', $checklistId)
            ->setAttribute('user_id', $userId)
            ->setAttribute('org_id', $checklist->org_id)
            ->setAttribute('assignee_id', $request->getAttribute('assignee_id'))
            ->setAttribute('task_id', $request->getAttribute('task_id'))
            ->setAttribute('notify_client', $request->getAttribute('notify_client', false))
            ->setAttribute('client_update_sent_at', $request->getAttribute('client_update_sent_at'));

        $newDue = $this->checklistRepo->getDueDateFormat($request->getAttribute('due'));
        $item->setAttribute('due', $newDue);

        $newItem = $this->itemRepo->create($item);

        return $this->presenter->setTransformer(new ItemTransformer())
            ->setCustomBaseUrl($item)
            ->serialize(new JsonApiSerializer($this->presenter->getBaseUrl()), 'items')
            ->setStatusCode(201)
            ->renderItem($newItem);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(JsonApiRequest $request, $checklistId, $itemId)
    {
        $checklist = $this->checklistRepo->find($checklistId);

        $item = $this->itemRepo->find($itemId);

        if ($item->checklist_id != (int) $checklistId) {
            throw new ModelNotFoundException();
        }

        $this->authorize($item);

        return $this->presenter->setTransformer(new ItemTransformer())
            ->setCustomBaseUrl($item)
            ->parseIncludes($request->getIncludes())
            ->serialize(new JsonApiSerializer($this->presenter->getBaseUrl()), 'items')
            ->setStatusCode(200)
            ->renderItem($item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Auth $auth, UpdateItemsRequest $request, $checklistId, $itemId)
    {
        $item = $this->itemRepo->find($itemId);
        $this->authorize($item);

        if ($item->checklist_id !== (int) $checklistId) {
            throw new ModelNotFoundException();
        }

        $item
            ->setAttribute('description', $request->getAttribute('description', $item->getAttribute('description')))
            ->setAttribute('meta', $request->getAttribute('meta', $item->getAttribute('meta')))
            ->setAttribute('urgency', $request->getAttribute('urgency', $item->getAttribute('urgency')))
            ->setAttribute('updated_by', $auth->user()->getKey())
            ->setAttribute('assignee_id', $request->getAttribute('assignee_id', $item->getAttribute('assignee_id')))
            ->setAttribute('task_id', $request->getAttribute('task_id', $item->getAttribute('task_id')))
            ->setAttribute('notify_client', $request->getAttribute('notify_client'))
            ->setAttribute('client_update_sent_at', $request->getAttribute('client_update_sent_at'));

        $newDue = $this->checklistRepo->getDueDateFormat(
            $request->getAttribute('due', $item->getAttribute('due'))
        );
        $item->setAttribute('due', $newDue);

        $updatedItem = $this->itemRepo->update($item);

        return $this->presenter->setTransformer(new ItemTransformer())
            ->setCustomBaseUrl($item)
            ->serialize(new JsonApiSerializer($this->presenter->getBaseUrl()), 'items')
            ->setStatusCode(200)
            ->renderItem($updatedItem);
    }

    public function bulk(Auth $auth, BulkItemRequest $request, $checklistId)
    {
        $bulkResponse = [];

        $requestItems = collect($request->getData());

        $foundItems = collect($this->itemRepo->findByIds($requestItems->pluck('id')));

        foreach ($foundItems as $item) {
            $updateItem = $requestItems->firstWhere('id', $item->id);
            $action = array_get($updateItem, 'action');
            if ($item->checklist_id !== (int) $checklistId) {
                array_push($bulkResponse, [
                    'id' => $item->id,
                    'action' => array_get($updateItem, $action),
                    'status' => 404,
                ]);
                continue;
            }

            $user = $auth->user();

            if ($user->cant($action, $item)) {
                array_push($bulkResponse, [
                    'id' => $item->id,
                    'action' => array_get($updateItem, 'action'),
                    'status' => 403,
                ]);
                continue;
            }

            if ($action == 'update') {
                $item->setAttribute('updated_by', $auth->user()->getKey());
                $fields = [
                    'description',
                    'due',
                    'urgency',
                    'assignee_id',
                    'task_id',
                    'notify_client',
                    'client_update_sent_at'
                ];
                foreach ($fields as $field) {
                    $value = array_has($updateItem, "attributes.$field")
                        ? array_get($updateItem, "attributes.$field")
                        : $item->getAttribute($field);
                    $item->setAttribute($field, $value);
                }

                $this->itemRepo->update($item);

                array_push($bulkResponse, [
                    'id' => $item->id,
                    'action' => 'update',
                    'status' => 200,
                ]);
            }
        }

        $notFoundItems = $requestItems->pluck('id')->diff($foundItems->pluck('id')->toArray());

        foreach ($notFoundItems as $notFoundItem) {
            $existingItemNotFoundData = $requestItems->firstWhere('id', $notFoundItem);

            array_push($bulkResponse, [
                'id' => array_get($existingItemNotFoundData, 'id'),
                'action' => array_get($existingItemNotFoundData, 'action'),
                'status' => 404,
            ]);
        }

        return response()->json(['data' => $bulkResponse]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($checklistId, $itemId)
    {
        $item = $this->itemRepo->find($itemId);

        $this->authorize($item);

        if ($item->checklist_id !== (int) $checklistId) {
            throw new ModelNotFoundException();
        }

        $this->itemRepo->delete($item);

        return response(null, 204);
    }

    public function complete(CompleteItemsRequest $request, Auth $auth)
    {
        $itemIds = array_pluck($request->getData(), 'item_id');
        $user = $auth->user();

        $userIds = [$user->getKey()];
        if ($this->isTeamScope($user)) {
            $userIds = $this->pno->appendInactiveMembersToTeam(
                $user->getToken(),
                $user->getKwoid(),
                $user->getKwGroupMembers()
            );
        }

        $orgId = null;
        if ($user->isService() && $user->getConsumer() == 'mcGateway') {
            $orgId = $user->getKwoid();
        }
        $result = $this->itemRepo->complete($itemIds, $userIds, $orgId);

        return response()->json(['data' => $result]);
    }

    public function incomplete(CompleteItemsRequest $request, Auth $auth)
    {
        $itemIds = array_pluck($request->getData(), 'item_id');
        $user = $auth->user();
        $userIds = [$user->getKey()];
        if ($this->isTeamScope($user)) {
            $userIds = $this->pno->appendInactiveMembersToTeam(
                $user->getToken(),
                $user->getKwoid(),
                $user->getKwGroupMembers()
            );
        }

        $orgId = null;
        if ($user->isService() && $user->getConsumer() == 'mcGateway') {
            $orgId = $user->getKwoid();
        }

        $result = $this->itemRepo->incomplete($itemIds, $userIds, $orgId);

        return response()->json(['data' => $result]);
    }

    public function count(Auth $auth, Request $request)
    {
        $this->validate($request, [
            'date' => 'required|date',
            'tz' => ['nullable', 'string', function ($attribute, $value, $fail) {
                if (!in_array($value, timezone_identifiers_list())) {
                    $fail($attribute . ' is invalid.');
                }
            }],
            'object_domain' => 'nullable|string'
        ]);

        $kwuid = $auth->user()->getKey();
        $orgId = array_get($request->query(), 'filter.org_id.is');

        $date = new Carbon($request->input('date'));

        $constraint = [
            'tz' => $request->input('tz'),
            'object_domain' => $request->input('object_domain')
        ];

        $summary = $this->itemRepo->summary($kwuid, $date, $constraint, $orgId);

        return response()->json(['data' => $summary]);
    }

    public function checklistItemCount(Auth $auth, SearchRequest $request)
    {
        $filters = $request->getFilters();
        $checklistIds = array_get($filters, 'checklist_id.in');
        $orgId = array_get($filters, 'org_id.is');

        $user = $auth->user();

        $result = $this->itemRepo->checklistItemCount($user, $checklistIds, $orgId);

        return response()->json(['data' => $result]);
    }

    private function isTeamScope($user)
    {
        if ($user->isService() && $user->getConsumer() == 'teamGateway') {
            return true;
        }

        return false;
    }
}
