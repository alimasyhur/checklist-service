<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use Libs\Presenters\JsonApiSerializer;
use CorePackage\Requests\JsonApiRequest;
use App\Http\Requests\StoreChecklistRequest;
use App\Http\Requests\UpdateChecklistRequest;
use App\Services\Checklists\ChecklistEloquent;
use Illuminate\Contracts\Auth\Factory as Auth;
use Libs\Presenters\JsonApiPresenterInterface;
use App\Services\Checklists\Presenters\ChecklistTransformer;
use App\Services\Checklists\Repositories\ChecklistRepository;
use App\Services\Roles\RoleService;
use CorePackage\Infrastructures\Adapters\SearchAdapterBuilder;

class ChecklistController extends Controller
{
    protected $presenter;

    protected $repository;

    protected $roleService;

    public function __construct(
        JsonApiPresenterInterface $presenter,
        ChecklistRepository $repository,
        RoleService $roleService
    ) {
        parent::__construct($presenter);
        $this->repository = $repository;
        $this->presenter = $presenter;
        $this->roleService = $roleService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SearchRequest $request)
    {
        $adapterBuilder = new SearchAdapterBuilder();

        $adapter = $adapterBuilder
            ->setPager($request->getLimit(), $request->getOffset(), $request->all())
            ->setSorter($request->getSorts())
            ->addFields($request->getFields())
            ->addResources($request->getIncludes())
            ->addFilters($request->getFilters())
            ->build();

        $cheklists = $this->repository->browse($adapter);

        return $this->presenter->setTransformer(new ChecklistTransformer())
            ->setCustomBaseUrl($request, true)
            ->parseIncludes($request->getIncludes())
            ->serialize(new JsonApiSerializer($this->presenter->getBaseUrl()), 'checklists')
            ->setStatusCode(200)
            ->renderCollection($cheklists);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Auth $auth, StoreChecklistRequest $request, ChecklistEloquent $checklist)
    {
        $user = $auth->user();
        $userId = $user->getKey();
        if (in_array(config('app.env'), ['local', 'dev', 'qa'])) {
            $userId = $request->getAttribute('user_id') ?: $userId;
        }

        $checklist
            ->setAttribute('object_domain', $request->getAttribute('object_domain'))
            ->setAttribute('object_id', $request->getAttribute('object_id'))
            ->setAttribute('description', $request->getAttribute('description'))
            ->setAttribute('urgency', $request->getAttribute('urgency', 0))
            ->setAttribute('is_completed', false)
            ->setAttribute('completed_at', null)
            ->setAttribute('user_id', $userId);

        $newDueDate = $this->repository->getDueDateFormat($request->getAttribute('due'));
        $checklist->setAttribute('due', $newDueDate);

        if ($user->isService() &&
            $user->getConsumer() == 'mcGateway' &&
            $this->roleService->hasDestructiveAction($user)
        ) {
            $checklist->setAttribute('org_id', $user->getKwoid());
        }

        $newChecklist = $this->repository->create($checklist);

        return $this->presenter->setTransformer(new ChecklistTransformer())
            ->setCustomBaseUrl($checklist, true)
            ->serialize(new JsonApiSerializer($this->presenter->getBaseUrl()), 'checklists')
            ->setStatusCode(201)
            ->renderItem($newChecklist);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(JsonApiRequest $request, $id)
    {
        $checklist = $this->repository->find($id);
        $this->authorize($checklist);

        return $this->presenter->setTransformer(new ChecklistTransformer())
            ->setCustomBaseUrl($checklist, true)
            ->parseIncludes($request->getIncludes())
            ->parseSparseFieldsets('checklists', $request->getFields())
            ->serialize(new JsonApiSerializer($this->presenter->getBaseUrl()), 'checklists')
            ->setStatusCode(200)
            ->renderItem($checklist);
    }

    public function destroy($id)
    {
        $checklist = $this->repository->find($id);

        $this->authorize($checklist);

        $this->repository->delete($checklist);

        return response(null, 204);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Auth $auth, UpdateChecklistRequest $request, $id)
    {
        $checklist = $this->repository->find($id);
        $this->authorize($checklist);

        $objectDomain = $request->getAttribute('object_domain', $checklist->getAttribute('object_domain'));
        $objectId = $request->getAttribute('object_id', $checklist->getAttribute('object_id'));
        $description = $request->getAttribute('description', $checklist->getAttribute('description'));
        $urgency = $request->getAttribute('urgency', $checklist->getAttribute('urgency'));
        $kwuid = $auth->user()->getKey();
        $checklist
            ->setAttribute('object_domain', $objectDomain)
            ->setAttribute('object_id', $objectId)
            ->setAttribute('description', $description)
            ->setAttribute('urgency', $urgency)
            ->setAttribute('updated_by', $kwuid);

        $newDueDate = $this->repository->getDueDateFormat(
            $request->getAttribute('due', $checklist->getAttribute('due'))
        );
        $checklist->setAttribute('due', $newDueDate);

        $updatedChecklist = $this->repository->update($checklist);

        return $this->presenter->setTransformer(new ChecklistTransformer())
            ->setCustomBaseUrl($checklist, true)
            ->serialize(new JsonApiSerializer($this->presenter->getBaseUrl()), 'checklists')
            ->setStatusCode(200)
            ->renderItem($updatedChecklist);
    }
}
