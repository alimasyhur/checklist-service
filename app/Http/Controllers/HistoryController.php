<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Services\History\Presenters\HistoryTransformer;
use App\Services\History\Repositories\HistoryRepository;
use CorePackage\Infrastructures\Adapters\SearchAdapterBuilder;
use CorePackage\Requests\JsonApiRequest;
use Libs\Presenters\JsonApiPresenterInterface;
use Libs\Presenters\JsonApiSerializer;

class HistoryController extends Controller
{
    protected $presenter;

    protected $repository;

    public function __construct(JsonApiPresenterInterface $presenter, HistoryRepository $repository)
    {
        parent::__construct($presenter);
        $this->repository = $repository;
        $this->presenter = $presenter;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SearchRequest $request)
    {
        $adapterBuilder = new SearchAdapterBuilder();

        $adapter = $adapterBuilder
            ->setPager($request->getLimit(), $request->getOffset(), $request->all())
            ->setSorter($request->getSorts())
            ->addFields($request->getFields())
            ->addResources($request->getIncludes())
            ->addFilters($request->getFilters())
            ->build();

        $histories = $this->repository->browse($adapter);

        return $this->presenter->setTransformer(new HistoryTransformer())
            ->parseIncludes($request->getIncludes())
            ->serialize(new JsonApiSerializer($this->presenter->getBaseUrl()), 'checklists/histories')
            ->setStatusCode(200)
            ->renderCollection($histories);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(JsonApiRequest $request, $id)
    {
        $history = $this->repository->find($id);

        return $this->presenter->setTransformer(new HistoryTransformer())
            ->parseIncludes($request->getIncludes())
            ->parseSparseFieldsets('histories', $request->getFields())
            ->serialize(new JsonApiSerializer($this->presenter->getBaseUrl()), 'checklists/histories')
            ->setStatusCode(200)
            ->renderItem($history);
    }
}
