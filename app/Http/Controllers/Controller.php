<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Routing\Controller as BaseController;
use Libs\Presenters\ApiResponseTrait;
use Libs\Presenters\JsonApiPresenterInterface;

class Controller extends BaseController
{
    use ApiResponseTrait;

    protected $service;
    protected $presenter;

    public function __construct(JsonApiPresenterInterface $presenter)
    {
        $this->presenter = $presenter;
        $this->presenter->setBaseUrl(env('PRESENTER_BASE_URL', url('/api/v1')));
    }

    public function getEntity(int $resourceId)
    {
        $entity = $this->repository->find($resourceId);

        if (! $entity) {
            throw new ModelNotFoundException();
        }

        return $entity;
    }
}
