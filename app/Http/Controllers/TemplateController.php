<?php

namespace App\Http\Controllers;

use App\Http\Requests\AssignsTemplateRequest;
use App\Http\Requests\AssignTemplateRequest;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\StoreTemplateRequest;
use App\Http\Requests\UpdateTemplateRequest;
use App\Services\Checklists\Presenters\ChecklistTransformer;
use App\Services\Roles\RoleService;
use App\Services\Template\Presenters\TemplateTransformer;
use App\Services\Template\Repositories\TemplateRepository;
use App\Services\Template\TemplateEloquent;
use CorePackage\Infrastructures\Adapters\SearchAdapterBuilder;
use CorePackage\Requests\JsonApiRequest;
use Illuminate\Contracts\Auth\Factory as Auth;
use Libs\Presenters\JsonApiPresenterInterface;
use Libs\Presenters\JsonApiSerializer;

class TemplateController extends Controller
{
    protected $presenter;

    protected $repository;

    protected $roleService;

    public function __construct(
        JsonApiPresenterInterface $presenter,
        TemplateRepository $repository,
        RoleService $roleService
    ) {
        parent::__construct($presenter);
        $this->repository = $repository;
        $this->presenter = $presenter;
        $this->roleService = $roleService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SearchRequest $request)
    {
        $adapterBuilder = new SearchAdapterBuilder();

        $adapter = $adapterBuilder
            ->setPager($request->getLimit(), $request->getOffset(), $request->all())
            ->setSorter($request->getSorts())
            ->addFields($request->getFields())
            ->addFilters($request->getFilters())
            ->build();

        $templates = $this->repository->browse($adapter);

        return $this->presenter->setTransformer(new TemplateTransformer())
            ->setCustomBaseUrl($request)
            ->parseIncludes($request->getIncludes())
            ->serialize(new JsonApiSerializer($this->presenter->getBaseUrl()), 'templates')
            ->setStatusCode(200)
            ->renderCollection($templates);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(JsonApiRequest $request, $id)
    {
        $template = $this->repository->find($id);
        $this->authorize($template);

        return $this->presenter->setTransformer(new TemplateTransformer())
            ->setCustomBaseUrl($template)
            ->parseIncludes($request->getIncludes())
            ->parseSparseFieldsets('templates', $request->getFields())
            ->serialize(new JsonApiSerializer($this->presenter->getBaseUrl()), 'templates')
            ->setStatusCode(200)
            ->renderItem($template);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Auth $auth, StoreTemplateRequest $request, TemplateEloquent $template)
    {
        $user = $auth->user();

        $data = [
            'checklist' => $request->getAttribute('checklist'),
            'items' => $request->getAttribute('items'),
        ];

        $userId = $user->getKey();
        if (in_array(config('app.env'), ['local', 'dev', 'qa'])) {
            $userId = $request->getAttribute('user_id') ?: $userId;
        }

        $template
            ->setAttribute('name', $request->getAttribute('name'))
            ->setAttribute('user_id', $userId)
            ->setAttribute('data', $data);

        if ($user->isService() &&
            $user->getConsumer() == 'mcGateway' &&
            $this->roleService->hasDestructiveAction($user)
        ) {
            $template->setAttribute('org_id', $user->getKwoid());
        }

        $newTemplate = $this->repository->create($template);

        return $this->presenter->setTransformer(new TemplateTransformer())
            ->setCustomBaseUrl($template)
            ->serialize(new JsonApiSerializer($this->presenter->getBaseUrl()), 'templates')
            ->setStatusCode(201)
            ->renderItem($newTemplate);
    }

    public function destroy($id)
    {
        $template = $this->repository->find($id);

        $this->authorize($template);

        $this->repository->delete($template);

        return response(null, 204);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTemplateRequest $request, $id)
    {
        $template = $this->repository->find($id);
        $this->authorize($template);

        $data = [
            'checklist' => $request->getAttribute('checklist'),
            'items' => $request->getAttribute('items'),
        ];

        $template
            ->setAttribute('name', $request->getAttribute('name', $template->getAttribute('name')))
            ->setAttribute('data', $data);

        $updatedTemplate = $this->repository->update($template);

        return $this->presenter->setTransformer(new TemplateTransformer())
            ->setCustomBaseUrl($template)
            ->serialize(new JsonApiSerializer($this->presenter->getBaseUrl()), 'templates')
            ->setStatusCode(200)
            ->renderItem($updatedTemplate);
    }

    public function assign(AssignTemplateRequest $request, $id)
    {
        $template = $this->repository->find($id);
        $this->authorize($template);

        $objectDomain = $request->getAttribute('object_domain');
        $objectId = $request->getAttribute('object_id');

        $checklist = $this->repository->assign($template, $objectDomain, $objectId);

        return $this->presenter->setTransformer(new ChecklistTransformer())
            ->setCustomBaseUrl($template)
            ->parseIncludes(['items'])
            ->serialize(new JsonApiSerializer($this->presenter->getBaseUrl()), 'checklists')
            ->setStatusCode(201)
            ->renderItem($checklist);
    }

    public function assigns(AssignsTemplateRequest $request, $id)
    {
        $requestData = $request->getData();
        $template = $this->repository->find($id);
        $this->authorize($template);

        $checklists = [];
        foreach ($requestData as $data) {
            $objectDomain = array_get($data, 'attributes.object_domain');
            $objectId = array_get($data, 'attributes.object_id');
            $checklists[] = $this->repository->assign($template, $objectDomain, $objectId);
        }

        return $this->presenter->setTransformer(new ChecklistTransformer())
            ->setCustomBaseUrl($template)
            ->parseIncludes(['items'])
            ->serialize(new JsonApiSerializer($this->presenter->getBaseUrl()), 'checklists')
            ->setStatusCode(201)
            ->renderCollection(collect($checklists));
    }
}
