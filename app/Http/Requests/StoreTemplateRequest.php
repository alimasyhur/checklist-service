<?php

namespace App\Http\Requests;

use CorePackage\Requests\FormRequest;
use CorePackage\Requests\Traits\JsonApiRequestTrait;
use Illuminate\Support\Facades\Auth;

class StoreTemplateRequest extends FormRequest
{
    use JsonApiRequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data.attributes.name'                   => 'required|string',
            'data.attributes.checklist'              => 'required|array',
            'data.attributes.checklist.description'  => 'required|string',
            'data.attributes.checklist.due_interval' => 'integer|min:1',
            'data.attributes.checklist.due_unit'     => 'string|in:minute,hour,day,week,month'
                    . '|required_with:data.attributes.checklist.due_interval',
            'data.attributes.items'                  => 'array',
            'data.attributes.items.*.description'    => 'required|string',
            'data.attributes.items.*.due_interval'   => 'integer|min:1',
            'data.attributes.items.*.due_unit'       => 'string|in:minute,hour,day,week,month'
                    . '|required_with:data.attributes.items.*.due_interval',
        ];
    }
}
