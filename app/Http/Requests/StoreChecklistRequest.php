<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use CorePackage\Requests\FormRequest;
use CorePackage\Requests\Traits\JsonApiRequestTrait;

class StoreChecklistRequest extends FormRequest
{
    use JsonApiRequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data.attributes.object_domain' => 'required',
            'data.attributes.description'   => 'required',
            'data.attributes.object_id'     => 'required',
            'data.attributes.due'           => 'nullable|date',
        ];
    }
}
