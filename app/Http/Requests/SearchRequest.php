<?php

namespace App\Http\Requests;

use CorePackage\Infrastructures\Adapters\SearchAdapterBuilder;
use CorePackage\Requests\FormRequest;
use CorePackage\Requests\Traits\JsonApiRequestTrait;
use Illuminate\Support\Facades\Auth;

class SearchRequest extends FormRequest
{
    use JsonApiRequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->getFilters()) {
            $this->sanitize();
        }

        return [];
    }

    /**
     * Replace CURRENT_USER with Auth::id().
     */
    public function sanitize()
    {
        $input = $this->all();

        foreach ($this->getFilters() as $key => $filter) {
            foreach ($filter as $filterKey => $operator) {
                if ('CURRENT_USER' == $operator) {
                    $operator = Auth::id();
                }

                $input['filter'][$key][$filterKey] = $operator;
            }
        }

        $this->replace($input);
    }

    public function getAdapter()
    {
        $adapterBuilder = new SearchAdapterBuilder();

        return $adapterBuilder
            ->setPager($this->getLimit(), $this->getOffset(), $this->all())
            ->setSorter($this->getSorts())
            ->addFields($this->getFields())
            ->addResources($this->getIncludes())
            ->addFilters($this->getFilters())
            ->build();
    }
}
