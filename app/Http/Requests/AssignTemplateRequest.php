<?php

namespace App\Http\Requests;

use CorePackage\Requests\FormRequest;
use CorePackage\Requests\Traits\JsonApiRequestTrait;
use Illuminate\Support\Facades\Auth;

class AssignTemplateRequest extends FormRequest
{
    use JsonApiRequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data.attributes.object_domain' => 'required',
            'data.attributes.object_id'     => 'required',
        ];
    }
}
