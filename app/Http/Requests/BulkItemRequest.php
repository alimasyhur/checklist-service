<?php

namespace App\Http\Requests;

use CorePackage\Requests\Traits\JsonApiRequestTrait;
use Illuminate\Support\Facades\Auth;

class BulkItemRequest extends StoreItemsRequest
{
    use JsonApiRequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data' => 'array|min:1',
            'data.*.id' => 'required',
            'data.*.action' => 'required|in:update',
            'data.*.attributes.description' => 'string|nullable|' . $this->generateRequiredWithoutAll('description'),
            'data.*.attributes.due' => 'string|nullable|' . $this->generateRequiredWithoutAll('due'),
            'data.*.attributes.urgency' => 'integer|nullable|' . $this->generateRequiredWithoutAll('urgency'),
            'data.*.attributes.task_id' => 'string|nullable|' . $this->generateRequiredWithoutAll('task_id'),
            'data.*.attributes.assignee_id' => 'integer|nullable|' . $this->generateRequiredWithoutAll('assignee_id'),
            'data.*.attributes.notify_client' => 'boolean',
            'data.*.attributes.client_update_sent_at' => 'nullable|date',
        ];
    }

    private function generateRequiredWithoutAll($field)
    {
        $validation = collect([
            "data.*.attributes.description",
            "data.*.attributes.due",
            "data.*.attributes.urgency",
            "data.*.attributes.task_id",
            "data.*.attributes.assignee_id",
        ]);

        foreach ($validation as $key => $value) {
            if ($value == "data.*.attributes.$field") {
                unset($validation[$key]);
            }
        }

        return "required_without_all:" . implode(',', $validation->all());
    }
}
