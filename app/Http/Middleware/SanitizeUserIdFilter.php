<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Symfony\Component\HttpFoundation\ParameterBag;

class SanitizeUserIdFilter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = app(Auth::class)->user();
        $queryString = $request->query();
        $queryString['filter']['user_id'] = ['is' => $user->getKey()];
        $request->query = new ParameterBag($queryString);

        return $next($request);
    }
}
