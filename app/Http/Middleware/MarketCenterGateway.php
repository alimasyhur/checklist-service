<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\ParameterBag;
use Illuminate\Support\Facades\App;
use KWRI\KongHandler\Models\ServiceAccount;

class MarketCenterGateway
{
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if ($user->isService() && $user->getConsumer() == 'mcGateway') {
            $fakeOrgIds = config('app.fake_org_ids');
            if (in_array($user->getKwoid(), array_keys($fakeOrgIds))) {
                $request = $this->fakeUser($request);
                $user = $request->user();
            }

            $queryString = $request->query();
            $queryString['filter']['org_id'] = ['is' => $user->getKwoid()];
            array_forget($queryString, 'filter.user_id');
            $request->query = new ParameterBag($queryString);

            return $next($request);
        }

        return $next($request);
    }

    private function fakeUser($request)
    {
        if (!App::environment(['local', 'testing', 'dev', 'qa'])) {
            return $request;
        }

        $user = $request->user();
        $fakeOrgIds = config('app.fake_org_ids');

        $fakeUser = new ServiceAccount([
            'info' => $user->getInfo(),
            'key' => $user->getKey(),
            'kwoid' => $fakeOrgIds[$user->getKwoid()],
            'kwGroupMembers' => $user->getKwGroupMembers(),
            'token' => 'Bearer ' . $user->getToken(),
            'timezone' => 'UTC',
            'isService' => $user->isService(),
            'consumer' => $user->getConsumer(),
            'audience' => $user->getAudience(),
            'isFaked' => true
        ]);

        $request->setUserResolver(function () use ($fakeUser) {
            return $fakeUser;
        });

        return $request;
    }
}
