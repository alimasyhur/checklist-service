<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Symfony\Component\HttpFoundation\ParameterBag;
use App\Services\PNO\PNOService;

class TeamGateway
{
    /**
     * @var PNOService
     */
    private $pno;

    /**
     * Create a new policy instance.
     */
    public function __construct(PNOService $pno)
    {
        $this->pno = $pno;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = app(Auth::class)->user();
        if ($user->isService() && $user->getConsumer() == 'teamGateway') {
            $queryString = $request->query();
            $ids = $this->pno->appendInactiveMembersToTeam(
                $user->getToken(),
                $user->getKwoid(),
                $user->getKwGroupMembers()
            );

            $ids = implode(',', $ids);

            $queryString['filter']['user_id'] = ['in' => $ids];
            $request->query = new ParameterBag($queryString);
            
            return $next($request);
        }
        
        return $next($request);
    }
}
