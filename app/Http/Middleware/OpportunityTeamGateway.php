<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Symfony\Component\HttpFoundation\ParameterBag;
use Illuminate\Auth\Access\AuthorizationException;
use App\Services\PNO\PNOService;

class OpportunityTeamGateway
{
    /**
     * @var PNOService
     */
    private $pno;

    /**
     * Create a new policy instance.
     */
    public function __construct(PNOService $pno)
    {
        $this->pno = $pno;
    }

    public function handle($request, Closure $next)
    {
        $currentUser = app(Auth::class)->user();
        $queryString = $request->query();

        $queryString = $this->changeCreatedByToUserId($queryString);

        if ($currentUser->isService() && $currentUser->getConsumer() == 'teamGateway') {
            $teamMembers = collect($this->pno->appendInactiveMembersToTeam(
                $currentUser->getToken(),
                $currentUser->getKwoid(),
                $currentUser->getKwGroupMembers()
            ));

            $assigneeIds = array_get($queryString, 'filter.assignee_id', []);
            foreach ($assigneeIds as $key => $assigneeId) {
                $ids = collect(explode(',', $assigneeId));
                $diff = $ids->diff($teamMembers);
                if ($diff->count() > 0) {
                    throw new AuthorizationException("Assignee {$diff->implode(',')} is not in your team member");
                }
            }

            $userIds = array_get($queryString, 'filter.user_id', []);
            foreach ($userIds as $userId) {
                $ids = collect(explode(',', $userId));
                $diff = $ids->diff($teamMembers);
                if ($diff->count() > 0) {
                    throw new AuthorizationException("Created By {$diff->implode(',')} is not in your team member");
                }
            }

            if (empty($userIds)) {
                array_set($queryString, 'filter.user_id.in', $teamMembers->implode(','));
            }

            $request->query = new ParameterBag($queryString);

            return $next($request);
        }

        if ($this->isFindingOwnAssigneeId($currentUser, $queryString)) {
            array_forget($queryString, 'filter.user_id');
        } else {
            array_forget($queryString, 'filter.user_id');
            array_set($queryString, 'filter.user_id.is', $currentUser->getKey());
        }

        $request->query = new ParameterBag($queryString);

        return $next($request);
    }

    private function changeCreatedByToUserId($queryString)
    {
        $createdByValues = array_get($queryString, 'filter.created_by', []);
        $values = [];
        foreach ($createdByValues as $val) {
            $vals = explode(',', $val);
            $values = array_merge($values, $vals);
        }

        if (count($values) > 1) {
            $values = implode(',', $values);
            array_set($queryString, 'filter.user_id.in', $values);
        } elseif (count($values) > 0) {
            array_set($queryString, 'filter.user_id.is', $values[0]);
        }

        unset($queryString['filter']['created_by']);

        return $queryString;
    }

    private function isFindingOwnAssigneeId($currentUser, $queryString)
    {
        if ($currentUser->getKey() == array_get($queryString, 'filter.assignee_id.is')) {
            return true;
        }

        return false;
    }
}
