<?php

use Illuminate\Support\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name'  => $faker->name,
        'email' => $faker->email,
    ];
});

// ChecklistEloquent model factory

$factory->define(App\Services\Checklists\ChecklistEloquent::class, function (Faker\Generator $faker) {
    $todayStart = Carbon::now()->toDatetimeString();
    $todayEnd = Carbon::now()->addMinutes(1)->toDatetimeString();

    return [
        'object_domain' => $faker->word,
        'object_id'     => $faker->uuid,
        'description'   => $faker->text,
        'due'           => $faker->dateTimeBetween($todayStart, $todayEnd),
        'urgency'       => rand(1, 10),
        'is_completed'  => 0,
        'completed_at'  => null,
        'updated_by'    => null,
        'user_id'       => rand(556397, 556398),
    ];
});

// ItemEloquent model factory

$factory->define(App\Services\Items\ItemEloquent::class, function (Faker\Generator $faker) {
    $todayStart = Carbon::now()->toDatetimeString();
    $todayEnd = Carbon::now()->addMinutes(1)->toDatetimeString();

    return [
        'meta'         => [$faker->word => $faker->word],
        'description'  => $faker->text,
        'is_completed' => $faker->boolean,
        'due'          => $faker->dateTimeBetween($todayStart, $todayEnd),
        'urgency'      => rand(1, 10),
        'completed_at' => null,
        'completed_by' => null,
        'updated_by'   => null,
        'checklist_id' => rand(20, 3),
        'user_id'      => rand(556397, 556398),
        'assignee_id'  => rand(556397, 556398),
        'task_id'      => $faker->uuid,
        'notify_client' => false,
        'client_update_sent_at' => $faker->dateTimeBetween($todayStart, $todayEnd),
    ];
});

$factory->define(App\Services\Template\TemplateEloquent::class, function (Faker\Generator $faker) {
    return [
        'name'    => 'test-' . implode('-', $faker->words(3)),
        'user_id' => 556396,
        'data'    => [
            'checklist' => [
                'description'  => 'test-' . implode('-', $faker->words(3)) . '-checklist',
                'due_interval' => 3,
                'due_unit'     => 'hour',
            ],
            'items' => [
                [
                    'meta'         => [$faker->word => $faker->word],
                    'description'  => 'test-' . implode('-', $faker->words(3)) . '-item',
                    'urgency'      => 2,
                    'due_interval' => 40,
                    'due_unit'     => 'minute',
                    'notify_client' => false
                ],
                [
                    'meta'         => [$faker->word => $faker->word],
                    'description'  => 'test-' . implode('-', $faker->words(3)) . '-item',
                    'urgency'      => 3,
                    'due_interval' => 30,
                    'due_unit'     => 'minute',
                    'notify_client' => false
                ],
            ],
        ],
        'org_id' => $faker->randomDigit,
    ];
});
