<?php

use Illuminate\Database\Seeder;

class CreateChecklistItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        factory(ChecklistEloquent::class, 20)->create(['user_id' => 556396]);
        factory(ChecklistEloquent::class, 20)->create(['user_id' => 556397]);

        foreach (ChecklistEloquent::get() as $checklist) {
            factory(ItemEloquent::class, 30)->create([
                'checklist_id' => $checklist->id,
                'user_id'      => $checklist->user_id,
            ]);
        }
    }
}
