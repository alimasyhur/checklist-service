<?php

use App\Services\Checklists\ChecklistEloquent;
use App\Services\Items\ItemEloquent;
use Illuminate\Database\Seeder;

class TestNotificationJobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        factory(ChecklistEloquent::class, 1)->create(['user_id' => 556396]);
        // factory(ChecklistEloquent::class, 1)->create(['user_id' => 556397]);

        foreach (ChecklistEloquent::get() as $checklist) {
            factory(ItemEloquent::class, 2)->create([
                'checklist_id' => $checklist->id,
                'user_id'      => $checklist->user_id,
            ]);
        }
    }
}
