<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTaskIdToItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('checklists', function (Blueprint $table) {
            $table->dropColumn(['task_id']);
        });

        Schema::table('items', function (Blueprint $table) {
            $table->uuid('task_id')->after('user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('checklists', function (Blueprint $table) {
            $table->uuid('task_id')->after('user_id')->nullable();
        });

        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn('task_id');
        });
    }
}
