<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('checklists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('object_domain');
            $table->string('object_id');
            $table->string('description');
            $table->boolean('is_completed')->default(false);
            $table->timestamp('due')->nullable();
            $table->integer('urgency')->default(0);
            $table->timestamp('completed_at')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('user_id');
            $table->softDeletes();
            $table->index(['object_domain', 'object_id', 'user_id']);
            $table->index(['is_completed', 'due']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('checklists');
    }
}
