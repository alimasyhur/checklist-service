<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrgIdToItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->unsignedInteger('org_id')->nullable()->after('user_id');

            $table->index([
                'org_id', 'description', 'completed_at', 'checklist_id',
            ], 'items_description_completed_at_checklist_id_org_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('items', 'org_id')) {
            Schema::table('items', function (Blueprint $table) {
                $table->dropColumn('org_id');

                $table->dropIndex('items_description_completed_at_checklist_id_org_id_index');
            });
        }
    }
}
