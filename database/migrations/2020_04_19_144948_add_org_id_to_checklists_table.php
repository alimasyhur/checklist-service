<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrgIdToChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('checklists', function (Blueprint $table) {
            $table->unsignedInteger('org_id')->nullable()->after('user_id');

            $table->index(['org_id', 'object_domain', 'object_id'
            ], 'checklists_org_id_object_domain_object_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('checklists', 'org_id')) {
            Schema::table('checklists', function (Blueprint $table) {
                $table->dropColumn('org_id');
                $table->dropIndex('checklists_org_id_object_domain_object_id_index');
            });
        }
    }
}
