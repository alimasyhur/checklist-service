<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrgIdToTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('templates', function (Blueprint $table) {
            $table->unsignedInteger('org_id')->nullable()->after('user_id');

            $table->index(['org_id', 'name'], 'templates_org_id_name_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('templates', 'org_id')) {
            Schema::table('templates', function (Blueprint $table) {
                $table->dropColumn('org_id');

                $table->dropIndex('templates_org_id_name_index');
            });
        }
    }
}
