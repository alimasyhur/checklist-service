<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->boolean('is_completed')->default(false);
            $table->timestamp('due')->nullable();
            $table->integer('urgency')->default(0);
            $table->timestamp('completed_at')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('checklist_id');
            $table->integer('user_id');
            $table->softDeletes();
            $table->index(['description', 'completed_at', 'checklist_id', 'user_id']);
            $table->index(['is_completed', 'due']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
