<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('loggable_type');
            $table->unsignedInteger('loggable_id');
            $table->string('action');
            $table->unsignedInteger('kwuid');
            $table->string('value')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->index(['loggable_type', 'loggable_id']);
            $table->index(['kwuid', 'loggable_type', 'loggable_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histories');
    }
}
