<?php

namespace Test\Jobs\Audit;

use App\Services\Consumers\ConsumersService;
use App\Services\Deals\DealService;
use App\Services\PNO\PNOService;
use Illuminate\Support\Arr;
use Mockery;

trait AuditTrait
{
    private $token = 'token';
    private $org = [
        'id' => 1,
        'type' => 'Team',
    ];

    public function mockConsumer()
    {
        $consumer  = Mockery::mock(ConsumersService::class, function ($mock) {
            $mock->shouldReceive('createConsumerToken')->andReturn([
                'jwt' => $this->token,
            ]);
        });
        $this->app->instance(ConsumersService::class, $consumer);

        return $consumer;
    }

    public function mockPNO()
    {
        $this->app->instance(PNOService::class, Mockery::mock(PNOService::class, function ($mock) {
            return $mock->shouldReceive('getUserOrgs')
                ->withArgs([$this->token, $this->user->getKey()])
                ->andReturn([
                    'data' => [
                        [
                            'id' => $this->org['id'],
                            'name' => $this->org['type'],
                        ],
                    ],
                ]);
        }));
    }

    public function mockDeal($item)
    {
        $deal = Mockery::mock(DealService::class, function ($mock) use ($item) {
            $mock->shouldReceive('getDeal')
                ->withArgs([$this->token, $item->checklist->object_id])
                ->andReturn([
                    'data' => [$this->getDeal($item)],
                ]);
        });
        $this->app->instance(DealService::class, $deal);

        return $deal;
    }

    public function getMoreInfo(array $deal): string
    {
        return implode(';', [
            'deal_id:' . Arr::get($deal, 'id'),
            'mc_id:' . Arr::get($deal, 'attributes.mc_id'),
            'opportunity_id:' . Arr::get($deal, 'attributes.opportunity_id'),
            'opportunity_name:' . Arr::get($deal, 'attributes.deal_name'),
            'opportunity_type:' . Arr::get($deal, 'attributes.type'),
        ]);
    }

    public function getDeal($item)
    {
        return [
            'id' => $item->checklist->getAttribute('object_id'),
            'attributes' => [
                'mc_id' => 100,
                'opportunity_id' => 'O135-13',
                'deal_name' => 'Auto Deal',
                'type' => 'Buyer',
            ],
            'links' => [
                'Self' => url('/'),
            ]
        ];
    }
}
