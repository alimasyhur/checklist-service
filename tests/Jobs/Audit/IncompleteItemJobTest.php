<?php

namespace Test\Jobs\Audit;

use App\Jobs\Audit\IncompleteItemJob;
use App\Services\Audit\PublisherService;
use App\Services\Checklists\ChecklistEloquent;
use App\Services\Items\ItemEloquent;
use Illuminate\Support\Arr;
use Test\Jobs\JobTestCase;
use Mockery;

class IncompleteItemJobTest extends JobTestCase
{
    use AuditTrait;

    public function testHandle(): void
    {
        ChecklistEloquent::unsetEventDispatcher();
        $item = factory(ItemEloquent::class)->create([
            'checklist_id' => factory(ChecklistEloquent::class)->create(),
        ]);

        $this->mockConsumer();
        $this->mockDeal($item);
        $this->mockPNO();

        $deal = $this->getDeal($item);

        $audit = Mockery::mock(PublisherService::class);
        $audit->shouldReceive('setToken')
            ->with($this->token)
            ->andReturn($audit);
        $audit->shouldReceive('setUser')
            ->with([
                'id' => (int) $this->user->getKey(),
                'name' => $this->user->getInfo()->getName(),
            ])
            ->andReturn($audit);
        $audit->shouldReceive('setOrg')
            ->with($this->org)
            ->andReturn($audit);
        $audit->shouldReceive('publishMessage')
            ->with([
                'type' => config('audit.type'),
                'sub_type' => config('audit.sub_type.item.incomplete'),
                'timestamp' => $item->getAttribute('created_at')->toIso8601ZuluString(),
                'message' => vsprintf('Checklist Item "%s" marked as incomplete for Opportunity %s by %s.', [
                    $item->getAttribute('description'),
                    Arr::get($deal, 'attributes.opportunity_id', $item->checklist->getAttribute('object_id')),
                    $this->user->getInfo()->getName(),
                ]),
                'more_info' => $this->getMoreInfo($deal),
                'link' => url('/'),
            ])
            ->andReturn([]);

        $job = new IncompleteItemJob($this->user, $item);
        $job->handle($audit);

        $this->assertEquals($job->getMoreInfo($deal), $this->getMoreInfo($deal));
    }
}
