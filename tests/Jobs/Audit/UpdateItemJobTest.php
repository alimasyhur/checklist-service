<?php

namespace Test\Jobs\Audit;

use App\Jobs\Audit\UpdateItemJob;
use App\Services\Audit\PublisherService;
use App\Services\Checklists\ChecklistEloquent;
use App\Services\Items\ItemEloquent;
use App\Services\Users\UserService;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Support\Arr;
use Test\Jobs\JobTestCase;
use Mockery;

class UpdateItemJobTest extends JobTestCase
{
    use AuditTrait;

    private $messages = [
        'description' => 'Checklist Item "{{ITEM}}" updated for {{DEAL}} from "{{OLD}}" to "{{NEW}}" by {{USER}}.',
        'due' => 'Checklist Item "{{ITEM}}" due changed for {{DEAL}} from "{{OLD}}" to "{{NEW}}" by {{USER}}',
        'assignee' => 'Checklist Item "{{ITEM}}" assigned for {{DEAL}} from "{{OLD}}" to "{{NEW}}" by {{USER}}',
    ];

    public function testHandle(): void
    {
        $faker = Factory::create();

        ChecklistEloquent::unsetEventDispatcher();
        $item = factory(ItemEloquent::class)->create([
            'checklist_id' => factory(ChecklistEloquent::class)->create(),
        ]);

        $old = $item->getOriginal();

        $item->fill(['description' => $faker->text]);
        $item->save();

        $this->mockConsumer();
        $this->mockDeal($item);
        $this->mockPNO();

        $deal = $this->getDeal($item);

        $audit = Mockery::mock(PublisherService::class);
        $audit->shouldReceive('setToken')
            ->with($this->token)
            ->andReturnSelf();
        $audit->shouldReceive('setUser')
            ->with([
                'id' => (int) $this->user->getKey(),
                'name' => $this->user->getInfo()->getName(),
            ])
            ->andReturnSelf();
        $audit->shouldReceive('setOrg')
            ->with($this->org)
            ->andReturnSelf();

        $placeholders = [
            '{{USER}}' => $this->user->getInfo()->getName(),
            '{{ITEM}}' => $item->description,
            '{{DEAL}}' => Arr::get($deal, 'attributes.opportunity_id', Arr::get($deal, 'id')),
            '{{OLD}}' => Arr::get($old, 'description'),
            '{{NEW}}' => $item->getAttribute('description'),
        ];
        $message = $this->messages['description'];

        $audit->shouldReceive('publishMessage')
            ->with([
                'type' => config('audit.type'),
                'sub_type' => config('audit.sub_type.item.edit'),
                'timestamp' => $item->getAttribute('created_at')->toIso8601ZuluString(),
                'message' => str_replace(array_keys($placeholders), array_values($placeholders), $message),
                'more_info' => $this->getMoreInfo($deal),
                'link' => url('/'),
            ])
            ->andReturn([]);

        $job = new UpdateItemJob($this->user, $item, $old);
        $job->handle($audit);

        $this->assertEquals($job->getMoreInfo($deal), $this->getMoreInfo($deal));
    }

    public function testHandleDueUpdated(): void
    {
        ChecklistEloquent::unsetEventDispatcher();
        $item = factory(ItemEloquent::class)->create([
            'checklist_id' => factory(ChecklistEloquent::class)->create(),
        ]);

        $old = $item->getOriginal();

        $item->fill(['due' => Carbon::now()->addDay()]);
        $item->save();

        $this->mockConsumer();
        $this->mockDeal($item);
        $this->mockPNO();

        $deal = $this->getDeal($item);

        $audit = Mockery::mock(PublisherService::class);
        $audit->shouldReceive('setToken')
            ->with($this->token)
            ->andReturnSelf();
        $audit->shouldReceive('setUser')
            ->with([
                'id' => (int) $this->user->getKey(),
                'name' => $this->user->getInfo()->getName(),
            ])
            ->andReturnSelf();
        $audit->shouldReceive('setOrg')
            ->with($this->org)
            ->andReturnSelf();

        $placeholders = [
            '{{USER}}' => $this->user->getInfo()->getName(),
            '{{ITEM}}' => $item->description,
            '{{DEAL}}' => Arr::get($deal, 'attributes.opportunity_id', Arr::get($deal, 'id')),
            '{{OLD}}' => Arr::get($old, 'due'),
            '{{NEW}}' => $item->due,
        ];
        $message = $this->messages['due'];

        $audit->shouldReceive('publishMessage')
            ->with([
                'type' => config('audit.type'),
                'sub_type' => config('audit.sub_type.item.edit'),
                'timestamp' => $item->getAttribute('created_at')->toIso8601ZuluString(),
                'message' => str_replace(array_keys($placeholders), array_values($placeholders), $message),
                'more_info' => $this->getMoreInfo($deal),
                'link' => url('/'),
            ])
            ->andReturn([]);

        $job = new UpdateItemJob($this->user, $item, $old);
        $job->handle($audit);

        $this->assertEquals($job->getMoreInfo($deal), $this->getMoreInfo($deal));
    }

    public function testHandleAssigneeUpdated(): void
    {
        ChecklistEloquent::unsetEventDispatcher();
        $item = factory(ItemEloquent::class)->create([
            'checklist_id' => factory(ChecklistEloquent::class)->create(),
        ]);

        $old = $item->getOriginal();

        $item->fill(['assignee_id' => 321]);
        $item->save();

        $consumer = $this->mockConsumer();
        $this->mockDeal($item);
        $this->mockPNO();

        $deal = $this->getDeal($item);

        $audit = Mockery::mock(PublisherService::class);
        $audit->shouldReceive('setToken')
            ->with($this->token)
            ->andReturnSelf();
        $audit->shouldReceive('setUser')
            ->with([
                'id' => (int) $this->user->getKey(),
                'name' => $this->user->getInfo()->getName(),
            ])
            ->andReturnSelf();
        $audit->shouldReceive('setOrg')
            ->with($this->org)
            ->andReturnSelf();

        // mock user service
        $this->app->instance(UserService::class, Mockery::mock(UserService::class, function ($mock) {
            $mock->shouldReceive('getProfile')
                ->with($this->token)
                ->andReturn([
                    'data' => [
                        'attributes' => [
                            'first_name' => 'John',
                            'last_name' => 'Doe',
                        ]
                    ]
                ]);
        }));

        $placeholders = [
            '{{USER}}' => $this->user->getInfo()->getName(),
            '{{ITEM}}' => $item->description,
            '{{DEAL}}' => Arr::get($deal, 'attributes.opportunity_id', Arr::get($deal, 'id')),
            '{{OLD}}' => 'John Doe',
            '{{NEW}}' => 'John Doe',
        ];
        $message = $this->messages['assignee'];

        $audit->shouldReceive('publishMessage')
            ->with([
                'type' => config('audit.type'),
                'sub_type' => config('audit.sub_type.item.edit'),
                'timestamp' => $item->getAttribute('created_at')->toIso8601ZuluString(),
                'message' => str_replace(array_keys($placeholders), array_values($placeholders), $message),
                'more_info' => $this->getMoreInfo($deal),
                'link' => url('/'),
            ])
            ->andReturn([]);

        $job = new UpdateItemJob($this->user, $item, $old);
        $job->handle($audit);

        $this->assertEquals($job->getMoreInfo($deal), $this->getMoreInfo($deal));
    }
}
