<?php

namespace Test\Jobs;

use KWRI\KongHandler\Models\User;
use KWRI\KongHandler\Models\UserInfo;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Test\TestCase;

class JobTestCase extends TestCase
{
    use DatabaseTransactions;

    protected $userID;
    protected $headers = [];
    protected $headersMCScope = [];
    protected $userinfo;
    protected $user;

    public function setUp()
    {
        parent::setUp();

        $userinfo = [
            'given_name' => 'kelle1',
            'password' => 'Password1',
            'client_id' => 'e3fb1740-d9e2-0135-3d40-06dd31cd45e266469',
            'updated_at' => '2018-01-23T07:19:36.950Z',
            'family_name' => 'kelle1',
            'preferred_username' => 'kelle1',
            'custom_fields' => ['KW_UID' => '556396'],
            'email' => 'kelle1@kw.com',
            'name' => 'kelle1 kelle1',
            'sub' => '37087183',
        ];
        $this->userID = (int) array_get($userinfo, 'custom_fields.KW_UID');
        $xUserInfo = json_encode($userinfo);
        $xConsumerUsername = 'onelogin';

        $this->headers = [
            'Authorization' => 'Bearer ABC',
            'x-userinfo' => $xUserInfo,
            'x-consumer-username' => $xConsumerUsername,
        ];
        $userInfo = new UserInfo(['email' => 'test@test.com', 'name' => 'test']);
        $this->userInfo = $userInfo;

        $this->user = new User([
            'timezone' => 'UTC',
            'token' => 'token',
            'kwoid' => null,
            'isService' => false,
            'info' => $userInfo,
            'key' => $this->userID,
        ]);

        $this->headersMCScope = [
            'Authorization' => 'Bearer ABC',
            'Content-Type' => 'application/vnd.api+json',
            'Accept' => 'application/vnd.api+json',
            'x-kwcommand-group' => 'mc=101',
        ];
    }
}
