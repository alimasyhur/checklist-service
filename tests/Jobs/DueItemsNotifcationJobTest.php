<?php

namespace Test\Jobs;

use App\Jobs\DueItemsNotificationJob;
use App\Services\Checklists\ChecklistEloquent;
use App\Services\Checklists\Repositories\ChecklistRepository;
use App\Services\Items\ItemEloquent;
use App\Services\Items\Repositories\ItemRepository;
use App\Jobs\SendNotificationJob;
use Carbon\Carbon;
use CorePackage\Testing\AssertJob;
use Illuminate\Support\Facades\Request;
use KWRI\KongHandler\Models\User;
use Mockery;

class DueItemsNotifcationJobTest extends JobTestCase
{
    use AssertJob;

    public function testHandle()
    {
        $request = Request::create('/', 'GET');
        $request->headers->replace($this->headersMCScope);
        $request->setUserResolver(function () {
            return $this->user;
        });

        ChecklistEloquent::unsetEventDispatcher();
        ItemEloquent::unsetEventDispatcher();
        $item = factory(ItemEloquent::class)->make([
            'due' => Carbon::now()->addDays(2),
            'is_completed' => false,
            'checklist_id' => $checklist = factory(ChecklistEloquent::class)->create([
                'is_completed' => false,
            ]),
        ]);

        $user = Mockery::mock(User::class);
        $checklistRepository = Mockery::mock(ChecklistRepository::class);
        $itemRepository = Mockery::mock(ItemRepository::class);

        $checklistRepository->shouldReceive('find')
            ->andReturn($checklist);
        $itemRepository->shouldReceive('find')
            ->andReturn($item);
        $user->shouldReceive('getKey')
            ->andReturn($request->user()->getKey());

        $this->assertJobDispatched(SendNotificationJob::class, [
            function () use ($item) {
                return $item->is_completed === false;
            },
        ]);

        $job = new DueItemsNotificationJob($item, $this->user);
        $job->handle($itemRepository, $checklistRepository);
    }
}
