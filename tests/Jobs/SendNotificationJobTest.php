<?php

namespace Test\Jobs;

use App\Services\Checklists\ChecklistEloquent;
use App\Services\Consumers\ConsumersService;
use App\Services\Notifications\Notification;
use App\Jobs\SendNotificationJob;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Mockery;

class SendNotificationJobTest extends JobTestCase
{
    public function testHandle()
    {
        ChecklistEloquent::unsetEventDispatcher();
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'due' => Carbon::now()->addDays(2),
            'is_completed' => false,
        ]);

        $consumer = Mockery::mock(ConsumersService::class);
        $consumer->shouldReceive('createConsumerToken')
            ->andReturn([
                'jwt' => '123',
            ]);

        $notification = Mockery::mock(Notification::class);

        $notification->shouldReceive('getFrom')
            ->andReturn([
                'id' => $this->userID,
                'email' => 'system',
            ]);
        $notification->shouldReceive('getTargets')
            ->andReturn([
                ['id' => $this->userID, 'type' => 'user']
            ]);
        $notification->shouldReceive('requestToken')
            ->andReturn('123');
        $notification->shouldReceive('toParams')
            ->andReturn($this->paramBuilder($checklist));

        $this->app->instance(Client::class, Mockery::mock(Client::class, function ($mock) {
            $mock->shouldReceive('request')->andReturn([]);
        }));

        $job = new SendNotificationJob($notification);
        $job->handle();
    }

    private function paramBuilder(ChecklistEloquent $checklist)
    {
        $message = $checklist->description . ' due date is ' . $checklist->due->toDateTimeString();
        $tags = [
            'notif',
            'checklist' . $checklist->id,
            'user' . $this->userID,
        ];

        $notification = (new Notification())
            ->setTitle('You have due checklist.')
            ->setMessage($message)
            ->setOrigin('web')
            ->setDomain($checklist->object_domain)
            ->setObjectID($checklist->object_id)
            ->setType('reminder')
            ->setTargets([
                ['type' => 'user', 'id' => $this->userID],
            ])
            ->setFrom($this->userID, 'system')
            ->setQueueTags($tags);

        return $notification->toParams();
    }
}
