<?php

namespace Test\Jobs;

use App\Jobs\DueChecklistsNotificationJob;
use App\Services\Checklists\ChecklistEloquent;
use App\Services\Checklists\Repositories\ChecklistRepository;
use App\Jobs\SendNotificationJob;
use Carbon\Carbon;
use CorePackage\Testing\AssertJob;
use Illuminate\Support\Facades\Request;
use KWRI\KongHandler\Models\User;
use Mockery;

class DueChecklistsNotificationJobTest extends JobTestCase
{
    use AssertJob;

    public function testHandle()
    {
        $request = Request::create('/', 'GET');
        $request->headers->replace($this->headersMCScope);
        $request->setUserResolver(function () {
            return $this->user;
        });

        ChecklistEloquent::unsetEventDispatcher();
        $checklist = factory(ChecklistEloquent::class)->create([
            'due' => Carbon::now()->addDays(2),
            'is_completed' => false,
        ]);

        $user = Mockery::mock(User::class);
        $checklistRepository = Mockery::mock(ChecklistRepository::class);

        $checklistRepository->shouldReceive('find')
            ->andReturn($checklist);
        $user->shouldReceive('getKey')
            ->andReturn($request->user()->getKey());

        $this->assertJobDispatched(SendNotificationJob::class, [
            function () use ($checklist) {
                return $checklist->is_completed === false;
            },
        ]);

        $job = new DueChecklistsNotificationJob($checklist, $this->user);
        $job->handle($checklistRepository);
    }
}
