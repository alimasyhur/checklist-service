<?php

namespace Test\Jobs;

use App\Jobs\SendItemAssignedEmailJob;
use App\Services\Checklists\ChecklistEloquent;
use App\Services\Consumers\ConsumersService;
use App\Services\Deals\DealService;
use App\Services\Emails\Mailjet;
use App\Services\Items\ItemEloquent;
use App\Services\Users\UserService;
use Illuminate\Support\Facades\DB;
use KWRI\KongHandler\Models\User;
use KWRI\SDK\Client;
use Test\TestCase;
use Mockery as m;

class SendItemAssignedEmailJobTest extends JobTestCase
{

    public function setUp()
    {
        parent::setUp();

        $this->userJson = '{
            "data": {
                "attributes":{
                    "first_name":"Michael",
                    "last_name":"Reid",
                    "email":"kelle@kw.com"
                }
            }
        }';

        $this->app->instance(UserService::class, m::mock(UserService::class, function ($mock) {
            $mock->shouldReceive('getProfile')->andReturn(
                json_decode($this->userJson, true)
            );
        }));

        $this->app->instance(ConsumersService::class, m::mock(ConsumersService::class, function ($mock) {
            $mock->shouldReceive('createConsumerToken')->andReturn([
                'jwt' => '123'
            ]);
        }));

        $kwc = m::mock(Client::class, function ($mock) {
            $mock->shouldReceive('preferences')->andReturn($mock)
                ->shouldReceive('all')->andReturn(collect(['data' => 'preferences']));
        });

        $this->app->bind(Client::class, function () use ($kwc) {
            return $kwc;
        });
    }


    public function testHandle()
    {
        $checklist = factory(ChecklistEloquent::class)->create();
        $item = factory(ItemEloquent::class)->make([
            'checklist_id' => $checklist->id
        ]);

        $user = m::mock(User::class);
        $dealService = m::mock(DealService::class);
        $mailjet = m::mock(Mailjet::class);
        $mailjet->shouldReceive('send');

        $job = new SendItemAssignedEmailJob($item, $user);
        $job->handle($mailjet, $dealService);
    }
}
