<?php

namespace Test\Jobs;

use App\Jobs\GroupedItemsNotificationJob;
use App\Services\Checklists\ChecklistEloquent;
use App\Services\Consumers\ConsumersService;
use App\Services\Deals\DealService;
use App\Services\Emails\Mailjet;
use App\Services\Items\ItemEloquent;
use App\Services\Items\Repositories\ItemRepository;
use App\Services\Users\UserService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use KWRI\SDK\Client;
use Mockery as m;
use Test\TestCase;

class GroupedItemsNotificationJobTest extends JobTestCase
{

    public function setUp()
    {
        parent::setUp();

        $this->userJson = '{
            "data": {
                "attributes":{
                    "first_name":"Michael",
                    "last_name":"Reid",
                    "email":"kelle@kw.com"
                }
            }
        }';

        $this->app->instance(ConsumersService::class, m::mock(ConsumersService::class, function ($mock) {
            $mock->shouldReceive('createConsumerToken')->andReturn([
                'jwt' => '123'
            ]);
        }));

        $kwc = m::mock(Client::class, function ($mock) {
            $mock->shouldReceive('preferences')->andReturn($mock)
                ->shouldReceive('all')->andReturn(collect(['data' => 'preferences']));
        });

        $this->app->bind(Client::class, function () use ($kwc) {
            return $kwc;
        });
    }

    public function testHandle()
    {
        $assigneeId = 112;
        $checklist = factory(ChecklistEloquent::class)->create([
            'object_domain' => 'deals'
        ]);
        $items = factory(ItemEloquent::class, 3)->make([
            'checklist_id' => $checklist->id,
            'assignee_id' => $assigneeId,
            'due' => Carbon::now()->toDateString(),
            'is_completed' => false
        ]);

        $sqlFriendlyArray = collect($items->toArray())
            ->map(function ($item) {
                $item['meta'] = json_encode($item['meta']);
                return $item;
            })->toArray();
        DB::table('items')->insert($sqlFriendlyArray);

        $due = Carbon::now();
        $itemRepository = m::mock(ItemRepository::class);
        $dealService = m::mock(DealService::class);
        $userService = m::mock(UserService::class);
        $mailjet = m::mock(Mailjet::class);

        $mailjet->shouldReceive('send');
        $dealService->shouldReceive('getDeal')
            ->andReturn('123');
        $userService->shouldReceive('getProfile')
            ->andReturn(json_decode($this->userJson, true));
        $itemRepository
            ->shouldReceive('getOpportunityTaskDueReminder')
            ->andReturn(
                DB::table('items')
                    ->select('items.*', 'checklists.object_domain', 'checklists.object_id')
                    ->leftJoin('checklists', 'items.checklist_id', '=', 'checklists.id')
                    ->where('checklists.object_domain', 'deals')
                    ->where('items.is_completed', false)
                    ->get()
            );

        $job = new GroupedItemsNotificationJob($assigneeId, $due);
        $job->handle($mailjet, $itemRepository, $dealService, $userService);
    }
}
