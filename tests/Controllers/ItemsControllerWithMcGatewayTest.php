<?php

namespace Test\Controllers;

use Carbon\Carbon;
use App\Services\Items\ItemEloquent;
use App\Services\Checklists\ChecklistEloquent;

class ItemsControllerWithMcGatewayTest extends ItemsControllerTestCase
{
    use \Codeception\Specify;

    public function setUp()
    {
        parent::setUp();

        $this->mockInactiveUsersResponse([$this->inactiveMemberId]);
    }

    public function testIndex()
    {
        $this->withoutEvents();
        $this->checklist1 = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
            'object_domain' => 'mc_recruits',
        ]);
        $this->checklist2 = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->faker->numberBetween(1000, 9000),
            'object_domain' => 'opportunity',
        ]);
        $this->items1 = factory(ItemEloquent::class, 4)->create([
            'user_id'      => $this->userID,
            'assignee_id'  => 123,
            'checklist_id' => $this->checklist1->id,
            'org_id'       => $this->checklist1->org_id,
        ]);
        $this->items2 = factory(ItemEloquent::class, 3)->create([
            'user_id'      => $this->userID,
            'assignee_id'  => 123,
            'checklist_id' => $this->checklist2->id,
            'org_id'       => $this->checklist2->org_id,
        ]);

        $this->should('response with success response', function () {
            $this->json('GET', "/api/v1/checklists/{$this->checklist1->id}/items", [], $this->postHeadersMcScope)
                ->seeStatusCode(200)
                ->seeJsonStructure([
                    'meta'  => ['count', 'total'],
                    'data'  => [$this->basicJsonStructure['data']],
                    'links' => ['first', 'last', 'next', 'prev'],
                ])->seeJson(['count' => count($this->items1)]);
            foreach ($this->items1 as $item) {
                $this->seeJson(['id' => (string) $item->id]);
            }
            foreach ($this->items2 as $item) {
                $this->dontSeeJson(['id' => (string) $item->id]);
            }
        });

        $this->should('response with forbidden access', function () {
            $this->json('GET', "/api/v1/checklists/{$this->checklist2->id}/items", [], $this->postHeadersMcScope)
                ->seeStatusCode(403);
        });
    }

    public function testIndexWithoutChecklist()
    {
        $this->withoutEvents();

        $orgId = $this->orgId;
        $userId = $this->faker->randomDigit;
        $otherUserId = 556397;
        $otherOrgId = $this->faker->randomDigit;

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $userId,
            'org_id' => $orgId,
        ]);
        $notRelated = factory(ItemEloquent::class)->create([
            'checklist_id' => $checklist->id,
            'user_id'      => $otherUserId,
            'assignee_id'  => $otherUserId,
            'org_id' => $otherOrgId,
        ]);
        $sameOrgNotOwnedOrAssigned = factory(ItemEloquent::class)->create([
            'checklist_id' => $checklist->id,
            'user_id'      => $otherUserId,
            'assignee_id'  => $otherUserId,
            'org_id' => $orgId,
        ]);
        $onlyAssigned = factory(ItemEloquent::class)->create([
            'checklist_id' => $checklist->id,
            'user_id'      => $otherUserId,
            'assignee_id'  => $userId,
            'org_id' => $orgId,
        ]);
        $ownedAssigned = factory(ItemEloquent::class)->create([
            'checklist_id' => $checklist->id,
            'user_id'      => $userId,
            'assignee_id'  => $userId,
            'org_id' => $orgId,
        ]);
        $onlyOwned = factory(ItemEloquent::class)->create([
            'checklist_id' => $checklist->id,
            'user_id'      => $userId,
            'assignee_id'  => $otherUserId,
            'org_id' => $orgId,
        ]);
        $userIdInactiveMcMember = $this->inactiveMemberId;
        $mcOwnedInactive = factory(ItemEloquent::class)->create([
            'checklist_id' => $checklist->id,
            'user_id' => $userIdInactiveMcMember,
            'org_id' => $orgId,
        ]);

        $this->json('GET', '/api/v1/checklists/items?include=checklist', [], $this->postHeadersMcScope)
            ->seeJsonStructure([
                'meta'  => ['count', 'total'],
                'data'  => [$this->basicJsonStructure['data']],
                'links' => ['first', 'last', 'next', 'prev'],
            ])->seeJson(['id' => (string) $checklist->id])
            ->seeJson(['meta' => ['count' => 5, 'total' => 5]])
            ->seeJson(['id' => (string) $ownedAssigned->id])
            ->seeJson(['id' => (string) $onlyOwned->id])
            ->seeJson(['id' => (string) $onlyAssigned->id])
            ->seeJson(['id' => (string) $sameOrgNotOwnedOrAssigned->id])
            ->seeJson(['id' => (string) $mcOwnedInactive->id])
            ->dontSeeJson(['id' => (string) $notRelated->id])
            ->seeStatusCode(200);
    }

    public function testIndexWithoutChecklistFilterByChecklistDomain()
    {
        $this->withoutEvents();

        $userId = $this->userID;
        $orgId = $this->orgId;
        $otherOrgId = $this->faker->randomDigit;

        $this->recruitsChecklistFirst = factory(ChecklistEloquent::class)->create([
            'user_id'       => $userId,
            'org_id'       => $orgId,
            'object_domain' => 'recruits',
            'object_id'     => 1,
        ]);
        $this->recruitsChecklistSecond = factory(ChecklistEloquent::class)->create([
            'user_id'       => $userId,
            'org_id'       => $orgId,
            'object_domain' => 'recruits',
            'object_id'     => 2,
        ]);
        $this->recruitsChecklistOtherMc = factory(ChecklistEloquent::class)->create([
            'user_id'       => $userId,
            'org_id'       => $otherOrgId,
            'object_domain' => 'recruits',
            'object_id'     => 2,
        ]);

        $this->opportunityChecklist = factory(ChecklistEloquent::class)->create([
            'user_id'       => $userId,
            'org_id'       => $otherOrgId,
            'object_domain' => 'opportunity',
            'object_id'     => 12,
        ]);

        $this->recruitsFirstItems = factory(ItemEloquent::class, 2)->create([
            'user_id'       => $userId,
            'org_id'       => $orgId,
            'assignee_id'  => $userId,
            'checklist_id' => $this->recruitsChecklistFirst,
        ]);

        $this->recruitsSecondItems = factory(ItemEloquent::class, 2)->create([
            'user_id'       => $userId,
            'org_id'       => $orgId,
            'assignee_id'  => $userId,
            'checklist_id' => $this->recruitsChecklistSecond,
        ]);
        $this->recruitsItemsOtherMc = factory(ItemEloquent::class, 2)->create([
            'user_id'       => $userId,
            'org_id'       => $otherOrgId,
            'assignee_id'  => $userId,
            'checklist_id' => $this->recruitsChecklistSecond,
        ]);

        $this->opportunityItems = factory(ItemEloquent::class, 2)->create([
            'user_id'       => $userId,
            'org_id'       => $orgId,
            'assignee_id'  => $this->userID,
            'checklist_id' => $this->opportunityChecklist,
        ]);

        $queryString = '?filter[checklist.object_domain][is]=recruits';
        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeadersMcScope)
                ->seeJson(['id' => (string) $this->recruitsFirstItems[0]->id])
                ->seeJson(['id' => (string) $this->recruitsSecondItems[0]->id])
                ->dontSeeJson(['id' => (string) $this->recruitsItemsOtherMc[0]->id])
                ->dontSeeJson(['id' => (string) $this->opportunityItems[0]->id]);
    }

    public function testStore()
    {
        $this->mockRoleServiceRequest(true);
        $this->withoutJobs();

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'is_completed' => false,
            'org_id' => $this->orgId,
        ]);
        $item = factory(ItemEloquent::class)->create([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => false,
            'org_id' => $this->orgId,
        ]);
        $data = $this->setupPayloads($item->toArray());
        $response = $this->setupResponse($item, [], ['id']);

        $this->json('POST', '/api/v1/checklists/' . $checklist->id . '/items', $data, $this->postHeadersMcScope)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(201);

        $id = $this->decodeResponseJson()['data']['id'];
        $dbResult = $this->setupDBResult($item, ['id' => $id], ['type']);
        array_set($dbResult, 'is_completed', false);
        array_set($dbResult, 'org_id', $this->orgId);

        $this->seeInDatabase('items', $dbResult);
    }

    public function testStoreItemOwnedMcChecklist()
    {
        $this->mockRoleServiceRequest(false);
        $this->withoutJobs();

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'is_completed' => false,
            'org_id' => $this->orgId,
        ]);
        $item = factory(ItemEloquent::class)->make([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => false,
            'org_id' => $this->orgId,
        ]);
        $data = $this->setupPayloads($item->toArray());
        $response = $this->setupResponse($item, [], ['id']);

        $this->json('POST', '/api/v1/checklists/' . $checklist->id . '/items', $data, $this->postHeadersMcScope)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(201);

        $id = $this->decodeResponseJson()['data']['id'];
        $dbResult = $this->setupDBResult($item, ['id' => $id], ['type']);
        array_set($dbResult, 'is_completed', false);
        array_set($dbResult, 'org_id', $this->orgId);

        $this->seeInDatabase('items', $dbResult);
    }

    public function testShowMcOwnItems()
    {
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
        ]);
        $item = factory(ItemEloquent::class)->create([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'org_id' => $checklist->org_id,
        ]);
        $response = $this->setupResponse($item);
        $this->json(
            'GET',
            '/api/v1/checklists/' . $item->checklist_id . '/items/' . $item->id,
            [],
            $this->postHeadersMcScope
        )->seeJson($response)
        ->seeJsonStructure($this->basicJsonStructure)
        ->assertResponseStatus(200);
    }

    public function testShowMcInactiveMemberItems()
    {
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->inactiveMemberId,
            'org_id' => $this->orgId,
        ]);
        $item = factory(ItemEloquent::class)->create([
            'user_id' => $this->inactiveMemberId,
            'checklist_id' => $checklist->id,
            'org_id' => $checklist->org_id,
        ]);
        $response = $this->setupResponse($item);
        $this->json(
            'GET',
            '/api/v1/checklists/' . $item->checklist_id . '/items/' . $item->id,
            [],
            $this->postHeadersMcScope
        )
        ->seeJson($response)
        ->seeJsonStructure($this->basicJsonStructure)
        ->assertResponseStatus(200);
    }

    public function testShowOtherMcItems()
    {
        $userId = $this->faker->randomDigit;
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $userId,
            'org_id' => $this->faker->randomDigit,
        ]);
        $item = factory(ItemEloquent::class)->create([
            'user_id' => $userId,
            'checklist_id' => $checklist->id,
            'org_id' => $checklist->org_id,
        ]);
        $this->json(
            'GET',
            '/api/v1/checklists/' . $item->checklist_id . '/items/' . $item->id,
            [],
            $this->postHeadersMcScope
        )->dontSeeJson(['id' => (string) $item->id])
        ->assertResponseStatus(403);
    }

    public function testShowMCAUserAbleToAccess()
    {
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
            'object_domain' => 'mc_recruits',
        ]);
        $item = factory(ItemEloquent::class)->create([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'org_id' => $checklist->org_id,
        ]);
        $response = $this->setupResponse($item);
        $this->json(
            'GET',
            '/api/v1/checklists/' . $item->checklist_id . '/items/' . $item->id,
            [],
            $this->postHeadersMcScope
        )->seeJson($response)
        ->assertResponseStatus(200);
    }

    public function testShowMCAUserAbleToAccessOtherItem()
    {
        $otherUserId = $this->faker->numberBetween(1000, 9000);
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $otherUserId,
            'org_id' => $this->orgId,
            'object_domain' => 'mc_recruits',
        ]);
        $item = factory(ItemEloquent::class)->create([
            'user_id' => $otherUserId,
            'checklist_id' => $checklist->id,
            'org_id' => $checklist->org_id,
        ]);
        $response = $this->setupResponse($item);
        $this->json(
            'GET',
            '/api/v1/checklists/' . $item->checklist_id . '/items/' . $item->id,
            [],
            $this->postHeadersMcScope
        )->seeJson($response)
        ->assertResponseStatus(200);
    }

    public function testCompleteByMc()
    {
        $this->withoutJobs();

        $dateCompleted = Carbon::now()->toIso8601String();
        Carbon::setTestNow($dateCompleted);

        $checklistLoginUser = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
        ]);
        $otherUserId = $this->faker->randomDigit;
        $checklistOtherUser = factory(ChecklistEloquent::class)->create([
            'user_id' => $otherUserId,
            'org_id' => $this->orgId,
        ]);
        $savedItemsLoginUser = factory(ItemEloquent::class, 5)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
            'checklist_id' => $checklistLoginUser->id,
            'completed_at' => null,
            'is_completed' => 0
        ]);
        $savedItemsOtherUser = factory(ItemEloquent::class, 2)->create([
            'user_id' => $otherUserId,
            'org_id' => $this->orgId,
            'checklist_id' => $checklistOtherUser->id,
            'completed_at' => null,
            'is_completed' => 0
        ]);

        $data = $this->setupPayloadsComplete($savedItemsLoginUser, $savedItemsOtherUser);
        $response = $this->setupResponseAllComplete($savedItemsLoginUser, $savedItemsOtherUser, $dateCompleted);

        $result = $this->json('POST', '/api/v1/checklists/complete', $data, $this->postHeadersMcScope)
            ->seeJson($response)
            ->seeJsonStructure([
                'data' => [
                    [
                        'checklist_id',
                        'item_id',
                        'completed_at',
                        'is_completed',
                    ],
                ],
            ])
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResultAllComplete($savedItemsLoginUser, $savedItemsOtherUser, true, $this->userID);

        foreach ($dbResult as $result) {
            array_set($result, 'org_id', $this->orgId);
            $this->seeInDatabase('items', $result);
        }

        $this->seeInDatabase('checklists', [
            'id'           => $checklistLoginUser->id,
            'is_completed' => 1,
            'org_id' => $this->orgId,
        ]);

        $this->seeInDatabase('checklists', [
            'id'           => $checklistOtherUser->id,
            'is_completed' => 1,
            'org_id' => $this->orgId,
        ]);
    }

    public function testCompleteInactiveMemberItemByMc()
    {
        $dateCompleted = Carbon::now()->toIso8601String();
        Carbon::setTestNow($dateCompleted);

        $checklistLoginUser = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
        ]);
        $checklistInactiveUser = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->inactiveMemberId,
            'org_id' => $this->orgId,
        ]);
        $savedItemsLoginUser = factory(ItemEloquent::class, 5)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
            'checklist_id' => $checklistLoginUser->id,
            'completed_at' => null,
            'is_completed' => 0
        ]);
        $savedItemsInactiveUser = factory(ItemEloquent::class, 2)->create([
            'user_id' => $this->inactiveMemberId,
            'org_id' => $this->orgId,
            'checklist_id' => $checklistInactiveUser->id,
            'completed_at' => null,
            'is_completed' => 0
        ]);

        $data = $this->setupPayloadsComplete($savedItemsLoginUser, $savedItemsInactiveUser);
        $response = $this->setupResponseAllComplete($savedItemsLoginUser, $savedItemsInactiveUser, $dateCompleted);

        $result = $this->json(
            'POST',
            '/api/v1/checklists/complete',
            $data,
            $this->postHeadersMcScope
        )
            ->seeJson($response)
            ->seeJsonStructure([
                'data' => [
                    [
                        'checklist_id',
                        'item_id',
                        'completed_at',
                        'is_completed',
                    ],
                ],
            ])
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResultAllComplete($savedItemsLoginUser, $savedItemsInactiveUser, true, $this->userID);

        foreach ($dbResult as $result) {
            array_set($result, 'org_id', $this->orgId);
            $this->seeInDatabase('items', $result);
        }

        $this->seeInDatabase('checklists', [
            'id'           => $checklistLoginUser->id,
            'is_completed' => 1,
            'org_id' => $this->orgId,
        ]);

        $this->seeInDatabase('checklists', [
            'id'           => $checklistInactiveUser->id,
            'is_completed' => 1,
            'org_id' => $this->orgId,
        ]);
    }


    public function testIncompleteByMc()
    {
        $this->withoutJobs();

        $checklistLoginUser = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
        ]);
        $otherUserId = $this->faker->randomDigit;
        $checklistOtherUser = factory(ChecklistEloquent::class)->create([
            'user_id' => $otherUserId,
            'org_id' => $this->orgId,
            'completed_at' => '2019-01-01 00:00:00',
            'is_completed' => 1,
            'user_id'      => 123,
        ]);
        $savedItemsLoginUser = factory(ItemEloquent::class, 5)->create([
            'user_id'      => $this->userID,
            'org_id'       => $this->orgId,
            'checklist_id' => $checklistLoginUser->id,
            'completed_at' => '2019-01-01 00:00:00',
            'is_completed' => 1,
            'completed_by' => $this->userID,
        ]);
        $savedItemsOtherUser = factory(ItemEloquent::class, 2)->create([
            'user_id'      => $otherUserId,
            'org_id'       => $this->orgId,
            'checklist_id' => $checklistOtherUser->id,
            'completed_at' => '2019-01-01 00:00:00',
            'is_completed' => 1,
            'completed_by' => 123,
        ]);

        $data = $this->setupPayloadsComplete($savedItemsLoginUser, $savedItemsOtherUser);
        $response = $this->setupResponseAllComplete($savedItemsLoginUser, $savedItemsOtherUser, null, false);

        $result = $this->json('POST', '/api/v1/checklists/incomplete', $data, $this->postHeadersMcScope);
        $result->seeJson($response)
            ->seeJsonStructure([
                'data' => [
                    [
                        'checklist_id',
                        'item_id',
                        'completed_at',
                        'is_completed',
                    ],
                ],
            ])
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResultAllComplete($savedItemsLoginUser, $savedItemsOtherUser, false);

        foreach ($dbResult as $result) {
            array_set($result, 'org_id', $this->orgId);
            $this->seeInDatabase('items', $result);
        }

        $this->seeInDatabase('checklists', [
            'id'           => $checklistLoginUser->id,
            'is_completed' => 0,
            'org_id'       => $this->orgId,
        ]);

        $this->seeInDatabase('checklists', [
            'id'           => $checklistOtherUser->id,
            'is_completed' => 0,
            'org_id'       => $this->orgId,
        ]);
    }

    public function testUpdateMcOwnCustomItem()
    {
        $this->mockRoleServiceRequest(false);
        $this->withoutJobs();

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
        ]);
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $checklist->org_id,
            'checklist_id' => $checklist->id,
        ]);
        $updateItem = factory(ItemEloquent::class)->make([
            'user_id' => $this->userID,
            'org_id' => $savedItem->org_id,
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by'   => $this->userID,
        ]);
        $data = $this->setupPayloads($updateItem->toArray());
        $response = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeadersMcScope
        )->seeJson($response)
        ->seeJsonStructure($this->basicJsonStructure)
        ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateItem, ['id' => $savedItem->id], ['type']);
        array_set($dbResult, 'updated_by', $this->userID);
        array_set($dbResult, 'user_id', $this->userID);
        array_set($dbResult, 'org_id', $this->orgId);

        $this->seeInDatabase('items', $dbResult);
    }

    public function testUpdateMcCascadingItem()
    {
        $this->mockRoleServiceRequest(true);
        $this->withoutJobs();

        $metaValue = [
            'template_item_id' => $this->faker->uuid,
        ];
        $checklist = factory(ChecklistEloquent::class)->create([
            'object_domain' => 'mc_recruits',
            'user_id' => $this->userID,
            'org_id'  => $this->orgId,
        ]);
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
            'org_id'       => $checklist->org_id,
            'meta'         => $metaValue,
        ]);
        $updateItem = factory(ItemEloquent::class)->make([
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by'   => $this->userID,
            'user_id'      => $savedItem->user_id,
            'org_id'       => $checklist->org_id,
            'meta'         => $metaValue,
        ]);
        $data = $this->setupPayloads($updateItem->toArray());
        $response = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id, 'updated_by' => $this->userID]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeadersMcScope
        )->seeJson($response)
        ->seeJsonStructure($this->basicJsonStructure)
        ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateItem, [
            'id' => $savedItem->id,
            'user_id' => $this->userID,
            'updated_by' => $this->userID,
            'org_id' => $this->orgId,
        ], ['type']);

        $this->seeInDatabase('items', $dbResult);
        $this->seeJsonInRecord(
            ['meta' => $metaValue],
            ItemEloquent::class,
            ['id' => $savedItem['id']]
        );
    }

    public function testUpdateMcCascadingItemForbidden()
    {
        $this->mockRoleServiceRequest(true);
        $this->withoutJobs();

        $orgId = $this->faker->numberBetween(1000, 9000);

        $metaValue = [
            'template_item_id' => $this->faker->uuid,
        ];
        $checklist = factory(ChecklistEloquent::class)->create([
            'object_domain' => 'mc_recruits',
            'user_id' => $this->userID,
            'org_id'  => $orgId,
        ]);
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
            'org_id'       => $checklist->org_id,
            'meta'         => $metaValue,
        ]);
        $updateItem = factory(ItemEloquent::class)->make([
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by'   => $this->userID,
            'user_id'      => $savedItem->user_id,
            'org_id'       => $checklist->org_id,
            'meta'         => $metaValue,
        ]);
        $data = $this->setupPayloads($updateItem->toArray());
        $response = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id, 'updated_by' => $this->userID]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeadersMcScope
        )->dontSeeJson($response)
        ->assertResponseStatus(403);
    }

    public function testUpdateInactiveMcMemberItem()
    {
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->inactiveMemberId,
            'org_id' => $this->orgId,
        ]);
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id'      => $this->inactiveMemberId,
            'org_id' => $this->orgId,
            'checklist_id' => $checklist->id,
        ]);
        $updateItem = factory(ItemEloquent::class)->make([
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by'   => $this->userID,
            'user_id'      => $savedItem->user_id,
            'org_id'      => $savedItem->org_id,
        ]);
        $data = $this->setupPayloads($updateItem->toArray());
        $response = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id, 'updated_by' => $this->userID]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeadersMcScope
        )->seeJson($response)
        ->seeJsonStructure($this->basicJsonStructure)
        ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateItem, [
            'id' => $savedItem->id,
            'user_id' => $this->inactiveMemberId,
            'org_id' => $this->orgId
        ], ['type']);
        $dbResult['updated_by'] = $this->userID;

        $this->seeInDatabase('items', $dbResult);
    }

    public function testUpdateMcOwnItemByUserDonthaveDestructiveAction()
    {
        $this->mockRoleServiceRequest(false);
        $this->mockPNOUnauthorized();

        $userId = $this->faker->numberBetween(1000, 9000);

        $checklist = factory(ChecklistEloquent::class)->create([
            'object_domain' => 'mc_recruits',
            'user_id' => $userId,
            'org_id' => $this->orgId,
        ]);
        $savedItem = factory(ItemEloquent::class)->create([
            'meta' => ['template_item_id' => $this->faker->uuid],
            'user_id' => $userId,
            'org_id' => $this->orgId,
            'checklist_id' => $checklist->id,
        ]);
        $updateItem = factory(ItemEloquent::class)->make([
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by'   => $this->userID,
            'user_id'      => $savedItem->user_id,
        ]);
        $data = $this->setupPayloads($updateItem->toArray());
        $response = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeadersMcScope
        )->assertResponseStatus(403);
    }

    public function testUpdateOtherMcItem()
    {
        $userId = $this->faker->randomDigit;
        $orgId = $this->faker->randomDigit;

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $userId,
            'org_id' => $orgId,
        ]);
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id' => $userId,
            'org_id' => $orgId,
            'checklist_id' => $checklist->id,
        ]);
        $updateItem = factory(ItemEloquent::class)->make([
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by'   => $this->userID,
            'user_id'      => $savedItem->user_id,
        ]);
        $data = $this->setupPayloads($updateItem->toArray());
        $response = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id, 'updated_by' => $this->userID]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeadersMcScope
        )->dontSeeJson($response)
        ->assertResponseStatus(403);
    }

    public function testBulkUpdateNotFound()
    {
        $checklist = factory(ChecklistEloquent::class)->create([
            'org_id' => $this->orgId,
        ]);

        $updateItems = [];
        $updateItems = factory(ItemEloquent::class, 2)
            ->make()
            ->transform(function ($updateItem) {
                return [
                    'id'         => random_int(100, 999),
                    'action'     => 'update',
                    'attributes' => [
                        'description' => $updateItem->description,
                        'due'         => (string) $updateItem->due,
                        'urgency'     => $updateItem->urgency,
                    ],
                ];
            })
            ->toArray();

        $data = $this->setupBulkPayloads($updateItems);

        $response = $this->setupBulkResponse($data, 404);

        $this->json(
            'POST',
            '/api/v1/checklists/' . $checklist->id . '/items/_bulk',
            $data,
            $this->postHeadersMcScope
        )->seeJson($response)
        ->assertResponseStatus(200);
    }

    public function testBulkUpdateNotAllowed()
    {
        $this->mockUserFound(123);

        $userId = $this->faker->numberBetween(1000, 9000);
        $orgId = $this->faker->numberBetween(1000, 9000);
        $checklist = factory(ChecklistEloquent::class)->create([
            'org_id' => $orgId,
        ]);

        $savedItems = factory(ItemEloquent::class, 2)->create([
            'user_id'      => $userId,
            'assignee_id'  => $userId,
            'checklist_id' => $checklist->id,
            'org_id' => $checklist->org_id,
        ]);

        $updateItems = [];

        foreach ($savedItems as $savedItem) {
            array_push($updateItems, [
                'id'         => $savedItem->id,
                'action'     => 'update',
                'attributes' => [
                    'description' => $savedItem->description,
                    'due'         => (string) $savedItem->due,
                    'urgency'     => 1,
                ],
            ]);
        }

        $data = $this->setupBulkPayloads($updateItems);

        $response = $this->setupBulkResponse($data, 403);

        $this->json(
            'POST',
            '/api/v1/checklists/' . $checklist->id . '/items/_bulk',
            $data,
            $this->postHeadersMcScope
        )->seeJson($response)
        ->assertResponseStatus(200);
    }

    public function testBulkUpdateSuccess()
    {
        $this->mockUserFound(123);
        $this->withoutJobs();

        $checklist = factory(ChecklistEloquent::class)->create([
            'org_id' => $this->orgId,
        ]);

        $owned = factory(ItemEloquent::class, 2)->create([
            'user_id'      => $this->userID,
            'org_id' => $checklist->org_id,
            'checklist_id' => $checklist->id,
        ]);
        $assigned = factory(ItemEloquent::class, 2)->create([
            'user_id'      => $this->faker->numberBetween(1000, 9000),
            'org_id' => $checklist->org_id,
            'assignee_id'  => $this->userID,
            'checklist_id' => $checklist->id,
        ]);
        $savedItems = $owned->merge($assigned);

        $updateItems = [];

        foreach ($savedItems as $savedItem) {
            array_push($updateItems, [
                'id'         => $savedItem->id,
                'action'     => 'update',
                'attributes' => [
                    'description' => $savedItem->description,
                    'due'         => (string) $savedItem->due,
                    'urgency'     => 1,
                    'notify_client' => true,
                    'client_update_sent_at' => '2020-01-04T01:01:01+00:00'
                ],
            ]);
        }

        $data = $this->setupBulkPayloads($updateItems);

        $response = $this->setupBulkResponse($data, 200);

        $this->json(
            'POST',
            '/api/v1/checklists/' . $checklist->id . '/items/_bulk',
            $data,
            $this->postHeadersMcScope
        )->seeJson($response)
        ->assertResponseStatus(200);

        foreach ($updateItems as $itemKey => $itemValue) {
            $dbResult = $this->setupDBResult(
                $savedItems[$itemKey],
                [
                    'id'                    => array_get($itemValue, 'id'),
                    'description'           => array_get($itemValue, 'attributes.description'),
                    'due'                   => array_get($itemValue, 'attributes.due'),
                    'urgency'               => array_get($itemValue, 'attributes.urgency'),
                    'notify_client'         => array_get($itemValue, 'attributes.notify_client'),
                    'client_update_sent_at' => array_get($itemValue, 'attributes.client_update_sent_at'),
                    'updated_by'            => $this->userID,
                    'org_id'                => $this->orgId,
                ]
            );

            $this->seeInDatabase('items', $dbResult);
        }
    }

    public function testBulkCascadingUpdateSuccess()
    {
        $this->mockRoleServiceRequest(true);
        $this->mockUserFound(123);
        $this->withoutJobs();

        $metaValue = ['template_item_id' => $this->faker->uuid];

        $checklist = factory(ChecklistEloquent::class)->create([
            'object_domain' => 'mc_recruits',
            'org_id' => $this->orgId,
        ]);

        $owned = factory(ItemEloquent::class, 2)->create([
            'meta'      => $metaValue,
            'user_id'      => $this->userID,
            'org_id' => $checklist->org_id,
            'checklist_id' => $checklist->id,
        ]);
        $assigned = factory(ItemEloquent::class, 2)->create([
            'meta'      => $metaValue,
            'user_id'      => $this->faker->numberBetween(1000, 9000),
            'org_id' => $checklist->org_id,
            'assignee_id'  => $this->userID,
            'checklist_id' => $checklist->id,
        ]);
        $savedItems = $owned->merge($assigned);

        $updateItems = [];

        foreach ($savedItems as $savedItem) {
            array_push($updateItems, [
                'id'         => $savedItem->id,
                'action'     => 'update',
                'attributes' => [
                    'description' => $savedItem->description,
                    'due'         => (string) $savedItem->due,
                    'urgency'     => 1,
                    'notify_client' => true,
                    'client_update_sent_at' => '2020-01-04T01:01:01+00:00'
                ],
            ]);
        }

        $data = $this->setupBulkPayloads($updateItems);

        $response = $this->setupBulkResponse($data, 200);

        $this->json(
            'POST',
            '/api/v1/checklists/' . $checklist->id . '/items/_bulk',
            $data,
            $this->postHeadersMcScope
        )->seeJson($response)
        ->assertResponseStatus(200);

        foreach ($updateItems as $itemKey => $itemValue) {
            $dbResult = $this->setupDBResult(
                $savedItems[$itemKey],
                [
                    'id'                    => array_get($itemValue, 'id'),
                    'description'           => array_get($itemValue, 'attributes.description'),
                    'due'                   => array_get($itemValue, 'attributes.due'),
                    'urgency'               => array_get($itemValue, 'attributes.urgency'),
                    'notify_client'         => array_get($itemValue, 'attributes.notify_client'),
                    'client_update_sent_at' => array_get($itemValue, 'attributes.client_update_sent_at'),
                    'updated_by'            => $this->userID,
                    'org_id'                => $this->orgId,
                ]
            );

            $this->seeInDatabase('items', $dbResult);
            $this->seeJsonInRecord(
                ['meta' => $metaValue],
                ItemEloquent::class,
                ['id' => array_get($itemValue, 'id')]
            );
        }
    }

    public function testBulkCascadingUpdateForbidden()
    {
        $this->mockRoleServiceRequest(false);
        $this->mockUserFound(123);
        $this->withoutJobs();

        $metaValue = ['template_item_id' => $this->faker->uuid];

        $checklist = factory(ChecklistEloquent::class)->create([
            'object_domain' => 'mc_recruits',
            'org_id' => $this->orgId,
        ]);

        $owned = factory(ItemEloquent::class, 2)->create([
            'meta'      => $metaValue,
            'user_id'      => $this->userID,
            'org_id' => $checklist->org_id,
            'checklist_id' => $checklist->id,
        ]);
        $assigned = factory(ItemEloquent::class, 2)->create([
            'meta'      => $metaValue,
            'user_id'      => $this->faker->numberBetween(1000, 9000),
            'org_id' => $checklist->org_id,
            'assignee_id'  => $this->userID,
            'checklist_id' => $checklist->id,
        ]);
        $savedItems = $owned->merge($assigned);

        $updateItems = [];

        foreach ($savedItems as $savedItem) {
            array_push($updateItems, [
                'id'         => $savedItem->id,
                'action'     => 'update',
                'attributes' => [
                    'description' => $savedItem->description,
                    'due'         => (string) $savedItem->due,
                    'urgency'     => 1,
                    'notify_client' => true,
                    'client_update_sent_at' => '2020-01-04T01:01:01+00:00'
                ],
            ]);
        }

        $data = $this->setupBulkPayloads($updateItems);

        $response = $this->setupBulkResponse($data, 403);

        $this->json(
            'POST',
            '/api/v1/checklists/' . $checklist->id . '/items/_bulk',
            $data,
            $this->postHeadersMcScope
        )->seeJson($response)
        ->assertResponseStatus(200);
    }

    public function testDestroyInOneMc()
    {
        $this->mockRoleServiceRequest(true);
        $this->withoutJobs();

        $checklist = factory(ChecklistEloquent::class)->create([
            'org_id' => $this->orgId,
        ]);
        $item = factory(ItemEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
            'checklist_id' => $checklist->id
        ]);

        $this->json(
            'DELETE',
            '/api/v1/checklists/' . $checklist->id . '/items/' . $item->id,
            [],
            $this->postHeadersMcScope
        );
        $this->seeStatusCode(204);
        $this->missingFromDatabase('items', [
            'id'         => $item->id,
            'deleted_at' => null,
        ]);
    }

    public function testDestroyInactiveMemberInOneMc()
    {
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->inactiveMemberId,
            'org_id' => $this->orgId,
        ]);
        $item = factory(ItemEloquent::class)->create([
            'user_id' => $this->inactiveMemberId,
            'org_id' => $this->orgId,
            'checklist_id' => $checklist->id,
        ]);

        $this->json(
            'DELETE',
            '/api/v1/checklists/' . $checklist->id . '/items/' . $item->id,
            [],
            $this->postHeadersMcScope
        );
        $this->seeStatusCode(204);
        $this->missingFromDatabase('items', [
            'id'         => $item->id,
            'deleted_at' => null,
        ]);
    }

    public function testChecklistItemCount()
    {
        $this->firstChecklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
            'object_domain' => 'mc_recruits',
            'object_id' => $this->faker->numberBetween(1, 10),
        ]);
        $this->incompleteItemOne = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'org_id' => $this->orgId,
            'is_completed' => 0,
            'checklist_id' => $this->firstChecklist->id
        ]);
        $this->completedItemOne = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'org_id' => $this->orgId,
            'is_completed' => 1,
            'checklist_id' => $this->firstChecklist->id
        ]);
        $otherUserId = $this->faker->numberBetween(1000, 9000);
        $this->mcOwnIncompleteItemOne = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $this->orgId,
            'is_completed' => 0,
            'checklist_id' => $this->firstChecklist->id
        ]);
        $this->mcOwnCompletedItemOne = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $this->orgId,
            'is_completed' => 1,
            'checklist_id' => $this->firstChecklist->id
        ]);
        $otherOrgId = $this->faker->numberBetween(1000, 9000);
        $this->otherMcIncompleteItemOne = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $otherOrgId,
            'is_completed' => 0,
            'checklist_id' => $this->firstChecklist->id
        ]);
        $this->otherMcCompletedItemOne = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $otherOrgId,
            'is_completed' => 1,
            'checklist_id' => $this->firstChecklist->id
        ]);

        $this->secondChecklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
            'object_domain' => 'mc_recruits',
            'object_id' => $this->faker->numberBetween(1, 10),
        ]);
        $this->incompleteItemTwo = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'org_id' => $this->orgId,
            'is_completed' => 0,
            'checklist_id' => $this->secondChecklist->id
        ]);
        $this->completedItemTwo = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'org_id' => $this->orgId,
            'is_completed' => 1,
            'checklist_id' => $this->secondChecklist->id
        ]);
        $otherUserId = $this->faker->numberBetween(1000, 9000);
        $this->mcOwnIncompleteItemTwo = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $this->orgId,
            'is_completed' => 0,
            'checklist_id' => $this->secondChecklist->id
        ]);
        $this->mcOwnCompletedItemTwo = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $this->orgId,
            'is_completed' => 1,
            'checklist_id' => $this->secondChecklist->id
        ]);
        $otherOrgId = $this->faker->numberBetween(1000, 9000);
        $this->otherMcIncompleteItemTwo = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $otherOrgId,
            'is_completed' => 0,
            'checklist_id' => $this->secondChecklist->id
        ]);
        $this->otherMcCompletedItemTwo = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $otherOrgId,
            'is_completed' => 1,
            'checklist_id' => $this->secondChecklist->id
        ]);
        $this->thirdChecklistOtherMC = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->faker->numberBetween(1000, 9000),
            'object_domain' => 'mc_recruits',
            'object_id' => $this->faker->numberBetween(1, 10),
        ]);
        $this->incompleteItemThree = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'org_id' => $this->thirdChecklistOtherMC->org_id,
            'is_completed' => 0,
            'checklist_id' => $this->thirdChecklistOtherMC->id,
        ]);
        $this->completedItemThree = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'org_id' => $this->thirdChecklistOtherMC->org_id,
            'is_completed' => 1,
            'checklist_id' => $this->thirdChecklistOtherMC->id,
        ]);

        $this->should('show correct count with checklist_id filter', function () {
            $firstChecklistId = $this->firstChecklist->id;
            $secondChecklistId = $this->secondChecklist->id;
            $thirdChecklistId = $this->thirdChecklistOtherMC->id;
            $randomChecklistId = $this->faker->numberBetween(1000, 9000);
            $query = "filter[checklist_id][in]=$firstChecklistId,$secondChecklistId,
                        $thirdChecklistId,$randomChecklistId";
            $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeadersMcScope)
                ->seeStatusCode(200)
                ->seeJson([
                    'data' => [
                        'counts' => [
                            [
                                'checklist_id' => $firstChecklistId,
                                'completed_items' => $this->completedItemOne->count()
                                                    + $this->mcOwnCompletedItemOne->count(),
                                'incomplete_items' => $this->incompleteItemOne->count()
                                                    + $this->mcOwnIncompleteItemOne->count(),
                                'total_items' => $this->completedItemOne->count()
                                                + $this->mcOwnCompletedItemOne->count()
                                                + $this->incompleteItemOne->count()
                                                + $this->mcOwnIncompleteItemOne->count(),
                            ],
                            [
                                'checklist_id' => $secondChecklistId,
                                'completed_items' => $this->completedItemTwo->count()
                                                    + $this->mcOwnCompletedItemTwo->count(),
                                'incomplete_items' => $this->incompleteItemTwo->count()
                                                    + $this->mcOwnIncompleteItemTwo->count(),
                                'total_items' => $this->completedItemTwo->count()
                                                + $this->mcOwnCompletedItemTwo->count()
                                                + $this->incompleteItemTwo->count()
                                                + $this->mcOwnIncompleteItemTwo->count(),
                            ]
                        ]
                    ]
                ])
                ->dontSeeJson([
                    'data' => [
                        'counts' => [
                            [
                                'checklist_id' => $thirdChecklistId,
                                'completed_items' => $this->completedItemThree->count(),
                                'incomplete_items' => $this->incompleteItemThree->count(),
                                'total_items' => $this->completedItemThree->count()
                                                + $this->incompleteItemThree->count(),
                            ],
                            [
                                'checklist_id' => $randomChecklistId,
                            ]
                        ]
                    ]
                ]);
        });

        $this->should('show no data without checklist_id filter', function () {
            $this->json('GET', "/api/v1/checklists/items/counts", [], $this->postHeadersMcScope)
                ->seeStatusCode(200)
                ->seeJson([
                    'data' => []
                ]);
        });
    }

    public function testChecklistItemCountAfterCreatingItem()
    {
        $this->withoutJobs();
        $this->checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
            'object_domain' => 'mc_recruits',
            'object_id' => $this->faker->numberBetween(1, 10),
        ]);
        $this->incompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'org_id' => $this->orgId,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->completedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'org_id' => $this->orgId,
            'is_completed' => 1,
            'completed_at' => '2020-05-12 16:40:28',
            'checklist_id' => $this->checklist->id
        ]);
        $otherUserId = $this->faker->numberBetween(1000, 9000);
        $this->mcOwnIncompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $this->orgId,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->mcOwnCompletedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $this->orgId,
            'is_completed' => 1,
            'completed_at' => '2020-05-12 16:40:28',
            'checklist_id' => $this->checklist->id
        ]);
        $otherOrgId = $this->faker->numberBetween(1000, 9000);
        $this->otherMcIncompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $otherOrgId,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->otherMcCompletedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $otherOrgId,
            'is_completed' => 1,
            'checklist_id' => $this->checklist->id
        ]);

        $checklistId = $this->checklist->id;
        $query = "filter[checklist_id][in]=$checklistId";
        $firstResult = [
            'checklist_id' => $checklistId,
            'completed_items' => $this->completedItem->count()
                + $this->mcOwnCompletedItem->count(),
            'incomplete_items' => $this->incompleteItem->count()
                + $this->mcOwnIncompleteItem->count(),
            'total_items' => $this->completedItem->count()
                + $this->mcOwnCompletedItem->count()
                + $this->incompleteItem->count()
                + $this->mcOwnIncompleteItem->count(),
        ];
        $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeadersMcScope)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => [
                    'counts' => [$firstResult]
                ]
            ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertEquals($firstResult, $cachedItemCount);

        $item = factory(ItemEloquent::class)->make([
            'user_id' => $this->userID,
            'checklist_id' => $this->checklist->id,
            'is_completed' => false,
            'org_id' => $this->orgId,
        ]);
        $data = $this->setupPayloads($item->toArray());
        $response = $this->setupResponse($item, [], ['id']);

        $this->json('POST', '/api/v1/checklists/' . $this->checklist->id . '/items', $data, $this->postHeadersMcScope)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(201);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertNull($cachedItemCount);

        $secondResult = [
            'checklist_id' => $checklistId,
            'completed_items' => $this->completedItem->count()
                + $this->mcOwnCompletedItem->count(),
            'incomplete_items' => $this->incompleteItem->count()
                + $this->mcOwnIncompleteItem->count()
                + count($data),
            'total_items' => $this->completedItem->count()
                + $this->mcOwnCompletedItem->count()
                + $this->incompleteItem->count()
                + $this->mcOwnIncompleteItem->count()
                + count($data),
        ];
        $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeadersMcScope)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => [
                    'counts' => [$secondResult]
                ]
            ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertEquals($secondResult, $cachedItemCount);
    }

    public function testChecklistItemCountAfterDeletingItem()
    {
        $this->checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
            'object_domain' => 'mc_recruits',
            'object_id' => $this->faker->numberBetween(1, 10),
        ]);
        $this->incompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'org_id' => $this->orgId,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->completedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'org_id' => $this->orgId,
            'is_completed' => 1,
            'completed_at' => '2020-05-12 16:40:28',
            'checklist_id' => $this->checklist->id
        ]);
        $otherUserId = $this->faker->numberBetween(1000, 9000);
        $this->mcOwnIncompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $this->orgId,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->mcOwnCompletedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $this->orgId,
            'is_completed' => 1,
            'completed_at' => '2020-05-12 16:40:28',
            'checklist_id' => $this->checklist->id
        ]);
        $otherOrgId = $this->faker->numberBetween(1000, 9000);
        $this->otherMcIncompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $otherOrgId,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->otherMcCompletedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $otherOrgId,
            'is_completed' => 1,
            'checklist_id' => $this->checklist->id
        ]);

        $checklistId = $this->checklist->id;
        $query = "filter[checklist_id][in]=$checklistId";
        $firstResult = [
            'checklist_id' => $checklistId,
            'completed_items' => $this->completedItem->count()
                + $this->mcOwnCompletedItem->count(),
            'incomplete_items' => $this->incompleteItem->count()
                + $this->mcOwnIncompleteItem->count(),
            'total_items' => $this->completedItem->count()
                + $this->mcOwnCompletedItem->count()
                + $this->incompleteItem->count()
                + $this->mcOwnIncompleteItem->count(),
        ];
        $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeadersMcScope)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => [
                    'counts' => [$firstResult]
                ]
            ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertEquals($firstResult, $cachedItemCount);

        $this->json(
            'DELETE',
            '/api/v1/checklists/' . $checklistId . '/items/' . $this->completedItem[0]->id,
            [],
            $this->postHeadersMcScope
        );
        $this->seeStatusCode(204);
        $this->missingFromDatabase('items', [
            'id'         => $this->completedItem[0]->id,
            'deleted_at' => null,
        ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertNull($cachedItemCount);

        $secondResult = [
            'checklist_id' => $checklistId,
            'completed_items' => $this->completedItem->count()
                + $this->mcOwnCompletedItem->count()
                - 1,
            'incomplete_items' => $this->incompleteItem->count()
                + $this->mcOwnIncompleteItem->count(),
            'total_items' => $this->completedItem->count()
                + $this->mcOwnCompletedItem->count()
                + $this->incompleteItem->count()
                + $this->mcOwnIncompleteItem->count()
                - 1,
        ];
        $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeadersMcScope)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => [
                    'counts' => [$secondResult]
                ]
            ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertEquals($secondResult, $cachedItemCount);
    }

    public function testChecklistItemCountAfterCompletingItem()
    {
        $this->checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
            'object_domain' => 'mc_recruits',
            'object_id' => $this->faker->numberBetween(1, 10),
        ]);
        $this->incompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'org_id' => $this->orgId,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->completedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'org_id' => $this->orgId,
            'is_completed' => 1,
            'checklist_id' => $this->checklist->id
        ]);
        $otherUserId = $this->faker->numberBetween(1000, 9000);
        $this->mcOwnIncompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $this->orgId,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->mcOwnCompletedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $this->orgId,
            'is_completed' => 1,
            'checklist_id' => $this->checklist->id
        ]);
        $otherOrgId = $this->faker->numberBetween(1000, 9000);
        $this->otherMcIncompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $otherOrgId,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->otherMcCompletedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $otherOrgId,
            'is_completed' => 1,
            'checklist_id' => $this->checklist->id
        ]);

        $checklistId = $this->checklist->id;
        $query = "filter[checklist_id][in]=$checklistId";
        $firstResult = [
            'checklist_id' => $checklistId,
            'completed_items' => $this->completedItem->count()
                + $this->mcOwnCompletedItem->count(),
            'incomplete_items' => $this->incompleteItem->count()
                + $this->mcOwnIncompleteItem->count(),
            'total_items' => $this->completedItem->count()
                + $this->mcOwnCompletedItem->count()
                + $this->incompleteItem->count()
                + $this->mcOwnIncompleteItem->count(),
        ];
        $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeadersMcScope)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => [
                    'counts' => [$firstResult]
                ]
            ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertEquals($firstResult, $cachedItemCount);

        $arrItemToComplete = [];
        foreach ($this->incompleteItem as $item) {
            array_push($arrItemToComplete, ['item_id' => $item->id]);
        }

        foreach ($this->mcOwnIncompleteItem as $item) {
            array_push($arrItemToComplete, ['item_id' => $item->id]);
        }

        $payload = [
            "data" => $arrItemToComplete
        ];

        $this->json('POST', "/api/v1/checklists/complete", $payload, $this->postHeadersMcScope)
            ->seeStatusCode(200);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertNull($cachedItemCount);

        $secondResult = [
            'checklist_id' => $checklistId,
            'completed_items' => $this->completedItem->count()
                + $this->mcOwnCompletedItem->count()
                + $this->incompleteItem->count()
                + $this->mcOwnIncompleteItem->count(),
            'incomplete_items' => 0,
            'total_items' => $this->completedItem->count()
                + $this->mcOwnCompletedItem->count()
                + $this->incompleteItem->count()
                + $this->mcOwnIncompleteItem->count(),
        ];
        $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeadersMcScope)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => [
                    'counts' => [$secondResult]
                ]
            ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertEquals($secondResult, $cachedItemCount);
    }

    public function testChecklistItemCountAfterIncompletingItem()
    {
        $this->checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
            'object_domain' => 'mc_recruits',
            'object_id' => $this->faker->numberBetween(1, 10),
        ]);
        $this->incompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'org_id' => $this->orgId,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->completedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'org_id' => $this->orgId,
            'is_completed' => 1,
            'completed_at' => '2020-05-12 16:40:28',
            'checklist_id' => $this->checklist->id
        ]);
        $otherUserId = $this->faker->numberBetween(1000, 9000);
        $this->mcOwnIncompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $this->orgId,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->mcOwnCompletedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $this->orgId,
            'is_completed' => 1,
            'completed_at' => '2020-05-12 16:40:28',
            'checklist_id' => $this->checklist->id
        ]);
        $otherOrgId = $this->faker->numberBetween(1000, 9000);
        $this->otherMcIncompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $otherOrgId,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->otherMcCompletedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'org_id' => $otherOrgId,
            'is_completed' => 1,
            'checklist_id' => $this->checklist->id
        ]);

        $checklistId = $this->checklist->id;
        $query = "filter[checklist_id][in]=$checklistId";
        $firstResult = [
            'checklist_id' => $checklistId,
            'completed_items' => $this->completedItem->count()
                + $this->mcOwnCompletedItem->count(),
            'incomplete_items' => $this->incompleteItem->count()
                + $this->mcOwnIncompleteItem->count(),
            'total_items' => $this->completedItem->count()
                + $this->mcOwnCompletedItem->count()
                + $this->incompleteItem->count()
                + $this->mcOwnIncompleteItem->count(),
        ];
        $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeadersMcScope)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => [
                    'counts' => [$firstResult]
                ]
            ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertEquals($firstResult, $cachedItemCount);

        $arrItemToIncomplete = [];
        foreach ($this->completedItem as $item) {
            array_push($arrItemToIncomplete, ['item_id' => $item->id]);
        }

        foreach ($this->mcOwnCompletedItem as $item) {
            array_push($arrItemToIncomplete, ['item_id' => $item->id]);
        }

        $payload = [
            "data" => $arrItemToIncomplete
        ];

        $this->json('POST', "/api/v1/checklists/incomplete", $payload, $this->postHeadersMcScope)
            ->seeStatusCode(200);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertNull($cachedItemCount);

        $secondResult = [
            'checklist_id' => $checklistId,
            'completed_items' => 0,
            'incomplete_items' => $this->completedItem->count()
                + $this->mcOwnCompletedItem->count()
                + $this->incompleteItem->count()
                + $this->mcOwnIncompleteItem->count(),
            'total_items' => $this->completedItem->count()
                + $this->mcOwnCompletedItem->count()
                + $this->incompleteItem->count()
                + $this->mcOwnIncompleteItem->count(),
        ];
        $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeadersMcScope)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => [
                    'counts' => [$secondResult]
                ]
            ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertEquals($secondResult, $cachedItemCount);
    }
}
