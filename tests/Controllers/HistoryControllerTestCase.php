<?php

namespace Test\Controllers;

use App\Services\History\HistoryEloquent;

abstract class HistoryControllerTestCase extends ControllerTestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->basicJsonStructure = [
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'loggable_type',
                    'loggable_id',
                    'action',
                    'kwuid',
                    'value',
                    'created_at',
                    'updated_at',
                ],
                'links' => ['self'],
            ],
        ];
    }

    protected function setupResponse(HistoryEloquent $history)
    {
        return [
            'id' => (string) $history->getKey(),
            'loggable_type' => $history->getAttribute('loggable_type'),
            'loggable_id' => $history->getAttribute('loggable_id'),
            'action' => $history->getAttribute('action'),
            'kwuid' => $history->getAttribute('kwuid'),
            'value' => (string) $history->getAttribute('value'),
            'created_at' => $history->getAttribute('created_at')->toIso8601String(),
            'updated_at' => $history->getAttribute('updated_at')->toIso8601String(),
        ];
    }
}
