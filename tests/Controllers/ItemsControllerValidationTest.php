<?php

namespace Test\Controllers;

use App\Jobs\Audit\CreateItemJob;
use App\Jobs\Audit\UpdateItemJob;
use App\Services\Items\ItemEloquent;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Services\Checklists\ChecklistEloquent;

class ItemsControllerValidationTest extends ChecklistControllerTestCase
{
    use DatabaseTransactions;

    private const DEAL_OBJECT = 'deals';

    public function testItemsBulkUpdateNotifyClientValidateion()
    {
        $checklist = factory(ChecklistEloquent::class)->create();

        $savedItems = factory(ItemEloquent::class, 2)->create([
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
        ]);

        $updateItems = [];

        foreach ($savedItems as $savedItem) {
            array_push($updateItems, [
                'id'         => $savedItem->id,
                'action'     => 'update',
                'attributes' => [
                    'due'                       => null,
                    'urgency'                   => 0,
                    'task_id'                   => null,
                    'assignee_id'               => null,
                    'notify_client'             => null,
                    'client_update_sent_at'     => 1234,
                ],
            ]);
        }

        $data = [
            "data" => $updateItems
        ];

        $this->json('POST', '/api/v1/checklists/' . $checklist->id . '/items/_bulk', $data, $this->postHeaders)
            ->assertResponseStatus(422);
    }

    /**
     * @dataProvider dueStoreDataProvider
     */
    public function testStoreWithDue($duePayload, $responseStatusCode)
    {
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'is_completed' => false
        ]);
        $item = factory(ItemEloquent::class)->make([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => false
        ]);
        $itemPayloads = $this->setupPayloads($item->toArray());
        $itemPayloads['data']['attributes']['due'] = $duePayload;

        if ($responseStatusCode === 201 && $checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(CreateItemJob::class);
        }

        $this->json('POST', '/api/v1/checklists/' . $checklist->id . '/items', $itemPayloads, $this->postHeaders)
            ->assertResponseStatus($responseStatusCode);
    }

    public function dueStoreDataProvider()
    {
        return [
            [null, 201],
            ['01234567890', 422],
            ['random string', 422],
            ['2001-01-01 10:01:01', 201],
            ['2018-06-21T10:10:23.185Z', 201],
            ['2002-02-02T10:02:02+00:00', 201],
        ];
    }

    /**
     * @dataProvider dueUpdateDataProvider
     */
    public function testUpdateWithDue($duePayload, $responseStatusCode)
    {
        $checklist = factory(ChecklistEloquent::class)->create();
        $savedItem = factory(ItemEloquent::class)->create([
            'due'          => null,
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
        ]);

        $itemPayload = [
            'data' => [
                'attributes' => [
                    'due' => $duePayload,
                ],
            ],
        ];

        if ($responseStatusCode === 200 && $checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(UpdateItemJob::class);
        }

        $endpointUri = '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id;
        $this->json('PATCH', $endpointUri, $itemPayload, $this->postHeaders);
        $this->assertResponseStatus($responseStatusCode);
    }

    public function dueUpdateDataProvider()
    {
        return [
            [null, 200],
            ['01234567890', 422],
            ['random string', 422],
            ['2001-01-01 10:01:01', 200],
            ['2018-06-21T10:10:23.185Z', 200],
            ['2002-02-02T10:02:02+00:00', 200],
        ];
    }
}
