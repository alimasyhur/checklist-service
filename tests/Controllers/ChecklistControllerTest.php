<?php

namespace Test\Controllers;

use App\Jobs\DueChecklistsNotificationJob;
use App\Services\Checklists\ChecklistEloquent;
use App\Services\Items\ItemEloquent;
use Carbon\Carbon;
use Faker;
use CorePackage\Testing\AssertJob;
use Illuminate\Support\Arr;

class ChecklistControllerTest extends ChecklistControllerTestCase
{
    use AssertJob;

    public function setUp()
    {
        parent::setUp();

        $this->mockInactiveUsersResponse([$this->inactiveMemberId]);
    }

    public function testIndex()
    {
        $faker = Faker\Factory::create();
        $this->checklists = factory(ChecklistEloquent::class, 2)->create(['user_id' => $this->userID]);
        $this->item1 = factory(ItemEloquent::class)->create([
            'checklist_id' => $this->checklists[0]->id,
            'description' => $faker->unique()->sentence(3),
        ]);
        $this->item2 = factory(ItemEloquent::class)->create([
            'checklist_id' => $this->checklists[0]->id,
            'description' => $faker->unique()->sentence(3),
        ]);
        $this->item3 = factory(ItemEloquent::class)->create([
            'checklist_id' => $this->checklists[1]->id,
            'description' => $faker->unique()->sentence(3),
        ]);
        $this->otherChecklist = factory(ChecklistEloquent::class)->create(['user_id' => 123]);

        $this->should('only show my checklist', function () {
            $this->json('GET', '/api/v1/checklists', [], $this->postHeaders)
                 ->seeStatusCode(200)
                 ->seeJsonStructure([
                     'meta' => [ 'count', 'total' ],
                     'data' => [ $this->basicJsonStructure['data'] ],
                     'links' => [ 'first', 'last', 'next', 'prev' ],
                 ])
                 ->seeJson(['id' => (string) $this->checklists[0]->id])
                 ->seeJson(['id' => (string) $this->checklists[1]->id])
                 ->dontSeeJson(['id' => (string) $this->otherChecklist->id]);
        });

        $this->should('include related items with ?include=items', function () {
            $query = '?include=items';
            $this->json('GET', "/api/v1/checklists$query", [], $this->postHeaders)
                 ->seeJson(['id' => (string) $this->item1->id])
                 ->seeJson(['id' => (string) $this->item2->id])
                 ->seeJson(['id' => (string) $this->item3->id]);
        });

        $this->should('able to filter by related item attributes', function () {
            $query = "?filter[items.description][is]={$this->item1->description}";
            $this->json('GET', "/api/v1/checklists$query", [], $this->postHeaders)
                 ->seeJson(['id' => (string) $this->checklists[0]->id])
                 ->dontSeeJson(['id' => (string) $this->checklists[1]->id]);

            $query = "?filter[items.description][is]={$this->item2->description}";
            $this->json('GET', "/api/v1/checklists$query", [], $this->postHeaders)
                 ->seeJson(['id' => (string) $this->checklists[0]->id])
                 ->dontSeeJson(['id' => (string) $this->checklists[1]->id]);

            $query = "?filter[items.description][is]={$this->item3->description}";
            $this->json('GET', "/api/v1/checklists$query", [], $this->postHeaders)
                 ->dontSeeJson(['id' => (string) $this->checklists[0]->id])
                 ->seeJson(['id' => (string) $this->checklists[1]->id]);
        });
    }

    public function testIndexTeam()
    {
        $ownedData = factory(ChecklistEloquent::class)->create(['user_id' => 556396]);
        $oneTeamData = factory(ChecklistEloquent::class)->create(['user_id' => 556397]);
        $otherTeamData = factory(ChecklistEloquent::class)->create(['user_id' => 556399]);

        $this->json('GET', '/api/v1/checklists?sort=-id', [], $this->postHeadersTeamScope);

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'meta'  => [
                'count',
                'total',
            ],
            'data'  => [
                $this->basicJsonStructure['data'],
            ],
            'links' => [
                'first',
                'last',
                'next',
                'prev',
            ],
        ]);

        $responseDataIDs = collect($this->response->getData()->data)->pluck('id');

        $this->assertContains((string) $ownedData->getKey(), $responseDataIDs);
        $this->assertContains((string) $oneTeamData->getKey(), $responseDataIDs);
        $this->assertNotContains((string) $otherTeamData->getKey(), $responseDataIDs);
    }

    public function testIndexTeamWithInactiveMember()
    {
        $ownedData = factory(ChecklistEloquent::class)->create(['user_id' => 556396]);
        $inactiveMemberData = factory(ChecklistEloquent::class)->create(['user_id' => $this->inactiveMemberId]);
        $otherTeamData = factory(ChecklistEloquent::class)->create(['user_id' => 556399]);

        $this->json('GET', '/api/v1/checklists?sort=-id', [], $this->postHeadersTeamScope);

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'meta'  => [
                'count',
                'total',
            ],
            'data'  => [
                $this->basicJsonStructure['data'],
            ],
            'links' => [
                'first',
                'last',
                'next',
                'prev',
            ],
        ]);

        $responseDataIDs = collect($this->response->getData()->data)->pluck('id');

        $this->assertContains((string) $ownedData->getKey(), $responseDataIDs);
        $this->assertContains((string) $inactiveMemberData->getKey(), $responseDataIDs);
        $this->assertNotContains((string) $otherTeamData->getKey(), $responseDataIDs);
    }

    public function testShow()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);
        $response = $this->setupResponse($checklist);

        $this->json('GET', '/api/v1/checklists/' . $checklist->id, [], $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $responseData = json_decode($this->response->getContent());

        $this->assertInternalType('string', $responseData->data->attributes->object_id);
        $this->assertInternalType('bool', $responseData->data->attributes->is_completed);
    }

    public function testShowInactiveUser()
    {
        $this->mockUserNotFound();
        
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->inactiveMemberId]);
        $response = $this->setupResponse($checklist);

        $this->json('GET', '/api/v1/checklists/' . $checklist->id, [], $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $responseData = json_decode($this->response->getContent());

        $this->assertInternalType('string', $responseData->data->attributes->object_id);
        $this->assertInternalType('bool', $responseData->data->attributes->is_completed);
    }

    public function testShowNotAllowed()
    {
        $this->mockUserFound(123);
        
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => 123]);

        $this->json('GET', '/api/v1/checklists/' . $checklist->id, [], $this->postHeaders)
            ->assertResponseStatus(403);
    }

    public function testShowTeamOwnChecklist()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);
        $response = $this->setupResponse($checklist);

        $this->json('GET', '/api/v1/checklists/' . $checklist->id, [], $this->postHeadersTeamScope)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $responseData = json_decode($this->response->getContent());

        $this->assertInternalType('string', $responseData->data->attributes->object_id);
        $this->assertInternalType('bool', $responseData->data->attributes->is_completed);
    }

    public function testShowTeamChecklist()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => 556397]);
        $response = $this->setupResponse($checklist);

        $this->json('GET', '/api/v1/checklists/' . $checklist->id, [], $this->postHeadersTeamScope)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $responseData = json_decode($this->response->getContent());

        $this->assertInternalType('string', $responseData->data->attributes->object_id);
        $this->assertInternalType('bool', $responseData->data->attributes->is_completed);
    }

    public function testShowTeamInactiveMemberChecklist()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->inactiveMemberId]);
        $response = $this->setupResponse($checklist);

        $this->json(
            'GET',
            '/api/v1/checklists/' . $checklist->id,
            [],
            $this->postHeadersTeamScope
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $responseData = json_decode($this->response->getContent());

        $this->assertInternalType('string', $responseData->data->attributes->object_id);
        $this->assertInternalType('bool', $responseData->data->attributes->is_completed);
    }

    public function testShowTeamOtherChecklist()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => 556399]);
        $response = $this->setupResponse($checklist);

        $this->json('GET', '/api/v1/checklists/' . $checklist->id, [], $this->postHeadersTeamScope)
            ->assertResponseStatus(403);
    }

    public function testStore()
    {
        $checklist = factory(ChecklistEloquent::class)->make(['user_id' => $this->userID]);
        $checklistPayloads = $this->setupPayloads($checklist->toArray());
        $response = $this->setupResponse($checklist, [], ['id']);

        $this->json('POST', '/api/v1/checklists', $checklistPayloads, $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(201);

        $id = $this->decodeResponseJson()['data']['id'];
        $dbResult = $this->setupDBResult($checklist, ['id' => $id], ['type']);
        $dbResult['updated_by'] = null;
        $dbResult['completed_at'] = null;
        $dbResult['is_completed'] = false;

        $this->seeInDatabase('checklists', $dbResult);
    }

    /**
     * @dataProvider dueDateDataProvider
     */
    public function testStoreWithDueDate($duePayload, $dueResponse)
    {
        $checklist = factory(ChecklistEloquent::class)->make(['user_id' => $this->userID]);
        $checklistPayloads = $this->setupPayloads($checklist->toArray());
        $checklistPayloads['data']['attributes']['due'] = $duePayload;

        $response = $this->setupResponse($checklist, ['due' => $dueResponse], ['id']);

        $this->json('POST', '/api/v1/checklists', $checklistPayloads, $this->postHeaders)
            ->seeJson($response)
            ->assertResponseStatus(201);
    }

    public function testStoreWithUserId()
    {
        config(['app.env' => 'dev']);
        $userId = 885;

        $checklist = factory(ChecklistEloquent::class)->make(['user_id' => $userId]);
        $checklistPayloads = $this->setupPayloads($checklist->toArray());
        Arr::set($checklistPayloads, 'data.attributes.user_id', $userId);

        $response = $this->setupResponse($checklist, [], ['id']);

        $this->json('POST', '/api/v1/checklists', $checklistPayloads, $this->postHeaders)
            ->seeJson($response)
            ->assertResponseStatus(201);

        config(['app.env' => 'prod']);

        $checklist = factory(ChecklistEloquent::class)->make(['user_id' => $userId]);
        $checklistPayloads = $this->setupPayloads($checklist->toArray());
        Arr::set($checklistPayloads, 'data.attributes.user_id', $userId);

        $response = $this->setupResponse($checklist, ['created_by' => $this->userID], ['id']);

        $this->json('POST', '/api/v1/checklists', $checklistPayloads, $this->postHeaders)
            ->seeJson($response)
            ->assertResponseStatus(201);
    }

    public function testUpdate()
    {
        $savedChecklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);

        $this->seeInDatabase('checklists', $savedChecklist->toArray());

        $updateChecklist = factory(ChecklistEloquent::class)->make(['user_id' => $this->userID]);
        $checklistPayloads = $this->setupPayloads($updateChecklist->toArray());
        $response = $this->setupResponse($updateChecklist, ['id' => (string) $savedChecklist->id]);

        $this->json('PATCH', '/api/v1/checklists/' . $savedChecklist->id, $checklistPayloads, $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateChecklist, ['id' => $savedChecklist->id], ['type', 'created_by']);
        $dbResult['updated_by'] = $this->userID;
        $dbResult['user_id'] = $this->userID;

        $this->seeInDatabase('checklists', $dbResult);
    }

    public function testUpdateNotAllowed()
    {
        $this->mockUserFound(123);

        $savedChecklist = factory(ChecklistEloquent::class)->create(['user_id' => 123]);

        $this->seeInDatabase('checklists', $savedChecklist->toArray());

        $updateChecklist = factory(ChecklistEloquent::class)->make(['user_id' => $this->userID]);
        $checklistPayloads = $this->setupPayloads($updateChecklist->toArray());

        $this->json('PATCH', '/api/v1/checklists/' . $savedChecklist->id, $checklistPayloads, $this->postHeaders)
            ->assertResponseStatus(403);
    }

    public function testUpdateInactiveUser()
    {
        $this->mockUserNotFound();

        $savedChecklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->inactiveMemberId]);

        $this->seeInDatabase('checklists', $savedChecklist->toArray());

        $updateChecklist = factory(ChecklistEloquent::class)->make(['user_id' => $this->inactiveMemberId]);
        $checklistPayloads = $this->setupPayloads($updateChecklist->toArray());
        $response = $this->setupResponse($updateChecklist, ['id' => (string) $savedChecklist->id]);

        $this->json('PATCH', '/api/v1/checklists/' . $savedChecklist->id, $checklistPayloads, $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateChecklist, ['id' => $savedChecklist->id], ['type', 'created_by']);
        $dbResult['updated_by'] = $this->userID;
        $dbResult['user_id'] = $this->inactiveMemberId;

        $this->seeInDatabase('checklists', $dbResult);
    }

    /**
     * @dataProvider dueDateDataProvider
     */
    public function testUpdateDueDate($duePayload, $dueResponse)
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);

        $checklistPayloads = [
            'data' => [
                'attributes' => [
                    'due' => $duePayload,
                ],
            ],
        ];

        $response = [
            'id'  => (string) $checklist->id,
            'due' => $dueResponse,
        ];

        $this->json('PATCH', '/api/v1/checklists/' . $checklist->id, $checklistPayloads, $this->postHeaders);
        $this->seeJson($response);
    }

    public function dueDateDataProvider()
    {
        return [
            [null, null],
            ['2001-01-01 10:01:01', '2001-01-01T10:01:01+00:00'],
            ['2018-06-21T10:10:23.185Z', '2018-06-21T10:10:23+00:00'],
            ['2002-02-02T10:02:02+00:00', '2002-02-02T10:02:02+00:00'],
        ];
    }

    public function testUpdateTeamOwnChecklist()
    {
        $savedChecklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);
        $updateChecklist = factory(ChecklistEloquent::class)->make(['user_id' => $this->userID]);
        $checklistPayloads = $this->setupPayloads($updateChecklist->toArray());
        $response = $this->setupResponse($updateChecklist, ['id' => (string) $savedChecklist->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedChecklist->id,
            $checklistPayloads,
            $this->postHeadersTeamScope
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateChecklist, ['id' => $savedChecklist->id], ['type', 'created_by']);
        $dbResult['updated_by'] = $this->userID;
        $dbResult['user_id'] = $this->userID;

        $this->seeInDatabase('checklists', $dbResult);
    }

    public function testUpdateTeamChecklist()
    {
        $savedChecklist = factory(ChecklistEloquent::class)->create(['user_id' => 556397]);
        $updateChecklist = factory(ChecklistEloquent::class)->make(['user_id' => 556397]);
        $checklistPayloads = $this->setupPayloads($updateChecklist->toArray());
        $response = $this->setupResponse($updateChecklist, ['id' => (string) $savedChecklist->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedChecklist->id,
            $checklistPayloads,
            $this->postHeadersTeamScope
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateChecklist, ['id' => $savedChecklist->id], ['type', 'created_by']);
        $dbResult['updated_by'] = $this->userID;
        $dbResult['user_id'] = 556397;

        $this->seeInDatabase('checklists', $dbResult);
    }

    public function testUpdateTeamInactiveMemberChecklist()
    {
        $savedChecklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->inactiveMemberId]);
        $updateChecklist = factory(ChecklistEloquent::class)->make(['user_id' => $this->inactiveMemberId]);
        $checklistPayloads = $this->setupPayloads($updateChecklist->toArray());
        $response = $this->setupResponse($updateChecklist, ['id' => (string) $savedChecklist->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedChecklist->id,
            $checklistPayloads,
            $this->postHeadersTeamScope
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateChecklist, ['id' => $savedChecklist->id], ['type', 'created_by']);
        $dbResult['updated_by'] = $this->userID;
        $dbResult['user_id'] = $this->inactiveMemberId;

        $this->seeInDatabase('checklists', $dbResult);
    }

    public function testUpdateTeamOtherChecklist()
    {
        $savedChecklist = factory(ChecklistEloquent::class)->create(['user_id' => 556399]);
        $updateChecklist = factory(ChecklistEloquent::class)->make(['user_id' => 556399]);
        $checklistPayloads = $this->setupPayloads($updateChecklist->toArray());
        $response = $this->setupResponse($updateChecklist, ['id' => (string) $savedChecklist->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedChecklist->id,
            $checklistPayloads,
            $this->postHeadersTeamScope
        )
            ->assertResponseStatus(403);
    }

    public function testDestroy()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);

        $this->json('DELETE', '/api/v1/checklists/' . $checklist->id, [], $this->postHeaders);

        $this->seeStatusCode(204);
        $this->missingFromDatabase('checklists', [
            'id'         => $checklist->id,
            'deleted_at' => null,
        ]);
    }

    public function testDestroyInactiveUser()
    {
        $this->mockUserNotFound();
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->inactiveMemberId]);

        $this->json('DELETE', '/api/v1/checklists/' . $checklist->id, [], $this->postHeaders);

        $this->seeStatusCode(204);
        $this->missingFromDatabase('checklists', [
            'id'         => $checklist->id,
            'deleted_at' => null,
        ]);
    }

    public function testDestroyTeamOwnChecklist()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);

        $this->json('DELETE', '/api/v1/checklists/' . $checklist->id, [], $this->postHeadersTeamScope);

        $this->seeStatusCode(204);
        $this->missingFromDatabase('checklists', [
            'id'         => $checklist->id,
            'deleted_at' => null,
        ]);
    }

    public function testDestroyTeamChecklist()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => 556397]);

        $this->json('DELETE', '/api/v1/checklists/' . $checklist->id, [], $this->postHeadersTeamScope);

        $this->seeStatusCode(204);
        $this->missingFromDatabase('checklists', [
            'id'         => $checklist->id,
            'deleted_at' => null,
        ]);
    }

    public function testDestroyTeamOtherChecklist()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => 556399]);

        $this->json('DELETE', '/api/v1/checklists/' . $checklist->id, [], $this->postHeadersTeamScope);

        $this->seeStatusCode(403);
        $this->seeInDatabase('checklists', ['id' => $checklist->id]);
    }

    public function testDestroyTeamInactiveMemberChecklist()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->inactiveMemberId]);

        $this->json(
            'DELETE',
            '/api/v1/checklists/' . $checklist->id,
            [],
            $this->postHeadersTeamScope
        );

        $this->seeStatusCode(204);
        $this->missingFromDatabase('checklists', [
            'id'         => $checklist->id,
            'deleted_at' => null,
        ]);
    }

    public function testUpdateNullify()
    {
        $savedChecklist = factory(ChecklistEloquent::class)->create([
            'urgency' => 3,
            'user_id' => $this->userID,
            'due'     => '2019-01-01 01:01:01',
        ]);
        $this->seeInDatabase('checklists', array_merge($savedChecklist->toArray(), [
            'urgency'    => 3,
            'updated_by' => null,
            'due'        => '2019-01-01 01:01:01',
        ]));

        $checklistPayloads = [
            'data' => [
                'attributes' => [
                    'due'     => null,
                    'urgency' => 0,
                ],
            ],
        ];

        $response = $this->setupResponse($savedChecklist, ['due' => null, 'urgency' => 0]);

        $this->json('PATCH', '/api/v1/checklists/' . $savedChecklist->id, $checklistPayloads, $this->postHeaders)
            ->seeJson($response)
            ->assertResponseStatus(200);

        $this->should('checklist due can be nullified, urgency can be reset to 0', function () use ($savedChecklist) {
            $dbResult = $this->setupDBResult($savedChecklist, [
                'due'        => null,
                'urgency'    => 0,
                'updated_by' => $this->userID,
            ], ['type']);

            $this->seeInDatabase('checklists', $dbResult);
        });
    }

    public function testDueChecklistNotificationWhenChecklistCreated()
    {
        $checklist = factory(ChecklistEloquent::class)->make([
            'user_id' => $this->userID,
            'is_completed' => false,
            'due' => Carbon::now()->addDays(2),
        ]);
        $this->actingAs($this->user);
        $this->activateObservers();

        $this->assertJobDispatched(DueChecklistsNotificationJob::class, [
            function () use ($checklist) {
                return $checklist->is_completed === false && $checklist->due->gt(Carbon::now());
            }
        ]);

        $checklistPayloads = $this->setupPayloads($checklist->toArray());
        $response = $this->setupResponse($checklist, [], ['id']);

        $this->json('POST', '/api/v1/checklists', $checklistPayloads, $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(201);

        $id = $this->decodeResponseJson()['data']['id'];
        $dbResult = $this->setupDBResult($checklist, ['id' => $id], ['type']);
        $dbResult['updated_by'] = null;
        $dbResult['completed_at'] = null;
        $dbResult['is_completed'] = false;

        $this->seeInDatabase('checklists', $dbResult);
    }

    public function testDueChecklistNotificationWhenChecklistUpdated()
    {
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'is_completed' => false,
            'due' => Carbon::now()->addDays(2),
        ]);

        $this->seeInDatabase('checklists', $checklist->toArray());

        $updateChecklist = factory(ChecklistEloquent::class)->make([
            'user_id' => $this->userID,
            'is_completed' => false
        ]);
        $checklistPayloads = $this->setupPayloads($updateChecklist->toArray());
        $response = $this->setupResponse($updateChecklist, ['id' => (string) $checklist->id]);

        $this->actingAs($this->user);
        $this->activateObservers();

        $this->assertJobDispatched(DueChecklistsNotificationJob::class, [
            function () use ($checklist, $updateChecklist) {
                return $checklist->is_completed === false
                    && $checklist->due->gt(Carbon::now())
                    && $checklist->due != $updateChecklist->due;
            },
        ]);

        $this->json('PATCH', '/api/v1/checklists/' . $checklist->id, $checklistPayloads, $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateChecklist, ['id' => $checklist->id], ['type', 'created_by']);
        $dbResult['updated_by'] = $this->userID;
        $dbResult['user_id'] = $this->userID;

        $this->seeInDatabase('checklists', $dbResult);
    }
}
