<?php

namespace Test\Controllers;

use App\Jobs\Audit\CompleteItemJob;
use App\Jobs\Audit\DeleteItemJob;
use App\Jobs\Audit\IncompleteItemJob;
use App\Services\Checklists\ChecklistEloquent;
use App\Services\Items\ItemEloquent;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;

class ItemHistoryTest extends ItemsControllerTestCase
{
    private const DEAL_OBJECT = 'deals';

    public function dueDateDataProvider()
    {
        $dueDate = Carbon::now()->addDays(5);
        Carbon::setTestNow($dueDate);

        return [
            [$dueDate, $dueDate->toIso8601String()],
            [null, null],
        ];
    }
    /**
     * @dataProvider dueDateDataProvider
     */
    public function testHistoryItemSnooze($duePayload, $dueResponse)
    {
        $this->withoutJobs();

        $checklist = factory(ChecklistEloquent::class)->create();
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
        ]);

        $initialDispatcher = Event::getFacadeRoot();
        Event::fake();
        ItemEloquent::setEventDispatcher($initialDispatcher);

        $updateItem = factory(ItemEloquent::class)->make([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by' => $this->userID,
            'due' => $duePayload,
        ]);

        $data = $this->setupPayloads($updateItem->toArray());

        $response = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeaders
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateItem, ['id' => $savedItem->id], ['type']);
        $dbResult['updated_by'] = $this->userID;

        $this->seeInDatabase('items', $dbResult);

        $history = [
            "loggable_type" => "items",
            "loggable_id" => $savedItem->id,
            "action" => "snooze",
            "kwuid" => $this->userID,
            "value" => $dueResponse,
        ];

        $this->seeInDatabase('histories', $history);
    }

    public function testHistoryItemComplete()
    {

        $dateCompleted = Carbon::now()->toIso8601String();
        Carbon::setTestNow($dateCompleted);

        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);
        $items = factory(ItemEloquent::class, 5)->create([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'completed_at' => null,
            'is_completed' => 0
        ]);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(CompleteItemJob::class);
        }

        $data = ['data' => $items->map(function ($item) {
            return ['item_id' => $item->id];
        })->toArray()];

        $initialDispatcher = Event::getFacadeRoot();
        Event::fake();
        ChecklistEloquent::setEventDispatcher($initialDispatcher);

        $result = $this->json('POST', '/api/v1/checklists/complete', $data, $this->postHeaders)
            ->assertResponseStatus(200);

        foreach ($items as $item) {
            $this->seeInDatabase('histories', [
                'loggable_type' => 'items',
                'loggable_id' => $item->id,
                'action' => 'complete',
                'kwuid' => $this->userID,
                'value' => 1,
            ]);
        }
    }

    public function testHistoryItemIncomplete()
    {

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'is_completed' => true,
            'completed_at' => '2019-01-01 00:00:00',
        ]);
        $items = factory(ItemEloquent::class, 5)->create([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => true,
            'completed_at' => '2019-01-01 00:00:00',
        ]);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(IncompleteItemJob::class);
        }

        $data = ['data' => $items->map(function ($item) {
            return ['item_id' => $item->id];
        })->toArray()];

        $this->json('POST', '/api/v1/checklists/incomplete', $data, $this->postHeaders)
            ->assertResponseStatus(200);

        foreach ($items as $item) {
            $this->seeInDatabase('histories', [
                'loggable_type' => 'items',
                'loggable_id' => $item->id,
                'action' => 'complete',
                'kwuid' => $this->userID,
                'value' => 0,
            ]);
        }
    }

    public function testHistoryItemAssign()
    {
        $this->withoutJobs();
        $checklist = factory(ChecklistEloquent::class)->create();
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'assignee_id' => null,
        ]);

        $initialDispatcher = Event::getFacadeRoot();
        Event::fake();
        ItemEloquent::setEventDispatcher($initialDispatcher);

        $updateItem = factory(ItemEloquent::class)->make([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by' => $this->userID,
            'assignee_id' => random_int(100, 999),
        ]);

        $data = $this->setupPayloads($updateItem->toArray());

        $this->json(
            'PATCH',
            "/api/v1/checklists/{$savedItem->checklist_id}/items/{$savedItem->id}",
            $data,
            $this->postHeaders
        )->assertResponseStatus(200);

        $this->seeInDatabase('histories', [
            'loggable_type' => 'items',
            'loggable_id' => $savedItem->id,
            'action' => 'assign',
            'kwuid' => $this->userID,
            'value' => $updateItem->assignee_id,
        ]);
    }

    public function testHistoryItemReAssign()
    {
        $this->withoutJobs();
        $checklist = factory(ChecklistEloquent::class)->create();
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
        ]);

        $initialDispatcher = Event::getFacadeRoot();
        Event::fake();
        ItemEloquent::setEventDispatcher($initialDispatcher);

        $updateItem = factory(ItemEloquent::class)->make([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by' => $this->userID,
            "assignee_id" => random_int(100, 999),
        ]);

        $data = $this->setupPayloads($updateItem->toArray());

        $response = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeaders
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateItem, ['id' => $savedItem->id], ['type']);
        $dbResult['updated_by'] = $this->userID;

        $this->seeInDatabase('items', $dbResult);

        $history = [
            "loggable_type" => "items",
            "loggable_id" => $savedItem->id,
            "action" => "assign",
            "kwuid" => $this->userID,
            "value" => $updateItem->assignee_id,
        ];

        $this->seeInDatabase('histories', $history);
    }

    public function testHistoryItemUnAssign()
    {
        $this->withoutJobs();
        $checklist = factory(ChecklistEloquent::class)->create();
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            "assignee_id" => random_int(100, 999),
        ]);

        $initialDispatcher = Event::getFacadeRoot();
        Event::fake();
        ItemEloquent::setEventDispatcher($initialDispatcher);

        $updateItem = factory(ItemEloquent::class)->make([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by' => $this->userID,
            "assignee_id" => null,
        ]);

        $data = $this->setupPayloads($updateItem->toArray());

        $response = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeaders
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateItem, ['id' => $savedItem->id], ['type']);
        $dbResult['updated_by'] = $this->userID;

        $this->seeInDatabase('items', $dbResult);

        $history = [
            "loggable_type" => "items",
            "loggable_id" => $savedItem->id,
            "action" => "assign",
            "kwuid" => $this->userID,
            "value" => null,
        ];

        $this->seeInDatabase('histories', $history);
    }

    public function testHistoryItemArchive()
    {
        $now = Carbon::now();
        Carbon::setTestNow($now);

        $checklist = factory(ChecklistEloquent::class)->create();
        $item = factory(ItemEloquent::class)->create([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id
        ]);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(DeleteItemJob::class);
        }

        $initialDispatcher = Event::getFacadeRoot();
        Event::fake();
        ChecklistEloquent::setEventDispatcher($initialDispatcher);

        $this->json(
            'DELETE',
            "/api/v1/checklists/{$checklist->id}/items/{$item->id}",
            [],
            $this->postHeaders
        )->seeStatusCode(204);

        $this->seeInDatabase('histories', [
            'loggable_type' => 'items',
            'loggable_id' => $item->id,
            'action' => 'archive',
            'kwuid' => $this->userID,
            'value' => 1,
        ]);
    }
}
