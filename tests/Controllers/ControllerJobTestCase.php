<?php

namespace Test\Controllers;

use Laravel\Lumen\Testing\DatabaseTransactions;
use Test\TestCase;

abstract class ControllerJobTestCase extends TestCase
{
    use DatabaseTransactions;

    /**
     * Setup test.
     */
    public function setUp()
    {
        parent::setUp();

        $userinfo = [
            'given_name'         => 'kelle1',
            'password'           => 'Password1',
            'client_id'          => 'e3fb1740-d9e2-0135-3d40-06dd31cd45e266469',
            'updated_at'         => '2018-01-23T07:19:36.950Z',
            'family_name'        => 'kelle1',
            'preferred_username' => 'kelle1',
            'custom_fields'      => ['KW_UID' => '556396'],
            'email'              => 'kelle1@kw.com',
            'name'               => 'kelle1 kelle1',
            'sub'                => '37087183',
        ];

        $this->userinfo = $userinfo;

        $this->userID = (int) array_get($userinfo, 'custom_fields.KW_UID');

        $xUserInfo         = json_encode($userinfo);
        $xConsumerUsername = 'onelogin';

        $this->headers = [
            'Authorization'       => 'Bearer ABC',
            'x-userinfo'          => $xUserInfo,
            'x-consumer-username' => $xConsumerUsername,
        ];

        $this->postHeaders = array_merge(
            $this->headers,
            [
            'Content-Type' => 'application/vnd.api+json',
            'Accept'       => 'application/vnd.api+json',
            ]
        );
    }

    public function setupPayloads(array $properties)
    {
        $data = [
            'data' => [
                'attributes' => $properties,
            ],
        ];

        return $data;
    }

    public function setupDeletePayloads($properties)
    {
        $data = [
            'deleted_count' => $properties,
        ];

        return $data;
    }

    public function seeJson(array $data = null, $negate = false)
    {
        return $this->seeJsonContains($data, $negate);
    }

    /**
     * Return the decoded response JSON.
     *
     * @return array
     */
    protected function decodeResponseJson()
    {
        $decodedResponse = json_decode($this->response->getContent(), true);

        if (is_null($decodedResponse) || false === $decodedResponse) {
            $this->fail('Invalid JSON was returned from the route. Perhaps an exception was thrown?');
        }

        return $decodedResponse;
    }

    /**
     * Parse date fields to UTC as we will store any date field to UTC on db.
     */
    protected function setupRecord(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            if ($this->containsDate($value)) {
                $attributes[$key] = $this->parseDateValue($value);
            }
        }

        return $attributes;
    }

    /**
     * Check if value is a date.
     */
    protected function containsDate($value)
    {
        if ((is_object($value) && $value instanceof Carbon)
          || (
              ! empty($value)
            && strtotime($value)
            && preg_match('/(\d{4})-(\d{2})-(\d{2}) (\d{2})\:(\d{2})\:(\d{2})/', $value)
          )
          ) {
            return true;
        }

        return false;
    }
}
