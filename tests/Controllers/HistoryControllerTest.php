<?php

namespace Test\Controllers;

use App\Services\History\HistoryEloquent;

class HistoryControllerTest extends HistoryControllerTestCase
{
    public function testIndex()
    {
        $history = HistoryEloquent::create([
            "loggable_type" => "items",
            "loggable_id" => 1,
            "action" => "assign",
            "kwuid" => 123,
            "value" => 123,
        ]);

        $this->json('GET', '/api/v1/checklists/histories', [], $this->postHeaders);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'meta' => [
                'count',
                'total',
            ],
            'data' => [
                $this->basicJsonStructure['data'],
            ],
            'links' => [
                'first',
                'last',
                'next',
                'prev',
            ],
        ]);

        $this->seeJson($this->setupResponse($history));
    }

    public function testShow()
    {
        $history = HistoryEloquent::create([
            "loggable_type" => "items",
            "loggable_id" => 1,
            "action" => "assign",
            "kwuid" => 123,
            "value" => 123,
        ]);

        $this->json('GET', "/api/v1/checklists/histories/$history->id", [], $this->postHeaders)
            ->seeJson($this->setupResponse($history))
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);
    }
}
