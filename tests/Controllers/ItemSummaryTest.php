<?php

namespace Test\Controllers;

use App\Services\Checklists\ChecklistEloquent;
use App\Services\Items\ItemEloquent;
use Illuminate\Support\Carbon;

class ItemSummaryTest extends ControllerTestCase
{
    use \Codeception\Specify;

    public function testCount()
    {
        $dueToday = Carbon::createFromDate(2019, 04, 25)->setTime(0, 0, 0);
        $pastWeek = Carbon::createFromDate(2019, 04, 20)->setTime(0, 0, 0);
        $pastMonth = Carbon::createFromDate(2019, 03, 10)->setTime(0, 0, 0);
        $nextMonth = Carbon::createFromDate(2019, 05, 10)->setTime(0, 0, 0);

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'object_domain' => 'deals',
            'object_id' => 1
        ]);
        $checklistFromOtherUser = factory(ChecklistEloquent::class)->create([
            'user_id' => 1,
            'object_domain' => 'deals',
            'object_id' => 1
        ]);
        $checklistWithOtherObjectDomain = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'object_domain' => 'others',
            'object_id' => 1
        ]);

        $this->itemToday = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'is_completed' => 0,
            'due' => $dueToday,
            'checklist_id' => $checklist->id
        ]);
        $this->itemPastWeek = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'is_completed' => 0,
            'due' => $pastWeek,
            'checklist_id' => $checklist->id
        ]);
        $this->itemPastMonth = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'is_completed' => 0,
            'due' => $pastMonth,
            'checklist_id' => $checklist->id
        ]);
        $this->itemNextMonth = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'is_completed' => 0,
            'due' => $nextMonth,
            'checklist_id' => $checklist->id
        ]);
        $this->itemFromOtherUserToday = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => 1,
            'is_completed' => 0,
            'due' => $dueToday,
            'checklist_id' => $checklistFromOtherUser->id
        ]);
        $this->itemWithOtherObjectDomainToday = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'is_completed' => 0,
            'due' => $dueToday,
            'checklist_id' => $checklistWithOtherObjectDomain->id
        ]);

        $this->should('show correct count with object_domain', function () use ($dueToday) {
            $dueTodayStr = $dueToday->format('Y-m-d');
            $query = "date=$dueTodayStr&object_domain=deals";
            $this->json('GET', "/api/v1/checklists/items/summaries?$query", [], $this->postHeaders)
                 ->seeStatusCode(200)
                 ->seeJson([
                     'data' => [
                         'today' => $this->itemToday->count(),
                         'past_due' => $this->itemToday->count()
                                     + $this->itemPastWeek->count()
                                     + $this->itemPastMonth->count(),
                         'week' => [
                             'current' => $this->itemToday->count(),
                             'past' => $this->itemPastWeek->count()
                         ],
                         'month' => [
                             'current' => $this->itemToday->count()
                                     + $this->itemPastWeek->count(),
                             'past' => $this->itemPastMonth->count()
                         ],
                         'total' => $this->itemToday->count()
                                 + $this->itemPastWeek->count()
                                 + $this->itemPastMonth->count()
                                 + $this->itemNextMonth->count(),
                     ]
                 ]);
        });

        $this->should('show correct count without object_domain', function () use ($dueToday) {
            $dueTodayStr = $dueToday->format('Y-m-d');
            $query = "date=$dueTodayStr";
            $this->json('GET', "/api/v1/checklists/items/summaries?$query", [], $this->postHeaders)
                 ->seeStatusCode(200)
                 ->seeJson([
                     'data' => [
                         'today' => $this->itemToday->count()
                                 + $this->itemWithOtherObjectDomainToday->count(),
                         'past_due' => $this->itemToday->count()
                                     + $this->itemPastWeek->count()
                                     + $this->itemPastMonth->count()
                                     + $this->itemWithOtherObjectDomainToday->count(),
                         'week' => [
                             'current' => $this->itemToday->count()
                                     + $this->itemWithOtherObjectDomainToday->count(),
                             'past' => $this->itemPastWeek->count()
                         ],
                         'month' => [
                             'current' => $this->itemToday->count()
                                     + $this->itemPastWeek->count()
                                     + $this->itemWithOtherObjectDomainToday->count(),
                             'past' => $this->itemPastMonth->count()
                         ],
                         'total' => $this->itemToday->count()
                                 + $this->itemPastWeek->count()
                                 + $this->itemPastMonth->count()
                                 + $this->itemNextMonth->count()
                                 + $this->itemWithOtherObjectDomainToday->count(),
                     ]
                 ]);
        });
    }

    public function testCountWithTimezone()
    {
        $dueToday = Carbon::createFromDate(2019, 04, 25)->setTime(0, 0, 0);
        $dueYesterday = $dueToday->copy()->subDays(1)->setTime(17, 0, 0);
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'object_domain' => 'deals',
            'object_id' => 1
        ]);
        $itemToday = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'is_completed' => 0,
            'due' => $dueToday,
            'checklist_id' => $checklist->id
        ]);
        $itemYesterday = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'is_completed' => 0,
            'due' => $dueYesterday,
            'checklist_id' => $checklist->id
        ]);

        // Due today in Asia/Jakarta would be started from 2019-04-24T17:00:00Z
        $dueTodayStr = $dueToday->format('Y-m-d');
        $query = "date=$dueTodayStr&tz=Asia/Jakarta";

        $this->json('GET', "/api/v1/checklists/items/summaries?$query", [], $this->postHeaders)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => [
                    'today' => 10,
                    'past_due' => 10,
                    'week' => [
                        'current' => 10,
                        'past' => 0
                    ],
                    'month' => [
                        'current' => 10,
                        'past' => 0
                    ],
                    'total' => 10
                ]
            ]);
    }

    public function testCacheInvalidationWhenItemCreated()
    {
        $this->withoutJobs();

        $assigneeId = $this->userID;
        $dueToday = Carbon::createFromDate(2019, 04, 25)->setTime(0, 0, 0);
        $dueTomorrow = $dueToday->copy()->addDays(1);

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'object_domain' => 'deals',
            'object_id' => 1
        ]);
        $itemDueToday = factory(ItemEloquent::class)->create([
            'user_id' => $this->userID,
            'is_completed' => 0,
            'due' => $dueToday,
            'checklist_id' => $checklist->id,
            'assignee_id' => $assigneeId
        ]);
        $newItemDueTomorrow = factory(ItemEloquent::class)->make([
            'user_id' => $this->userID,
            'is_completed' => 0,
            'due' => $dueTomorrow,
            'checklist_id' => $checklist->id,
            'assignee_id' => $assigneeId
        ]);

        $dueTodayStr = $dueToday->format('Y-m-d');
        $query = "date=$dueTodayStr";

        $this->activateObservers();

        // When item newly created

        $this->json('GET', "/api/v1/checklists/items/summaries?$query", [], $this->postHeaders)
            ->seeJson([
                'data' => [
                    'today' => 1,
                    'past_due' => 1,
                    'week' => [
                        'current' => 1,
                        'past' => 0
                    ],
                    'month' => [
                        'current' => 1,
                        'past' => 0
                    ],
                    'total' => 1
                ]
            ])
            ->seeStatusCode(200);

        $newItemDueToday = factory(ItemEloquent::class)->make([
            'user_id' => $this->userID,
            'is_completed' => 0,
            'due' => $dueToday,
            'checklist_id' => $checklist->id,
            'assignee_id' => $assigneeId
        ]);
        $payloads = $this->setupPayloads($newItemDueToday->toArray());
        $this->json('POST', "/api/v1/checklists/{$checklist->id}/items", $payloads, $this->postHeaders)
            ->seeStatusCode(201);

        $this->json('GET', "/api/v1/checklists/items/summaries?$query", [], $this->postHeaders)
            ->seeJson([
                'data' => [
                    'today' => 2,
                    'past_due' => 2,
                    'week' => [
                        'current' => 2,
                        'past' => 0
                    ],
                    'month' => [
                        'current' => 2,
                        'past' => 0
                    ],
                    'total' => 2
                ]
            ])
            ->seeStatusCode(200);
    }

    public function testCacheInvalidationWhenItemUpdated()
    {
        $this->withoutJobs();

        $assigneeId = $this->userID;
        $dueToday = Carbon::createFromDate(2019, 04, 25)->setTime(0, 0, 0);
        $dueYesterday = $dueToday->copy()->subDays(1);

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'object_domain' => 'deals',
            'object_id' => 1
        ]);
        $itemWithNoAssignee = factory(ItemEloquent::class)->create([
            'user_id' => $this->userID,
            'is_completed' => 0,
            'due' => $dueToday,
            'checklist_id' => $checklist->id,
            'assignee_id' => null
        ]);

        $dueTodayStr = $dueToday->format('Y-m-d');
        $query = "date=$dueTodayStr";

        $this->activateObservers();

        // When item's assignee_id changed

        $this->json('GET', "/api/v1/checklists/items/summaries?$query", [], $this->postHeaders)
            ->seeJson([
                'data' => [
                    'today' => 0,
                    'past_due' => 0,
                    'week' => [
                        'current' => 0,
                        'past' => 0
                    ],
                    'month' => [
                        'current' => 0,
                        'past' => 0
                    ],
                    'total' => 0
                ]
            ])
            ->seeStatusCode(200);

        $newItemWithAssignee = factory(ItemEloquent::class)->make([
            'user_id' => $this->userID,
            'is_completed' => 0,
            'due' => $dueToday,
            'checklist_id' => $checklist->id,
            'assignee_id' => $assigneeId
        ]);
        $payloads = $this->setupPayloads($newItemWithAssignee->toArray());
        $this->json(
            'PATCH',
            "/api/v1/checklists/{$checklist->id}/items/{$itemWithNoAssignee->id}",
            $payloads,
            $this->postHeaders
        )
            ->seeStatusCode(200);

        $this->json('GET', "/api/v1/checklists/items/summaries?$query", [], $this->postHeaders)
            ->seeJson([
                'data' => [
                    'today' => 1,
                    'past_due' => 1,
                    'week' => [
                        'current' => 1,
                        'past' => 0
                    ],
                    'month' => [
                        'current' => 1,
                        'past' => 0
                    ],
                    'total' => 1
                ]
            ])
            ->seeStatusCode(200);

        // When item's due changed

        $newItemWithNewDue = factory(ItemEloquent::class)->make([
            'user_id' => $this->userID,
            'is_completed' => 0,
            'due' => $dueYesterday,
            'checklist_id' => $checklist->id,
            'assignee_id' => $assigneeId
        ]);
        $payloads = $this->setupPayloads($newItemWithNewDue->toArray());
        $this->json(
            'PATCH',
            "/api/v1/checklists/{$checklist->id}/items/{$itemWithNoAssignee->id}",
            $payloads,
            $this->postHeaders
        )
            ->seeStatusCode(200);

        $this->json('GET', "/api/v1/checklists/items/summaries?$query", [], $this->postHeaders)
            ->seeJson([
                'data' => [
                    'today' => 0,
                    'past_due' => 1,
                    'week' => [
                        'current' => 1,
                        'past' => 0
                    ],
                    'month' => [
                        'current' => 1,
                        'past' => 0
                    ],
                    'total' => 1
                ]
            ])
            ->seeStatusCode(200);

        // When item's is_completed changed to complete

        $this->json('POST', "/api/v1/checklists/complete", [
            'data' => [[
                'item_id' => $itemWithNoAssignee->id
            ]]
        ], $this->postHeaders)
            ->seeStatusCode(200);

        $this->json('GET', "/api/v1/checklists/items/summaries?$query", [], $this->postHeaders)
            ->seeJson([
                'data' => [
                    'today' => 0,
                    'past_due' => 0,
                    'week' => [
                        'current' => 0,
                        'past' => 0
                    ],
                    'month' => [
                        'current' => 0,
                        'past' => 0
                    ],
                    'total' => 0
                ]
            ])
            ->seeStatusCode(200);

        // When item's is_completed changed to incomplete

        $this->json('POST', "/api/v1/checklists/incomplete", [
            'data' => [[
                'item_id' => $itemWithNoAssignee->id
            ]]
        ], $this->postHeaders)
            ->seeStatusCode(200);

        $this->json('GET', "/api/v1/checklists/items/summaries?$query", [], $this->postHeaders)
            ->seeJson([
                'data' => [
                    'today' => 0,
                    'past_due' => 1,
                    'week' => [
                        'current' => 1,
                        'past' => 0
                    ],
                    'month' => [
                        'current' => 1,
                        'past' => 0
                    ],
                    'total' => 1
                ]
            ])
            ->seeStatusCode(200);
    }

    public function testCountWithMcGateway()
    {
        $dueToday = Carbon::createFromDate(2019, 04, 25)->setTime(0, 0, 0);
        $pastWeek = Carbon::createFromDate(2019, 04, 20)->setTime(0, 0, 0);
        $pastMonth = Carbon::createFromDate(2019, 03, 10)->setTime(0, 0, 0);
        $nextMonth = Carbon::createFromDate(2019, 05, 10)->setTime(0, 0, 0);

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
            'object_domain' => 'deals',
            'object_id' => 1
        ]);
        $checklistFromOtherUser = factory(ChecklistEloquent::class)->create([
            'user_id' => 1,
            'org_id' => $this->orgId,
            'object_domain' => 'deals',
            'object_id' => 1
        ]);
        $checklistWithOtherObjectDomain = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
            'object_domain' => 'others',
            'object_id' => 1
        ]);
        $userIdOtherMc = $this->faker->numberBetween(1000, 9000);
        $orgIdOtherMc = $this->faker->numberBetween(1000, 9000);
        $checklistOtherMc = factory(ChecklistEloquent::class)->create([
            'user_id' => $userIdOtherMc,
            'org_id' => $orgIdOtherMc,
            'object_domain' => 'others',
            'object_id' => 1
        ]);

        $this->itemToday = factory(ItemEloquent::class, 5)->create([
            'is_completed' => 0,
            'due' => $dueToday,
            'checklist_id' => $checklist->id,
            'org_id' => $checklist->org_id,

        ]);
        $this->itemPastWeek = factory(ItemEloquent::class, 5)->create([
            'is_completed' => 0,
            'due' => $pastWeek,
            'checklist_id' => $checklist->id,
            'org_id' => $checklist->org_id,
        ]);
        $this->itemPastMonth = factory(ItemEloquent::class, 5)->create([
            'is_completed' => 0,
            'due' => $pastMonth,
            'checklist_id' => $checklist->id,
            'org_id' => $checklist->org_id,
        ]);
        $this->itemNextMonth = factory(ItemEloquent::class, 5)->create([
            'is_completed' => 0,
            'due' => $nextMonth,
            'checklist_id' => $checklist->id,
            'org_id' => $checklist->org_id,
        ]);
        $this->itemFromOtherUserToday = factory(ItemEloquent::class, 5)->create([
            'is_completed' => 0,
            'due' => $dueToday,
            'checklist_id' => $checklistFromOtherUser->id,
            'org_id' => $checklistFromOtherUser->org_id,
        ]);
        $this->itemWithOtherObjectDomainToday = factory(ItemEloquent::class, 5)->create([
            'is_completed' => 0,
            'due' => $dueToday,
            'checklist_id' => $checklistWithOtherObjectDomain->id,
            'org_id' => $checklistWithOtherObjectDomain->org_id,
        ]);
        $this->itemOtherMcToday = factory(ItemEloquent::class, 5)->create([
            'is_completed' => 0,
            'due' => $dueToday,
            'checklist_id' => $checklistOtherMc->id,
            'org_id' => $checklistOtherMc->org_id,
        ]);

        $this->should('show correct count with object_domain', function () use ($dueToday) {
            $dueTodayStr = $dueToday->format('Y-m-d');
            $query = "date=$dueTodayStr&object_domain=deals";
            $this->json('GET', "/api/v1/checklists/items/summaries?$query", [], $this->postHeadersMcScope)
                ->seeStatusCode(200)
                ->seeJson([
                    'data' => [
                        'today' => $this->itemToday->count()
                            + $this->itemFromOtherUserToday->count(),
                        'past_due' => $this->itemToday->count()
                            + $this->itemFromOtherUserToday->count()
                            + $this->itemPastWeek->count()
                            + $this->itemPastMonth->count(),
                        'week' => [
                            'current' => $this->itemToday->count()
                                + $this->itemFromOtherUserToday->count(),
                            'past' => $this->itemPastWeek->count()
                        ],
                        'month' => [
                            'current' => $this->itemToday->count()
                                + $this->itemFromOtherUserToday->count()
                                + $this->itemPastWeek->count(),
                            'past' => $this->itemPastMonth->count()
                        ],
                        'total' => $this->itemToday->count()
                            + $this->itemFromOtherUserToday->count()
                            + $this->itemPastWeek->count()
                            + $this->itemPastMonth->count()
                            + $this->itemNextMonth->count(),
                    ]
                ]);
        });

        $this->should('show correct count without object_domain', function () use ($dueToday) {
            $dueTodayStr = $dueToday->format('Y-m-d');
            $query = "date=$dueTodayStr";
            $this->json('GET', "/api/v1/checklists/items/summaries?$query", [], $this->postHeadersMcScope)
                ->seeStatusCode(200)
                ->seeJson([
                    'data' => [
                        'today' => $this->itemToday->count()
                            + $this->itemFromOtherUserToday->count()
                            + $this->itemWithOtherObjectDomainToday->count(),
                        'past_due' => $this->itemToday->count()
                            + $this->itemFromOtherUserToday->count()
                            + $this->itemWithOtherObjectDomainToday->count()
                            + $this->itemPastWeek->count()
                            + $this->itemPastMonth->count(),
                        'week' => [
                            'current' => $this->itemToday->count()
                                + $this->itemFromOtherUserToday->count()
                                + $this->itemWithOtherObjectDomainToday->count(),
                            'past' => $this->itemPastWeek->count()
                        ],
                        'month' => [
                            'current' => $this->itemToday->count()
                                + $this->itemFromOtherUserToday->count()
                                + $this->itemWithOtherObjectDomainToday->count()
                                + $this->itemPastWeek->count(),
                            'past' => $this->itemPastMonth->count()
                        ],
                        'total' => $this->itemToday->count()
                            + $this->itemFromOtherUserToday->count()
                            + $this->itemWithOtherObjectDomainToday->count()
                            + $this->itemPastWeek->count()
                            + $this->itemPastMonth->count()
                            + $this->itemNextMonth->count(),
                    ]
                ]);
        });
    }
}
