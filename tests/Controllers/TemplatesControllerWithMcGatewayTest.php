<?php

namespace Test\Controllers;

use App\Services\Template\TemplateEloquent;

class TemplatesControllerWithMcGatewayTest extends TemplatesControllerTest
{
    public function setUp()
    {
        parent::setUp();

        $this->basicJsonStructure = [
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'name',
                    'items' => [
                        [
                            'urgency',
                            'due_unit',
                            'description',
                            'due_interval',
                        ],
                    ],
                    'checklist' => [
                        'due_unit',
                        'description',
                        'due_interval',
                    ],
                ],
                'links' => ['self'],
            ],
        ];

        $this->mockInactiveUsersResponse([$this->inactiveMemberId]);
    }

    public function testIndex()
    {
        $data = factory(TemplateEloquent::class, 3)->create([
            'org_id' => $this->orgId,
            'user_id' => $this->faker->randomDigit,
        ]);

        $this->json('GET', '/api/v1/checklists/templates', [], $this->postHeadersMcScope);
        $this->seeJson(['id' => (string) $data[0]->id]);
        $this->seeJson(['id' => (string) $data[1]->id]);
        $this->seeJson(['id' => (string) $data[2]->id]);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'meta' => [
                'count',
                'total',
            ],
            'data' => [
                $this->basicJsonStructure['data'],
            ],
            'links' => [
                'first',
                'last',
                'next',
                'prev',
            ],
        ]);
    }

    public function testIndexDifferentOrgId()
    {
        $data = factory(TemplateEloquent::class, 3)->create([
            'org_id' => $this->faker->randomDigit,
            'user_id' => $this->faker->randomDigit,
        ]);

        $this->json('GET', '/api/v1/checklists/templates', [], $this->postHeadersMcScope);
        $this->dontSeeJson(['id' => (string) $data[0]->id]);
        $this->dontSeeJson(['id' => (string) $data[1]->id]);
        $this->dontSeeJson(['id' => (string) $data[2]->id]);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'meta' => [
                'count',
                'total',
            ],
            'data',
        ]);
    }

    public function testStore()
    {
        $this->mockRoleServiceRequest(true);

        $template = factory(TemplateEloquent::class)->create(['org_id' => $this->orgId]);
        $data = $this->setupPayloads($template->toArray());
        $response = $this->setupResponse($template, [], ['id']);
        $this->json('POST', '/api/v1/checklists/templates', $data, $this->postHeadersMcScope)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(201);

        $id = $this->decodeResponseJson()['data']['id'];
        $dbResult = $this->setupDBResult($template, ['id' => $id], ['type']);
        array_set($dbResult, 'org_id', $this->orgId);

        $this->seeInDatabase('templates', $dbResult);
    }

    public function testStoreDontHaveDestructiveAction()
    {
        $this->mockRoleServiceRequest(false);

        $template = factory(TemplateEloquent::class)->create();
        $data = $this->setupPayloads($template->toArray());
        $response = $this->setupResponse($template, [], ['id']);
        $this->json('POST', '/api/v1/checklists/templates', $data, $this->postHeadersMcScope)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(201);

        $id = $this->decodeResponseJson()['data']['id'];
        $dbResult = $this->setupDBResult($template, ['id' => $id, 'org_id' => null], ['type']);

        $this->seeInDatabase('templates', $dbResult);
    }

    public function testShowOwnMcTemplate()
    {
        $ownTemplate = factory(TemplateEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
        ]);
        $response = $this->setupResponse($ownTemplate);
        $this->json('GET', '/api/v1/checklists/templates/' . $ownTemplate->id, [], $this->postHeadersMcScope)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);
    }

    public function testShowOtherMcTemplate()
    {
        $otherTemplate = factory(TemplateEloquent::class)->create([
            'user_id' => $this->faker->randomDigit,
            'org_id' => $this->faker->randomDigit,
        ]);
        $this->json('GET', '/api/v1/checklists/templates/' . $otherTemplate->id, [], $this->postHeadersMcScope)
            ->dontSeeJson(['id' => (string) $otherTemplate->id])
            ->assertResponseStatus(403);
    }

    public function testShowInactiveMemberTemplate()
    {
        $inactiveUserTemplate = factory(TemplateEloquent::class)->create([
            'org_id' => $this->orgId,
            'user_id' => $this->inactiveMemberId,
        ]);
        $response = $this->setupResponse($inactiveUserTemplate);
        $this->json(
            'GET',
            '/api/v1/checklists/templates/' . $inactiveUserTemplate->id,
            [],
            $this->postHeadersMcScope
        )->seeJson($response)
        ->assertResponseStatus(200);
    }

    public function testShowInactiveMemberTemplateOnOtherMc()
    {
        $inactiveUserTemplate = factory(TemplateEloquent::class)->create([
            'org_id' => $this->faker->randomDigit,
            'user_id' => $this->inactiveMemberId,
        ]);
        $response = $this->setupResponse($inactiveUserTemplate);
        $this->json(
            'GET',
            '/api/v1/checklists/templates/' . $inactiveUserTemplate->id,
            [],
            $this->postHeadersMcScope
        )->dontSeeJson(['id' => (string) $inactiveUserTemplate->id])
        ->assertResponseStatus(403);
    }

    public function testShowMCAUserAbleToAccess()
    {
        $ownTemplate = factory(TemplateEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
        ]);
        $response = $this->setupResponse($ownTemplate);
        $this->json('GET', '/api/v1/checklists/templates/' . $ownTemplate->id, [], $this->postHeadersMcScope)
            ->seeJson($response)
            ->assertResponseStatus(200);
    }

    public function testShowMCAUserAbleToAccessOtherTemplate()
    {
        $ownTemplate = factory(TemplateEloquent::class)->create([
            'user_id' => $this->faker->numberBetween(1000, 9000),
            'org_id' => $this->orgId,
        ]);
        $response = $this->setupResponse($ownTemplate);
        $this->json('GET', '/api/v1/checklists/templates/' . $ownTemplate->id, [], $this->postHeadersMcScope)
            ->seeJson($response)
            ->assertResponseStatus(200);
    }

    public function testAssign()
    {
        $this->mockRoleServiceRequest(true);

        $template = factory(TemplateEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
        ]);
        $domain = ['object_id' => 12, 'object_domain' => 'contact'];
        $data = $this->setupAssignPayloads($domain);

        $this->json(
            'POST',
            '/api/v1/checklists/templates/' . $template->id . '/assign',
            $data,
            $this->postHeadersMcScope
        )->assertResponseStatus(201);

        $responseData = json_decode($this->response->getContent());
        $this->assertInternalType('string', $responseData->data->attributes->object_id);
        $this->assertInternalType('bool', $responseData->data->attributes->is_completed);

        $dbResultInChecklist = [
            'id' => $this->decodeResponseJson()['data']['id'],
            'description' => $template->data['checklist']['description'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
        ];

        $dbResultInItems0 = [
            'id' => $this->decodeResponseJson()['included'][0]['id'],
            'checklist_id' => $this->decodeResponseJson()['data']['id'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
        ];

        $dbResultInItems1 = [
            'id' => $this->decodeResponseJson()['included'][1]['id'],
            'checklist_id' => $this->decodeResponseJson()['data']['id'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
        ];

        $dbResultInChecklist = array_merge($dbResultInChecklist, $domain);

        $this->seeInDatabase('checklists', $dbResultInChecklist);
        $this->seeInDatabase('items', $dbResultInItems0);
        $this->seeInDatabase('items', $dbResultInItems1);
    }

    public function testAssignOtherMcTemplate()
    {
        $this->mockRoleServiceRequest(true);

        $template = factory(TemplateEloquent::class)->create([
            'user_id' => $this->faker->randomDigit,
            'org_id' => $this->faker->randomDigit,
        ]);
        $domain = ['object_id' => 12, 'object_domain' => 'contact'];
        $data = $this->setupAssignPayloads($domain);

        $this->json(
            'POST',
            '/api/v1/checklists/templates/' . $template->id . '/assign',
            $data,
            $this->postHeadersMcScope
        )->assertResponseStatus(403);
    }

    public function testAssignMcInactiveMember()
    {
        $this->mockRoleServiceRequest(true);

        $template = factory(TemplateEloquent::class)->create([
            'user_id' => $this->inactiveMemberId,
            'org_id' => $this->orgId,
        ]);
        $domain = ['object_id' => 12, 'object_domain' => 'contact'];
        $data = $this->setupAssignPayloads($domain);

        $this->json(
            'POST',
            '/api/v1/checklists/templates/' . $template->id . '/assign',
            $data,
            $this->postHeadersMcScope
        )
            ->assertResponseStatus(201);

        $responseData = json_decode($this->response->getContent());
        $this->assertInternalType('string', $responseData->data->attributes->object_id);
        $this->assertInternalType('bool', $responseData->data->attributes->is_completed);

        $dbResultInChecklist = [
            'id' => $this->decodeResponseJson()['data']['id'],
            'description' => $template->data['checklist']['description'],
            'user_id' => $template->user_id,
        ];

        $dbResultInItems0 = [
            'id' => $this->decodeResponseJson()['included'][0]['id'],
            'checklist_id' => $this->decodeResponseJson()['data']['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
        ];

        $dbResultInItems1 = [
            'id' => $this->decodeResponseJson()['included'][1]['id'],
            'checklist_id' => $this->decodeResponseJson()['data']['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
        ];

        $dbResultInChecklist = array_merge($dbResultInChecklist, $domain);

        $this->seeInDatabase('checklists', $dbResultInChecklist);
        $this->seeInDatabase('items', $dbResultInItems0);
        $this->seeInDatabase('items', $dbResultInItems1);
    }

    public function testAssigns()
    {
        $this->mockRoleServiceRequest(true);

        $template = factory(TemplateEloquent::class)->create([
            'user_id' => $this->faker->randomDigit,
            'org_id' => $this->orgId,
        ]);
        $domain = [
            ['object_id' => 1, 'object_domain' => 'contact'],
            ['object_id' => 2, 'object_domain' => 'contact'],
            ['object_id' => 3, 'object_domain' => 'contact'],
        ];

        $data = $this->setupAssignsPayloads($domain);

        $this->json(
            'POST',
            '/api/v1/checklists/templates/' . $template->id . '/assigns',
            $data,
            $this->postHeadersMcScope
        )
            ->assertResponseStatus(201);

        $responseData = json_decode($this->response->getContent());
        foreach ($responseData->data as $checklistResponseData) {
            $this->assertInternalType('string', $checklistResponseData->attributes->object_id);
            $this->assertInternalType('bool', $checklistResponseData->attributes->is_completed);
        }

        $dbResultInChecklist0 = [
            'id' => $this->decodeResponseJson()['data'][0]['id'],
            'description' => $template->data['checklist']['description'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
        ];

        $dbResultInChecklist1 = [
            'id' => $this->decodeResponseJson()['data'][1]['id'],
            'description' => $template->data['checklist']['description'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
        ];

        $dbResultInChecklist2 = [
            'id' => $this->decodeResponseJson()['data'][2]['id'],
            'description' => $template->data['checklist']['description'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
        ];

        $dbResultInChecklist0 = array_merge($dbResultInChecklist0, $domain[0]);
        $dbResultInChecklist1 = array_merge($dbResultInChecklist1, $domain[1]);
        $dbResultInChecklist2 = array_merge($dbResultInChecklist2, $domain[2]);

        $this->seeInDatabase('checklists', $dbResultInChecklist0);
        $this->seeInDatabase('checklists', $dbResultInChecklist1);
        $this->seeInDatabase('checklists', $dbResultInChecklist2);

        $dbResultInItems0 = [
            'id' => $this->decodeResponseJson()['included'][0]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][0]['id'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
        ];

        $dbResultInItems1 = [
            'id' => $this->decodeResponseJson()['included'][1]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][0]['id'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
        ];

        $dbResultInItems2 = [
            'id' => $this->decodeResponseJson()['included'][2]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][1]['id'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
        ];

        $dbResultInItems3 = [
            'id' => $this->decodeResponseJson()['included'][3]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][1]['id'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
        ];

        $dbResultInItems4 = [
            'id' => $this->decodeResponseJson()['included'][4]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][2]['id'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
        ];

        $dbResultInItems5 = [
            'id' => $this->decodeResponseJson()['included'][5]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][2]['id'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
        ];

        $this->seeInDatabase('items', $dbResultInItems0);
        $this->seeInDatabase('items', $dbResultInItems1);
        $this->seeInDatabase('items', $dbResultInItems2);
        $this->seeInDatabase('items', $dbResultInItems3);
        $this->seeInDatabase('items', $dbResultInItems4);
        $this->seeInDatabase('items', $dbResultInItems5);
    }

    public function testAssignsOtherMcTemplate()
    {
        $this->mockRoleServiceRequest(false);

        $template = factory(TemplateEloquent::class)->create([
            'user_id' => $this->faker->randomDigit,
            'org_id' => $this->faker->randomDigit,
        ]);
        $domain = [
            ['object_id' => 1, 'object_domain' => 'contact'],
            ['object_id' => 2, 'object_domain' => 'contact'],
            ['object_id' => 3, 'object_domain' => 'contact'],
        ];

        $data = $this->setupAssignsPayloads($domain);

        $this->json(
            'POST',
            '/api/v1/checklists/templates/' . $template->id . '/assigns',
            $data,
            $this->postHeadersMcScope
        )->assertResponseStatus(403);
    }

    public function testAssignsInactiveMember()
    {
        $this->mockRoleServiceRequest(true);

        $template = factory(TemplateEloquent::class)->create([
            'user_id' => $this->inactiveMemberId,
            'org_id' => $this->orgId,
        ]);
        $domain = [
            ['object_id' => 1, 'object_domain' => 'contact'],
            ['object_id' => 2, 'object_domain' => 'contact'],
            ['object_id' => 3, 'object_domain' => 'contact'],
        ];

        $data = $this->setupAssignsPayloads($domain);

        $this->json(
            'POST',
            '/api/v1/checklists/templates/' . $template->id . '/assigns',
            $data,
            $this->postHeadersMcScope
        )->assertResponseStatus(201);

        $responseData = json_decode($this->response->getContent());
        foreach ($responseData->data as $checklistResponseData) {
            $this->assertInternalType('string', $checklistResponseData->attributes->object_id);
            $this->assertInternalType('bool', $checklistResponseData->attributes->is_completed);
        }

        $dbResultInChecklist0 = [
            'id' => $this->decodeResponseJson()['data'][0]['id'],
            'description' => $template->data['checklist']['description'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
        ];

        $dbResultInChecklist1 = [
            'id' => $this->decodeResponseJson()['data'][1]['id'],
            'description' => $template->data['checklist']['description'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
        ];

        $dbResultInChecklist2 = [
            'id' => $this->decodeResponseJson()['data'][2]['id'],
            'description' => $template->data['checklist']['description'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
        ];

        $dbResultInChecklist0 = array_merge($dbResultInChecklist0, $domain[0]);
        $dbResultInChecklist1 = array_merge($dbResultInChecklist1, $domain[1]);
        $dbResultInChecklist2 = array_merge($dbResultInChecklist2, $domain[2]);

        $this->seeInDatabase('checklists', $dbResultInChecklist0);
        $this->seeInDatabase('checklists', $dbResultInChecklist1);
        $this->seeInDatabase('checklists', $dbResultInChecklist2);

        $dbResultInItems0 = [
            'id' => $this->decodeResponseJson()['included'][0]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][0]['id'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
        ];

        $dbResultInItems1 = [
            'id' => $this->decodeResponseJson()['included'][1]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][0]['id'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
        ];

        $dbResultInItems2 = [
            'id' => $this->decodeResponseJson()['included'][2]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][1]['id'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
        ];

        $dbResultInItems3 = [
            'id' => $this->decodeResponseJson()['included'][3]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][1]['id'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
        ];

        $dbResultInItems4 = [
            'id' => $this->decodeResponseJson()['included'][4]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][2]['id'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
        ];

        $dbResultInItems5 = [
            'id' => $this->decodeResponseJson()['included'][5]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][2]['id'],
            'user_id' => $template->user_id,
            'org_id' => $template->org_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
        ];

        $this->seeInDatabase('items', $dbResultInItems0);
        $this->seeInDatabase('items', $dbResultInItems1);
        $this->seeInDatabase('items', $dbResultInItems2);
        $this->seeInDatabase('items', $dbResultInItems3);
        $this->seeInDatabase('items', $dbResultInItems4);
        $this->seeInDatabase('items', $dbResultInItems5);
    }

    public function testUpdateMcOwnTemplate()
    {
        $this->mockRoleServiceRequest(true);

        $savedTemplate = factory(TemplateEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
        ]);
        $updateTemplate = factory(TemplateEloquent::class)->make([
            'org_id' => $this->orgId,
        ]);
        $data = $this->setupPayloads($updateTemplate->toArray());
        $response = $this->setupResponse($updateTemplate, ['id' => (string) $savedTemplate->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/templates/' . $savedTemplate->id,
            $data,
            $this->postHeadersMcScope
        )->seeJson($response)
        ->seeJsonStructure($this->basicJsonStructure)
        ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateTemplate, ['id' => $savedTemplate->id]);

        $this->seeInDatabase('templates', $dbResult);
    }

    public function testUpdateOtherMcTemplate()
    {
        $this->mockRoleServiceRequest(true);

        $userId = $this->faker->randomDigit;
        $orgId = $this->faker->randomDigit;
        $savedTemplate = factory(TemplateEloquent::class)->create([
            'user_id' => $userId,
            'org_id' => $orgId,
        ]);
        $updateTemplate = factory(TemplateEloquent::class)->make([
            'user_id' => $userId,
            'org_id' => $orgId,
        ]);
        $data = $this->setupPayloads($updateTemplate->toArray());

        $this->json(
            'PATCH',
            '/api/v1/checklists/templates/' . $savedTemplate->id,
            $data,
            $this->postHeadersMcScope
        )->assertResponseStatus(403);
    }

    public function testUpdateInactiveMcMemberTemplateByOtherMcMember()
    {
        $this->mockRoleServiceRequest(true);

        $inactiveMemberTemplate = factory(TemplateEloquent::class)->create([
            'user_id' => $this->inactiveMemberId,
            'org_id' => $this->orgId,
        ]);
        $updateTemplate = factory(TemplateEloquent::class)->make([
            'user_id' => $this->inactiveMemberId,
            'org_id' => $this->orgId,
        ]);
        $data = $this->setupPayloads($updateTemplate->toArray());
        $this->setupResponse($updateTemplate, ['id' => (string) $inactiveMemberTemplate->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/templates/' . $inactiveMemberTemplate->id,
            $data,
            $this->postHeadersMcScope
        )->assertResponseStatus(200);
    }

    public function testUpdateMcOwnTemplateByUserDonthaveDestructiveAction()
    {
        $this->mockRoleServiceRequest(false);
        $this->mockPNOUnauthorized();

        $otherUser = $this->faker->numberBetween(1000, 9000);
        $inactiveMemberTemplate = factory(TemplateEloquent::class)->create([
            'user_id' => $otherUser,
            'org_id' => $this->orgId,
        ]);
        $updateTemplate = factory(TemplateEloquent::class)->make([
            'user_id' => $otherUser,
            'org_id' => $this->orgId,
        ]);
        $data = $this->setupPayloads($updateTemplate->toArray());
        $this->setupResponse($updateTemplate, ['id' => (string) $inactiveMemberTemplate->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/templates/' . $inactiveMemberTemplate->id,
            $data,
            $this->postHeadersMcScope
        )->assertResponseStatus(403);
    }

    public function testDestroy()
    {
        $this->mockRoleServiceRequest(true);

        $template = factory(TemplateEloquent::class)->create([
            'org_id' => $this->orgId
        ]);
        $this->json('DELETE', '/api/v1/checklists/templates/' . $template->id, [], $this->postHeadersMcScope);
        $this->seeStatusCode(204);
        $this->missingFromDatabase('templates', [
            'id' => $template->id,
            'deleted_at' => null,
        ]);
    }

    public function testDestroyNotAllowed()
    {
        $this->mockRoleServiceRequest(true);
        $this->mockUserFound(123);

        $template = factory(TemplateEloquent::class)->create([
            'user_id' => $this->faker->randomDigit,
            'org_id' => $this->faker->randomDigit,
        ]);
        $this->json('DELETE', '/api/v1/checklists/templates/' . $template->id, [], $this->postHeadersMcScope);
        $this->seeStatusCode(403);

        $this->seeInDatabase('templates', [
            'id' => $template->id,
        ]);
    }

    public function testDestroyOtherMcTemplate()
    {
        $this->mockRoleServiceRequest(true);

        $template = factory(TemplateEloquent::class)->create([
            'user_id' => $this->faker->randomDigit,
            'org_id' => $this->faker->randomDigit,
        ]);

        $this->json('DELETE', '/api/v1/checklists/templates/' . $template->id, [], $this->postHeadersMcScope);

        $this->seeStatusCode(403);

        $this->seeInDatabase('templates', [
            'id' => $template->id,
        ]);
    }
}
