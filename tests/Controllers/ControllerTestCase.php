<?php

namespace Test\Controllers;

use App\Services\Checklists\ChecklistEloquent;
use App\Services\Items\ItemEloquent;
use App\Services\PNO\PNOService;
use App\Services\Roles\RoleService;
use Exception;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Test\TestCase;
use KWRI\KongHandler\Models\UserInfo;
use KWRI\KongHandler\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event;
use Mockery as m;
use Faker;
use GuzzleHttp\Psr7\Response as HttpResponse;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Client as HttpClient;

abstract class ControllerTestCase extends TestCase
{
    use DatabaseTransactions;

    /**
     * Setup test.
     */
    public function setUp()
    {
        parent::setUp();

        $userinfo = [
            'given_name'         => 'kelle1',
            'password'           => 'Password1',
            'client_id'          => 'e3fb1740-d9e2-0135-3d40-06dd31cd45e266469',
            'updated_at'         => '2018-01-23T07:19:36.950Z',
            'family_name'        => 'kelle1',
            'preferred_username' => 'kelle1',
            'custom_fields'      => ['KW_UID' => '556396'],
            'email'              => 'kelle1@kw.com',
            'name'               => 'kelle1 kelle1',
            'sub'                => '37087183',
        ];

        $this->userinfo = $userinfo;

        $this->userID = (int) array_get($userinfo, 'custom_fields.KW_UID');

        $this->token = 'ABC';

        $this->teamId = 't35004';
        $this->teamMembers = ['556396', '556397', '556398'];

        $xUserInfo         = json_encode($userinfo);
        $xConsumerUsername = 'onelogin';

        $this->headers = [
            'Authorization'       => 'Bearer ' . $this->token,
            'x-userinfo'          => $xUserInfo,
            'x-consumer-username' => $xConsumerUsername,
        ];
        $this->postHeaders = array_merge(
            $this->headers,
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept'       => 'application/vnd.api+json',
            ]
        );
        $this->postHeadersTeamScope = [
            'Authorization' => 'Bearer ' . $this->token,
            'Content-Type' => 'application/vnd.api+json',
            'Accept'       => 'application/vnd.api+json',
            'cache-control' => 'no-cache',
            'x-kwcommand-group-members' => implode(';', $this->teamMembers),
            'x-kwcommand-group' => 'org=' . $this->teamId,
            'x-consumer-username' => 'teamGateway',
            'x-consumer-token-user-email' => 'name=kelle1 kelle1;email=kelle1@kw.com',
            'x-consumer-token-user-id' => '556396',
            'x-consumer-aid' => 'teamGateway',
        ];

        $this->orgId = '21';

        $this->postHeadersMcScope = [
            'Authorization' => 'Bearer ' . $this->token,
            'Content-Type' => 'application/vnd.api+json',
            'Accept'       => 'application/vnd.api+json',
            'cache-control' => 'no-cache',
            'x-kwcommand-group' => 'org=' . $this->orgId,
            'x-consumer-username' => 'mcGateway',
            'x-consumer-token-user-email' => 'name=kelle1 kelle1;email=kelle1@kw.com',
            'x-consumer-token-user-id' => '556396',
            'x-consumer-aid' => 'mcGateway',
        ];

        $this->inactiveMemberId = 987654;

        $userInfo = new UserInfo(['email' => 'test@test.com', 'name' => 'test']);
        $this->user = new User([
            'timezone' => 'UTC',
            'token' => 'token',
            'kwoid' => null,
            'isService' => false,
            'info' => $userInfo,
            'key' => $this->userID
        ]);

        $this->faker = \Faker\Factory::create();

        ChecklistEloquent::unsetEventDispatcher();
        ItemEloquent::unsetEventDispatcher();
    }

    public function setupPayloads(array $properties)
    {
        $data = [
            'data' => [
                'attributes' => $properties,
            ],
        ];

        return $data;
    }

    public function setupDeletePayloads($properties)
    {
        $data = [
            'deleted_count' => $properties,
        ];

        return $data;
    }

    public function seeJson(array $data = null, $negate = false)
    {
        return $this->seeJsonContains($data, $negate);
    }

    /**
     * Return the decoded response JSON.
     *
     * @return array
     */
    protected function decodeResponseJson()
    {
        $decodedResponse = json_decode($this->response->getContent(), true);

        if (is_null($decodedResponse) || false === $decodedResponse) {
            $this->fail('Invalid JSON was returned from the route. Perhaps an exception was thrown?');
        }

        return $decodedResponse;
    }

    /**
     * Parse date fields to UTC as we will store any date field to UTC on db.
     */
    protected function setupRecord(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            if ($this->containsDate($value)) {
                $attributes[$key] = $this->parseDateValue($value);
            }
        }

        return $attributes;
    }

    /**
     * Check if value is a date.
     */
    protected function containsDate($value)
    {
        if ((is_object($value) && $value instanceof Carbon)
          || (
              ! empty($value)
            && strtotime($value)
            && preg_match('/(\d{4})-(\d{2})-(\d{2}) (\d{2})\:(\d{2})\:(\d{2})/', $value)
          )
          ) {
            return true;
        }

        return false;
    }

    // This is a hacky solutions found in here: https://github.com/laravel/framework/issues/18066#issuecomment-342630971
    // It seems that the Model Eloquent events are not triggered when in test.
    // More reference of the issue https://github.com/laravel/framework/issues/1181.
    // After this calls, all event will be triggered. Make sure to call this in the correct order.
    protected function activateObservers()
    {
        Model::setEventDispatcher(Event::getFacadeRoot());
    }

    protected function createInactiveUsers(array $kwuids)
    {
        $faker = Faker\Factory::create();
        $users = [];

        foreach ($kwuids as $kwuid) {
            $users[] = [
                'kw_uid' => $kwuid,
                'username' => $faker->username,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'kw_email' => $faker->email,
                'recovery_email' => $faker->email,
                'email' => $faker->email,
                'active' => 0,
                'km_active' => 0,
            ];
        }

        return [
            'data' => $users,
        ];
    }

    protected function createUserNotFound()
    {
        return [
            'error' => [
                'message' => 'Persons not found',
            ]
        ];
    }

    protected function createUserFound($kwuid)
    {
        $faker = Faker\Factory::create();

        return [
            'data' => [
                'kw_uid' => $kwuid,
                'username' => $faker->username,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'kw_email' => $faker->email,
                'recovery_email' => $faker->email,
                'email' => $faker->email,
                'active' => 1,
                'km_active' => 0,
            ]
        ];
    }


    protected function mockInactiveUsersResponse(array $kwuids)
    {
        $mock = new MockHandler();
        $handler = HandlerStack::create($mock);
        $guzzle = new HttpClient(['handler' => $handler]);
        
        $inactiveUsers = $this->createInactiveUsers($kwuids);
        $mock->append(new HttpResponse(200, [], json_encode($inactiveUsers)));

        $this->app->when(PNOService::class)
            ->needs(HttpClient::class)
            ->give(function () use ($guzzle) {
                return $guzzle;
            });
    }

    protected function mockUserNotFound()
    {
        $mock = new MockHandler();
        $handler = HandlerStack::create($mock);
        $guzzle = new HttpClient(['handler' => $handler]);
        
        $user = $this->createUserNotFound();
        $mock->append(new HttpResponse(404, [], json_encode($user)));

        $this->app->when(PNOService::class)
            ->needs(HttpClient::class)
            ->give(function () use ($guzzle) {
                return $guzzle;
            });
    }

    protected function mockUserFound($kwuid)
    {
        $mock = new MockHandler();
        $handler = HandlerStack::create($mock);
        $guzzle = new HttpClient(['handler' => $handler]);
        
        $user = $this->createUserFound($kwuid);
        $mock->append(new HttpResponse(200, [], json_encode($user)));

        $this->app->when(PNOService::class)
            ->needs(HttpClient::class)
            ->give(function () use ($guzzle) {
                return $guzzle;
            });
    }

    protected function mockRoleServiceRequest(bool $status)
    {
        $service = RoleService::class;
        $this->app->instance($service, m::mock($service, function ($roleService) use ($status) {
            $roleService->shouldReceive('hasDestructiveAction')->andReturn($status);
            $roleService->shouldReceive('getPersonRoles')->andReturn('some-response');
        }));
    }

    public function mockPNOUnauthorized()
    {
        $this->app->instance(PNOService::class, m::mock(PNOService::class, function ($mock) {
            $mock->shouldReceive('isActiveUser')
                ->andReturn(false);
            $mock->shouldReceive('getUserByKwuid')
                ->withArgs([$this->token, $this->user->getKey()])
                ->andThrow(new Exception('Unauthorized', 401));
        }));
    }

    /**
     * See json collection on found database record.
     */
    protected function seeJsonInRecord(array $jsonCollection, string $modelClass, array $where)
    {
        $dbRecord = $modelClass::where($where)->first();
        foreach ($jsonCollection as $column => $json) {
            $this->assertEquals($json, $dbRecord->$column);
        }
    }
}
