<?php

namespace Test\Controllers;

use App\Services\Items\ItemEloquent;
use Illuminate\Support\Facades\Cache;

abstract class ItemsControllerTestCase extends ControllerTestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->basicJsonStructure = [
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'description',
                    'is_completed',
                    'due',
                    'urgency',
                    'assignee_id',
                    'completed_at',
                    'updated_by',
                    'created_by',
                    'notify_client',
                    'client_update_sent_at',
                    'updated_at',
                    'created_at',
                ],
                'links' => ['self'],
            ],
        ];
    }

    protected function setupDBResult(ItemEloquent $item, $override = [], $unsetKeys = [])
    {
        $data = [
            'id'           => $item->getKey(),
            'description'  => $item->getAttribute('description'),
            'checklist_id' => $item->getAttribute('checklist_id'),
            'user_id'      => $item->getAttribute('user_id'),
            'assignee_id'  => $item->getAttribute('assignee_id'),
            'is_completed' => $item->getAttribute('is_completed'),
            'urgency'      => $item->getAttribute('urgency'),
            'task_id'      => $item->getAttribute('task_id'),
            'updated_by'   => $item->getAttribute('updated_by'),
        ];

        if (count($override)) {
            foreach ($override as $key => $value) {
                $data[$key] = $value;
            }
        }

        if (count($unsetKeys)) {
            foreach ($unsetKeys as $key) {
                unset($data[$key]);
            }
        }

        return $data;
    }

    protected function setupResponse(ItemEloquent $item, $override = [], $unsetKeys = [])
    {
        $due = $item->getAttribute('due');
        $dateAttribute = is_null($due) ? null : $due->toIso8601String();

        $data = [
            'id'          => (string) $item->getKey(),
            'type'        => 'items',
            'description'  => $item->getAttribute('description'),
            'meta'         => $item->getAttribute('meta'),
            'is_completed' => (bool) $item->getAttribute('is_completed'),
            'completed_at' => $item->getAttribute('completed_at'),
            'due'          => $dateAttribute,
            'urgency'      => $item->getAttribute('urgency'),
            'updated_by'   => $item->getAttribute('updated_by'),
            'created_by'   => $item->getAttribute('user_id'),
            'checklist_id' => $item->getAttribute('checklist_id'),
            'assignee_id'  => $item->getAttribute('assignee_id'),
            'task_id'      => $item->getAttribute('task_id')
        ];

        if (count($override)) {
            foreach ($override as $key => $value) {
                $data[$key] = $value;
            }
        }

        if (count($unsetKeys)) {
            foreach ($unsetKeys as $key) {
                unset($data[$key]);
            }
        }

        return $data;
    }

    public function setupPayloadsComplete($userItems, $otherItems)
    {
        $allItemsId = [];

        $itemIds = array_merge($userItems->pluck(['id'])->toArray(), $otherItems->pluck(['id'])->toArray());

        foreach ($itemIds as $id) {
            $allItemsId[] = ['item_id' => $id];
        }

        $data = [
            'data' => $allItemsId,
        ];

        return $data;
    }

    public function setupResponseAllComplete($userItems, $otherItems, $dateCompleted, $completed = true)
    {
        $allItemsId = [];

        $allItemsId = $this->getResponseItemsId($userItems, $allItemsId, $dateCompleted, $completed);
        $allItemsId = $this->getResponseItemsId($otherItems, $allItemsId, $dateCompleted, $completed);

        $data = [
            'data' => $allItemsId,
        ];

        return $data;
    }

    public function setupResponsesComplete($userItems, $otherItems, $dateCompleted, $completed = true)
    {
        $allItemsId = [];

        $allItemsId = $this->getResponseItemsId($userItems, $allItemsId, $dateCompleted, $completed);
        $allItemsId = $this->getResponseOtherItemsId($otherItems, $allItemsId);

        $data = [
            'data' => $allItemsId,
        ];

        return $data;
    }

    public function getResponseItemsId($items, array $allItemsId, $dateCompleted, $complete = true)
    {
        foreach ($items as $item) {
            $allItemsId[] = [
                'item_id' => $item->getKey(),
                'checklist_id' => $item->checklist_id,
                'is_completed' => $complete,
                'completed_at' => $dateCompleted,
                'completed_by' => $complete ? $this->userID : null,
            ];
        }

        return $allItemsId;
    }

    public function getResponseOtherItemsId($items, array $allItemsId)
    {
        foreach ($items as $item) {
            $allItemsId[] = [
                'item_id' => $item->getKey(),
                'result' => 'no-access',
            ];
        }

        return $allItemsId;
    }

    public function setupDBResultComplete($userItems, $otherItems, $complete = true)
    {
        $allItemsId = [];

        $allItemsId = $this->getResultInDBComplete($userItems, $allItemsId, $complete);
        $allItemsId = $this->getResultInDBComplete($otherItems, $allItemsId, !$complete);

        return $allItemsId;
    }

    public function setupDBResultAllComplete($userItems, $otherItems, $complete = true, $kwuid = null)
    {
        $allItemsId = [];

        $allItemsId = $this->getResultInDBCompleteByAgent($userItems, $allItemsId, $complete, $kwuid);
        $allItemsId = $this->getResultInDBCompleteByAgent($otherItems, $allItemsId, $complete, $kwuid);

        return $allItemsId;
    }

    public function getResultInDBComplete($items, array $allItemsId, $complete = 1)
    {
        foreach ($items as $item) {
            $allItemsId[] = [
                'id' => $item->getKey(),
                'checklist_id' => $item->checklist_id,
                'is_completed' => $complete,
                'completed_by' => $complete ? $item->user_id : null,
            ];
        }

        return $allItemsId;
    }

    public function getResultInDBCompleteByAgent($items, array $allItemsId, $complete = 1, $kwuid = null)
    {
        foreach ($items as $item) {
            $allItemsId[] = [
                'id' => $item->getKey(),
                'checklist_id' => $item->checklist_id,
                'is_completed' => $complete,
                'completed_by' => $kwuid,
            ];
        }

        return $allItemsId;
    }

    public function setupBulkPayloads(array $data)
    {
        return ['data' => $data];
    }

    public function setupBulkResponse($itemsData, $assertStatusCode)
    {
        $itemResponse = [];
        $itemsData = array_get($itemsData, 'data');

        foreach ($itemsData as $item) {
            array_push($itemResponse, [
                'id'     => array_get($item, 'id'),
                'action' => array_get($item, 'action'),
                'status' => $assertStatusCode,
            ]);
        }

        return ['data' => $itemResponse];
    }

    public function getChecklistItemCountCache($checklistId)
    {
        return Cache::tags(["summary", "checklist-id-$checklistId"])
            ->get("checklist-item-count-id-$checklistId");
    }
}
