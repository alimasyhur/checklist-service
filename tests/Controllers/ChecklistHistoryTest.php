<?php

namespace Test\Controllers;

use App\Jobs\Audit\CompleteItemJob;
use App\Jobs\Audit\IncompleteItemJob;
use App\Services\Checklists\ChecklistEloquent;
use App\Services\Items\ItemEloquent;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Event;

class ChecklistHistoryTest extends ChecklistControllerTestCase
{
    private const DEAL_OBJECT = 'deals';

    public function testChecklistSnooze()
    {
        $now = Carbon::now();
        $dueDate = $now->addDays(5);
        Carbon::setTestNow($dueDate);

        $savedChecklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);

        $updateChecklist = factory(ChecklistEloquent::class)->make([
            'user_id' => $this->userID,
            'due' => $dueDate,
        ]);
        $checklistPayloads = $this->setupPayloads($updateChecklist->toArray());

        $initialDispatcher = Event::getFacadeRoot();
        Event::fake();
        ChecklistEloquent::setEventDispatcher($initialDispatcher);

        $this->json('PATCH', '/api/v1/checklists/' . $savedChecklist->id, $checklistPayloads, $this->postHeaders)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateChecklist, ['id' => $savedChecklist->id], ['type', 'created_by']);
        $dbResult['updated_by'] = $this->userID;
        $dbResult['user_id'] = $this->userID;

        $this->seeInDatabase('checklists', $dbResult);

        $history = [
            'loggable_type' => 'checklists',
            'loggable_id' => $savedChecklist->id,
            'action' => 'snooze',
            'kwuid' => $this->userID,
            'value' => $dueDate->toIso8601String(),
        ];

        $this->seeInDatabase('histories', $history);
    }

    public function testChecklistComplete()
    {
        $now = Carbon::now();
        Carbon::setTestNow($now);
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'is_completed' => false,
            'completed_at' => null,
        ]);
        $items = factory(ItemEloquent::class, 3)->create([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => false,
            'completed_at' => null,
        ]);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(CompleteItemJob::class);
        }

        $checklistPayloads = ['data' => $items->map(function ($item) {
            return ['item_id' => $item->id];
        })->toArray()];

        $initialDispatcher = Event::getFacadeRoot();
        Event::fake();
        ChecklistEloquent::setEventDispatcher($initialDispatcher);

        $this->json('POST', '/api/v1/checklists/complete', $checklistPayloads, $this->postHeaders)
            ->assertResponseStatus(200);

        $this->seeInDatabase('checklists', [
            'id' => $checklist->id,
            'is_completed' => true,
            'completed_at' => $now->toIso8601String()
        ]);

        $this->seeInDatabase('histories', [
            'loggable_type' => 'checklists',
            'loggable_id' => $checklist->id,
            'action' => 'complete',
            'kwuid' => $this->userID,
            'value' => 1
        ]);
    }

    public function testChecklistIncomplete()
    {
        $now = Carbon::now();
        Carbon::setTestNow($now);
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'is_completed' => true,
            'completed_at' => '2019-01-01 00:00:00',
        ]);
        $items = factory(ItemEloquent::class, 3)->create([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => true,
            'completed_at' => '2019-01-01 00:00:00',
        ]);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(IncompleteItemJob::class);
        }

        $checklistPayloads = ['data' => $items->map(function ($item) {
            return ['item_id' => $item->id];
        })->toArray()];

        $initialDispatcher = Event::getFacadeRoot();
        Event::fake();
        ChecklistEloquent::setEventDispatcher($initialDispatcher);

        $this->json('POST', '/api/v1/checklists/incomplete', $checklistPayloads, $this->postHeaders)
            ->assertResponseStatus(200);

        $this->seeInDatabase('checklists', [
            'id' => $checklist->id,
            'is_completed' => false,
            'completed_at' => null
        ]);

        $this->seeInDatabase('histories', [
            'loggable_type' => 'checklists',
            'loggable_id' => $checklist->id,
            'action' => 'complete',
            'kwuid' => $this->userID,
            'value' => 0
        ]);
    }

    public function testChecklistArchive()
    {
        $now = Carbon::now();
        Carbon::setTestNow($now);
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);

        $initialDispatcher = Event::getFacadeRoot();
        Event::fake();
        ChecklistEloquent::setEventDispatcher($initialDispatcher);

        $this->json('DELETE', '/api/v1/checklists/' . $checklist->id, [], $this->postHeaders);
        $this->seeStatusCode(204);
        $this->missingFromDatabase('checklists', [
            'id' => $checklist->id,
            'deleted_at' => null,
        ]);

        $this->seeInDatabase('histories', [
            'loggable_type' => 'checklists',
            'loggable_id' => $checklist->id,
            'action' => 'archive',
            'kwuid' => $this->userID,
            'value' => 1
        ]);
    }
}
