<?php

namespace Test\Controllers;

use App\Services\Checklists\ChecklistEloquent;
use Illuminate\Support\Carbon;

abstract class ChecklistControllerTestCase extends ControllerTestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->basicJsonStructure = [
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'object_domain',
                    'object_id',
                    'description',
                    'is_completed',
                    'due',
                    'urgency',
                    'completed_at',
                    'updated_by',
                    'updated_at',
                    'created_at',
                ],
                'links' => ['self'],
            ],
        ];
    }

    public function setupPayloads(array $data)
    {
        $dataReturn = [
            'data' => [
                'attributes' => array_only($data, ['object_domain', 'object_id', 'description', 'due', 'urgency']),
            ],
        ];

        return $dataReturn;
    }

    protected function setupResponse(ChecklistEloquent $checklist, $override = [], $unsetKeys = [])
    {
        $dateAttribute = Carbon::createFromFormat('Y-m-d H:i:s', $checklist->getAttribute('due'))->toIso8601String();

        $data = [
            'id' => (string) $checklist->getKey(),
            'type' => 'checklists',
            'object_domain' => $checklist->getAttribute('object_domain'),
            'object_id' => $checklist->getAttribute('object_id'),
            'description' => $checklist->getAttribute('description'),
            'due' => $dateAttribute,
            'urgency' => $checklist->getAttribute('urgency'),
            'created_by' => $checklist->getAttribute('user_id'),
        ];

        if (count($override)) {
            foreach ($override as $key => $value) {
                $data[$key] = $value;
            }
        }

        if (count($unsetKeys)) {
            foreach ($unsetKeys as $key) {
                unset($data[$key]);
            }
        }

        return $data;
    }

    protected function setupDBResult(ChecklistEloquent $checklist, $override = [], $unsetKeys = [])
    {
        $data = [
            'id' => (string) $checklist->getKey(),
            'type' => 'checklists',
            'object_domain' => $checklist->getAttribute('object_domain'),
            'object_id' => $checklist->getAttribute('object_id'),
            'description' => $checklist->getAttribute('description'),
            'due' => $checklist->getAttribute('due'),
            'urgency' => $checklist->getAttribute('urgency'),
            'user_id' => $checklist->getAttribute('user_id'),
        ];

        if (count($override)) {
            foreach ($override as $key => $value) {
                $data[$key] = $value;
            }
        }

        if (count($unsetKeys)) {
            foreach ($unsetKeys as $key) {
                unset($data[$key]);
            }
        }

        return $data;
    }
}
