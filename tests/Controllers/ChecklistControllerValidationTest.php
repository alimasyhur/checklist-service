<?php

namespace Test\Controllers;

use App\Services\Checklists\ChecklistEloquent;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ChecklistControllerValidationTest extends ChecklistControllerTestCase
{
    use DatabaseTransactions;

    /**
     * @dataProvider dueStoreDataProvider
     */
    public function testStoreWithDue($duePayload, $responseStatusCode)
    {
        $checklist = factory(ChecklistEloquent::class)->make(['user_id' => $this->userID]);
        $checklistPayloads = $this->setupPayloads($checklist->toArray());
        $checklistPayloads['data']['attributes']['due'] = $duePayload;

        $this->json('POST', '/api/v1/checklists', $checklistPayloads, $this->postHeaders)
            ->assertResponseStatus($responseStatusCode);
    }

    public function dueStoreDataProvider()
    {
        return [
            [null, 201],
            ['01234567890', 422],
            ['random string', 422],
            ['2001-01-01 10:01:01', 201],
            ['2018-06-21T10:10:23.185Z', 201],
            ['2002-02-02T10:02:02+00:00', 201],
        ];
    }

    /**
     * @dataProvider dueUpdateDataProvider
     */
    public function testUpdateWithDue($duePayload, $responseStatusCode)
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);

        $checklistPayloads = [
            'data' => [
                'attributes' => [
                    'due' => $duePayload,
                ],
            ],
        ];
        $this->json('PATCH', '/api/v1/checklists/' . $checklist->id, $checklistPayloads, $this->postHeaders);
        $this->assertResponseStatus($responseStatusCode);
    }

    public function dueUpdateDataProvider()
    {
        return [
            [null, 200],
            ['01234567890', 422],
            ['random string', 422],
            ['2001-01-01 10:01:01', 200],
            ['2018-06-21T10:10:23.185Z', 200],
            ['2002-02-02T10:02:02+00:00', 200],
        ];
    }
}
