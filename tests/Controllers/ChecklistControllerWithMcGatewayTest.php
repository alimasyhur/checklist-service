<?php

namespace Test\Controllers;

use App\Services\Checklists\ChecklistEloquent;
use App\Services\Items\ItemEloquent;

class ChecklistControllerWithMcGatewayTest extends ChecklistControllerTestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->mockInactiveUsersResponse([$this->inactiveMemberId]);
    }

    public function testIndex()
    {
        $this->checklists = factory(ChecklistEloquent::class, 2)->create(['org_id' => $this->orgId]);
        $this->item1 = factory(ItemEloquent::class)->create([
            'checklist_id' => $this->checklists[0]->id,
            'description' => $this->faker->unique()->sentence(3),
            'org_id' => $this->orgId,
        ]);
        $this->item2 = factory(ItemEloquent::class)->create([
            'checklist_id' => $this->checklists[0]->id,
            'description' => $this->faker->unique()->sentence(3),
            'org_id' => $this->orgId
        ]);
        $this->item3 = factory(ItemEloquent::class)->create([
            'checklist_id' => $this->checklists[1]->id,
            'description' => $this->faker->unique()->sentence(3),
            'org_id' => $this->orgId
        ]);
        $this->otherChecklist = factory(ChecklistEloquent::class)->create(['org_id' => $this->faker->randomDigit]);

        $this->should('only show my checklist', function () {
            $this->json('GET', '/api/v1/checklists', [], $this->postHeadersMcScope)
                 ->seeStatusCode(200)
                 ->seeJsonStructure([
                     'meta' => [ 'count', 'total' ],
                     'data' => [ $this->basicJsonStructure['data'] ],
                     'links' => [ 'first', 'last', 'next', 'prev' ],
                 ])
                 ->seeJson(['id' => (string) $this->checklists[0]->id])
                 ->seeJson(['id' => (string) $this->checklists[1]->id])
                 ->dontSeeJson(['id' => (string) $this->otherChecklist->id]);
        });
    }

    public function testStore()
    {
        $this->mockRoleServiceRequest(true);

        $checklist = factory(ChecklistEloquent::class)->make([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
        ]);
        $checklistPayloads = $this->setupPayloads($checklist->toArray());
        $response = $this->setupResponse($checklist, [], ['id']);

        $this->json('POST', '/api/v1/checklists', $checklistPayloads, $this->postHeadersMcScope)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(201);

        $id = $this->decodeResponseJson()['data']['id'];
        $dbResult = $this->setupDBResult($checklist, ['id' => $id], ['type']);
        array_set($dbResult, 'updated_by', null);
        array_set($dbResult, 'completed_at', null);
        array_set($dbResult, 'is_completed', false);
        array_set($dbResult, 'org_id', $this->orgId);

        $this->seeInDatabase('checklists', $dbResult);
    }

    public function testStoreDontHaveDestructiveAction()
    {
        $this->mockRoleServiceRequest(false);

        $checklist = factory(ChecklistEloquent::class)->make([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
        ]);
        $checklistPayloads = $this->setupPayloads($checklist->toArray());
        $response = $this->setupResponse($checklist, [], ['id']);

        $this->json('POST', '/api/v1/checklists', $checklistPayloads, $this->postHeadersMcScope)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(201);

        $id = $this->decodeResponseJson()['data']['id'];
        $dbResult = $this->setupDBResult($checklist, ['id' => $id], ['type']);
        array_set($dbResult, 'updated_by', null);
        array_set($dbResult, 'completed_at', null);
        array_set($dbResult, 'is_completed', false);
        array_set($dbResult, 'org_id', null);

        $this->seeInDatabase('checklists', $dbResult);
    }

    public function testShowOwnMcChecklist()
    {
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
        ]);
        $response = $this->setupResponse($checklist);

        $this->json('GET', '/api/v1/checklists/' . $checklist->id, [], $this->postHeadersMcScope)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $responseData = json_decode($this->response->getContent());

        $this->assertInternalType('string', $responseData->data->attributes->object_id);
        $this->assertInternalType('bool', $responseData->data->attributes->is_completed);
    }
    
    public function testShowInactiveMemberChecklist()
    {
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->inactiveMemberId,
            'org_id' => $this->orgId,
        ]);
        $response = $this->setupResponse($checklist);

        $this->json(
            'GET',
            '/api/v1/checklists/' . $checklist->id,
            [],
            $this->postHeadersMcScope
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $responseData = json_decode($this->response->getContent());

        $this->assertInternalType('string', $responseData->data->attributes->object_id);
        $this->assertInternalType('bool', $responseData->data->attributes->is_completed);
    }

    public function testShowOtherMcChecklist()
    {
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->faker->randomDigit,
            'org_id' => $this->faker->randomDigit,
        ]);
        $response = $this->setupResponse($checklist);

        $this->json('GET', '/api/v1/checklists/' . $checklist->id, [], $this->postHeadersMcScope)
            ->dontSeeJson($response)
            ->assertResponseStatus(403);
    }

    public function testShowMCAUserAbleToAccess()
    {
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
            'object_domain' => 'mc_recruits',
        ]);
        $response = $this->setupResponse($checklist);

        $this->json('GET', '/api/v1/checklists/' . $checklist->id, [], $this->postHeadersMcScope)
            ->seeJson($response)
            ->assertResponseStatus(200);
    }

    public function testShowMCAUserAbleToAccessOtherChecklist()
    {
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->faker->numberBetween(1000, 9000),
            'org_id' => $this->orgId,
            'object_domain' => 'mc_recruits',
        ]);
        $response = $this->setupResponse($checklist);

        $this->json('GET', '/api/v1/checklists/' . $checklist->id, [], $this->postHeadersMcScope)
            ->seeJson($response)
            ->assertResponseStatus(200);
    }

    public function testDestroyMcwnChecklist()
    {
        $this->mockRoleServiceRequest(true);

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
        ]);

        $this->json('DELETE', '/api/v1/checklists/' . $checklist->id, [], $this->postHeadersMcScope);

        $this->seeStatusCode(204);
        $this->missingFromDatabase('checklists', [
            'id'         => $checklist->id,
            'deleted_at' => null,
        ]);
    }

    public function testDestroyMcMemberChecklist()
    {
        $this->mockRoleServiceRequest(true);

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->faker->randomDigit,
            'org_id' => $this->orgId,
        ]);

        $this->json('DELETE', '/api/v1/checklists/' . $checklist->id, [], $this->postHeadersMcScope);

        $this->seeStatusCode(204);
        $this->missingFromDatabase('checklists', [
            'id'         => $checklist->id,
            'deleted_at' => null,
        ]);
    }

    public function testDestroyOtherMcChecklist()
    {
        $this->mockRoleServiceRequest(true);

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->faker->randomDigit
        ]);

        $this->json('DELETE', '/api/v1/checklists/' . $checklist->id, [], $this->postHeadersMcScope);

        $this->seeStatusCode(403);
        $this->seeInDatabase('checklists', ['id' => $checklist->id]);
    }

    public function testDestroyMcInactiveMemberChecklist()
    {
        $this->mockRoleServiceRequest(true);

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->inactiveMemberId,
            'org_id' => $this->orgId,
        ]);

        $this->json(
            'DELETE',
            '/api/v1/checklists/' . $checklist->id,
            [],
            $this->postHeadersMcScope
        );

        $this->seeStatusCode(204);
        $this->missingFromDatabase('checklists', [
            'id'         => $checklist->id,
            'deleted_at' => null,
        ]);
    }

    public function testUpdateMcOwnChecklist()
    {
        $this->mockRoleServiceRequest(true);

        $savedChecklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
        ]);
        $updateChecklist = factory(ChecklistEloquent::class)->make([
            'user_id' => $this->userID,
            'org_id' => $this->orgId,
        ]);
        $checklistPayloads = $this->setupPayloads($updateChecklist->toArray());
        $response = $this->setupResponse($updateChecklist, ['id' => (string) $savedChecklist->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedChecklist->id,
            $checklistPayloads,
            $this->postHeadersMcScope
        )->seeJson($response)
        ->seeJsonStructure($this->basicJsonStructure)
        ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateChecklist, ['id' => $savedChecklist->id], ['type', 'created_by']);
        array_set($dbResult, 'updated_by', $this->userID);
        array_set($dbResult, 'user_id', $this->userID);
        array_set($dbResult, 'org_id', $this->orgId);

        $this->seeInDatabase('checklists', $dbResult);
    }

    public function testUpdateMcChecklist()
    {
        $this->mockRoleServiceRequest(true);

        $userId = $this->faker->randomDigit;
        $savedChecklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $userId,
            'org_id' => $this->orgId,
        ]);
        $updateChecklist = factory(ChecklistEloquent::class)->make([
            'user_id' => $userId,
            'org_id' => $this->orgId,
        ]);
        $checklistPayloads = $this->setupPayloads($updateChecklist->toArray());
        $response = $this->setupResponse($updateChecklist, ['id' => (string) $savedChecklist->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedChecklist->id,
            $checklistPayloads,
            $this->postHeadersMcScope
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateChecklist, ['id' => $savedChecklist->id], ['type', 'created_by']);
        array_set($dbResult, 'updated_by', $this->userID);
        array_set($dbResult, 'user_id', $userId);
        array_set($dbResult, 'org_id', $this->orgId);

        $this->seeInDatabase('checklists', $dbResult);
    }

    public function testUpdateInactiveMcMemberChecklist()
    {
        $this->mockRoleServiceRequest(true);

        $savedChecklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->inactiveMemberId,
            'org_id' => $this->orgId,
        ]);
        $updateChecklist = factory(ChecklistEloquent::class)->make([
            'user_id' => $this->inactiveMemberId,
            'org_id' => $this->orgId,
        ]);
        $checklistPayloads = $this->setupPayloads($updateChecklist->toArray());
        $response = $this->setupResponse($updateChecklist, ['id' => (string) $savedChecklist->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedChecklist->id,
            $checklistPayloads,
            $this->postHeadersMcScope
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateChecklist, ['id' => $savedChecklist->id], ['type', 'created_by']);
        array_set($dbResult, 'updated_by', $this->userID);
        array_set($dbResult, 'user_id', $this->inactiveMemberId);
        array_set($dbResult, 'org_id', $this->orgId);

        $this->seeInDatabase('checklists', $dbResult);
    }

    public function testUpdateMcOwnChecklistByUserDonthaveDestructiveAction()
    {
        $this->mockRoleServiceRequest(false);
        $this->mockPNOUnauthorized();

        $otherUser = $this->faker->numberBetween(1000, 9000);
        $inactiveMemberChecklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $otherUser,
            'org_id' => $this->orgId,
        ]);
        $updateChecklist = factory(ChecklistEloquent::class)->make([
            'user_id' => $otherUser,
            'org_id' => $this->orgId,
        ]);
        $data = $this->setupPayloads($updateChecklist->toArray());
        $this->setupResponse($updateChecklist, ['id' => (string) $inactiveMemberChecklist->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $inactiveMemberChecklist->id,
            $data,
            $this->postHeadersMcScope
        )->assertResponseStatus(403);
    }

    public function testUpdateOtherMcChecklist()
    {
        $this->mockRoleServiceRequest(true);

        $userId = $this->faker->randomDigit;
        $orgId = $this->faker->randomDigit;

        $savedChecklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $userId,
            'org_id' => $orgId,
        ]);
        $updateChecklist = factory(ChecklistEloquent::class)->make([
            'user_id' => $userId,
            'org_id' => $orgId,
        ]);
        $checklistPayloads = $this->setupPayloads($updateChecklist->toArray());

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedChecklist->id,
            $checklistPayloads,
            $this->postHeadersMcScope
        )->assertResponseStatus(403);
    }
}
