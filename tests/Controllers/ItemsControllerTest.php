<?php

namespace Test\Controllers;

use App\Jobs\Audit\CompleteItemJob;
use App\Jobs\Audit\CreateItemJob;
use App\Jobs\Audit\DeleteItemJob;
use App\Jobs\Audit\IncompleteItemJob;
use App\Jobs\Audit\UpdateItemJob;
use Carbon\Carbon;
use CorePackage\Testing\AssertJob;
use App\Services\Items\ItemEloquent;
use Illuminate\Support\Facades\Event;
use App\Jobs\SendItemAssignedEmailJob;
use Illuminate\Database\Eloquent\Model;
use App\Jobs\GroupedItemsNotificationJob;
use App\Services\Checklists\ChecklistEloquent;
use App\Services\Notifications\LastDueReminderEloquent;
use Illuminate\Support\Arr;

class ItemsControllerTest extends ItemsControllerTestCase
{
    use AssertJob;
    use \Codeception\Specify;

    private const DEAL_OBJECT = 'deals';

    public function setUp()
    {
        parent::setUp();

        $this->mockInactiveUsersResponse([$this->inactiveMemberId]);
    }

    public function testIndex()
    {
        $this->withoutEvents();
        $this->checklist1 = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'object_domain' => 'contact',
        ]);
        $this->checklist2 = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'object_domain' => 'opportunity',
        ]);
        $this->items1 = factory(ItemEloquent::class, 4)->create([
            'user_id'      => $this->userID,
            'assignee_id'  => 123,
            'checklist_id' => $this->checklist1->id,
        ]);
        $this->items2 = factory(ItemEloquent::class, 3)->create([
            'user_id'      => $this->userID,
            'assignee_id'  => 123,
            'checklist_id' => $this->checklist2->id,
        ]);

        $this->should('response with correct structure', function () {
            $this->json('GET', "/api/v1/checklists/{$this->checklist1->id}/items", [], $this->postHeaders)
                ->seeJsonStructure([
                    'meta'  => ['count', 'total'],
                    'data'  => [$this->basicJsonStructure['data']],
                    'links' => ['first', 'last', 'next', 'prev'],
                ]);
        });

        $this->should('only show related items', function () {
            $this->json('GET', "/api/v1/checklists/{$this->checklist1->id}/items", [], $this->postHeaders)
                ->seeStatusCode(200)
                ->seeJson(['count' => count($this->items1)]);
            foreach ($this->items1 as $item) {
                $this->seeJson(['id' => (string) $item->id]);
            }
            foreach ($this->items2 as $item) {
                $this->dontSeeJson(['id' => (string) $item->id]);
            }
        });

        $this->should('only include related checklist with ?include=checklist', function () {
            $query = '?include=checklist';
            $this->json('GET', "/api/v1/checklists/{$this->checklist1->id}/items$query", [], $this->postHeaders)
                ->seeJson([
                    'description' => $this->checklist1->description
                ])
                ->dontSeeJson([
                    'description' => $this->checklist2->description
                ]);
        });

        $this->should('optimize query count when using ?include=checklist', function () {
            $query = '?include=checklist&query_debug=1';
            $this->json('GET', "/api/v1/checklists/{$this->checklist1->id}/items$query", [], $this->postHeaders)
                ->seeJson(['query_count' => 4]);
        });

        $this->should('correctly sorted', function () {
            $query = '?sort=id';
            $this->json('GET', "/api/v1/checklists/{$this->checklist1->id}/items$query", [], $this->postHeaders);
            $this->assertEquals($this->items1[0]->id, $this->decodeResponseJson()['data'][0]['id']);

            $query = '?sort=-id';
            $this->json('GET', "/api/v1/checklists/{$this->checklist1->id}/items$query", [], $this->postHeaders);
            $this->assertEquals($this->items1[3]->id, $this->decodeResponseJson()['data'][0]['id']);
        });
    }

    public function testIndexOwnItemTeam()
    {
        $ownedChecklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);
        $ownedItem = factory(ItemEloquent::class)->create([
            'user_id' => $this->userID,
            'checklist_id' => $ownedChecklist->getKey()
        ]);
        $this->json(
            'GET',
            '/api/v1/checklists/' . $ownedChecklist->id . '/items?sort=-id',
            [],
            $this->postHeadersTeamScope
        );
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'meta'  => [
                'count',
                'total',
            ],
            'data'  => [
                $this->basicJsonStructure['data'],
            ],
            'links' => [
                'first',
                'last',
                'next',
                'prev',
            ],
        ]);

        $responseDataIDs = collect($this->response->getData()->data)->pluck('id');
        $this->assertContains((string) $ownedItem->getKey(), $responseDataIDs);
    }

    public function testIndexOneTeamItem()
    {
        $oneTeamChecklist = factory(ChecklistEloquent::class)->create(['user_id' => 556397]);
        $oneTeamItem = factory(ItemEloquent::class)->create([
            'user_id' => 556397,
            'checklist_id' => $oneTeamChecklist->id
        ]);
        $this->json(
            'GET',
            '/api/v1/checklists/' . $oneTeamChecklist->id . '/items?sort=-id',
            [],
            $this->postHeadersTeamScope
        );
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'meta'  => [
                'count',
                'total',
            ],
            'data'  => [
                $this->basicJsonStructure['data'],
            ],
            'links' => [
                'first',
                'last',
                'next',
                'prev',
            ],
        ]);

        $responseDataIDs = collect($this->response->getData()->data)->pluck('id');
        $this->assertContains((string) $oneTeamItem->getKey(), $responseDataIDs);
    }

    public function testIndexTeamInactiveMemberItem()
    {
        $inactiveMemberChecklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->inactiveMemberId]);
        $inactiveMemberChecklistItem = factory(ItemEloquent::class)->create([
            'user_id' => $this->inactiveMemberId,
            'checklist_id' => $inactiveMemberChecklist->id
        ]);
        $this->json(
            'GET',
            '/api/v1/checklists/' . $inactiveMemberChecklist->id . '/items?sort=-id',
            [],
            $this->postHeadersTeamScope
        );
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'meta'  => [
                'count',
                'total',
            ],
            'data'  => [
                $this->basicJsonStructure['data'],
            ],
            'links' => [
                'first',
                'last',
                'next',
                'prev',
            ],
        ]);

        $responseDataIDs = collect($this->response->getData()->data)->pluck('id');
        $this->assertContains((string) $inactiveMemberChecklistItem->getKey(), $responseDataIDs);
    }

    public function setupPayloads(array $data)
    {
        $data = [
            'data' => [
                'attributes' => array_only($data, [
                    'description',
                    'meta',
                    'assignee_id',
                    'urgency',
                    'due',
                    'task_id',
                    'assignee_id',
                    'checklist_id',
                    'is_completed',
                    'notify_client',
                    'client_update_sent_at',
                ]),
            ],
        ];

        return $data;
    }

    public function testShow()
    {
        $checklist = factory(ChecklistEloquent::class)->create();
        $item = factory(ItemEloquent::class)->create(['user_id' => $this->userID, 'checklist_id' => $checklist->id]);
        $response = $this->setupResponse($item);
        $this->json('GET', '/api/v1/checklists/' . $item->checklist_id . '/items/' . $item->id, [], $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);
    }

    public function testShowNotAllowed()
    {
        $this->mockUserFound(123);

        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => 123]);
        $item = factory(ItemEloquent::class)->create(['user_id' => 123, 'checklist_id' => $checklist->id]);
        $this->json('GET', '/api/v1/checklists/' . $item->checklist_id . '/items/' . $item->id, [], $this->postHeaders)
            ->assertResponseStatus(403);
    }

    public function testShowIncludeChecklist()
    {
        $checklist = factory(ChecklistEloquent::class)->create();
        $item = factory(ItemEloquent::class)->create(['user_id' => $this->userID, 'checklist_id' => $checklist->id]);
        $response = $this->setupResponse($item);
        $this->json(
            'GET',
            '/api/v1/checklists/' . $item->checklist_id . '/items/' . $item->id . '?include=checklist',
            [],
            $this->postHeaders
        )->seeJson($response)
            ->seeJson([
                'id'   => (string) $checklist->id,
                'type' => 'checklists',
            ])
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);
    }

    public function testShowTeamOwnItems()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);
        $item = factory(ItemEloquent::class)->create(['user_id' => $this->userID, 'checklist_id' => $checklist->id]);
        $response = $this->setupResponse($item);
        $this->json(
            'GET',
            '/api/v1/checklists/' . $item->checklist_id . '/items/' . $item->id,
            [],
            $this->postHeadersTeamScope
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);
    }

    public function testShowTeamItems()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => 556397]);
        $item = factory(ItemEloquent::class)->create(['user_id' => 556397, 'checklist_id' => $checklist->id]);
        $response = $this->setupResponse($item);
        $this->json(
            'GET',
            '/api/v1/checklists/' . $item->checklist_id . '/items/' . $item->id,
            [],
            $this->postHeadersTeamScope
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);
    }

    public function testShowTeamInactiveMemberItems()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->inactiveMemberId]);
        $item = factory(ItemEloquent::class)->create([
            'user_id' => $this->inactiveMemberId,
            'checklist_id' => $checklist->id
        ]);
        $response = $this->setupResponse($item);
        $this->json(
            'GET',
            '/api/v1/checklists/' . $item->checklist_id . '/items/' . $item->id,
            [],
            $this->postHeadersTeamScope
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);
    }

    public function testShowTeamOtherItems()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => 556399]);
        $item = factory(ItemEloquent::class)->create(['user_id' => 556399, 'checklist_id' => $checklist->id]);
        $response = $this->setupResponse($item);
        $this->json(
            'GET',
            '/api/v1/checklists/' . $item->checklist_id . '/items/' . $item->id,
            [],
            $this->postHeadersTeamScope
        )
            ->assertResponseStatus(403);
    }

    public function testStoreInTeam()
    {
        $teamUserId = 556397;
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $teamUserId, 'is_completed' => false]);
        $item = factory(ItemEloquent::class)->make([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => false
        ]);
        $data = $this->setupPayloads($item->toArray());
        $response = $this->setupResponse($item, [], ['id']);
        $this->json('POST', '/api/v1/checklists/' . $checklist->id . '/items', $data, $this->postHeadersTeamScope)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(201);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(CreateItemJob::class);
        }

        $id = $this->decodeResponseJson()['data']['id'];
        $dbResult = $this->setupDBResult($item, ['id' => $id], ['type']);
        $dbResult['updated_by'] = null;
        $dbResult['completed_at'] = null;
        $dbResult['is_completed'] = false;
        $this->seeInDatabase('items', $dbResult);

        $this->specify('created new item by team', function () use ($teamUserId, $checklist) {
            $teamUserChecklist = ChecklistEloquent::where(['user_id' => $teamUserId])->first();
            $newItemByCurrentUser = ItemEloquent::where(['user_id' => $this->userID])->first();

            $this->assertEquals($checklist->id, $teamUserChecklist->id);
            $this->assertEquals($checklist->id, $newItemByCurrentUser->checklist_id);
        });
    }

    public function testStore()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID, 'is_completed' => false]);
        $item = factory(ItemEloquent::class)->make([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => false
        ]);
        $data = $this->setupPayloads($item->toArray());
        $response = $this->setupResponse($item, [], ['id']);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(CreateItemJob::class);
        }

        $this->json('POST', '/api/v1/checklists/' . $checklist->id . '/items', $data, $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(201);

        $id = $this->decodeResponseJson()['data']['id'];
        $dbResult = $this->setupDBResult($item, ['id' => $id], ['type']);
        $dbResult['updated_by'] = null;
        $dbResult['completed_at'] = null;
        $dbResult['is_completed'] = false;
        $dbResult['notify_client'] = $item->notify_client;
        $dbResult['client_update_sent_at'] = $item->client_update_sent_at;

        $this->seeInDatabase('items', $dbResult);
        $this->seeJsonInRecord(['meta' => $item->meta], ItemEloquent::class, ['id' => $id]);
    }

    public function testStoreAndPublishToAudit()
    {
        $this->expectsJobs(CreateItemJob::class);

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'is_completed' => false,
            'object_domain' => self::DEAL_OBJECT,
        ]);
        $item = factory(ItemEloquent::class)->make([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => false
        ]);
        $data = $this->setupPayloads($item->toArray());
        $response = $this->setupResponse($item, [], ['id']);

        $this->json('POST', '/api/v1/checklists/' . $checklist->id . '/items', $data, $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(201);

        $id = $this->decodeResponseJson()['data']['id'];
        $dbResult = $this->setupDBResult($item, ['id' => $id], ['type']);
        $dbResult['updated_by'] = null;
        $dbResult['completed_at'] = null;
        $dbResult['is_completed'] = false;

        $this->seeInDatabase('items', $dbResult);
    }

    /**
     * @dataProvider dueDateDataProvider
     */
    public function testStoreWithDueDate($duePayload, $dueResponse)
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID, 'is_completed' => false]);
        $item = factory(ItemEloquent::class)->make([
            'user_id' => $this->userID, 'checklist_id' => $checklist->id, 'is_completed' => false
        ]);
        $itemPayloads = $this->setupPayloads($item->toArray());
        $itemPayloads['data']['attributes']['due'] = $duePayload;

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(CreateItemJob::class);
        }

        $response = $this->setupResponse($item, ['due' => $dueResponse], ['id']);
        $this->json('POST', '/api/v1/checklists/' . $checklist->id . '/items', $itemPayloads, $this->postHeaders)
            ->seeJson($response);
    }

    public function testStoreWithClientUpdateAttribute()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID, 'is_completed' => false]);
        $item = factory(ItemEloquent::class)->make([
            'user_id' => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => false,
            'notify_client' => false,
            'client_update_sent_at' => '2020-01-31 21:30:21',
        ]);
        $data = $this->setupPayloads($item->toArray());
        $response = $this->setupResponse($item, [], ['id']);

        $this->json('POST', '/api/v1/checklists/' . $checklist->id . '/items', $data, $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(201);

        $id = $this->decodeResponseJson()['data']['id'];
        $dbResult = $this->setupDBResult($item, ['id' => $id], ['type']);
        $dbResult['updated_by'] = null;
        $dbResult['completed_at'] = null;
        $dbResult['is_completed'] = false;
        $dbResult['notify_client'] = false;
        $dbResult['client_update_sent_at'] = '2020-01-31 21:30:21';

        $this->seeInDatabase('items', $dbResult);
    }

    public function testStoreWithUserId()
    {
        config(['app.env' => 'dev']);
        $userId = 885;

        $teamHeaders = $this->postHeadersTeamScope;
        Arr::set($teamHeaders, 'x-kwcommand-group-members', [$userId]);

        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $userId]);
        $item = factory(ItemEloquent::class)->make([
            'user_id' => $userId,
            'checklist_id' => $checklist->id,
            'is_completed' => false,
        ]);
        $itemPayloads = $this->setupPayloads($item->toArray());
        Arr::set($itemPayloads, 'data.attributes.user_id', $userId);

        $response = $this->setupResponse($item, [], ['id']);
        $this->json('POST', '/api/v1/checklists/' . $checklist->id . '/items', $itemPayloads, $teamHeaders)
            ->seeJson($response);

        config(['app.env' => 'prod']);

        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $userId]);
        $item = factory(ItemEloquent::class)->make([
            'user_id' => $userId,
            'checklist_id' => $checklist->id,
            'is_completed' => false,
        ]);
        $itemPayloads = $this->setupPayloads($item->toArray());
        Arr::set($itemPayloads, 'data.attributes.user_id', $userId);

        $response = $this->setupResponse($item, ['created_by' => $this->userID], ['id']);
        $this->json(
            'POST',
            '/api/v1/checklists/' . $checklist->id . '/items',
            $itemPayloads,
            $this->postHeadersTeamScope
        )->seeJson($response);
    }

    public function testComplete()
    {
        $dateCompleted = Carbon::now()->toIso8601String();
        Carbon::setTestNow($dateCompleted);

        $checklistLoginUser = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);
        $checklistOtherUser = factory(ChecklistEloquent::class)->create();
        $savedItemsLoginUser = factory(ItemEloquent::class, 5)->create([
            'user_id' => $this->userID,
            'checklist_id' => $checklistLoginUser->id,
            'completed_at' => null,
            'is_completed' => 0
        ]);
        $savedItemsOtherUser = factory(ItemEloquent::class, 2)->create([
            'checklist_id' => $checklistOtherUser->id,
            'completed_at' => null,
            'is_completed' => 0
        ]);

        if ($checklistLoginUser->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(CompleteItemJob::class);
        }

        $data = $this->setupPayloadsComplete($savedItemsLoginUser, $savedItemsOtherUser);
        $response = $this->setupResponsesComplete($savedItemsLoginUser, $savedItemsOtherUser, $dateCompleted);

        $result = $this->json('POST', '/api/v1/checklists/complete', $data, $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure([
                'data' => [
                    [
                        'checklist_id',
                        'item_id',
                        'completed_at',
                        'is_completed',
                    ],
                ],
            ])
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResultComplete($savedItemsLoginUser, $savedItemsOtherUser);

        foreach ($dbResult as $result) {
            $this->seeInDatabase('items', $result);
        }

        $this->seeInDatabase('checklists', [
            'id'           => $checklistLoginUser->id,
            'is_completed' => 1,
        ]);

        $this->seeInDatabase('checklists', [
            'id'           => $checklistOtherUser->id,
            'is_completed' => 0,
        ]);
    }

    public function testCompleteAndPublishToAudit()
    {
        $this->expectsJobs(CompleteItemJob::class);

        $dateCompleted = Carbon::now()->toIso8601String();
        Carbon::setTestNow($dateCompleted);

        $checklistLoginUser = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'object_domain' => self::DEAL_OBJECT,
        ]);
        $checklistOtherUser = factory(ChecklistEloquent::class)->create([
            'object_domain' => self::DEAL_OBJECT,
        ]);
        $savedItemsLoginUser = factory(ItemEloquent::class, 5)->create([
            'user_id' => $this->userID,
            'checklist_id' => $checklistLoginUser->id,
            'completed_at' => null,
            'is_completed' => 0
        ]);
        $savedItemsOtherUser = factory(ItemEloquent::class, 2)->create([
            'checklist_id' => $checklistOtherUser->id,
            'completed_at' => null,
            'is_completed' => 0
        ]);

        $data = $this->setupPayloadsComplete($savedItemsLoginUser, $savedItemsOtherUser);
        $response = $this->setupResponsesComplete($savedItemsLoginUser, $savedItemsOtherUser, $dateCompleted);

        $result = $this->json('POST', '/api/v1/checklists/complete', $data, $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure([
                'data' => [
                    [
                        'checklist_id',
                        'item_id',
                        'completed_at',
                        'is_completed',
                    ],
                ],
            ])
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResultComplete($savedItemsLoginUser, $savedItemsOtherUser);

        foreach ($dbResult as $result) {
            $this->seeInDatabase('items', $result);
        }

        $this->seeInDatabase('checklists', [
            'id'           => $checklistLoginUser->id,
            'is_completed' => 1,
        ]);

        $this->seeInDatabase('checklists', [
            'id'           => $checklistOtherUser->id,
            'is_completed' => 0,
        ]);
    }

    public function testCompleteByTeam()
    {
        $dateCompleted = Carbon::now()->toIso8601String();
        Carbon::setTestNow($dateCompleted);

        $checklistLoginUser = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);
        $checklistOtherUser = factory(ChecklistEloquent::class)->create(['user_id' => 556397]);
        $savedItemsLoginUser = factory(ItemEloquent::class, 5)->create([
            'user_id' => $this->userID,
            'checklist_id' => $checklistLoginUser->id,
            'completed_at' => null,
            'is_completed' => 0
        ]);
        $savedItemsOtherUser = factory(ItemEloquent::class, 2)->create([
            'user_id' => 556397,
            'checklist_id' => $checklistOtherUser->id,
            'completed_at' => null,
            'is_completed' => 0
        ]);

        if ($checklistLoginUser->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(CompleteItemJob::class);
        }

        $data = $this->setupPayloadsComplete($savedItemsLoginUser, $savedItemsOtherUser);
        $response = $this->setupResponseAllComplete($savedItemsLoginUser, $savedItemsOtherUser, $dateCompleted);

        $result = $this->json('POST', '/api/v1/checklists/complete', $data, $this->postHeadersTeamScope)
            ->seeJson($response)
            ->seeJsonStructure([
                'data' => [
                    [
                        'checklist_id',
                        'item_id',
                        'completed_at',
                        'is_completed',
                    ],
                ],
            ])
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResultAllComplete($savedItemsLoginUser, $savedItemsOtherUser, true, $this->userID);

        foreach ($dbResult as $result) {
            $this->seeInDatabase('items', $result);
        }

        $this->seeInDatabase('checklists', [
            'id'           => $checklistLoginUser->id,
            'is_completed' => 1,
        ]);

        $this->seeInDatabase('checklists', [
            'id'           => $checklistOtherUser->id,
            'is_completed' => 1,
        ]);
    }

    public function testCompleteInactiveMemberItemByTeam()
    {
        $dateCompleted = Carbon::now()->toIso8601String();
        Carbon::setTestNow($dateCompleted);

        $checklistLoginUser = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);
        $checklistInactiveUser = factory(ChecklistEloquent::class)->create(['user_id' => $this->inactiveMemberId]);
        $savedItemsLoginUser = factory(ItemEloquent::class, 5)->create([
            'user_id' => $this->userID,
            'checklist_id' => $checklistLoginUser->id,
            'completed_at' => null,
            'is_completed' => 0
        ]);
        $savedItemsInactiveUser = factory(ItemEloquent::class, 2)->create([
            'user_id' => $this->inactiveMemberId,
            'checklist_id' => $checklistInactiveUser->id,
            'completed_at' => null,
            'is_completed' => 0
        ]);

        $data = $this->setupPayloadsComplete($savedItemsLoginUser, $savedItemsInactiveUser);
        $response = $this->setupResponseAllComplete($savedItemsLoginUser, $savedItemsInactiveUser, $dateCompleted);

        $result = $this->json(
            'POST',
            '/api/v1/checklists/complete',
            $data,
            $this->postHeadersTeamScope
        )
            ->seeJson($response)
            ->seeJsonStructure([
                'data' => [
                    [
                        'checklist_id',
                        'item_id',
                        'completed_at',
                        'is_completed',
                    ],
                ],
            ])
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResultAllComplete($savedItemsLoginUser, $savedItemsInactiveUser, true, $this->userID);

        foreach ($dbResult as $result) {
            $this->seeInDatabase('items', $result);
        }

        $this->seeInDatabase('checklists', [
            'id'           => $checklistLoginUser->id,
            'is_completed' => 1,
        ]);

        $this->seeInDatabase('checklists', [
            'id'           => $checklistInactiveUser->id,
            'is_completed' => 1,
        ]);
    }

    public function testIncomplete()
    {
        $checklistLoginUser = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
        ]);
        $checklistOtherUser = factory(ChecklistEloquent::class)->create([
            'completed_at' => '2019-01-01 00:00:00',
            'is_completed' => 1,
            'user_id'      => 123,
        ]);
        $savedItemsLoginUser = factory(ItemEloquent::class, 5)->create([
            'user_id'      => $this->userID,
            'checklist_id' => $checklistLoginUser->id,
            'completed_at' => '2019-01-01 00:00:00',
            'is_completed' => 1,
            'completed_by' => $this->userID,
        ]);
        $savedItemsOtherUser = factory(ItemEloquent::class, 2)->create([
            'user_id'      => 123,
            'checklist_id' => $checklistOtherUser->id,
            'completed_at' => '2019-01-01 00:00:00',
            'is_completed' => 1,
            'completed_by' => 123,
        ]);

        if ($checklistLoginUser->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(IncompleteItemJob::class);
        }

        $data = $this->setupPayloadsComplete($savedItemsLoginUser, $savedItemsOtherUser);
        $response = $this->setupResponsesComplete($savedItemsLoginUser, $savedItemsOtherUser, null, false);

        $result = $this->json('POST', '/api/v1/checklists/incomplete', $data, $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure([
                'data' => [
                    [
                        'checklist_id',
                        'item_id',
                        'completed_at',
                        'is_completed',
                    ],
                ],
            ])
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResultComplete($savedItemsLoginUser, $savedItemsOtherUser, false);

        foreach ($dbResult as $result) {
            $this->seeInDatabase('items', $result);
        }

        $this->seeInDatabase('checklists', [
            'id'           => $checklistLoginUser->id,
            'is_completed' => 0,
        ]);

        $this->seeInDatabase('checklists', [
            'id'           => $checklistOtherUser->id,
            'is_completed' => 1,
        ]);
    }

    public function testIncompleteAndPublishToAudit()
    {
        $this->expectsJobs(IncompleteItemJob::class);

        $checklistLoginUser = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'object_domain' => self::DEAL_OBJECT,
        ]);
        $checklistOtherUser = factory(ChecklistEloquent::class)->create([
            'completed_at' => '2019-01-01 00:00:00',
            'is_completed' => 1,
            'user_id'      => 123,
            'object_domain' => self::DEAL_OBJECT,
        ]);
        $savedItemsLoginUser = factory(ItemEloquent::class, 5)->create([
            'user_id'      => $this->userID,
            'checklist_id' => $checklistLoginUser->id,
            'completed_at' => '2019-01-01 00:00:00',
            'is_completed' => 1,
            'completed_by' => $this->userID,
        ]);
        $savedItemsOtherUser = factory(ItemEloquent::class, 2)->create([
            'user_id'      => 123,
            'checklist_id' => $checklistOtherUser->id,
            'completed_at' => '2019-01-01 00:00:00',
            'is_completed' => 1,
            'completed_by' => 123,
        ]);

        $data = $this->setupPayloadsComplete($savedItemsLoginUser, $savedItemsOtherUser);
        $response = $this->setupResponsesComplete($savedItemsLoginUser, $savedItemsOtherUser, null, false);

        $result = $this->json('POST', '/api/v1/checklists/incomplete', $data, $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure([
                'data' => [
                    [
                        'checklist_id',
                        'item_id',
                        'completed_at',
                        'is_completed',
                    ],
                ],
            ])
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResultComplete($savedItemsLoginUser, $savedItemsOtherUser, false);

        foreach ($dbResult as $result) {
            $this->seeInDatabase('items', $result);
        }

        $this->seeInDatabase('checklists', [
            'id'           => $checklistLoginUser->id,
            'is_completed' => 0,
        ]);

        $this->seeInDatabase('checklists', [
            'id'           => $checklistOtherUser->id,
            'is_completed' => 1,
        ]);
    }

    public function testIncompleteByTeam()
    {
        $checklistLoginUser = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
        ]);
        $checklistOtherUser = factory(ChecklistEloquent::class)->create([
            'user_id' => 556397,
            'completed_at' => '2019-01-01 00:00:00',
            'is_completed' => 1,
            'user_id'      => 123,
        ]);
        $savedItemsLoginUser = factory(ItemEloquent::class, 5)->create([
            'user_id'      => $this->userID,
            'checklist_id' => $checklistLoginUser->id,
            'completed_at' => '2019-01-01 00:00:00',
            'is_completed' => 1,
            'completed_by' => $this->userID,
        ]);
        $savedItemsOtherUser = factory(ItemEloquent::class, 2)->create([
            'user_id' => 556397,
            'checklist_id' => $checklistOtherUser->id,
            'completed_at' => '2019-01-01 00:00:00',
            'is_completed' => 1,
            'completed_by' => 123,
        ]);

        if ($checklistLoginUser->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(IncompleteItemJob::class);
        }

        $data = $this->setupPayloadsComplete($savedItemsLoginUser, $savedItemsOtherUser);
        $response = $this->setupResponseAllComplete($savedItemsLoginUser, $savedItemsOtherUser, null, false);

        $result = $this->json('POST', '/api/v1/checklists/incomplete', $data, $this->postHeadersTeamScope);
        $result->seeJson($response)
            ->seeJsonStructure([
                'data' => [
                    [
                        'checklist_id',
                        'item_id',
                        'completed_at',
                        'is_completed',
                    ],
                ],
            ])
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResultAllComplete($savedItemsLoginUser, $savedItemsOtherUser, false);

        foreach ($dbResult as $result) {
            $this->seeInDatabase('items', $result);
        }

        $this->seeInDatabase('checklists', [
            'id'           => $checklistLoginUser->id,
            'is_completed' => 0,
        ]);

        $this->seeInDatabase('checklists', [
            'id'           => $checklistOtherUser->id,
            'is_completed' => 0,
        ]);
    }

    public function testIncompleteInactiveMemberItemByTeam()
    {
        $checklistLoginUser = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
        ]);
        $checklistInactiveUser = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->inactiveMemberId,
            'completed_at' => '2019-01-01 00:00:00',
            'is_completed' => 1,
            'user_id'      => 123,
        ]);
        $savedItemsLoginUser = factory(ItemEloquent::class, 5)->create([
            'user_id'      => $this->userID,
            'checklist_id' => $checklistLoginUser->id,
            'completed_at' => '2019-01-01 00:00:00',
            'is_completed' => 1,
            'completed_by' => $this->userID,
        ]);
        $savedItemsInactiveUser = factory(ItemEloquent::class, 2)->create([
            'user_id' => $this->inactiveMemberId,
            'checklist_id' => $checklistInactiveUser->id,
            'completed_at' => '2019-01-01 00:00:00',
            'is_completed' => 1,
            'completed_by' => 123,
        ]);

        $data = $this->setupPayloadsComplete($savedItemsLoginUser, $savedItemsInactiveUser);
        $response = $this->setupResponseAllComplete($savedItemsLoginUser, $savedItemsInactiveUser, null, false);

        $result = $this->json(
            'POST',
            '/api/v1/checklists/incomplete',
            $data,
            $this->postHeadersTeamScope
        );
        $result->seeJson($response)
            ->seeJsonStructure([
                'data' => [
                    [
                        'checklist_id',
                        'item_id',
                        'completed_at',
                        'is_completed',
                    ],
                ],
            ])
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResultAllComplete($savedItemsLoginUser, $savedItemsInactiveUser, false);

        foreach ($dbResult as $result) {
            $this->seeInDatabase('items', $result);
        }

        $this->seeInDatabase('checklists', [
            'id'           => $checklistLoginUser->id,
            'is_completed' => 0,
        ]);

        $this->seeInDatabase('checklists', [
            'id'           => $checklistInactiveUser->id,
            'is_completed' => 0,
        ]);
    }

    public function testUpdate()
    {
        $checklist = factory(ChecklistEloquent::class)->create();
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
        ]);
        $updateItem = factory(ItemEloquent::class)->make([
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by'   => $this->userID,
        ]);
        $data = $this->setupPayloads($updateItem->toArray());
        $response = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id]);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(UpdateItemJob::class);
        }

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeaders
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateItem, ['id' => $savedItem->id], ['type']);
        $dbResult['updated_by'] = $this->userID;

        $this->seeInDatabase('items', $dbResult);
        $this->seeJsonInRecord(['meta' => $updateItem->meta], ItemEloquent::class, ['id' => $savedItem->id]);
    }

    public function testUpdateAndPublishToAudit()
    {
        $this->expectsJobs(UpdateItemJob::class);

        $checklist = factory(ChecklistEloquent::class)->create([
            'object_domain' => self::DEAL_OBJECT,
        ]);
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
        ]);
        $updateItem = factory(ItemEloquent::class)->make([
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by'   => $this->userID,
        ]);
        $data = $this->setupPayloads($updateItem->toArray());
        $response = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeaders
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateItem, ['id' => $savedItem->id], ['type']);
        $dbResult['updated_by'] = $this->userID;

        $this->seeInDatabase('items', $dbResult);
    }

    public function testUpdateNotAllowed()
    {
        $this->mockUserFound(123);

        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => 123]);
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id'      => 123,
            'checklist_id' => $checklist->id,
        ]);
        $updateItem = factory(ItemEloquent::class)->make([
            'user_id'      => 123,
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by'   => $this->userID,
        ]);
        $data = $this->setupPayloads($updateItem->toArray());

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeaders
        )
            ->assertResponseStatus(403);
    }

    public function testUpdateInactiveUser()
    {
        $this->mockUserNotFound();

        $checklist = factory(ChecklistEloquent::class)->create();
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id'      => $this->inactiveMemberId,
            'checklist_id' => $checklist->id,
        ]);
        $updateItem = factory(ItemEloquent::class)->make([
            'user_id'      => $this->inactiveMemberId,
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by'   => $this->userID,
        ]);
        $data = $this->setupPayloads($updateItem->toArray());
        $response = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeaders
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateItem, ['id' => $savedItem->id], ['type']);
        $dbResult['updated_by'] = $this->userID;

        $this->seeInDatabase('items', $dbResult);
    }

    /**
     * @dataProvider dueDateDataProvider
     */
    public function testUpdateDueDate($duePayload, $dueResponse)
    {
        $checklist = factory(ChecklistEloquent::class)->create();
        $savedItem = factory(ItemEloquent::class)->create([
            'due'          => null,
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
        ]);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(UpdateItemJob::class);
        }

        $itemPayload = [
            'data' => [
                'attributes' => [
                    'due' => $duePayload,
                ],
            ],
        ];

        $response = [
            'id'  => (string) $savedItem->id,
            'due' => $dueResponse,
        ];

        $endpointUri = '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id;
        $this->json('PATCH', $endpointUri, $itemPayload, $this->postHeaders);
        $this->seeJson($response);
    }

    public function dueDateDataProvider()
    {
        return [
            [null, null],
            ['2001-01-01 10:01:01', '2001-01-01T10:01:01+00:00'],
            ['2018-06-21T10:10:23.185Z', '2018-06-21T10:10:23+00:00'],
            ['2002-02-02T10:02:02+00:00', '2002-02-02T10:02:02+00:00'],
        ];
    }

    public function testUpdateNullify()
    {
        $checklist = factory(ChecklistEloquent::class)->create();
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
        ]);
        $updateItem = factory(ItemEloquent::class)->make([
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by'   => $this->userID,
            'due'          => null,
            'urgency'      => 0,
            'assignee_id'  => null,
            'task_id'      => null,
        ]);
        $data = $this->setupPayloads($updateItem->toArray());
        $response = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id]);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(UpdateItemJob::class);
        }

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeaders
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateItem, ['id' => $savedItem->id], ['type']);
        $dbResult['updated_by'] = $this->userID;

        $this->seeInDatabase('items', $dbResult);
    }

    public function testUpdateWithClientUpdateAttributes()
    {
        $checklist = factory(ChecklistEloquent::class)->create();
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
            'notify_client' => false,
            'client_update_sent_at' => null,
        ]);
        $updateItem = factory(ItemEloquent::class)->make([
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by'   => $this->userID,
            'notify_client' => false,
            'client_update_sent_at' => '2020-01-31 21:30:21',
        ]);
        $data = $this->setupPayloads($updateItem->toArray());
        $response = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeaders
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateItem, ['id' => $savedItem->id], ['type']);
        $dbResult['updated_by'] = $this->userID;
        $dbResult['notify_client'] = false;
        $dbResult['client_update_sent_at'] = '2020-01-31 21:30:21';

        $this->seeInDatabase('items', $dbResult);
    }

    public function testUpdateBulkNotFound()
    {
        $checklist = factory(ChecklistEloquent::class)->create();

        $updateItems = [];
        $updateItems = factory(ItemEloquent::class, 2)
            ->make()
            ->transform(function ($updateItem) {
                return [
                    'id'         => random_int(100, 999),
                    'action'     => 'update',
                    'attributes' => [
                        'description' => $updateItem->description,
                        'due'         => (string) $updateItem->due,
                        'urgency'     => $updateItem->urgency,
                    ],
                ];
            })
            ->toArray();

        $data = $this->setupBulkPayloads($updateItems);

        $response = $this->setupBulkResponse($data, 404);

        $this->json('POST', '/api/v1/checklists/' . $checklist->id . '/items/_bulk', $data, $this->postHeaders)
            ->seeJson($response)
            ->assertResponseStatus(200);
    }

    public function testUpdateBulkNotAllowed()
    {
        $this->mockUserFound(123);

        $checklist = factory(ChecklistEloquent::class)->create();

        $savedItems = factory(ItemEloquent::class, 2)->create([
            'user_id'      => 123,
            'assignee_id'  => 123,
            'checklist_id' => $checklist->id,
        ]);

        $updateItems = [];

        foreach ($savedItems as $savedItem) {
            array_push($updateItems, [
                'id'         => $savedItem->id,
                'action'     => 'update',
                'attributes' => [
                    'description' => $savedItem->description,
                    'due'         => (string) $savedItem->due,
                    'urgency'     => 1,
                ],
            ]);
        }

        $data = $this->setupBulkPayloads($updateItems);

        $response = $this->setupBulkResponse($data, 403);

        $this->json('POST', '/api/v1/checklists/' . $checklist->id . '/items/_bulk', $data, $this->postHeaders)
            ->seeJson($response)
            ->assertResponseStatus(200);
    }

    public function testUpdateBulkSuccess()
    {
        $this->mockUserFound(123);

        $checklist = factory(ChecklistEloquent::class)->create();

        $owned = factory(ItemEloquent::class, 2)->create([
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
        ]);
        $assigned = factory(ItemEloquent::class, 2)->create([
            'user_id'      => 123,
            'assignee_id'  => $this->userID,
            'checklist_id' => $checklist->id,
        ]);
        $savedItems = $owned->merge($assigned);

        $updateItems = [];

        foreach ($savedItems as $savedItem) {
            array_push($updateItems, [
                'id'         => $savedItem->id,
                'action'     => 'update',
                'attributes' => [
                    'description' => $savedItem->description,
                    'due'         => (string) $savedItem->due,
                    'urgency'     => 1,
                    'notify_client' => true,
                    'client_update_sent_at' => '2020-01-04T01:01:01+00:00'
                ],
            ]);
        }

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(UpdateItemJob::class);
        }

        $data = $this->setupBulkPayloads($updateItems);

        $response = $this->setupBulkResponse($data, 200);

        $this->json('POST', '/api/v1/checklists/' . $checklist->id . '/items/_bulk', $data, $this->postHeaders)
            ->seeJson($response)
            ->assertResponseStatus(200);

        foreach ($updateItems as $itemKey => $itemValue) {
            $dbResult = $this->setupDBResult(
                $savedItems[$itemKey],
                [
                    'id'          => array_get($itemValue, 'id'),
                    'description' => array_get($itemValue, 'attributes.description'),
                    'due'         => array_get($itemValue, 'attributes.due'),
                    'urgency'     => array_get($itemValue, 'attributes.urgency'),
                    'notify_client'     => array_get($itemValue, 'attributes.notify_client'),
                    'client_update_sent_at'     => array_get($itemValue, 'attributes.client_update_sent_at'),
                ]
            );
            $dbResult['updated_by'] = $this->userID;

            $this->seeInDatabase('items', $dbResult);
        }
    }

    public function testUpdateBulkNullify()
    {
        $checklist = factory(ChecklistEloquent::class)->create();

        $savedItems = factory(ItemEloquent::class, 2)->create([
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
        ]);

        $updateItems = [];

        foreach ($savedItems as $savedItem) {
            array_push($updateItems, [
                'id'         => $savedItem->id,
                'action'     => 'update',
                'attributes' => [
                    'due'         => null,
                    'urgency'     => 0,
                    'task_id'     => null,
                    'assignee_id' => null,
                ],
            ]);
        }

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(UpdateItemJob::class);
        }

        $data = $this->setupBulkPayloads($updateItems);

        $response = $this->setupBulkResponse($data, 200);

        $this->json('POST', '/api/v1/checklists/' . $checklist->id . '/items/_bulk', $data, $this->postHeaders)
            ->seeJson($response)
            ->assertResponseStatus(200);

        foreach ($updateItems as $itemKey => $itemValue) {
            $dbResult = $this->setupDBResult(
                $savedItems[$itemKey],
                [
                    'id'          => array_get($itemValue, 'id'),
                    'due'         => array_get($itemValue, 'attributes.due'),
                    'urgency'     => array_get($itemValue, 'attributes.urgency'),
                    'assignee_id' => array_get($itemValue, 'attributes.assignee_id'),
                    'task_id'     => array_get($itemValue, 'attributes.task_id'),
                ]
            );
            $dbResult['updated_by'] = $this->userID;

            $this->seeInDatabase('items', $dbResult);
        }
    }

    public function testUpdateTeamOwnItem()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
        ]);
        $updateItem = factory(ItemEloquent::class)->make([
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by'   => $this->userID,
        ]);
        $data = $this->setupPayloads($updateItem->toArray());
        $response = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id]);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(UpdateItemJob::class);
        }

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeadersTeamScope
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateItem, ['id' => $savedItem->id], ['type']);
        $dbResult['updated_by'] = $this->userID;

        $this->seeInDatabase('items', $dbResult);
    }

    public function testUpdateTeamItem()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => 556397]);
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id'      => 556397,
            'checklist_id' => $checklist->id,
        ]);
        $updateItem = factory(ItemEloquent::class)->make([
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by'   => $this->userID,
            'user_id'      => $savedItem->user_id,
        ]);
        $data = $this->setupPayloads($updateItem->toArray());
        $response = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id, 'updated_by' => $this->userID]);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(UpdateItemJob::class);
        }

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeadersTeamScope
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateItem, ['id' => $savedItem->id, 'user_id' => 556397], ['type']);
        $dbResult['updated_by'] = $this->userID;

        $this->seeInDatabase('items', $dbResult);
    }

    public function testUpdateTeamInactiveMemberItem()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->inactiveMemberId]);
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id'      => $this->inactiveMemberId,
            'checklist_id' => $checklist->id,
        ]);
        $updateItem = factory(ItemEloquent::class)->make([
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by'   => $this->userID,
            'user_id'      => $savedItem->user_id,
        ]);
        $data = $this->setupPayloads($updateItem->toArray());
        $response = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id, 'updated_by' => $this->userID]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeadersTeamScope
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateItem, [
            'id' => $savedItem->id,
            'user_id' => $this->inactiveMemberId
        ], ['type']);
        $dbResult['updated_by'] = $this->userID;

        $this->seeInDatabase('items', $dbResult);
    }

    public function testUpdateTeamOtherItem()
    {
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => 556399]);
        $savedItem = factory(ItemEloquent::class)->create([
            'user_id'      => 556399,
            'checklist_id' => $checklist->id,
        ]);
        $updateItem = factory(ItemEloquent::class)->make([
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by'   => $this->userID,
            'user_id'      => $savedItem->user_id,
        ]);
        $data = $this->setupPayloads($updateItem->toArray());
        $response = $this->setupResponse($updateItem, [
            'id' => (string) $savedItem->id,
            'updated_by' => $this->userID
        ]);

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeadersTeamScope
        )
            ->assertResponseStatus(403);
    }

    public function testDestroy()
    {
        $checklist = factory(ChecklistEloquent::class)->create();
        $item = factory(ItemEloquent::class)->create(['user_id' => $this->userID, 'checklist_id' => $checklist->id]);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(DeleteItemJob::class);
        }

        $this->json('DELETE', '/api/v1/checklists/' . $checklist->id . '/items/' . $item->id, [], $this->postHeaders);
        $this->seeStatusCode(204);
        $this->missingFromDatabase('items', [
            'id'         => $item->id,
            'deleted_at' => null,
        ]);
    }

    public function testDestroyNotAllowed()
    {
        $this->mockUserFound(123);

        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => 123]);
        $item = factory(ItemEloquent::class)->create(['user_id' => 123, 'checklist_id' => $checklist->id]);
        $this->json('DELETE', '/api/v1/checklists/' . $checklist->id . '/items/' . $item->id, [], $this->postHeaders);
        $this->seeStatusCode(403);
        $this->seeInDatabase('items', [
            'id' => $item->id,
        ]);
    }

    public function testDestroyAndPublishToAudit()
    {
        $this->expectsJobs(DeleteItemJob::class);

        $checklist = factory(ChecklistEloquent::class)->create([
            'object_domain' => self::DEAL_OBJECT,
        ]);
        $item = factory(ItemEloquent::class)->create(['user_id' => $this->userID, 'checklist_id' => $checklist->id]);

        $this->json('DELETE', '/api/v1/checklists/' . $checklist->id . '/items/' . $item->id, [], $this->postHeaders);
        $this->seeStatusCode(204);
        $this->missingFromDatabase('items', [
            'id'         => $item->id,
            'deleted_at' => null,
        ]);
    }

    public function testDestroyInTeamGateWay()
    {
        $checklist = factory(ChecklistEloquent::class)->create();
        $item = factory(ItemEloquent::class)->create(['user_id' => $this->userID, 'checklist_id' => $checklist->id]);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(DeleteItemJob::class);
        }

        $this->json(
            'DELETE',
            '/api/v1/checklists/' . $checklist->id . '/items/' . $item->id,
            [],
            $this->postHeadersTeamScope
        );
        $this->seeStatusCode(204);
        $this->missingFromDatabase('items', [
            'id'         => $item->id,
            'deleted_at' => null,
        ]);
    }

    public function testDestroyInactiveMemberInTeamGateWay()
    {
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->inactiveMemberId
        ]);
        $item = factory(ItemEloquent::class)->create([
            'user_id' => $this->inactiveMemberId,
            'checklist_id' => $checklist->id
        ]);

        $this->json(
            'DELETE',
            '/api/v1/checklists/' . $checklist->id . '/items/' . $item->id,
            [],
            $this->postHeadersTeamScope
        );
        $this->seeStatusCode(204);
        $this->missingFromDatabase('items', [
            'id'         => $item->id,
            'deleted_at' => null,
        ]);
    }

    public function testShowItemWrongChecklist()
    {
        $checklists = factory(ChecklistEloquent::class, 2)->create([
            'user_id' => $this->userID,
        ]);
        $item = factory(ItemEloquent::class)->create([
            'user_id'      => $this->userID,
            'checklist_id' => $checklists[1]->id,
        ]);

        $this->json('GET', "/api/v1/checklists/{$checklists[0]->id}/items/{$item->id}", [], $this->postHeaders)
            ->assertResponseStatus(404);
    }

    public function testCompleteChecklistAfterDeleteItem()
    {
        $now = Carbon::now();
        Carbon::setTestNow($now);

        $checklist = factory(ChecklistEloquent::class)->create();

        $items = factory(ItemEloquent::class, 3)->create([
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => 1,
            'completed_at' => $now,
        ]);
        $deletedItem = factory(ItemEloquent::class)->create([
            'user_id'      => $this->userID,
            'checklist_id' => $checklist->id,
            'is_completed' => 0,
        ]);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(DeleteItemJob::class);
        }

        // This is a hacky solutions found in here:
        // https://github.com/laravel/framework/issues/18066#issuecomment-342630971
        // It seems that the Model Eloquent events are not triggered when in test.
        // More reference of the issue https://github.com/laravel/framework/issues/1181.
        // We call it in here to make sure only the event after this is called.
        // If we call this workaround in the top of this function, the other events will be called
        // and we don't want to do that.
        Model::setEventDispatcher(Event::getFacadeRoot());

        $this->json(
            'DELETE',
            '/api/v1/checklists/' . $checklist->id . '/items/' . $deletedItem->id,
            [],
            $this->postHeaders
        );
        $this->seeStatusCode(204);
        $this->missingFromDatabase('items', [
            'id'         => $deletedItem->id,
            'deleted_at' => null,
        ]);

        $this->seeInDatabase('checklists', [
            'id'           => $checklist->id,
            'is_completed' => 1,
            'completed_at' => $now,
        ]);
    }

    public function testIndexWithoutChecklist()
    {
        $this->withoutEvents();

        $userId = $this->userID;
        $otherUserId = 556397;

        $checklist = factory(ChecklistEloquent::class)->create();
        $notRelated = factory(ItemEloquent::class)->create([
            'checklist_id' => $checklist->id,
            'user_id'      => $otherUserId,
            'assignee_id'  => $otherUserId,
        ]);
        $onlyAssigned = factory(ItemEloquent::class)->create([
            'checklist_id' => $checklist->id,
            'user_id'      => $otherUserId,
            'assignee_id'  => $userId,
        ]);
        $ownedAssigned = factory(ItemEloquent::class)->create([
            'checklist_id' => $checklist->id,
            'user_id'      => $userId,
            'assignee_id'  => $userId,
        ]);
        $onlyOwned = factory(ItemEloquent::class)->create([
            'checklist_id' => $checklist->id,
            'user_id'      => $userId,
            'assignee_id'  => $otherUserId,
        ]);

        $this->json('GET', '/api/v1/checklists/items?include=checklist', [], $this->postHeaders)
            ->seeJsonStructure([
                'meta'  => ['count', 'total'],
                'data'  => [$this->basicJsonStructure['data']],
                'links' => ['first', 'last', 'next', 'prev'],
            ])
            ->seeJson(['id' => (string) $checklist->id])
            ->seeJson(['meta' => ['count' => 2, 'total' => 2]])
            ->seeJson(['id' => (string) $ownedAssigned->id])
            ->seeJson(['id' => (string) $onlyOwned->id])
            ->dontSeeJson(['id' => (string) $onlyAssigned->id])
            ->dontSeeJson(['id' => (string) $notRelated->id])
            ->seeStatusCode(200);
    }

    public function testIndexWithoutChecklistWithFilters()
    {
        $this->withoutEvents();

        $userIdLoggedIn = $this->userID;
        $assigneeIdLoggedIn = $this->userID;
        $userIdOther = 2;
        $assigneeIdOther = 456;

        $due0401 = Carbon::createFromFormat(\DateTime::ISO8601, '2019-04-01T09:00:00Z');
        $due0403 = Carbon::createFromFormat(\DateTime::ISO8601, '2019-04-03T12:00:00Z');
        $dueOther = Carbon::createFromFormat(\DateTime::ISO8601, '2019-04-03T13:00:00Z');

        $startDueDate = '2019-04-01T09:00:00Z';
        $endDueDate = '2019-04-03T12:00:00Z';

        $owned0401 = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdLoggedIn,
            'assignee_id' => $assigneeIdLoggedIn,
            'due'         => $due0401,
        ]);

        $owned0403 = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdLoggedIn,
            'assignee_id' => $assigneeIdLoggedIn,
            'due'         => $due0403,
        ]);

        // Different assignee_id. Won't be included in response
        $onlyOwned0401 = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdLoggedIn,
            'assignee_id' => $assigneeIdOther,
            'due'         => $due0401,
        ]);

        // Different user_id. This will be included in response
        $onlyAssigneed0401 = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdOther,
            'assignee_id' => $assigneeIdLoggedIn,
            'due'         => $due0401,
        ]);

        // Different due_time. Won't be included in response
        $ownedWithOutsideDue = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdLoggedIn,
            'assignee_id' => $assigneeIdLoggedIn,
            'due'         => $dueOther,
        ]);

        $queryString = "?filter[assignee_id][is]=$userIdLoggedIn" .
            "&filter[created_by][is]=$userIdLoggedIn&filter[due][between]=$startDueDate,$endDueDate";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeaders)
            ->seeJson(['meta' => ['count' => 3, 'total' => 3]])
            ->seeJsonStructure([
                'meta'  => ['count', 'total'],
                'data'  => [$this->basicJsonStructure['data']],
                'links' => ['first', 'last', 'next', 'prev'],
            ])
            ->seeJson(['id' => (string) $owned0401->id])
            ->seeJson(['id' => (string) $owned0403->id])
            ->seeJson(['id' => (string) $onlyAssigneed0401->id])
            ->dontSeeJson(['id' => (string) $ownedWithOutsideDue->id])
            ->dontSeeJson(['id' => (string) $onlyOwned0401->id])
            ->seeStatusCode(200);
    }

    public function testIndexWithoutChecklistWhenAssigneeIdIsHerOwn()
    {
        $this->withoutEvents();

        $assigneeIdLoggedIn = $this->userID;
        $assigneeIdOther = 556397;

        $userIdLoggedIn = $this->userID;
        $userIdOther = 556387;

        $owned = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdLoggedIn,
            'assignee_id' => $assigneeIdLoggedIn,
        ]);

        $onlyOwned = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdLoggedIn,
            'assignee_id' => $assigneeIdOther,
        ]);

        $onlyAssigned = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdOther,
            'assignee_id' => $assigneeIdLoggedIn,
        ]);

        $notRelated = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdOther,
            'assignee_id' => $assigneeIdOther,
        ]);

        // Without created_by filter

        $queryString = "?filter[assignee_id][is]=$assigneeIdLoggedIn";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeaders)
            ->seeJson(['meta' => ['count' => 2, 'total' => 2]])
            ->seeJson(['id' => (string) $owned->id])
            ->seeJson(['id' => (string) $onlyAssigned->id])
            ->dontSeeJson(['id' => (string) $onlyOwned->id])
            ->dontSeeJson(['id' => (string) $notRelated->id])
            ->seeStatusCode(200);

        // With created_by filter

        $queryString = "?filter[assignee_id][is]=$assigneeIdLoggedIn&filter[created_by][is]=$userIdOther";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeaders)
            ->seeJson(['meta' => ['count' => 2, 'total' => 2]])
            ->seeJson(['id' => (string) $owned->id])
            ->seeJson(['id' => (string) $onlyAssigned->id])
            ->dontSeeJson(['id' => (string) $onlyOwned->id])
            ->dontSeeJson(['id' => (string) $notRelated->id])
            ->seeStatusCode(200);

        // With multiple assignee_id

        $queryString = "?filter[assignee_id][in]=$assigneeIdLoggedIn,$assigneeIdOther";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeaders)
            ->seeJson(['meta' => ['count' => 2, 'total' => 2]])
            ->seeJson(['id' => (string) $owned->id])
            ->seeJson(['id' => (string) $onlyOwned->id])
            ->dontSeeJson(['id' => (string) $onlyAssigned->id])
            ->dontSeeJson(['id' => (string) $notRelated->id])
            ->seeStatusCode(200);

        // With multiple assignee_id and multiple created_by

        $queryString = "?filter[assignee_id][in]=$assigneeIdLoggedIn,$assigneeIdOther"
            . "&filter[created_by][in]=$userIdLoggedIn,$userIdOther";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeaders)
            ->seeJson(['meta' => ['count' => 2, 'total' => 2]])
            ->seeJson(['id' => (string) $owned->id])
            ->seeJson(['id' => (string) $onlyOwned->id])
            ->dontSeeJson(['id' => (string) $onlyAssigned->id])
            ->dontSeeJson(['id' => (string) $notRelated->id])
            ->seeStatusCode(200);
    }

    public function testIndexWithoutChecklistWhenAssigneeIdIsNotHerOwn()
    {
        $this->withoutEvents();

        $assigneeIdLoggedIn = $this->userID;
        $assigneeIdOther = 556397;

        $userIdLoggedIn = $this->userID;
        $userIdOther = 556387;

        $owned = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdLoggedIn,
            'assignee_id' => $assigneeIdLoggedIn,
        ]);

        $onlyOwned = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdLoggedIn,
            'assignee_id' => $assigneeIdOther,
        ]);

        $onlyAssigned = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdOther,
            'assignee_id' => $assigneeIdLoggedIn,
        ]);

        $notRelated = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdOther,
            'assignee_id' => $assigneeIdOther,
        ]);

        // Without created_by filter

        $queryString = "?filter[assignee_id][is]=$assigneeIdOther";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeaders)
            ->seeJson(['meta' => ['count' => 1, 'total' => 1]])
            ->seeJson(['id' => (string) $onlyOwned->id])
            ->dontSeeJson(['id' => (string) $owned->id])
            ->dontSeeJson(['id' => (string) $onlyAssigned->id])
            ->dontSeeJson(['id' => (string) $notRelated->id])
            ->seeStatusCode(200);

        // With created_by filter

        $queryString = "?filter[assignee_id][is]=$assigneeIdOther&filter[created_by][is]=$userIdOther";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeaders)
            ->seeJson(['meta' => ['count' => 1, 'total' => 1]])
            ->seeJson(['id' => (string) $onlyOwned->id])
            ->dontSeeJson(['id' => (string) $owned->id])
            ->dontSeeJson(['id' => (string) $onlyAssigned->id])
            ->dontSeeJson(['id' => (string) $notRelated->id])
            ->seeStatusCode(200);
    }

    public function testIndexWithoutChecklistWithDueDateVariation()
    {
        $this->withoutEvents();

        $userIdLoggedIn = $this->userID;

        $due0401 = Carbon::createFromFormat(\DateTime::ISO8601, '2019-04-01T09:00:00Z');
        $due0403 = Carbon::createFromFormat(\DateTime::ISO8601, '2019-04-03T12:00:00Z');
        $dueOther = Carbon::createFromFormat(\DateTime::ISO8601, '2019-04-03T13:00:00Z');

        $startDueWithTz = '2019-04-01T04:00:00-05:00'; // => 2019-04-01T09:00:00Z
        $endDueWithTzWrong = '2019-04-03T06:59:59-05:00'; // => 2019-04-03T11:59:59Z
        $endDueWithTzCorrect = '2019-04-03T07:00:00-05:00'; // => 2019-04-03T12:00:00Z
        $startDueWithZeroTz = '2019-04-01T09:00:00-00:00';
        $endDueWithZeroTz = '2019-04-03T12:00:00-00:00';
        $startDueUTC = '2019-04-01T09:00:00Z';
        $endDueUTC = '2019-04-03T12:00:00Z';

        $owned0401 = factory(ItemEloquent::class)->create([
            'user_id' => $userIdLoggedIn,
            'due'     => $due0401,
        ]);
        $owned0403 = factory(ItemEloquent::class)->create([
            'user_id' => $userIdLoggedIn,
            'due'     => $due0403,
        ]);
        $ownedWithOtherDue = factory(ItemEloquent::class)->create([
            'user_id' => $userIdLoggedIn,
            'due'     => $dueOther,
        ]);

        $queryString = "?filter[due][between]=$startDueWithTz,$endDueWithTzWrong";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeaders)
            ->seeJson(['meta' => ['count' => 1, 'total' => 1]])
            ->seeJsonStructure([
                'meta'  => ['count', 'total'],
                'data'  => [$this->basicJsonStructure['data']],
                'links' => ['first', 'last', 'next', 'prev'],
            ])
            ->seeJson(['id' => (string) $owned0401->id])
            ->dontSeeJson(['id' => (string) $owned0403->id])
            ->dontSeeJson(['id' => (string) $ownedWithOtherDue->id])
            ->seeStatusCode(200);

        $queryString = "?filter[due][between]=$startDueWithTz,$endDueWithTzCorrect";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeaders)
            ->seeJson(['meta' => ['count' => 2, 'total' => 2]])
            ->seeJsonStructure([
                'meta'  => ['count', 'total'],
                'data'  => [$this->basicJsonStructure['data']],
                'links' => ['first', 'last', 'next', 'prev'],
            ])
            ->seeJson(['id' => (string) $owned0401->id])
            ->seeJson(['id' => (string) $owned0403->id])
            ->seeStatusCode(200);

        $queryString = "?filter[due][between]=$startDueWithZeroTz,$endDueWithZeroTz";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeaders)
            ->seeJson(['meta' => ['count' => 2, 'total' => 2]])
            ->seeJsonStructure([
                'meta'  => ['count', 'total'],
                'data'  => [$this->basicJsonStructure['data']],
                'links' => ['first', 'last', 'next', 'prev'],
            ])
            ->seeJson(['id' => (string) $owned0401->id])
            ->seeJson(['id' => (string) $owned0403->id])
            ->dontSeeJson(['id' => (string) $ownedWithOtherDue->id])
            ->seeStatusCode(200);

        $queryString = "?filter[due][between]=$startDueUTC,$endDueUTC";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeaders)
            ->seeJson(['meta' => ['count' => 2, 'total' => 2]])
            ->seeJsonStructure([
                'meta'  => ['count', 'total'],
                'data'  => [$this->basicJsonStructure['data']],
                'links' => ['first', 'last', 'next', 'prev'],
            ])
            ->seeJson(['id' => (string) $owned0401->id])
            ->seeJson(['id' => (string) $owned0403->id])
            ->dontSeeJson(['id' => (string) $ownedWithOtherDue->id])
            ->seeStatusCode(200);
    }

    public function testIndexWithoutChecklistInTeamGateway()
    {
        $this->withoutEvents();

        $userIdLoggedIn = $this->userID;
        $userIdTeam = 556397;
        $userIdOtherTeam = 556399;

        $owned = factory(ItemEloquent::class)->create([
            'user_id' => $userIdLoggedIn,
        ]);
        $teamOwned = factory(ItemEloquent::class)->create([
            'user_id' => $userIdTeam,
        ]);
        $otherTeamOwned = factory(ItemEloquent::class)->create([
            'user_id' => $userIdOtherTeam,
        ]);

        $this->json('GET', "/api/v1/checklists/items", [], $this->postHeadersTeamScope)
            ->seeJsonStructure([
                'meta'  => ['count', 'total'],
                'data'  => [$this->basicJsonStructure['data']],
                'links' => ['first', 'last', 'next', 'prev'],
            ])
            ->seeJson(['meta' => ['count' => 2, 'total' => 2]])
            ->seeJson(['id' => (string) $owned->id])
            ->seeJson(['id' => (string) $teamOwned->id])
            ->dontSeeJson(['id' => (string) $otherTeamOwned->id])
            ->seeStatusCode(200);
    }

    public function testIndexWithoutChecklistInTeamGatewayWithInactiveMember()
    {
        $this->withoutEvents();

        $userIdLoggedIn = $this->userID;
        $userIdTeam = 556397;
        $userIdInactiveTeam = $this->inactiveMemberId;
        $userIdOtherTeam = 556399;

        $owned = factory(ItemEloquent::class)->create([
            'user_id' => $userIdLoggedIn,
        ]);
        $teamOwned = factory(ItemEloquent::class)->create([
            'user_id' => $userIdTeam,
        ]);
        $teamOwnedInactive = factory(ItemEloquent::class)->create([
            'user_id' => $userIdInactiveTeam,
        ]);
        $otherTeamOwned = factory(ItemEloquent::class)->create([
            'user_id' => $userIdOtherTeam,
        ]);

        $this->json('GET', "/api/v1/checklists/items", [], $this->postHeadersTeamScope)
            ->seeJsonStructure([
                'meta'  => ['count', 'total'],
                'data'  => [$this->basicJsonStructure['data']],
                'links' => ['first', 'last', 'next', 'prev'],
            ])
            ->seeJson(['meta' => ['count' => 3, 'total' => 3]])
            ->seeJson(['id' => (string) $owned->id])
            ->seeJson(['id' => (string) $teamOwned->id])
            ->seeJson(['id' => (string) $teamOwnedInactive->id])
            ->dontSeeJson(['id' => (string) $otherTeamOwned->id])
            ->seeStatusCode(200);
    }

    public function testIndexWithoutChecklistInTeamGatewayWithFilters()
    {
        $this->withoutEvents();

        $userIdLoggedIn = $this->userID;
        $userIdTeam = 556397;
        $userIdOtherTeam = 556399;

        $owned = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdLoggedIn,
            'assignee_id' => $userIdLoggedIn,
        ]);
        $onlyOwnedWithTeamAssignee = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdLoggedIn,
            'assignee_id' => $userIdTeam,
        ]);
        $onlyAssignedOwnedByTeam = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdTeam,
            'assignee_id' => $userIdLoggedIn,
        ]);
        $ownedByTeam = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdTeam,
            'assignee_id' => $userIdTeam,
        ]);
        $ownedByOtherTeam = factory(ItemEloquent::class)->create([
            'user_id'     => $userIdOtherTeam,
            'assignee_id' => $userIdOtherTeam,
        ]);

        // Single assignee id with default created by

        $queryString = "?filter[assignee_id][is]=$userIdLoggedIn";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeadersTeamScope)
            ->seeJson(['meta' => ['count' => 2, 'total' => 2]])
            ->seeJson(['id' => (string) $owned->id])
            ->seeJson(['id' => (string) $onlyAssignedOwnedByTeam->id])
            ->dontSeeJson(['id' => (string) $onlyOwnedWithTeamAssignee->id])
            ->dontSeeJson(['id' => (string) $ownedByTeam->id])
            ->dontSeeJson(['id' => (string) $ownedByOtherTeam->id])
            ->seeStatusCode(200);

        $queryString = "?filter[assignee_id][is]=$userIdTeam";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeadersTeamScope)
            ->seeJson(['meta' => ['count' => 2, 'total' => 2]])
            ->seeJson(['id' => (string) $onlyOwnedWithTeamAssignee->id])
            ->seeJson(['id' => (string) $ownedByTeam->id])
            ->dontSeeJson(['id' => (string) $owned->id])
            ->dontSeeJson(['id' => (string) $onlyAssignedOwnedByTeam->id])
            ->dontSeeJson(['id' => (string) $ownedByOtherTeam->id])
            ->seeStatusCode(200);

        // Single assignee id with created by

        $queryString = "?filter[assignee_id][is]=$userIdLoggedIn&filter[created_by][is]=$userIdLoggedIn";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeadersTeamScope)
            ->seeJson(['meta' => ['count' => 1, 'total' => 1]])
            ->seeJson(['id' => (string) $owned->id])
            ->dontSeeJson(['id' => (string) $onlyOwnedWithTeamAssignee->id])
            ->dontSeeJson(['id' => (string) $onlyAssignedOwnedByTeam->id])
            ->dontSeeJson(['id' => (string) $ownedByTeam->id])
            ->dontSeeJson(['id' => (string) $ownedByOtherTeam->id])
            ->seeStatusCode(200);

        $queryString = "?filter[assignee_id][is]=$userIdTeam&filter[created_by][is]=$userIdTeam";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeadersTeamScope)
            ->seeJson(['meta' => ['count' => 1, 'total' => 1]])
            ->seeJson(['id' => (string) $ownedByTeam->id])
            ->dontSeeJson(['id' => (string) $owned->id])
            ->dontSeeJson(['id' => (string) $onlyOwnedWithTeamAssignee->id])
            ->dontSeeJson(['id' => (string) $onlyAssignedOwnedByTeam->id])
            ->dontSeeJson(['id' => (string) $ownedByOtherTeam->id])
            ->seeStatusCode(200);

        // Multiple assignee id

        $queryString = "?filter[assignee_id][in]=$userIdLoggedIn,$userIdTeam";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeadersTeamScope)
            ->seeJson(['meta' => ['count' => 4, 'total' => 4]])
            ->seeJson(['id' => (string) $owned->id])
            ->seeJson(['id' => (string) $onlyOwnedWithTeamAssignee->id])
            ->seeJson(['id' => (string) $onlyAssignedOwnedByTeam->id])
            ->seeJson(['id' => (string) $ownedByTeam->id])
            ->dontSeeJson(['id' => (string) $ownedByOtherTeam->id])
            ->seeStatusCode(200);

        $queryString = "?filter[assignee_id][in]=$userIdLoggedIn,$userIdTeam&filter[created_by][is]=$userIdTeam";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeadersTeamScope)
            ->seeJson(['meta' => ['count' => 2, 'total' => 2]])
            ->seeJson(['id' => (string) $onlyAssignedOwnedByTeam->id])
            ->seeJson(['id' => (string) $ownedByTeam->id])
            ->dontSeeJson(['id' => (string) $owned->id])
            ->dontSeeJson(['id' => (string) $onlyOwnedWithTeamAssignee->id])
            ->dontSeeJson(['id' => (string) $ownedByOtherTeam->id])
            ->seeStatusCode(200);

        // Multiple created by

        $queryString = "?filter[created_by][in]=$userIdLoggedIn,$userIdTeam";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeadersTeamScope)
            ->seeJson(['meta' => ['count' => 4, 'total' => 4]])
            ->seeJson(['id' => (string) $owned->id])
            ->seeJson(['id' => (string) $onlyOwnedWithTeamAssignee->id])
            ->seeJson(['id' => (string) $onlyAssignedOwnedByTeam->id])
            ->seeJson(['id' => (string) $ownedByTeam->id])
            ->dontSeeJson(['id' => (string) $ownedByOtherTeam->id])
            ->seeStatusCode(200);

        $queryString = "?filter[assignee_id][is]=$userIdLoggedIn&filter[created_by][in]=$userIdLoggedIn,$userIdTeam";

        $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeadersTeamScope)
            ->seeJson(['meta' => ['count' => 2, 'total' => 2]])
            ->seeJson(['id' => (string) $owned->id])
            ->seeJson(['id' => (string) $onlyAssignedOwnedByTeam->id])
            ->dontSeeJson(['id' => (string) $onlyOwnedWithTeamAssignee->id])
            ->dontSeeJson(['id' => (string) $ownedByTeam->id])
            ->dontSeeJson(['id' => (string) $ownedByOtherTeam->id])
            ->seeStatusCode(200);
    }

    public function testNotificationWhenItemIsCreated()
    {
        $dueDate = Carbon::now()->addDays(2);
        $differentDueDate = Carbon::now()->addDays(5);

        $assigneeId = $this->userID;

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id'      => $this->userID,
            'is_completed' => false,
        ]);
        $itemWithAssigneeAndDue = factory(ItemEloquent::class)->make([
            'assignee_id'  => $assigneeId,
            'due'          => $dueDate,
            'is_completed' => false,
        ]);
        $itemWithAssigneeOnly = factory(ItemEloquent::class)->make([
            'assignee_id'  => $assigneeId,
            'due'          => null,
            'is_completed' => false,
        ]);
        $itemWithDueOnly = factory(ItemEloquent::class)->make([
            'assignee_id'  => null,
            'due'          => $dueDate,
            'is_completed' => false,
        ]);
        $itemAlreadyCompleted = factory(ItemEloquent::class)->make([
            'assignee_id'  => $assigneeId,
            'due'          => $dueDate,
            'is_completed' => true,
            'completed_at' => Carbon::now(),
        ]);
        $itemWithAssigneeAndDifferentDue = factory(ItemEloquent::class)->make([
            'assignee_id'  => $assigneeId,
            'due'          => $differentDueDate,
            'is_completed' => false,
            'completed_at' => null,
        ]);

        $this->activateObservers();

        $this->assertJobDispatched(GroupedItemsNotificationJob::class, [
            function ($job) use ($itemWithAssigneeAndDue) {
                return $job->assigneeId == $itemWithAssigneeAndDue->assignee_id
                && $job->due == $itemWithAssigneeAndDue->due->copy()->setTime(0, 0, 0);
            },
        ]);
        $this->assertJobDispatched(SendItemAssignedEmailJob::class, [
            function ($job) use ($itemWithAssigneeAndDue) {
                return $job->item->description == $itemWithAssigneeAndDue->description
                && $job->user->getKey() == $this->userID;
            },
        ]);


        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->assertJobDispatched(CreateItemJob::class);
        }

        $itemPayload = $this->setupPayloads($itemWithAssigneeAndDue->toArray());
        $this->actingAs($this->user)
            ->json('POST', '/api/v1/checklists/' . $checklist->id . '/items', $itemPayload, $this->postHeaders)
            ->assertResponseStatus(201);

        $this->seeInDatabase('last_due_reminders', [
            'kwuid' => $assigneeId,
            'due'   => $dueDate->format('Y-m-d'),
        ]);

        $itemPayload = $this->setupPayloads($itemWithAssigneeOnly->toArray());
        $this->actingAs($this->user)
            ->json('POST', '/api/v1/checklists/' . $checklist->id . '/items', $itemPayload, $this->postHeaders)
            ->assertResponseStatus(201);

        $count = LastDueReminderEloquent::where('kwuid', $assigneeId)
            ->where('due', $dueDate->format('Y-m-d'))
            ->count();

        $this->assertEquals(1, $count); // No last_due_reminders created

        $itemPayload = $this->setupPayloads($itemWithDueOnly->toArray());
        $this->actingAs($this->user)
            ->json('POST', '/api/v1/checklists/' . $checklist->id . '/items', $itemPayload, $this->postHeaders)
            ->assertResponseStatus(201);

        $count = LastDueReminderEloquent::where('kwuid', $assigneeId)
            ->where('due', $dueDate->format('Y-m-d'))
            ->count();

        $this->assertEquals(1, $count); // No last_due_reminders created

        $itemPayload = $this->setupPayloads($itemAlreadyCompleted->toArray());
        $this->actingAs($this->user)
            ->json('POST', '/api/v1/checklists/' . $checklist->id . '/items', $itemPayload, $this->postHeaders)
            ->assertResponseStatus(201);

        $count = LastDueReminderEloquent::where('kwuid', $assigneeId)
            ->where('due', $dueDate->format('Y-m-d'))
            ->count();

        $this->assertEquals(1, $count); // No last_due_reminders created

        $itemPayload = $this->setupPayloads($itemWithAssigneeAndDifferentDue->toArray());
        $this->actingAs($this->user)
            ->json('POST', '/api/v1/checklists/' . $checklist->id . '/items', $itemPayload, $this->postHeaders)
            ->assertResponseStatus(201);

        $count = LastDueReminderEloquent::where('kwuid', $assigneeId)
            ->where('due', $differentDueDate->format('Y-m-d'))
            ->count();

        $this->assertEquals(1, $count); // New last_due_reminders created with different due date
    }

    public function testGroupedItemNotificationWhenItemUpdated()
    {
        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id'      => $this->userID,
            'is_completed' => false,
        ]);
        $item = factory(ItemEloquent::class)->create([
            'checklist_id' => $checklist->id,
            'assignee_id'  => $this->userID,
            'due'          => Carbon::now()->addDays(1),
            'is_completed' => false,
            'completed_at' => null,
            'user_id'      => $this->userID,
        ]);
        $itemWithNewAssignee = factory(ItemEloquent::class)->make([
            'assignee_id' => 556397,
        ]);
        $itemWithNewDue = factory(ItemEloquent::class)->make([
            'due' => Carbon::now()->addDays(5),
        ]);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(UpdateItemJob::class);
        }

        $this->activateObservers();

        $this->assertJobDispatched(GroupedItemsNotificationJob::class, [
            function ($job) use ($itemWithNewAssignee) {
                return $job->assigneeId == $itemWithNewAssignee->assignee_id
                && $job->due == $itemWithNewAssignee->due->copy()->setTime(0, 0, 0);
            },
            function ($job) use ($itemWithNewDue) {
                return $job->assigneeId == $itemWithNewDue->assignee_id
                && $job->due == $itemWithNewDue->due->copy()->setTime(0, 0, 0);
            },
        ]);
        $this->assertJobDispatched(SendItemAssignedEmailJob::class, [
            function ($job) use ($itemWithNewAssignee) {
                return $job->item->description == $itemWithNewAssignee->description
                && $job->user->getKey() == $this->userID;
            },
        ]);

        $itemPayload = $this->setupPayloads($itemWithNewAssignee->toArray());
        $this->actingAs($this->user)
            ->json(
                'PATCH',
                '/api/v1/checklists/' . $checklist->id . '/items/' . $item->id,
                $itemPayload,
                $this->postHeaders
            )
            ->assertResponseStatus(200);

        $this->seeInDatabase('last_due_reminders', [
            'kwuid' => $itemWithNewAssignee->assignee_id,
            'due'   => $itemWithNewAssignee->due->format('Y-m-d'),
        ]);

        $itemPayload = $this->setupPayloads($itemWithNewDue->toArray());
        $this->actingAs($this->user)
            ->json(
                'PATCH',
                '/api/v1/checklists/' . $checklist->id . '/items/' . $item->id,
                $itemPayload,
                $this->postHeaders
            )
            ->assertResponseStatus(200);

        $this->seeInDatabase('last_due_reminders', [
            'kwuid' => $itemWithNewDue->assignee_id,
            'due'   => $itemWithNewDue->due->format('Y-m-d'),
        ]);
    }

    public function testIndexWithoutChecklistFilterByChecklistDomain()
    {
        $this->withoutEvents();

        $userId = $this->userID;
        $otherUserId = 556397;

        $this->contactChecklistFirst = factory(ChecklistEloquent::class)->create([
            'user_id'       => $this->userID,
            'object_domain' => 'contact',
            'object_id'     => 1,
        ]);
        $this->contactChecklistSecond = factory(ChecklistEloquent::class)->create([
            'user_id'       => $this->userID,
            'object_domain' => 'contact',
            'object_id'     => 2,
        ]);

        $this->opportunityChecklist = factory(ChecklistEloquent::class)->create([
            'user_id'       => $this->userID,
            'object_domain' => 'opportunity',
            'object_id'     => 12,
        ]);

        $this->contactFirstItems = factory(ItemEloquent::class, 2)->create([
            'user_id'      => $this->userID,
            'assignee_id'  => $this->userID,
            'checklist_id' => $this->contactChecklistFirst,
        ]);

        $this->contactSecondItems = factory(ItemEloquent::class, 2)->create([
            'user_id'      => $this->userID,
            'assignee_id'  => $this->userID,
            'checklist_id' => $this->contactChecklistSecond,
        ]);

        $this->opportunityItems = factory(ItemEloquent::class, 2)->create([
            'user_id'      => $this->userID,
            'assignee_id'  => $this->userID,
            'checklist_id' => $this->opportunityChecklist,
        ]);


        $this->should('able to filter by single relatioship attribute', function () {
            $queryString = '?filter[checklist.object_domain][is]=contact';
            $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeaders)
                 ->seeJson(['id' => (string) $this->contactFirstItems[0]->id])
                 ->seeJson(['id' => (string) $this->contactSecondItems[0]->id])
                 ->dontSeeJson(['id' => (string) $this->opportunityItems[0]->id]);
        });

        $this->should('able to filter by multiple relationship attribute', function () {
            $queryString = '?filter[checklist.object_domain][is]=contact'
                . '&filter[checklist.object_id][is]=' . $this->contactChecklistFirst->object_id;
            $this->json('GET', "/api/v1/checklists/items$queryString", [], $this->postHeaders)
                 ->seeJson(['id' => (string) $this->contactFirstItems[0]->id])
                 ->dontSeeJson(['id' => (string) $this->contactSecondItems[0]->id])
                 ->dontSeeJson(['id' => (string) $this->opportunityItems[0]->id]);
        });

        $this->should('correctly sorted when relationship filter applied', function () {
            $query = '?sort=-id&filter[checklist.object_domain][is]=contact&query_debug=1';
            $this->json('GET', "/api/v1/checklists/items$query", [], $this->postHeaders)
                 ->seeJson(['id' => (string) $this->contactFirstItems[0]->id])
                 ->seeJson(['id' => (string) $this->contactSecondItems[0]->id])
                 ->dontSeeJson(['id' => (string) $this->opportunityItems[0]->id]);

            $data = $this->decodeResponseJson()['data'];
            $this->assertEquals($this->contactSecondItems[1]->id, $data[0]['id']);
        });
    }

    public function testChecklistItemCount()
    {
        $this->checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'object_domain' => 'mc_recruits',
            'object_id' => $this->faker->numberBetween(1, 10),
        ]);
        $this->incompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->completedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'is_completed' => 1,
            'checklist_id' => $this->checklist->id
        ]);
        $otherUserId = $this->faker->numberBetween(1000, 9000);
        $this->otherUserIncompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->otherUserCompletedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'is_completed' => 1,
            'checklist_id' => $this->checklist->id
        ]);

        $this->otherUserChecklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->faker->numberBetween(1000, 9000),
            'object_domain' => 'mc_recruits',
            'object_id' => $this->faker->numberBetween(1, 10),
        ]);
        $this->otherIncompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'is_completed' => 0,
            'checklist_id' => $this->otherUserChecklist->id,
        ]);
        $this->otherCompletedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'is_completed' => 1,
            'checklist_id' => $this->otherUserChecklist->id,
        ]);

        $this->should('show correct count with checklist_id filter', function () {
            $checklistId = $this->checklist->id;
            $otherUserChecklistId = $this->otherUserChecklist->id;
            $randomChecklistId = $this->faker->numberBetween(1000, 9000);
            $query = "filter[checklist_id][in]=$checklistId,$otherUserChecklistId,$randomChecklistId";
            $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeaders)
                ->seeStatusCode(200)
                ->seeJson([
                    'data' => [
                        'counts' => [
                            [
                                'checklist_id' => $checklistId,
                                'completed_items' => $this->completedItem->count()
                                                    + $this->otherUserCompletedItem->count(),
                                'incomplete_items' => $this->incompleteItem->count()
                                                    + $this->otherUserIncompleteItem->count(),
                                'total_items' => $this->completedItem->count()
                                                + $this->incompleteItem->count()
                                                + $this->otherUserCompletedItem->count()
                                                + $this->otherUserIncompleteItem->count(),
                            ]
                        ]
                    ]
                ])
                ->dontSeeJson([
                    'data' => [
                        'counts' => [
                            [
                                'checklist_id' => $otherUserChecklistId,
                                'completed_items' => $this->otherCompletedItem->count(),
                                'incomplete_items' => $this->otherIncompleteItem->count(),
                                'total_items' => $this->otherCompletedItem->count()
                                                + $this->otherIncompleteItem->count()
                            ],
                            [
                                'checklist_id' => $randomChecklistId,
                            ]
                        ]
                    ]
                ]);
        });

        $this->should('show no data without checklist_id filter', function () {
            $this->json('GET', "/api/v1/checklists/items/counts", [], $this->postHeaders)
                ->seeStatusCode(200)
                ->seeJson([
                    'data' => []
                ]);
        });
    }

    public function testChecklistItemCountAfterCreatingItem()
    {
        $this->withoutJobs();

        $this->checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'object_domain' => 'mc_recruits',
            'object_id' => $this->faker->numberBetween(1, 10),
        ]);
        $this->incompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'user_id' => $this->userID,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);

        $this->completedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'user_id' => $this->userID,
            'is_completed' => 1,
            'completed_at' => '2020-05-12 16:40:28',
            'checklist_id' => $this->checklist->id
        ]);
        $otherUserId = $this->faker->numberBetween(1000, 9000);
        $this->otherUserIncompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->otherUserCompletedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'is_completed' => 1,
            'checklist_id' => $this->checklist->id
        ]);

        $checklistId = $this->checklist->id;
        $query = "filter[checklist_id][in]=$checklistId";
        $firstResult = [
            'checklist_id' => $checklistId,
            'completed_items' => $this->completedItem->count()
                + $this->otherUserCompletedItem->count(),
            'incomplete_items' => $this->incompleteItem->count()
                + $this->otherUserIncompleteItem->count(),
            'total_items' => $this->completedItem->count()
                + $this->incompleteItem->count()
                + $this->otherUserCompletedItem->count()
                + $this->otherUserIncompleteItem->count(),
        ];
        $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeaders)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => [
                    'counts' => [$firstResult]
                ]
            ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertEquals($firstResult, $cachedItemCount);

        $item = factory(ItemEloquent::class)->make([
            'meta' => ['template_item_id' => $this->faker->uuid],
            'user_id' => $this->userID,
            'checklist_id' => $this->checklist->id,
            'is_completed' => false
        ]);
        $data = $this->setupPayloads($item->toArray());
        $response = $this->setupResponse($item, [], ['id']);

        $this->json('POST', '/api/v1/checklists/' . $this->checklist->id . '/items', $data, $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(201);
        
        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertNull($cachedItemCount);

        $secondResult = [
            'checklist_id' => $checklistId,
            'completed_items' => $this->completedItem->count()
                + $this->otherUserCompletedItem->count(),
            'incomplete_items' => $this->incompleteItem->count()
                + $this->otherUserIncompleteItem->count()
                + count($data),
            'total_items' => $this->completedItem->count()
                + $this->incompleteItem->count()
                + $this->otherUserCompletedItem->count()
                + $this->otherUserIncompleteItem->count()
                + count($data),
        ];
        $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeaders)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => [
                    'counts' => [$secondResult]
                ]
            ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertEquals($secondResult, $cachedItemCount);
    }

    public function testChecklistItemCountAfterDeletingItem()
    {
        $this->checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'object_domain' => 'mc_recruits',
            'object_id' => $this->faker->numberBetween(1, 10),
        ]);
        $this->incompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'user_id' => $this->userID,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);

        $this->completedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'user_id' => $this->userID,
            'is_completed' => 1,
            'completed_at' => '2020-05-12 16:40:28',
            'checklist_id' => $this->checklist->id
        ]);
        $otherUserId = $this->faker->numberBetween(1000, 9000);
        $this->otherUserIncompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->otherUserCompletedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'is_completed' => 1,
            'checklist_id' => $this->checklist->id
        ]);

        $checklistId = $this->checklist->id;
        $query = "filter[checklist_id][in]=$checklistId";
        $firstResult = [
            'checklist_id' => $checklistId,
            'completed_items' => $this->completedItem->count()
                + $this->otherUserCompletedItem->count(),
            'incomplete_items' => $this->incompleteItem->count()
                + $this->otherUserIncompleteItem->count(),
            'total_items' => $this->completedItem->count()
                + $this->incompleteItem->count()
                + $this->otherUserCompletedItem->count()
                + $this->otherUserIncompleteItem->count(),
        ];
        $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeaders)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => [
                    'counts' => [$firstResult]
                ]
            ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertEquals($firstResult, $cachedItemCount);

        $this->json(
            'DELETE',
            '/api/v1/checklists/' . $this->checklist->id . '/items/' . $this->completedItem[0]->id,
            [],
            $this->postHeaders
        );
        $this->seeStatusCode(204);
        $this->missingFromDatabase('items', [
            'id'         => $this->completedItem[0]->id,
            'deleted_at' => null,
        ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertNull($cachedItemCount);

        $secondResult = [
            'checklist_id' => $checklistId,
            'completed_items' => $this->completedItem->count()
                + $this->otherUserCompletedItem->count()
                - 1,
            'incomplete_items' => $this->incompleteItem->count()
                + $this->otherUserIncompleteItem->count(),
            'total_items' => $this->completedItem->count()
                + $this->incompleteItem->count()
                + $this->otherUserCompletedItem->count()
                + $this->otherUserIncompleteItem->count()
                - 1,
        ];
        $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeaders)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => [
                    'counts' => [$secondResult]
                ]
            ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertEquals($secondResult, $cachedItemCount);
    }

    public function testChecklistItemCountAfterCompletingItem()
    {
        $this->checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'object_domain' => 'mc_recruits',
            'object_id' => $this->faker->numberBetween(1, 10),
        ]);
        $this->incompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'user_id' => $this->userID,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);

        $this->completedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'user_id' => $this->userID,
            'is_completed' => 1,
            'completed_at' => '2020-05-12 16:40:28',
            'checklist_id' => $this->checklist->id
        ]);
        $otherUserId = $this->faker->numberBetween(1000, 9000);
        $this->otherUserIncompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->otherUserCompletedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'is_completed' => 1,
            'checklist_id' => $this->checklist->id
        ]);

        $checklistId = $this->checklist->id;
        $query = "filter[checklist_id][in]=$checklistId";
        $firstResult = [
            'checklist_id' => $checklistId,
            'completed_items' => $this->completedItem->count()
                + $this->otherUserCompletedItem->count(),
            'incomplete_items' => $this->incompleteItem->count()
                + $this->otherUserIncompleteItem->count(),
            'total_items' => $this->completedItem->count()
                + $this->incompleteItem->count()
                + $this->otherUserCompletedItem->count()
                + $this->otherUserIncompleteItem->count(),
        ];
        $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeaders)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => [
                    'counts' => [$firstResult]
                ]
            ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertEquals($firstResult, $cachedItemCount);

        $arrItemToComplete = [];
        foreach ($this->incompleteItem as $item) {
            array_push($arrItemToComplete, ['item_id' => $item->id]);
        }

        $payload = [
            "data" => $arrItemToComplete
        ];

        $this->json('POST', "/api/v1/checklists/complete", $payload, $this->postHeaders)
            ->seeStatusCode(200);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertNull($cachedItemCount);

        $secondResult = [
            'checklist_id' => $checklistId,
            'completed_items' => $this->completedItem->count()
                + $this->otherUserCompletedItem->count()
                + $this->incompleteItem->count(),
            'incomplete_items' => $this->otherUserIncompleteItem->count(),
            'total_items' => $this->completedItem->count()
                + $this->incompleteItem->count()
                + $this->otherUserCompletedItem->count()
                + $this->otherUserIncompleteItem->count(),
        ];
        $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeaders)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => [
                    'counts' => [$secondResult]
                ]
            ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertEquals($secondResult, $cachedItemCount);
    }

    public function testChecklistItemCountAfterIncompletingItem()
    {
        $this->checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => $this->userID,
            'object_domain' => 'mc_recruits',
            'object_id' => $this->faker->numberBetween(1, 10),
        ]);
        $this->incompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'user_id' => $this->userID,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);

        $this->completedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $this->userID,
            'user_id' => $this->userID,
            'is_completed' => 1,
            'completed_at' => '2020-05-12 16:40:28',
            'checklist_id' => $this->checklist->id
        ]);
        $otherUserId = $this->faker->numberBetween(1000, 9000);
        $this->otherUserIncompleteItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'is_completed' => 0,
            'checklist_id' => $this->checklist->id
        ]);
        $this->otherUserCompletedItem = factory(ItemEloquent::class, 5)->create([
            'assignee_id' => $otherUserId,
            'is_completed' => 1,
            'checklist_id' => $this->checklist->id
        ]);

        $checklistId = $this->checklist->id;
        $query = "filter[checklist_id][in]=$checklistId";
        $firstResult = [
            'checklist_id' => $checklistId,
            'completed_items' => $this->completedItem->count()
                + $this->otherUserCompletedItem->count(),
            'incomplete_items' => $this->incompleteItem->count()
                + $this->otherUserIncompleteItem->count(),
            'total_items' => $this->completedItem->count()
                + $this->incompleteItem->count()
                + $this->otherUserCompletedItem->count()
                + $this->otherUserIncompleteItem->count(),
        ];
        $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeaders)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => [
                    'counts' => [$firstResult]
                ]
            ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertEquals($firstResult, $cachedItemCount);

        $arrItemToIncomplete = [];
        foreach ($this->completedItem as $item) {
            array_push($arrItemToIncomplete, ['item_id' => $item->id]);
        }

        $payload = [
            "data" => $arrItemToIncomplete
        ];

        $this->json('POST', "/api/v1/checklists/incomplete", $payload, $this->postHeaders)
            ->seeStatusCode(200);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertNull($cachedItemCount);

        $secondResult = [
            'checklist_id' => $checklistId,
            'completed_items' => $this->otherUserCompletedItem->count(),
            'incomplete_items' => $this->otherUserIncompleteItem->count()
                + $this->completedItem->count()
                + $this->incompleteItem->count(),
            'total_items' => $this->completedItem->count()
                + $this->incompleteItem->count()
                + $this->otherUserCompletedItem->count()
                + $this->otherUserIncompleteItem->count(),
        ];
        $this->json('GET', "/api/v1/checklists/items/counts?$query", [], $this->postHeaders)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => [
                    'counts' => [$secondResult]
                ]
            ]);

        $cachedItemCount = $this->getChecklistItemCountCache($checklistId);
        $this->assertEquals($secondResult, $cachedItemCount);
    }
}
