<?php

namespace Test\Controllers;

use App\Services\Items\ItemEloquent;
use App\Services\Template\TemplateEloquent;
use Illuminate\Support\Arr;

class TemplatesControllerTest extends ControllerTestCase
{
    private const DEAL_OBJECT = 'deals';

    public function setUp()
    {
        parent::setUp();

        $this->basicJsonStructure = [
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'name',
                    'items' => [
                        [
                            'urgency',
                            'due_unit',
                            'description',
                            'due_interval',
                        ],
                    ],
                    'checklist' => [
                        'due_unit',
                        'description',
                        'due_interval',
                    ],
                ],
                'links' => ['self'],
            ],
        ];

        $this->mockInactiveUsersResponse([$this->inactiveMemberId]);
    }

    public function testIndex()
    {
        $data = factory(TemplateEloquent::class, 3)->create();
        $this->json('GET', '/api/v1/checklists/templates', [], $this->postHeaders);
        $this->seeStatusCode(200);
        $this->seeJson(['id' => (string) $data[0]->id]);
        $this->seeJson(['id' => (string) $data[1]->id]);
        $this->seeJson(['id' => (string) $data[2]->id]);
        $this->seeJsonStructure([
            'meta' => [
                'count',
                'total',
            ],
            'data' => [
                $this->basicJsonStructure['data'],
            ],
            'links' => [
                'first',
                'last',
                'next',
                'prev',
            ],
        ]);
    }

    public function testIndexMarketCenterGateway()
    {
        $orgId = $this->orgId;
        $orgMatchUserMatch = factory(TemplateEloquent::class)
            ->create(['org_id' => $orgId, 'user_id' => $this->userID]);
        $orgMatchUserNotMatch = factory(TemplateEloquent::class)
            ->create(['org_id' => $orgId, 'user_id' => $this->faker->numberBetween(1000, 9000)]);
        $orgNotMatchUserMatch = factory(TemplateEloquent::class)
            ->create(['org_id' => 555, 'user_id' => $this->userID]);
        $this->json('GET', '/api/v1/checklists/templates?query_debug=1', [], $this->postHeadersMcScope);
        $this->seeStatusCode(200);
        $this->seeJson(['id' => (string) $orgMatchUserMatch->id]);
        $this->seeJson(['id' => (string) $orgMatchUserNotMatch->id]);
        $this->dontSeeJson(['id' => (string) $orgNotMatchUserMatch->id]);
        $this->seeJsonStructure([
            'meta' => [
                'count',
                'total',
            ],
            'data' => [
                $this->basicJsonStructure['data'],
            ],
            'links' => [
                'first',
                'last',
                'next',
                'prev',
            ],
        ]);
    }

    public function testIndexTeam()
    {
        $ownedData = factory(TemplateEloquent::class)->create();
        $oneTeamData = factory(TemplateEloquent::class)->create(['user_id' => 556397]);
        $otherTeamData = factory(TemplateEloquent::class)->create(['user_id' => 556399]);
        $this->json('GET', '/api/v1/checklists/templates', [], $this->postHeadersTeamScope);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'meta' => [
                'count',
                'total',
            ],
            'data' => [
                $this->basicJsonStructure['data'],
            ],
            'links' => [
                'first',
                'last',
                'next',
                'prev',
            ],
        ]);
        $responseDataIDs = collect($this->response->getData()->data)->pluck('id');
        $this->assertContains($ownedData->getKey(), $responseDataIDs);
        $this->assertContains($oneTeamData->getKey(), $responseDataIDs);
        $this->assertNotContains($otherTeamData->getKey(), $responseDataIDs);
    }

    public function testIndexTeamWithInactiveMember()
    {
        $ownedData = factory(TemplateEloquent::class)->create();
        $otherActiveMemberData = factory(TemplateEloquent::class)->create(['user_id' => 556398]);
        $inactiveMemberData = factory(TemplateEloquent::class)->create(['user_id' => $this->inactiveMemberId]);
        $otherTeamData = factory(TemplateEloquent::class)->create(['user_id' => 556399]);
        $this->json('GET', '/api/v1/checklists/templates', [], $this->postHeadersTeamScope);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'meta' => [
                'count',
                'total',
            ],
            'data' => [
                $this->basicJsonStructure['data'],
            ],
            'links' => [
                'first',
                'last',
                'next',
                'prev',
            ],
        ]);
        $responseDataIDs = collect($this->response->getData()->data)->pluck('id');
        $this->assertContains($ownedData->getKey(), $responseDataIDs);
        $this->assertContains($otherActiveMemberData->getKey(), $responseDataIDs);
        $this->assertContains($inactiveMemberData->getKey(), $responseDataIDs);
        $this->assertNotContains($otherTeamData->getKey(), $responseDataIDs);
    }

    public function testShow()
    {
        $template = factory(TemplateEloquent::class)->create();
        $response = $this->setupResponse($template);
        $this->json('GET', '/api/v1/checklists/templates/' . $template->id, [], $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);
    }

    public function testShowInactiveUser()
    {
        $this->mockUserNotFound();

        $template = factory(TemplateEloquent::class)->create([
            'user_id' => $this->inactiveMemberId,
        ]);
        $response = $this->setupResponse($template);
        $this->json('GET', '/api/v1/checklists/templates/' . $template->id, [], $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);
    }

    public function testShowNotAllowed()
    {
        $this->mockUserFound(123);

        $template = factory(TemplateEloquent::class)->create([
            'user_id' => 123,
        ]);
        $response = $this->setupResponse($template);
        $this->json('GET', '/api/v1/checklists/templates/' . $template->id, [], $this->postHeaders)
            ->assertResponseStatus(403);
    }

    public function testShowTeamOwnTemplate()
    {
        $ownTemplate = factory(TemplateEloquent::class)->create(['user_id' => $this->userID]);
        $response = $this->setupResponse($ownTemplate);
        $this->json('GET', '/api/v1/checklists/templates/' . $ownTemplate->id, [], $this->postHeadersTeamScope)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);
    }

    public function testShowTeamTemplate()
    {
        $teamTemplate = factory(TemplateEloquent::class)->create(['user_id' => 556397]);
        $response = $this->setupResponse($teamTemplate);
        $this->json('GET', '/api/v1/checklists/templates/' . $teamTemplate->id, [], $this->postHeadersTeamScope)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);
    }

    public function testShowTeamOtherTemplate()
    {
        $otherTemplate = factory(TemplateEloquent::class)->create(['user_id' => 556399]);
        $response = $this->setupResponse($otherTemplate);
        $this->json('GET', '/api/v1/checklists/templates/' . $otherTemplate->id, [], $this->postHeadersTeamScope)
            ->assertResponseStatus(403);
    }

    public function testShowInactiveMemberTemplateToOtherTeamMember()
    {
        $inactiveUserTemplate = factory(TemplateEloquent::class)->create(['user_id' => $this->inactiveMemberId]);
        $response = $this->setupResponse($inactiveUserTemplate);
        $this->json(
            'GET',
            '/api/v1/checklists/templates/' . $inactiveUserTemplate->id,
            [],
            $this->postHeadersTeamScope
        )
            ->assertResponseStatus(200);
    }

    public function setupPayloads(array $data)
    {
        $data = [
            'data' => [
                'attributes' => [
                    'name' => $data['name'],
                    'checklist' => $data['data']['checklist'],
                    'items' => $data['data']['items'],
                ],
            ],
        ];

        return $data;
    }

    public function setupAssignPayloads(array $data)
    {
        $data = [
            'data' => [
                'attributes' => $data,
            ],
        ];

        return $data;
    }

    public function setupAssignsPayloads(array $dataDomains)
    {
        $data = [];
        foreach ($dataDomains as $dataDomain) {
            $data[] = ['attributes' => $dataDomain];
        }

        return ['data' => $data];
    }

    public function castToJson($json)
    {
        if (is_array($json)) {
            $json = addslashes(json_encode($json));
        } elseif (is_null($json) || is_null(json_decode($json))) {
            throw new \Exception('A valid JSON string was not provided.');
        }

        return \DB::raw("CAST('{$json}' AS JSON)");
    }

    public function testStore()
    {
        $template = factory(TemplateEloquent::class)->create();
        $data = $this->setupPayloads($template->toArray());
        $response = $this->setupResponse($template, [], ['id']);
        $this->json('POST', '/api/v1/checklists/templates', $data, $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(201);

        $id = $this->decodeResponseJson()['data']['id'];
        $dbResult = $this->setupDBResult($template, ['id' => $id], ['type']);

        $this->seeInDatabase('templates', $dbResult);
    }

    public function testStoreWithUserId()
    {
        config(['app.env' => 'dev']);
        $userId = 885;

        $template = factory(TemplateEloquent::class)->make();
        $data = $this->setupPayloads($template->toArray());
        Arr::set($data, 'data.attributes.user_id', $userId);
        $response = $this->setupResponse($template, [], ['id']);
        $this->json('POST', '/api/v1/checklists/templates', $data, $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(201);

        $id = $this->decodeResponseJson()['data']['id'];

        $dbResult = $this->setupDBResult($template, ['id' => $id, 'user_id' => $userId], ['type']);

        $this->seeInDatabase('templates', $dbResult);

        config(['app.env' => 'prod']);

        $template = factory(TemplateEloquent::class)->make();
        $data = $this->setupPayloads($template->toArray());
        Arr::set($data, 'data.attributes.user_id', $userId);
        $response = $this->setupResponse($template, [], ['id']);
        $this->json('POST', '/api/v1/checklists/templates', $data, $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(201);

        $id = $this->decodeResponseJson()['data']['id'];

        $dbResult = $this->setupDBResult($template, ['id' => $id, 'user_id' => $this->userID], ['type']);

        $this->seeInDatabase('templates', $dbResult);
    }

    public function testAssign()
    {
        $template = factory(TemplateEloquent::class)->create();
        $domain = ['object_id' => 12, 'object_domain' => 'contact'];
        $data = $this->setupAssignPayloads($domain);

        $this->json('POST', '/api/v1/checklists/templates/' . $template->id . '/assign', $data, $this->postHeaders)
            ->assertResponseStatus(201);

        $responseData = json_decode($this->response->getContent());
        $this->assertInternalType('string', $responseData->data->attributes->object_id);
        $this->assertInternalType('bool', $responseData->data->attributes->is_completed);

        $dbResultInChecklist = [
            'id' => $this->decodeResponseJson()['data']['id'],
            'description' => $template->data['checklist']['description'],
            'user_id' => $template->user_id,
        ];

        $dbResultInItems0 = [
            'id' => $this->decodeResponseJson()['included'][0]['id'],
            'checklist_id' => $this->decodeResponseJson()['data']['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
        ];

        $dbResultInItems1 = [
            'id' => $this->decodeResponseJson()['included'][1]['id'],
            'checklist_id' => $this->decodeResponseJson()['data']['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
        ];

        $dbResultInChecklist = array_merge($dbResultInChecklist, $domain);

        $this->seeInDatabase('checklists', $dbResultInChecklist);
        $this->seeInDatabase('items', $dbResultInItems0);
        $this->seeInDatabase('items', $dbResultInItems1);

        $metaItem0 = array_get($template->data, 'items.0.meta');
        $metaItem1 = array_get($template->data, 'items.1.meta');
        $this->seeJsonInRecord(
            ['meta' => $metaItem0],
            ItemEloquent::class,
            ['id' => $dbResultInItems0['id']]
        );
        $this->seeJsonInRecord(
            ['meta' => $metaItem1],
            ItemEloquent::class,
            ['id' => $dbResultInItems1['id']]
        );
    }

    public function testAssignNotAllowed()
    {
        $this->mockUserFound(123);

        $template = factory(TemplateEloquent::class)->create(['user_id' => 123]);
        $domain = ['object_id' => 12, 'object_domain' => 'contact'];
        $data = $this->setupAssignPayloads($domain);

        $this->json('POST', '/api/v1/checklists/templates/' . $template->id . '/assign', $data, $this->postHeaders)
            ->assertResponseStatus(403);
    }

    public function testAssignInactiveUserTemplateByOtherUser()
    {
        $this->mockUserNotFound();

        $template = factory(TemplateEloquent::class)->create([
            'user_id' => $this->inactiveMemberId
        ]);
        $domain = ['object_id' => 12, 'object_domain' => 'contact'];
        $data = $this->setupAssignPayloads($domain);

        $this->json('POST', '/api/v1/checklists/templates/' . $template->id . '/assign', $data, $this->postHeaders)
            ->assertResponseStatus(201);

        $responseData = json_decode($this->response->getContent());
        $this->assertInternalType('string', $responseData->data->attributes->object_id);
        $this->assertInternalType('bool', $responseData->data->attributes->is_completed);

        $dbResultInChecklist = [
            'id' => $this->decodeResponseJson()['data']['id'],
            'description' => $template->data['checklist']['description'],
            'user_id' => $template->user_id,
        ];

        $dbResultInItems0 = [
            'id' => $this->decodeResponseJson()['included'][0]['id'],
            'checklist_id' => $this->decodeResponseJson()['data']['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
        ];

        $dbResultInItems1 = [
            'id' => $this->decodeResponseJson()['included'][1]['id'],
            'checklist_id' => $this->decodeResponseJson()['data']['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
        ];

        $dbResultInChecklist = array_merge($dbResultInChecklist, $domain);

        $this->seeInDatabase('checklists', $dbResultInChecklist);
        $this->seeInDatabase('items', $dbResultInItems0);
        $this->seeInDatabase('items', $dbResultInItems1);
    }

    public function testAssignWithClientUpdateAttributesOnItems()
    {
        $templateData = [
            'checklist' => [
                'description' => 'test--checklist',
                'due_interval' => 3,
                'due_unit' => 'hour',
            ],
            'items' => [
                [
                    'description' => 'test-item-1',
                    'urgency' => 2,
                    'due_interval' => 40,
                    'due_unit' => 'minute',
                    'notify_client' => true,
                    'client_update_sent_at' => '2020-01-31 19:59:00',
                ], [
                    'description' => 'test-item-2',
                    'urgency' => 3,
                    'due_interval' => 30,
                    'due_unit' => 'minute',
                    'notify_client' => true,
                    'client_update_sent_at' => '2020-01-31 19:59:00',
                ],
            ],
        ];
        $template = factory(TemplateEloquent::class)->create(['data' => $templateData]);
        $domain = ['object_id' => 12, 'object_domain' => 'contact'];
        $payload = $this->setupAssignPayloads($domain);

        $this->json('POST', '/api/v1/checklists/templates/' . $template->id . '/assign', $payload, $this->postHeaders)
            ->assertResponseStatus(201);
        $responseData = json_decode($this->response->getContent());
        $this->assertInternalType('string', $responseData->data->attributes->object_id);
        $this->assertInternalType('bool', $responseData->data->attributes->is_completed);

        $response = $this->decodeResponseJson();

        $this->seeInDatabase('checklists', array_merge([
            'id' => $response['data']['id'],
            'description' => $template->data['checklist']['description'],
            'user_id' => $template->user_id,
        ], $domain));

        foreach ($templateData['items'] as $itemKey => $item) {
            $this->seeInDatabase('items', [
                'id' => $response['included'][$itemKey]['id'],
                'checklist_id' => $response['data']['id'],
                'user_id' => $template->user_id,
                'description' => $templateData['items'][$itemKey]['description'],
                'urgency' => $templateData['items'][$itemKey]['urgency'],
                'urgency' => $templateData['items'][$itemKey]['urgency'],
                'notify_client' => $templateData['items'][$itemKey]['notify_client'],
                'client_update_sent_at' => $templateData['items'][$itemKey]['client_update_sent_at'],
            ]);
        }
    }

    public function testAssignsWithClientUpdateAttributesOnItems()
    {
        $template = factory(TemplateEloquent::class)->create();
        $domain = [
            ['object_id' => 1, 'object_domain' => 'contact'],
            ['object_id' => 2, 'object_domain' => 'contact'],
            ['object_id' => 3, 'object_domain' => 'contact'],
        ];

        $data = $this->setupAssignsPayloads($domain);
        $this->json('POST', '/api/v1/checklists/templates/' . $template->id . '/assigns', $data, $this->postHeaders)
            ->assertResponseStatus(201);

        $responseData = $this->decodeResponseJson();
        foreach ($responseData['data'] as $checklistResponseData) {
            $this->assertInternalType('string', $checklistResponseData['attributes']['object_id']);
            $this->assertInternalType('bool', $checklistResponseData['attributes']['is_completed']);
        }

        foreach ($responseData['data'] as $index => $checklist) {
            $checklist = array_merge([
                'id' => $responseData['data'][$index]['id'],
                'description' => $template->data['checklist']['description'],
                'user_id' => $template->user_id,
            ], $domain[$index]);
            $this->seeInDatabase('checklists', $checklist);
        }

        $item1 = [
            'id' => $responseData['included'][0]['id'],
            'checklist_id' => $responseData['data'][0]["id"],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
            "notify_client" => $template->data['items'][0]['notify_client'],
            "client_update_sent_at" => null
        ];

        $item2 = [
            'id' => $responseData['included'][1]['id'],
            'checklist_id' => $responseData['data'][0]["id"],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
            "notify_client" => $template->data['items'][1]['notify_client'],
            "client_update_sent_at" => null
        ];

        $item3 = [
            'id' => $responseData['included'][2]['id'],
            'checklist_id' => $responseData['data'][1]["id"],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
            "notify_client" => $template->data['items'][0]['notify_client'],
            "client_update_sent_at" => null
        ];

        $item4 = [
            'id' => $responseData['included'][3]['id'],
            'checklist_id' => $responseData['data'][1]["id"],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
            "notify_client" => $template->data['items'][1]['notify_client'],
            "client_update_sent_at" => null
        ];

        $item5 = [
            'id' => $responseData['included'][4]['id'],
            'checklist_id' => $responseData['data'][2]["id"],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
            "notify_client" => $template->data['items'][0]['notify_client'],
            "client_update_sent_at" => null
        ];

        $item6 = [
            'id' => $responseData['included'][5]['id'],
            'checklist_id' => $responseData['data'][2]["id"],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
            "notify_client" => $template->data['items'][1]['notify_client'],
            "client_update_sent_at" => null
        ];

        $this->seeInDatabase('items', $item1);
        $this->seeInDatabase('items', $item2);
        $this->seeInDatabase('items', $item3);
        $this->seeInDatabase('items', $item4);
        $this->seeInDatabase('items', $item5);
        $this->seeInDatabase('items', $item6);
    }

    public function testAssigns()
    {
        $template = factory(TemplateEloquent::class)->create();
        $domain = [
            ['object_id' => 1, 'object_domain' => 'contact'],
            ['object_id' => 2, 'object_domain' => 'contact'],
            ['object_id' => 3, 'object_domain' => 'contact'],
        ];

        $data = $this->setupAssignsPayloads($domain);

        $this->json('POST', '/api/v1/checklists/templates/' . $template->id . '/assigns', $data, $this->postHeaders)
            ->assertResponseStatus(201);

        $responseData = json_decode($this->response->getContent());
        foreach ($responseData->data as $checklistResponseData) {
            $this->assertInternalType('string', $checklistResponseData->attributes->object_id);
            $this->assertInternalType('bool', $checklistResponseData->attributes->is_completed);
        }

        $dbResultInChecklist0 = [
            'id' => $this->decodeResponseJson()['data'][0]['id'],
            'description' => $template->data['checklist']['description'],
            'user_id' => $template->user_id,
        ];

        $dbResultInChecklist1 = [
            'id' => $this->decodeResponseJson()['data'][1]['id'],
            'description' => $template->data['checklist']['description'],
            'user_id' => $template->user_id,
        ];

        $dbResultInChecklist2 = [
            'id' => $this->decodeResponseJson()['data'][2]['id'],
            'description' => $template->data['checklist']['description'],
            'user_id' => $template->user_id,
        ];

        $dbResultInChecklist0 = array_merge($dbResultInChecklist0, $domain[0]);
        $dbResultInChecklist1 = array_merge($dbResultInChecklist1, $domain[1]);
        $dbResultInChecklist2 = array_merge($dbResultInChecklist2, $domain[2]);

        $this->seeInDatabase('checklists', $dbResultInChecklist0);
        $this->seeInDatabase('checklists', $dbResultInChecklist1);
        $this->seeInDatabase('checklists', $dbResultInChecklist2);

        $dbResultInItems0 = [
            'id' => $this->decodeResponseJson()['included'][0]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][0]['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
        ];

        $dbResultInItems1 = [
            'id' => $this->decodeResponseJson()['included'][1]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][0]['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
        ];

        $dbResultInItems2 = [
            'id' => $this->decodeResponseJson()['included'][2]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][1]['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
        ];

        $dbResultInItems3 = [
            'id' => $this->decodeResponseJson()['included'][3]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][1]['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
        ];

        $dbResultInItems4 = [
            'id' => $this->decodeResponseJson()['included'][4]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][2]['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
        ];

        $dbResultInItems5 = [
            'id' => $this->decodeResponseJson()['included'][5]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][2]['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
        ];

        $this->seeInDatabase('items', $dbResultInItems0);
        $this->seeInDatabase('items', $dbResultInItems1);
        $this->seeInDatabase('items', $dbResultInItems2);
        $this->seeInDatabase('items', $dbResultInItems3);
        $this->seeInDatabase('items', $dbResultInItems4);
        $this->seeInDatabase('items', $dbResultInItems5);

        $metaItem0 = array_get($template->data, 'items.0.meta');
        $metaItem1 = array_get($template->data, 'items.1.meta');
        $dbResultsItem0 = [$dbResultInItems0, $dbResultInItems2, $dbResultInItems4];
        $dbResultsItem1 = [$dbResultInItems1, $dbResultInItems3, $dbResultInItems5];
        foreach ($dbResultsItem0 as $resultItem) {
            $this->seeJsonInRecord(
                ['meta' => $metaItem0],
                ItemEloquent::class,
                ['id' => $resultItem['id']]
            );
        }
        foreach ($dbResultsItem1 as $resultItem) {
            $this->seeJsonInRecord(
                ['meta' => $metaItem1],
                ItemEloquent::class,
                ['id' => $resultItem['id']]
            );
        }
    }

    public function testAssignsNotAllowed()
    {
        $this->mockUserFound(123);

        $template = factory(TemplateEloquent::class)->create(['user_id' => $this->inactiveMemberId]);
        $domain = [
            ['object_id' => 1, 'object_domain' => 'contact'],
            ['object_id' => 2, 'object_domain' => 'contact'],
            ['object_id' => 3, 'object_domain' => 'contact'],
        ];

        $data = $this->setupAssignsPayloads($domain);

        $this->json('POST', '/api/v1/checklists/templates/' . $template->id . '/assigns', $data, $this->postHeaders)
            ->assertResponseStatus(403);
    }

    public function testAssignsInactiveUserTemplateByOtherUser()
    {
        $this->mockUserNotFound();

        $template = factory(TemplateEloquent::class)->create([
            'user_id' => $this->inactiveMemberId
        ]);
        $domain = [
            ['object_id' => 1, 'object_domain' => 'contact'],
            ['object_id' => 2, 'object_domain' => 'contact'],
            ['object_id' => 3, 'object_domain' => 'contact'],
        ];

        $data = $this->setupAssignsPayloads($domain);

        $this->json('POST', '/api/v1/checklists/templates/' . $template->id . '/assigns', $data, $this->postHeaders)
            ->assertResponseStatus(201);

        $responseData = json_decode($this->response->getContent());
        foreach ($responseData->data as $checklistResponseData) {
            $this->assertInternalType('string', $checklistResponseData->attributes->object_id);
            $this->assertInternalType('bool', $checklistResponseData->attributes->is_completed);
        }

        $dbResultInChecklist0 = [
            'id' => $this->decodeResponseJson()['data'][0]['id'],
            'description' => $template->data['checklist']['description'],
            'user_id' => $template->user_id,
        ];

        $dbResultInChecklist1 = [
            'id' => $this->decodeResponseJson()['data'][1]['id'],
            'description' => $template->data['checklist']['description'],
            'user_id' => $template->user_id,
        ];

        $dbResultInChecklist2 = [
            'id' => $this->decodeResponseJson()['data'][2]['id'],
            'description' => $template->data['checklist']['description'],
            'user_id' => $template->user_id,
        ];

        $dbResultInChecklist0 = array_merge($dbResultInChecklist0, $domain[0]);
        $dbResultInChecklist1 = array_merge($dbResultInChecklist1, $domain[1]);
        $dbResultInChecklist2 = array_merge($dbResultInChecklist2, $domain[2]);

        $this->seeInDatabase('checklists', $dbResultInChecklist0);
        $this->seeInDatabase('checklists', $dbResultInChecklist1);
        $this->seeInDatabase('checklists', $dbResultInChecklist2);

        $dbResultInItems0 = [
            'id' => $this->decodeResponseJson()['included'][0]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][0]['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
        ];

        $dbResultInItems1 = [
            'id' => $this->decodeResponseJson()['included'][1]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][0]['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
        ];

        $dbResultInItems2 = [
            'id' => $this->decodeResponseJson()['included'][2]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][1]['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
        ];

        $dbResultInItems3 = [
            'id' => $this->decodeResponseJson()['included'][3]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][1]['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
        ];

        $dbResultInItems4 = [
            'id' => $this->decodeResponseJson()['included'][4]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][2]['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][0]['description'],
            'urgency' => $template->data['items'][0]['urgency'],
        ];

        $dbResultInItems5 = [
            'id' => $this->decodeResponseJson()['included'][5]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][2]['id'],
            'user_id' => $template->user_id,
            'description' => $template->data['items'][1]['description'],
            'urgency' => $template->data['items'][1]['urgency'],
        ];

        $this->seeInDatabase('items', $dbResultInItems0);
        $this->seeInDatabase('items', $dbResultInItems1);
        $this->seeInDatabase('items', $dbResultInItems2);
        $this->seeInDatabase('items', $dbResultInItems3);
        $this->seeInDatabase('items', $dbResultInItems4);
        $this->seeInDatabase('items', $dbResultInItems5);
    }

    public function testAssignTeam()
    {
        $teamTemplate = factory(TemplateEloquent::class)->create(['user_id' => 556397]);
        $domain = ['object_id' => 12, 'object_domain' => 'contact'];
        $data = $this->setupAssignPayloads($domain);

        $this->json(
            'POST',
            '/api/v1/checklists/templates/' . $teamTemplate->id . '/assign',
            $data,
            $this->postHeadersTeamScope
        )
            ->assertResponseStatus(201);

        $responseData = json_decode($this->response->getContent());
        $this->assertInternalType('string', $responseData->data->attributes->object_id);
        $this->assertInternalType('bool', $responseData->data->attributes->is_completed);

        $dbResultInChecklist = [
            'id' => $this->decodeResponseJson()['data']['id'],
            'description' => $teamTemplate->data['checklist']['description'],
            'user_id' => $teamTemplate->user_id,
        ];

        $dbResultInItems0 = [
            'id' => $this->decodeResponseJson()['included'][0]['id'],
            'checklist_id' => $this->decodeResponseJson()['data']['id'],
            'user_id' => $teamTemplate->user_id,
            'description' => $teamTemplate->data['items'][0]['description'],
            'urgency' => $teamTemplate->data['items'][0]['urgency'],
        ];

        $dbResultInItems1 = [
            'id' => $this->decodeResponseJson()['included'][1]['id'],
            'checklist_id' => $this->decodeResponseJson()['data']['id'],
            'user_id' => $teamTemplate->user_id,
            'description' => $teamTemplate->data['items'][1]['description'],
            'urgency' => $teamTemplate->data['items'][1]['urgency'],
        ];

        $dbResultInChecklist = array_merge($dbResultInChecklist, $domain);

        $this->seeInDatabase('checklists', $dbResultInChecklist);
        $this->seeInDatabase('items', $dbResultInItems0);
        $this->seeInDatabase('items', $dbResultInItems1);
    }

    public function testAssignTeamOtherTemplate()
    {
        $teamTemplate = factory(TemplateEloquent::class)->create(['user_id' => 123]);
        $domain = ['object_id' => 12, 'object_domain' => 'contact'];
        $data = $this->setupAssignPayloads($domain);

        $this->json(
            'POST',
            '/api/v1/checklists/templates/' . $teamTemplate->id . '/assign',
            $data,
            $this->postHeadersTeamScope
        )
            ->assertResponseStatus(403);
    }

    public function testAssignTeamInactiveMember()
    {
        $teamTemplate = factory(TemplateEloquent::class)->create(['user_id' => $this->inactiveMemberId]);
        $domain = ['object_id' => 12, 'object_domain' => 'contact'];
        $data = $this->setupAssignPayloads($domain);

        $this->json(
            'POST',
            '/api/v1/checklists/templates/' . $teamTemplate->id . '/assign',
            $data,
            $this->postHeadersTeamScope
        )
            ->assertResponseStatus(201);

        $responseData = json_decode($this->response->getContent());
        $this->assertInternalType('string', $responseData->data->attributes->object_id);
        $this->assertInternalType('bool', $responseData->data->attributes->is_completed);

        $dbResultInChecklist = [
            'id' => $this->decodeResponseJson()['data']['id'],
            'description' => $teamTemplate->data['checklist']['description'],
            'user_id' => $teamTemplate->user_id,
        ];

        $dbResultInItems0 = [
            'id' => $this->decodeResponseJson()['included'][0]['id'],
            'checklist_id' => $this->decodeResponseJson()['data']['id'],
            'user_id' => $teamTemplate->user_id,
            'description' => $teamTemplate->data['items'][0]['description'],
            'urgency' => $teamTemplate->data['items'][0]['urgency'],
        ];

        $dbResultInItems1 = [
            'id' => $this->decodeResponseJson()['included'][1]['id'],
            'checklist_id' => $this->decodeResponseJson()['data']['id'],
            'user_id' => $teamTemplate->user_id,
            'description' => $teamTemplate->data['items'][1]['description'],
            'urgency' => $teamTemplate->data['items'][1]['urgency'],
        ];

        $dbResultInChecklist = array_merge($dbResultInChecklist, $domain);

        $this->seeInDatabase('checklists', $dbResultInChecklist);
        $this->seeInDatabase('items', $dbResultInItems0);
        $this->seeInDatabase('items', $dbResultInItems1);
    }

    public function testAssignsTeam()
    {
        $teamTemplate = factory(TemplateEloquent::class)->create(['user_id' => 556397]);
        $domain = [
            ['object_id' => 1, 'object_domain' => 'contact'],
            ['object_id' => 2, 'object_domain' => 'contact'],
            ['object_id' => 3, 'object_domain' => 'contact'],
        ];

        $data = $this->setupAssignsPayloads($domain);

        $this->json(
            'POST',
            '/api/v1/checklists/templates/' . $teamTemplate->id . '/assigns',
            $data,
            $this->postHeadersTeamScope
        )
            ->assertResponseStatus(201);

        $responseData = json_decode($this->response->getContent());
        foreach ($responseData->data as $checklistResponseData) {
            $this->assertInternalType('string', $checklistResponseData->attributes->object_id);
            $this->assertInternalType('bool', $checklistResponseData->attributes->is_completed);
        }

        $dbResultInChecklist0 = [
            'id' => $this->decodeResponseJson()['data'][0]['id'],
            'description' => $teamTemplate->data['checklist']['description'],
            'user_id' => $teamTemplate->user_id,
        ];

        $dbResultInChecklist1 = [
            'id' => $this->decodeResponseJson()['data'][1]['id'],
            'description' => $teamTemplate->data['checklist']['description'],
            'user_id' => $teamTemplate->user_id,
        ];

        $dbResultInChecklist2 = [
            'id' => $this->decodeResponseJson()['data'][2]['id'],
            'description' => $teamTemplate->data['checklist']['description'],
            'user_id' => $teamTemplate->user_id,
        ];

        $dbResultInChecklist0 = array_merge($dbResultInChecklist0, $domain[0]);
        $dbResultInChecklist1 = array_merge($dbResultInChecklist1, $domain[1]);
        $dbResultInChecklist2 = array_merge($dbResultInChecklist2, $domain[2]);

        $this->seeInDatabase('checklists', $dbResultInChecklist0);
        $this->seeInDatabase('checklists', $dbResultInChecklist1);
        $this->seeInDatabase('checklists', $dbResultInChecklist2);

        $dbResultInItems0 = [
            'id' => $this->decodeResponseJson()['included'][0]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][0]['id'],
            'user_id' => $teamTemplate->user_id,
            'description' => $teamTemplate->data['items'][0]['description'],
            'urgency' => $teamTemplate->data['items'][0]['urgency'],
        ];

        $dbResultInItems1 = [
            'id' => $this->decodeResponseJson()['included'][1]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][0]['id'],
            'user_id' => $teamTemplate->user_id,
            'description' => $teamTemplate->data['items'][1]['description'],
            'urgency' => $teamTemplate->data['items'][1]['urgency'],
        ];

        $dbResultInItems2 = [
            'id' => $this->decodeResponseJson()['included'][2]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][1]['id'],
            'user_id' => $teamTemplate->user_id,
            'description' => $teamTemplate->data['items'][0]['description'],
            'urgency' => $teamTemplate->data['items'][0]['urgency'],
        ];

        $dbResultInItems3 = [
            'id' => $this->decodeResponseJson()['included'][3]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][1]['id'],
            'user_id' => $teamTemplate->user_id,
            'description' => $teamTemplate->data['items'][1]['description'],
            'urgency' => $teamTemplate->data['items'][1]['urgency'],
        ];

        $dbResultInItems4 = [
            'id' => $this->decodeResponseJson()['included'][4]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][2]['id'],
            'user_id' => $teamTemplate->user_id,
            'description' => $teamTemplate->data['items'][0]['description'],
            'urgency' => $teamTemplate->data['items'][0]['urgency'],
        ];

        $dbResultInItems5 = [
            'id' => $this->decodeResponseJson()['included'][5]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][2]['id'],
            'user_id' => $teamTemplate->user_id,
            'description' => $teamTemplate->data['items'][1]['description'],
            'urgency' => $teamTemplate->data['items'][1]['urgency'],
        ];

        $this->seeInDatabase('items', $dbResultInItems0);
        $this->seeInDatabase('items', $dbResultInItems1);
        $this->seeInDatabase('items', $dbResultInItems2);
        $this->seeInDatabase('items', $dbResultInItems3);
        $this->seeInDatabase('items', $dbResultInItems4);
        $this->seeInDatabase('items', $dbResultInItems5);
    }

    public function testAssignsTeamOtherTemplate()
    {
        $teamTemplate = factory(TemplateEloquent::class)->create(['user_id' => 123]);
        $domain = [
            ['object_id' => 1, 'object_domain' => 'contact'],
            ['object_id' => 2, 'object_domain' => 'contact'],
            ['object_id' => 3, 'object_domain' => 'contact'],
        ];

        $data = $this->setupAssignsPayloads($domain);

        $this->json(
            'POST',
            '/api/v1/checklists/templates/' . $teamTemplate->id . '/assigns',
            $data,
            $this->postHeadersTeamScope
        )
            ->assertResponseStatus(403);
    }

    public function testAssignsTeamInactiveMember()
    {
        $teamTemplate = factory(TemplateEloquent::class)->create(['user_id' => $this->inactiveMemberId]);
        $domain = [
            ['object_id' => 1, 'object_domain' => 'contact'],
            ['object_id' => 2, 'object_domain' => 'contact'],
            ['object_id' => 3, 'object_domain' => 'contact'],
        ];

        $data = $this->setupAssignsPayloads($domain);

        $this->json(
            'POST',
            '/api/v1/checklists/templates/' . $teamTemplate->id . '/assigns',
            $data,
            $this->postHeadersTeamScope
        )
            ->assertResponseStatus(201);

        $responseData = json_decode($this->response->getContent());
        foreach ($responseData->data as $checklistResponseData) {
            $this->assertInternalType('string', $checklistResponseData->attributes->object_id);
            $this->assertInternalType('bool', $checklistResponseData->attributes->is_completed);
        }

        $dbResultInChecklist0 = [
            'id' => $this->decodeResponseJson()['data'][0]['id'],
            'description' => $teamTemplate->data['checklist']['description'],
            'user_id' => $teamTemplate->user_id,
        ];

        $dbResultInChecklist1 = [
            'id' => $this->decodeResponseJson()['data'][1]['id'],
            'description' => $teamTemplate->data['checklist']['description'],
            'user_id' => $teamTemplate->user_id,
        ];

        $dbResultInChecklist2 = [
            'id' => $this->decodeResponseJson()['data'][2]['id'],
            'description' => $teamTemplate->data['checklist']['description'],
            'user_id' => $teamTemplate->user_id,
        ];

        $dbResultInChecklist0 = array_merge($dbResultInChecklist0, $domain[0]);
        $dbResultInChecklist1 = array_merge($dbResultInChecklist1, $domain[1]);
        $dbResultInChecklist2 = array_merge($dbResultInChecklist2, $domain[2]);

        $this->seeInDatabase('checklists', $dbResultInChecklist0);
        $this->seeInDatabase('checklists', $dbResultInChecklist1);
        $this->seeInDatabase('checklists', $dbResultInChecklist2);

        $dbResultInItems0 = [
            'id' => $this->decodeResponseJson()['included'][0]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][0]['id'],
            'user_id' => $teamTemplate->user_id,
            'description' => $teamTemplate->data['items'][0]['description'],
            'urgency' => $teamTemplate->data['items'][0]['urgency'],
        ];

        $dbResultInItems1 = [
            'id' => $this->decodeResponseJson()['included'][1]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][0]['id'],
            'user_id' => $teamTemplate->user_id,
            'description' => $teamTemplate->data['items'][1]['description'],
            'urgency' => $teamTemplate->data['items'][1]['urgency'],
        ];

        $dbResultInItems2 = [
            'id' => $this->decodeResponseJson()['included'][2]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][1]['id'],
            'user_id' => $teamTemplate->user_id,
            'description' => $teamTemplate->data['items'][0]['description'],
            'urgency' => $teamTemplate->data['items'][0]['urgency'],
        ];

        $dbResultInItems3 = [
            'id' => $this->decodeResponseJson()['included'][3]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][1]['id'],
            'user_id' => $teamTemplate->user_id,
            'description' => $teamTemplate->data['items'][1]['description'],
            'urgency' => $teamTemplate->data['items'][1]['urgency'],
        ];

        $dbResultInItems4 = [
            'id' => $this->decodeResponseJson()['included'][4]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][2]['id'],
            'user_id' => $teamTemplate->user_id,
            'description' => $teamTemplate->data['items'][0]['description'],
            'urgency' => $teamTemplate->data['items'][0]['urgency'],
        ];

        $dbResultInItems5 = [
            'id' => $this->decodeResponseJson()['included'][5]['id'],
            'checklist_id' => $this->decodeResponseJson()['data'][2]['id'],
            'user_id' => $teamTemplate->user_id,
            'description' => $teamTemplate->data['items'][1]['description'],
            'urgency' => $teamTemplate->data['items'][1]['urgency'],
        ];

        $this->seeInDatabase('items', $dbResultInItems0);
        $this->seeInDatabase('items', $dbResultInItems1);
        $this->seeInDatabase('items', $dbResultInItems2);
        $this->seeInDatabase('items', $dbResultInItems3);
        $this->seeInDatabase('items', $dbResultInItems4);
        $this->seeInDatabase('items', $dbResultInItems5);
    }

    public function testUpdate()
    {
        $savedTemplate = factory(TemplateEloquent::class)->create();
        $updateTemplate = factory(TemplateEloquent::class)->make();
        $data = $this->setupPayloads($updateTemplate->toArray());
        $response = $this->setupResponse($updateTemplate, ['id' => (string) $savedTemplate->id]);

        $this->json('PATCH', '/api/v1/checklists/templates/' . $savedTemplate->id, $data, $this->postHeaders)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateTemplate, ['id' => $savedTemplate->id]);

        $this->seeInDatabase('templates', $dbResult);
    }

    public function testUpdateNotAllowed()
    {
        $this->mockUserFound(123);

        $savedTemplate = factory(TemplateEloquent::class)->create(['user_id' => 123]);
        $updateTemplate = factory(TemplateEloquent::class)->make(['user_id' => 123]);
        $data = $this->setupPayloads($updateTemplate->toArray());

        $this->json('PATCH', '/api/v1/checklists/templates/' . $savedTemplate->id, $data, $this->postHeaders)
            ->assertResponseStatus(403);
    }

    public function testUpdateTeamOwnTemplate()
    {
        $savedTemplate = factory(TemplateEloquent::class)->create(['user_id' => $this->userID]);
        $updateTemplate = factory(TemplateEloquent::class)->make();
        $data = $this->setupPayloads($updateTemplate->toArray());
        $response = $this->setupResponse($updateTemplate, ['id' => (string) $savedTemplate->id]);

        $this->json('PUT', '/api/v1/checklists/templates/' . $savedTemplate->id, $data, $this->postHeadersTeamScope)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateTemplate, ['id' => $savedTemplate->id]);

        $this->seeInDatabase('templates', $dbResult);
    }

    public function testUpdateTeamTemplate()
    {
        $savedTemplate = factory(TemplateEloquent::class)->create(['user_id' => 556397]);
        $updateTemplate = factory(TemplateEloquent::class)->make(['user_id' => 556397]);
        $data = $this->setupPayloads($updateTemplate->toArray());
        $response = $this->setupResponse($updateTemplate, ['id' => (string) $savedTemplate->id]);

        $this->json('PUT', '/api/v1/checklists/templates/' . $savedTemplate->id, $data, $this->postHeadersTeamScope)
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateTemplate, ['id' => $savedTemplate->id, 'user_id' => 556397]);

        $this->seeInDatabase('templates', $dbResult);
    }

    public function testUpdateTeamOtherTemplate()
    {
        $savedTemplate = factory(TemplateEloquent::class)->create(['user_id' => 556399]);
        $updateTemplate = factory(TemplateEloquent::class)->make(['user_id' => 556399]);
        $data = $this->setupPayloads($updateTemplate->toArray());

        $this->json('PUT', '/api/v1/checklists/templates/' . $savedTemplate->id, $data, $this->postHeadersTeamScope)
            ->assertResponseStatus(403);
    }

    public function testUpdateTeamInactiveMemberTemplateByOtherTeamMember()
    {
        $inactiveMemberTemplate = factory(TemplateEloquent::class)->create(['user_id' => $this->inactiveMemberId]);
        $updateTemplate = factory(TemplateEloquent::class)->make(['user_id' => $this->inactiveMemberId]);
        $data = $this->setupPayloads($updateTemplate->toArray());
        $response = $this->setupResponse($updateTemplate, ['id' => (string) $inactiveMemberTemplate->id]);

        $this->json(
            'PUT',
            '/api/v1/checklists/templates/' . $inactiveMemberTemplate->id,
            $data,
            $this->postHeadersTeamScope
        )
            ->assertResponseStatus(200);
    }

    public function testUpdateTeamInactiveMemberTemplateByOtherUser()
    {
        $this->mockUserNotFound();

        $inactiveMemberTemplate = factory(TemplateEloquent::class)->create(['user_id' => $this->inactiveMemberId]);
        $updateTemplate = factory(TemplateEloquent::class)->make(['user_id' => $this->inactiveMemberId]);
        $data = $this->setupPayloads($updateTemplate->toArray());
        $response = $this->setupResponse($updateTemplate, ['id' => (string) $inactiveMemberTemplate->id]);

        $this->json(
            'PUT',
            '/api/v1/checklists/templates/' . $inactiveMemberTemplate->id,
            $data,
            $this->postHeaders
        )
            ->assertResponseStatus(200);
    }

    public function testDestroy()
    {
        $template = factory(TemplateEloquent::class)->create();
        $this->json('DELETE', '/api/v1/checklists/templates/' . $template->id, [], $this->postHeaders);
        $this->seeStatusCode(204);
        $this->missingFromDatabase('templates', [
            'id' => $template->id,
            'deleted_at' => null,
        ]);
    }

    public function testDestroyNotAllowed()
    {
        $this->mockUserFound(123);

        $template = factory(TemplateEloquent::class)->create(['user_id' => 123]);
        $this->json('DELETE', '/api/v1/checklists/templates/' . $template->id, [], $this->postHeaders);
        $this->seeStatusCode(403);

        $this->seeInDatabase('templates', [
            'id' => $template->id,
        ]);
    }

    public function testDestroyTeamOwnTemplate()
    {
        $template = factory(TemplateEloquent::class)->create(['user_id' => $this->userID]);

        $this->json('DELETE', '/api/v1/checklists/templates/' . $template->id, [], $this->postHeadersTeamScope);

        $this->seeStatusCode(204);

        $this->missingFromDatabase('templates', [
            'id' => $template->id,
            'deleted_at' => null,
        ]);
    }

    public function testDestroyTeamTemplate()
    {
        $template = factory(TemplateEloquent::class)->create(['user_id' => 556397]);

        $this->json('DELETE', '/api/v1/checklists/templates/' . $template->id, [], $this->postHeadersTeamScope);

        $this->seeStatusCode(204);

        $this->missingFromDatabase('templates', [
            'id' => $template->id,
            'deleted_at' => null,
        ]);
    }

    public function testDestroyTeamOtherTemplate()
    {
        $template = factory(TemplateEloquent::class)->create(['user_id' => 556399]);

        $this->json('DELETE', '/api/v1/checklists/templates/' . $template->id, [], $this->postHeadersTeamScope);

        $this->seeStatusCode(403);

        $this->seeInDatabase('templates', [
            'id' => $template->id,
        ]);
    }

    public function testDestroyTeamInactiveMemberTemplateByOtherMember()
    {
        $template = factory(TemplateEloquent::class)->create(['user_id' => $this->inactiveMemberId]);

        $this->json(
            'DELETE',
            '/api/v1/checklists/templates/' . $template->id,
            [],
            $this->postHeadersTeamScope
        );

        $this->seeStatusCode(204);

        $this->missingFromDatabase('templates', [
            'id' => $template->id,
            'deleted_at' => null,
        ]);
    }

    public function testDestroyTeamInactiveMemberTemplateByOtherUser()
    {
        $this->mockUserNotFound();

        $template = factory(TemplateEloquent::class)->create(['user_id' => $this->inactiveMemberId]);

        $this->json(
            'DELETE',
            '/api/v1/checklists/templates/' . $template->id,
            [],
            $this->postHeaders
        );

        $this->seeStatusCode(204);

        $this->missingFromDatabase('templates', [
            'id' => $template->id,
            'deleted_at' => null,
        ]);
    }

    protected function setupResponse(TemplateEloquent $template, $override = [], $unsetKeys = [])
    {
        $data = [
            'id' => (string) $template->getKey(),
            'type' => 'templates',
            'name' => $template->getAttribute('name'),
            'checklist' => array_get($template->getAttribute('data'), 'checklist'),
            'items' => array_get($template->getAttribute('data'), 'items'),
        ];

        if (count($override)) {
            foreach ($override as $key => $value) {
                $data[$key] = $value;
            }
        }

        if (count($unsetKeys)) {
            foreach ($unsetKeys as $key) {
                unset($data[$key]);
            }
        }

        return $data;
    }

    protected function setupDBResult(TemplateEloquent $template, $override = [], $unsetKeys = [])
    {
        $data = [
            'id' => (string) $template->getKey(),
            'user_id' => $this->userID,
            'name' => $template->getAttribute('name'),
            'data' => $this->castToJson($template->getAttribute('data')),
        ];

        if (count($override)) {
            foreach ($override as $key => $value) {
                $data[$key] = $value;
            }
        }

        if (count($unsetKeys)) {
            foreach ($unsetKeys as $key) {
                unset($data[$key]);
            }
        }

        return $data;
    }
}
