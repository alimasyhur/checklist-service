<?php

namespace Test\Controllers;

use App\Jobs\Audit\CompleteItemJob;
use App\Jobs\Audit\DeleteItemJob;
use App\Jobs\Audit\IncompleteItemJob;
use App\Jobs\Audit\UpdateItemJob;
use App\Services\Checklists\ChecklistEloquent;
use App\Services\Items\ItemEloquent;

class ItemPolicyTest extends ItemsControllerTestCase
{
    private const DEAL_OBJECT = 'deals';

    public function testCantBrowse()
    {
        $this->mockUserFound(123);

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => 123,
        ]);
        $item = factory(ItemEloquent::class)->create([
            'user_id'      => 123,
            'checklist_id' => $checklist->id,
        ]);
        $this->json('GET', "/api/v1/checklists/{$checklist->id}/items", [], $this->postHeaders)
            ->assertResponseStatus(403);
    }

    public function testCantShow()
    {
        $this->mockUserFound(123);

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id' => 123,
        ]);

        $item = factory(ItemEloquent::class)->create([
            'user_id'      => 123,
            'checklist_id' => $checklist->id,
        ]);

        $this->json('GET', "/api/v1/checklists/{$checklist->id}/items/{$item->id}", [], $this->postHeaders)
            ->assertResponseStatus(403);
    }

    public function testShowByAssignee()
    {
        $this->mockUserFound(123);

        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => 123]);
        $item = factory(ItemEloquent::class)->create([
            'user_id' => 123,
            'checklist_id' => $checklist->id,
            'assignee_id' => $this->userID
        ]);
        $response = $this->setupResponse($item);
        $this->json('GET', '/api/v1/checklists/' . $item->checklist_id . '/items/' . $item->id, [], $this->postHeaders)
             ->seeJson($response)
             ->seeJsonStructure($this->basicJsonStructure)
             ->assertResponseStatus(200);
    }

    public function testShowByTeam()
    {
        $this->mockInactiveUsersResponse([]);

        $teamUserId = 556397;
        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => $this->userID]);
        $item = factory(ItemEloquent::class)->create([
            'user_id' => $teamUserId,
            'checklist_id' => $checklist->id,
        ]);
        $response = $this->setupResponse($item);
        $this->json(
            'GET',
            '/api/v1/checklists/' . $item->checklist_id . '/items/' . $item->id,
            [],
            $this->postHeadersTeamScope
        )
             ->seeJson($response)
             ->seeJsonStructure($this->basicJsonStructure)
             ->assertResponseStatus(200);
    }

    public function testUpdateByAssignee()
    {
        $this->mockUserFound(123);

        $checklist  = factory(ChecklistEloquent::class)->create(['user_id' => 123]);
        $savedItem  = factory(ItemEloquent::class)->create([
            'user_id' => 123,
            'checklist_id' => $checklist->id,
            'assignee_id' => $this->userID,
        ]);
        $updateItem = factory(ItemEloquent::class)->make([
            'user_id' => 123,
            'checklist_id' => $checklist->id,
            'is_completed' => $savedItem->is_completed,
            'updated_by' => $this->userID,
            'assignee_id' => $this->userID
        ]);
        $data       = $this->setupPayloads($updateItem->toArray());
        $response   = $this->setupResponse($updateItem, ['id' => (string) $savedItem->id]);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(UpdateItemJob::class);
        }

        $this->json(
            'PATCH',
            '/api/v1/checklists/' . $savedItem->checklist_id . '/items/' . $savedItem->id,
            $data,
            $this->postHeaders
        )
            ->seeJson($response)
            ->seeJsonStructure($this->basicJsonStructure)
            ->assertResponseStatus(200);

        $dbResult = $this->setupDBResult($updateItem, ['id' => $savedItem->id], ['type']);
        $dbResult['updated_by'] = $this->userID;

        $this->seeInDatabase('items', $dbResult);
    }

    public function testDestroyByAssignee()
    {
        $this->mockUserFound(123);

        $checklist = factory(ChecklistEloquent::class)->create(['user_id' => 123]);
        $item      = factory(ItemEloquent::class)->create([
            'user_id' => 123,
            'checklist_id' => $checklist->id,
            'assignee_id' => $this->userID
        ]);

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(DeleteItemJob::class);
        }

        $this->json('DELETE', '/api/v1/checklists/' . $checklist->id . '/items/' . $item->id, [], $this->postHeaders);
        $this->seeStatusCode(204);
        $this->missingFromDatabase('items', [
            'id'         => $item->id,
            'deleted_at' => null,
        ]);
    }

    public function testForbiddenToDestroyAsTeamIfNotMember()
    {
        $this->mockInactiveUsersResponse([]);

        $checklist = factory(ChecklistEloquent::class)->create([
            'user_id'       => 999999,
        ]);
        $item = factory(ItemEloquent::class)->create([
            'user_id'       => 999999,
            'checklist_id' => $checklist->id
        ]);
        $this->json(
            'DELETE',
            '/api/v1/checklists/' . $checklist->id . '/items/' . $item->id,
            [],
            $this->postHeadersTeamScope
        )
            ->seeStatusCode(403);
    }

    public function testCompleteByAssignee()
    {
        $this->mockUserFound(123);

        $checklist  = factory(ChecklistEloquent::class)->create(['user_id' => 123]);
        $item = factory(ItemEloquent::class)->create([
            'checklist_id' => $checklist->id,
            'completed_at' => null,
            'is_completed' => 0,
            'user_id' => 123,
            'assignee_id' => $this->userID
        ]);

        $data = ['data' => [[ 'item_id' => $item->getKey() ]]];

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(CompleteItemJob::class);
        }

        $this->json('POST', '/api/v1/checklists/complete', $data, $this->postHeaders)
             ->seeJson([
                 'item_id' => $item->id,
                 'is_completed' => true
             ])
             ->seeJsonStructure([
                 'data' => [
                     [
                         'checklist_id',
                         'item_id',
                         'completed_at',
                         'is_completed',
                     ],
                 ],
             ])
             ->assertResponseStatus(200);
    }

    public function testIncompleteByAssignee()
    {
        $this->mockUserFound(123);

        $checklist  = factory(ChecklistEloquent::class)->create(['user_id' => 123]);
        $item = factory(ItemEloquent::class)->create([
            'user_id' => 123,
            'checklist_id' => $checklist->id,
            'completed_at' => '2019-01-01 00:00:00',
            'is_completed' => 1,
            'assignee_id' => $this->userID
        ]);

        $data = ['data' => [[ 'item_id' => $item->getKey() ]]];

        if ($checklist->getAttribute('object_domain') === self::DEAL_OBJECT) {
            $this->expectsJobs(IncompleteItemJob::class);
        }

        $this->json('POST', '/api/v1/checklists/incomplete', $data, $this->postHeaders)
             ->seeJson([
                 'item_id' => $item->id,
                 'is_completed' => false
             ])
             ->seeJsonStructure([
                 'data' => [
                     [
                         'checklist_id',
                         'item_id',
                         'completed_at',
                         'is_completed',
                     ],
                 ],
             ])
             ->assertResponseStatus(200);
    }
}
