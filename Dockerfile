FROM kellerwilliams/docker-base:7.3
MAINTAINER Jerry Crabb <jerry.crabb@murmillo.io>

# App install directory
ENV PROJECT_DIR /srv

# Set node environment to production to prevent installing dev dependencies
ENV NODE_ENV production

# Change the working directory to the project
WORKDIR ${PROJECT_DIR}

# Install dependencies early on so Docker builds cache the vendor/ packages
# We run composer later again to run the scripts, but it should be relatively faster
ARG GITHUB_TOKEN
ENV COMPOSER_AUTH="{\"github-oauth\": {\"github.com\": \"${GITHUB_TOKEN}\"}}"
ENV COMPOSER_ALLOW_SUPERUSER 1
COPY composer.* ${PROJECT_DIR}/
RUN composer install --no-interaction --prefer-dist --no-progress --no-scripts --no-autoloader

# Copy php.ini
COPY var/docker/php.ini /etc/php/7.3/cli/php.ini
COPY var/docker/php.ini /etc/php/7.3/fpm/php.ini

# Copy php7.3 modules config
COPY var/docker/php/mods-available/* /etc/php/7.3/mods-available/

# Copy php-fpm
COPY var/docker/php-fpm.conf /etc/php/7.3/fpm/php-fpm.conf

# Copy nginx config
COPY var/docker/nginx/nginx.conf /etc/nginx/nginx.conf
COPY var/docker/nginx/conf.d/default.conf.dist /etc/nginx/conf.d/default.conf.dist
COPY var/docker/nginx/html /usr/share/nginx/html

# Copy supervisor config
COPY var/docker/supervisor/conf.d /etc/supervisor/conf.d/

# Copy entrypoint
COPY var/docker/entrypoint.sh /entrypoint.sh

# Copy the project to the Docker project directory
# Do this as late as possible as it will most likely not be cached on subsequent builds
COPY . ${PROJECT_DIR}

# Install dependencies and remove .git folders
RUN rm -fr var/cache/* && rm -fr var/log/* \
    && composer install -o --no-interaction --prefer-dist --no-progress \
    && find vendor/ -type d -name .git -exec rm -rf {} +  \

    # Create log files
    && touch ${PROJECT_DIR}/storage/logs/lumen.log \
    && touch ${PROJECT_DIR}/storage/logs/laravel.log \

    # Configure var permissions
    # && chown -R www-data:www-data storage \
    && chmod -R 775 var/logs  \
    && chmod -R 775 ${PROJECT_DIR}/storage

VOLUME ${PROJECT_DIR}

ENTRYPOINT ["/entrypoint.sh"]

# Run supervisord which should run the basic services we need (nginx/php7.3-fpm) locally
CMD ["supervisord"]
