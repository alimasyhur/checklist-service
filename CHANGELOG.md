# Change Log

## [v1.2.6](https://github.com/KWRI/checklist-microservice/tree/v1.2.6) (2019-07-01)
[Full Changelog](https://github.com/KWRI/checklist-microservice/compare/v1.2.5...v1.2.6)

**Merged pull requests:**

- CSP-1859: add /health & /ping endpoints [\#102](https://github.com/KWRI/checklist-microservice/pull/102) ([andy65535](https://github.com/andy65535))

## [v1.2.5](https://github.com/KWRI/checklist-microservice/tree/v1.2.5) (2019-05-20)
[Full Changelog](https://github.com/KWRI/checklist-microservice/compare/v1.2.4...v1.2.5)

**Merged pull requests:**

- COM-10876 Adjust change to the new deals-manager get deal endpoint [\#99](https://github.com/KWRI/checklist-microservice/pull/99) ([adhatama](https://github.com/adhatama))
- COM-10817 Update README [\#98](https://github.com/KWRI/checklist-microservice/pull/98) ([jeremylombogia](https://github.com/jeremylombogia))
- Improving code quality by phpcs [\#97](https://github.com/KWRI/checklist-microservice/pull/97) ([rawaludin](https://github.com/rawaludin))

## [v1.2.4](https://github.com/KWRI/checklist-microservice/tree/v1.2.4) (2019-05-13)
[Full Changelog](https://github.com/KWRI/checklist-microservice/compare/v1.2.3...v1.2.4)

**Merged pull requests:**

- Update changelog [\#96](https://github.com/KWRI/checklist-microservice/pull/96) ([adhatama](https://github.com/adhatama))
- fix\(item\): fix hard coded object\_domain [\#94](https://github.com/KWRI/checklist-microservice/pull/94) ([rawaludin](https://github.com/rawaludin))
- COM-10470 Fix bug to query item's summary with deals object domain [\#91](https://github.com/KWRI/checklist-microservice/pull/91) ([adhatama](https://github.com/adhatama))
- Merge master to develop [\#90](https://github.com/KWRI/checklist-microservice/pull/90) ([adhatama](https://github.com/adhatama))
- COM-10470 Item's summary due query string adjustment [\#88](https://github.com/KWRI/checklist-microservice/pull/88) ([adhatama](https://github.com/adhatama))
- Merge master to develop [\#87](https://github.com/KWRI/checklist-microservice/pull/87) ([adhatama](https://github.com/adhatama))
- COM-10470 Adjust item's summary to match existing smartplan filter [\#85](https://github.com/KWRI/checklist-microservice/pull/85) ([adhatama](https://github.com/adhatama))

## [v1.2.3](https://github.com/KWRI/checklist-microservice/tree/v1.2.3) (2019-05-06)
[Full Changelog](https://github.com/KWRI/checklist-microservice/compare/v1.2.2...v1.2.3)

**Merged pull requests:**

- docs: Update Changelog [\#84](https://github.com/KWRI/checklist-microservice/pull/84) ([jeremylombogia](https://github.com/jeremylombogia))
- Merge master to develop [\#82](https://github.com/KWRI/checklist-microservice/pull/82) ([nafiesl](https://github.com/nafiesl))
- style\(Item\): Change ItemsEloquent model name to ItemEloquent [\#80](https://github.com/KWRI/checklist-microservice/pull/80) ([nafiesl](https://github.com/nafiesl))
- COM-10079 Add request validation for checklist and checklist item due [\#79](https://github.com/KWRI/checklist-microservice/pull/79) ([nafiesl](https://github.com/nafiesl))
- com-9515: refactor [\#78](https://github.com/KWRI/checklist-microservice/pull/78) ([rawaludin](https://github.com/rawaludin))
- COM-10080 Fixing error on ISO 8601 date with microseconds for due date [\#77](https://github.com/KWRI/checklist-microservice/pull/77) ([nafiesl](https://github.com/nafiesl))
- COM-10035 Allow add items to team's checklist [\#76](https://github.com/KWRI/checklist-microservice/pull/76) ([nafiesl](https://github.com/nafiesl))
- fix\(Item\): Fix item deletion by team not working [\#74](https://github.com/KWRI/checklist-microservice/pull/74) ([nafiesl](https://github.com/nafiesl))
- Update changelog [\#73](https://github.com/KWRI/checklist-microservice/pull/73) ([adhatama](https://github.com/adhatama))
- COM-9889: Add new summary count endpoint [\#64](https://github.com/KWRI/checklist-microservice/pull/64) ([adhatama](https://github.com/adhatama))

## [v1.2.2](https://github.com/KWRI/checklist-microservice/tree/v1.2.2) (2019-04-29)
[Full Changelog](https://github.com/KWRI/checklist-microservice/compare/v1.1.5...v1.2.2)

**Implemented enhancements:**

- COM-9429 Add a history log table to checklist items [\#46](https://github.com/KWRI/checklist-microservice/pull/46) ([jeremylombogia](https://github.com/jeremylombogia))

**Merged pull requests:**

- Update master [\#72](https://github.com/KWRI/checklist-microservice/pull/72) ([adhatama](https://github.com/adhatama))
- COM-9287 Try to fix preferences service call to works in QA [\#70](https://github.com/KWRI/checklist-microservice/pull/70) ([adhatama](https://github.com/adhatama))
- docs\(CHANGELOG.md\): Update changelog [\#69](https://github.com/KWRI/checklist-microservice/pull/69) ([adhatama](https://github.com/adhatama))
- COM-9490: Fix incomplete email copywriting [\#67](https://github.com/KWRI/checklist-microservice/pull/67) ([adhatama](https://github.com/adhatama))
- COM-9429 - fix: Fixing wrong url naming in JSON presenter [\#66](https://github.com/KWRI/checklist-microservice/pull/66) ([jeremylombogia](https://github.com/jeremylombogia))
- fix\(travis.yml\): Update .travis.yml env [\#63](https://github.com/KWRI/checklist-microservice/pull/63) ([nafiesl](https://github.com/nafiesl))
- COM-9429 Add history controller test [\#62](https://github.com/KWRI/checklist-microservice/pull/62) ([jeremylombogia](https://github.com/jeremylombogia))
- COM-9287 Add opportunity and contact name from deals-manager [\#60](https://github.com/KWRI/checklist-microservice/pull/60) ([adhatama](https://github.com/adhatama))
- com-9515: allow items filter by checklist attribute [\#59](https://github.com/KWRI/checklist-microservice/pull/59) ([rawaludin](https://github.com/rawaludin))
- com-9363: update nullify [\#58](https://github.com/KWRI/checklist-microservice/pull/58) ([rawaludin](https://github.com/rawaludin))
- COM-9490 Grouped due items email notification [\#57](https://github.com/KWRI/checklist-microservice/pull/57) ([adhatama](https://github.com/adhatama))
- COM-9287 Send email when item is assigned to someone [\#44](https://github.com/KWRI/checklist-microservice/pull/44) ([adhatama](https://github.com/adhatama))

## [v1.1.5](https://github.com/KWRI/checklist-microservice/tree/v1.1.5) (2019-04-24)
[Full Changelog](https://github.com/KWRI/checklist-microservice/compare/v1.1.4...v1.1.5)

**Merged pull requests:**

- Fix conflict [\#54](https://github.com/KWRI/checklist-microservice/pull/54) ([jeremylombogia](https://github.com/jeremylombogia))
- COM-9363 assignee\_id and task\_id validation [\#51](https://github.com/KWRI/checklist-microservice/pull/51) ([jeremylombogia](https://github.com/jeremylombogia))
- Update changelog [\#50](https://github.com/KWRI/checklist-microservice/pull/50) ([jeremylombogia](https://github.com/jeremylombogia))

## [v1.1.4](https://github.com/KWRI/checklist-microservice/tree/v1.1.4) (2019-04-22)
[Full Changelog](https://github.com/KWRI/checklist-microservice/compare/v1.1.3...v1.1.4)

**Merged pull requests:**

- Fix conflict [\#48](https://github.com/KWRI/checklist-microservice/pull/48) ([jeremylombogia](https://github.com/jeremylombogia))
- com-9478: add completed\_by to items [\#43](https://github.com/KWRI/checklist-microservice/pull/43) ([rawaludin](https://github.com/rawaludin))
- COM-9433 Add test bulk update items [\#42](https://github.com/KWRI/checklist-microservice/pull/42) ([jeremylombogia](https://github.com/jeremylombogia))
- com-9437: access by assignee\_id [\#41](https://github.com/KWRI/checklist-microservice/pull/41) ([rawaludin](https://github.com/rawaludin))
- COM-9428 Add endpoints to get all checklist's items with filters [\#40](https://github.com/KWRI/checklist-microservice/pull/40) ([adhatama](https://github.com/adhatama))
- Update changelog conflict for v1.1.3 [\#37](https://github.com/KWRI/checklist-microservice/pull/37) ([jeremylombogia](https://github.com/jeremylombogia))
- Update added task\_id & assignee\_id in Postman collection [\#36](https://github.com/KWRI/checklist-microservice/pull/36) ([jeremylombogia](https://github.com/jeremylombogia))
- Updated changelog v1.1.3 [\#35](https://github.com/KWRI/checklist-microservice/pull/35) ([jeremylombogia](https://github.com/jeremylombogia))

## [v1.1.3](https://github.com/KWRI/checklist-microservice/tree/v1.1.3) (2019-04-15)
[Full Changelog](https://github.com/KWRI/checklist-microservice/compare/v1.1.2...v1.1.3)

**Merged pull requests:**

- Moving task\_id from checklists to checklist item [\#33](https://github.com/KWRI/checklist-microservice/pull/33) ([jeremylombogia](https://github.com/jeremylombogia))
- Master [\#32](https://github.com/KWRI/checklist-microservice/pull/32) ([rawaludin](https://github.com/rawaludin))

## [v1.1.2](https://github.com/KWRI/checklist-microservice/tree/v1.1.2) (2019-04-15)
[Full Changelog](https://github.com/KWRI/checklist-microservice/compare/v1.1.1...v1.1.2)

**Merged pull requests:**

- Update changelog for v1.1.2 [\#31](https://github.com/KWRI/checklist-microservice/pull/31) ([jeremylombogia](https://github.com/jeremylombogia))
- COM-8923 [\#29](https://github.com/KWRI/checklist-microservice/pull/29) ([jeremylombogia](https://github.com/jeremylombogia))

## [v1.1.1](https://github.com/KWRI/checklist-microservice/tree/v1.1.1) (2019-04-01)
[Full Changelog](https://github.com/KWRI/checklist-microservice/compare/v1.1.0...v1.1.1)

**Merged pull requests:**

- Com 8319 [\#23](https://github.com/KWRI/checklist-microservice/pull/23) ([jeremylombogia](https://github.com/jeremylombogia))

## [v1.1.0](https://github.com/KWRI/checklist-microservice/tree/v1.1.0) (2019-03-30)
[Full Changelog](https://github.com/KWRI/checklist-microservice/compare/v1.0.5...v1.1.0)

**Merged pull requests:**

- COM 8071 - assign team template [\#18](https://github.com/KWRI/checklist-microservice/pull/18) ([rawaludin](https://github.com/rawaludin))
- Fix checklist complete after an item was deleted [\#17](https://github.com/KWRI/checklist-microservice/pull/17) ([rawaludin](https://github.com/rawaludin))

## [v1.0.5](https://github.com/KWRI/checklist-microservice/tree/v1.0.5) (2019-02-27)
[Full Changelog](https://github.com/KWRI/checklist-microservice/compare/v1.0.4...v1.0.5)

**Merged pull requests:**

- Merge pull request \#8 from KWRI/develop [\#15](https://github.com/KWRI/checklist-microservice/pull/15) ([sayasuhendra](https://github.com/sayasuhendra))
- update postman test collection. [\#14](https://github.com/KWRI/checklist-microservice/pull/14) ([sayasuhendra](https://github.com/sayasuhendra))
- Com 6371 [\#13](https://github.com/KWRI/checklist-microservice/pull/13) ([sayasuhendra](https://github.com/sayasuhendra))
- Refactor by php cs fixer [\#12](https://github.com/KWRI/checklist-microservice/pull/12) ([sayasuhendra](https://github.com/sayasuhendra))
- make sure is\_completed and object\_id response as define. [\#11](https://github.com/KWRI/checklist-microservice/pull/11) ([sayasuhendra](https://github.com/sayasuhendra))
- Fix assign response [\#10](https://github.com/KWRI/checklist-microservice/pull/10) ([sayasuhendra](https://github.com/sayasuhendra))
- add bulk assign checklist template [\#8](https://github.com/KWRI/checklist-microservice/pull/8) ([sayasuhendra](https://github.com/sayasuhendra))
- add bulk assign checklist template. [\#7](https://github.com/KWRI/checklist-microservice/pull/7) ([sayasuhendra](https://github.com/sayasuhendra))

## [v1.0.4](https://github.com/KWRI/checklist-microservice/tree/v1.0.4) (2019-02-02)
[Full Changelog](https://github.com/KWRI/checklist-microservice/compare/v1.0.3...v1.0.4)

## [v1.0.3](https://github.com/KWRI/checklist-microservice/tree/v1.0.3) (2019-01-30)
[Full Changelog](https://github.com/KWRI/checklist-microservice/compare/v1.0.2...v1.0.3)

## [v1.0.2](https://github.com/KWRI/checklist-microservice/tree/v1.0.2) (2019-01-30)
[Full Changelog](https://github.com/KWRI/checklist-microservice/compare/v1.0.1...v1.0.2)

**Merged pull requests:**

- Add horizon worker [\#4](https://github.com/KWRI/checklist-microservice/pull/4) ([toopay](https://github.com/toopay))

## [v1.0.1](https://github.com/KWRI/checklist-microservice/tree/v1.0.1) (2019-01-28)
[Full Changelog](https://github.com/KWRI/checklist-microservice/compare/v1.0.0...v1.0.1)

**Merged pull requests:**

- New release [\#3](https://github.com/KWRI/checklist-microservice/pull/3) ([rawaludin](https://github.com/rawaludin))

## [v1.0.0](https://github.com/KWRI/checklist-microservice/tree/v1.0.0) (2019-01-28)
**Merged pull requests:**

- Develop [\#2](https://github.com/KWRI/checklist-microservice/pull/2) ([rawaludin](https://github.com/rawaludin))
- Templates feature [\#1](https://github.com/KWRI/checklist-microservice/pull/1) ([rawaludin](https://github.com/rawaludin))



\* *This Change Log was automatically generated by [github_changelog_generator](https://github.com/skywinder/Github-Changelog-Generator)*