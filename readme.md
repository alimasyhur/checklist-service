# Checklist Service

## Install

This project uses [Composer](https://getcomposer.org/). Go check them out if you don't have them locally installed.

### Server requirement
- PHP 7.3.4 is recomended
- Since MariaDB doesn't support JSON, so we need to use MySQL instead, v5.7 is recomended.

### Set the enviroment

-   Copy the `.env.example` to `.env`

### Installing package

Run `composer install`

## Getting Started

### Database Migration & Seed
Run the database migration and seeder using `php artisan migrate --seed`, This will be run all migration and seeder (including from core-package).

### Run the Test

Make sure test is passed before a commit or creating a pull request.

#### Controller Test

```
./vendor/bin/phpunit --no-coverage --stop-on-fail --stop-on-error --testsuite="Controller Test"
```

### Run / serve the service
Run `php -S localhost:8000 -t public`

### Updating Package

Before merging from another branch or someone work, run `rm -rf vendor && composer install` to make sure package is fresh updated.
