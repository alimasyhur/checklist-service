<?php

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['middleware' => ['sanitize-user-id-filter', 'team-gateway', 'mc-gateway']], function ($router) {
    $router->get('/checklists/templates', 'TemplateController@index');
    $router->get('/checklists', 'ChecklistController@index');
});

$router->group(['middleware' => ['opportunity-team-gateway', 'mc-gateway']], function ($router) {
    $router->get('/checklists/items', 'ItemController@indexWithoutChecklist');
    $router->get('/checklists/items/summaries', 'ItemController@count');
    $router->get('/checklists/items/counts', 'ItemController@checklistItemCount');
});

$router->get('/checklists/{checklistId}/items', 'ItemController@index');
$router->post('/checklists/templates', 'TemplateController@store');

/*
 * Route for complete/incomplete items
 */
$router->post('/checklists/complete', 'ItemController@complete');
$router->post('/checklists/incomplete', 'ItemController@incomplete');

/*
 * Route for templates
 */
$router->post('/checklists/templates/{templateId}/assign', 'TemplateController@assign');
$router->post('/checklists/templates/{templateId}/assigns', 'TemplateController@assigns');
$router->get('/checklists/templates/{templateId}', 'TemplateController@show');
$router->patch('/checklists/templates/{templateId}', 'TemplateController@update');
$router->put('/checklists/templates/{templateId}', 'TemplateController@update');
$router->delete('/checklists/templates/{templateId}', 'TemplateController@destroy');

$router->post('/checklists/{checklistId}/items', 'ItemController@store');
$router->get('/checklists/{checklistId}/items/{itemId}', 'ItemController@show');
$router->patch('/checklists/{checklistId}/items/{itemId}', 'ItemController@update');
$router->delete('/checklists/{checklistId}/items/{itemId}', 'ItemController@destroy');

$router->post('/checklists/{checklistId}/items/_bulk', 'ItemController@bulk');

$router->get('/checklists/histories/', 'HistoryController@index');
$router->get('/checklists/histories/{historiesId}', 'HistoryController@show');

# API routes for testing log
$router->get('/checklists/log', function (\Illuminate\Http\Request $request) {
    Log::emergency('emergency log');
    Log::alert('alert log');
    Log::critical('critical log');
    Log::error('error log');
    Log::warning('warning log');
    Log::notice('notice log');
    Log::info('info log');
    Log::debug('debug log');

    if (!empty($request->message)) {
        Log::debug($request->message);
    }

    throw new Exception('Test exception log.');
});

$router->post('/checklists', 'ChecklistController@store');
$router->get('/checklists/{checklistId}', 'ChecklistController@show');
$router->patch('/checklists/{checklistId}', 'ChecklistController@update');
$router->delete('/checklists/{checklistId}', 'ChecklistController@destroy');
